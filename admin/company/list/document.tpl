<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Campaign System</title>
	<meta charset="utf-8"> 
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
		<h2 class="content-header-title">Documents</h2>
		<ol class="breadcrumb">
			<li><a href="/">Home</a></li>
			<li><a href="/company/list/">Company</a></li>
			<li><a href="#">{$companyData.company_name}</a></li>
			<li class="active">Documents</li>
		</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					Document list
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="/company/list/document.php?code={$companyData.company_code}" method="POST" data-validate="parsley" class="form parsley-form" enctype="multipart/form-data">
				<p>Below is a list of your documents.</p>
				<table class="table table-bordered">	
					<thead>
						<tr>
							<td>Type of Document</td>
							<td>Description</td>
							<td>Start Date</td>
							<td>End Date</td>
							<td>Download</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						{foreach from=$mediaData item=item}
							<tr>
								<td>{$item.item_name}</td>							
								<td>{$item.media_description|default:'N / A'}</td>	
								<td>{$item.media_date_start}</td>	
								<td>{$item.media_date_end}</td>	
								<td>
									<a href="http://brownsense.nolali.co.za{$item.media_path}{$item.media_code}{$item.media_ext}" target="_blank">
										Download
									</a>
								</td>
								<td>
									<button value="Delete" class="btn btn-danger" onclick="deleteitemModal('{$companyData.company_code}', '{$item.media_code}'); return false;">Delete</button>
								</td>
							</tr>	
							{foreachelse}	
							<tr>
								<td align="center" colspan="2">There are currently no items</td>
							</tr>								
						{/foreach}
					</tbody>					  
				</table>
				<p>Add images below</p>
				<div class="form-group">
                  <label for="item_code" class="error">Type</label>
                  <select id="item_code" name="item_code" class="form-control">
					{html_options options=$parentSelect}
				  </select>
				  {if isset($errorArray.item_code)}<span class="error">{$errorArray.item_code}</span>{/if}					  
                </div>
                <div class="form-group">
					<label for="mediadescription">File Description</label>
					<textarea id="mediadescription" name="mediadescription" rows="3" class="form-control"></textarea>
					{if isset($errorArray.mediadescription)}<br /><span class="error">{$errorArray.mediadescription}</span>{/if}
                </div>				
				<div class="form-group">
                  <label for="media_date_start">Valid From</label>
                  <input type="text" id="media_date_start" name="media_date_start" class="form-control" />
				  {if isset($errorArray.media_date_start)}<span class="error">{$errorArray.media_date_start}</span>{/if}					  
                </div>
				<div class="form-group">
                  <label for="media_date_end">Exirey Date</label>
                  <input type="text" id="media_date_end" name="media_date_end" class="form-control" />
				  {if isset($errorArray.media_date_end)}<span class="error">{$errorArray.media_date_end}</span>{/if}					  
                </div>
                <div class="form-group">
					<label for="mediafile">Upload</label>
					<input type="file" id="mediafile" name="mediafile" />
					{if isset($errorArray.mediafile)}<br /><span class="error">{$errorArray.mediafile}</span>{/if}					  
					<br /><span>Allowed files are .png, .jpg, .jpeg, .pdf, .xlsx, .xls, .txt, .docx, .doc, .cvs and .zip<span>
                </div>					
                <div class="form-group">
					<button type="submit" class="btn btn-primary">Upload and Save</button>
				</div>
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col -->	
		<div class="col-sm-3">
			<div class="list-group">  
				<a href="/company/list/" class="list-group-item">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/company/list/details.php?code={$companyData.company_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/company/list/media.php?code={$companyData.company_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Media
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/company/list/tag.php?code={$companyData.company_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Tag(s)
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/company/list/document.php?code={$companyData.company_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Document(s)
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>		
				<a class="list-group-item" href="/company/list/comment.php?code={$companyData.company_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Comments
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>				
			</div> <!-- /.list-group -->
		</div>			
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
{literal}
<script type="text/javascript">
$(document).ready(function(){
	$( "#media_date_start" ).datepicker({
		defaultDate: "+1w",
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		numberOfMonths: 1,
		onClose: function( selectedDate ) {
			$( "#media_date_end" ).datepicker( "option", "minDate", selectedDate );
		}
	});

	$( "#media_date_end" ).datepicker({
		defaultDate: "+1w",
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		numberOfMonths: 1,
		onClose: function( selectedDate ) {
			$( "#media_date_start" ).datepicker( "option", "maxDate", selectedDate );
		}
	});	
});

function updateRecord(code) {
	$.ajax({
			type: "GET",
			url: "media.php?code={/literal}{$campaignData.campaign_code}{literal}",
			data: "update_code="+code+"&media_description="+$("#media_description_"+code).val(),
			dataType: "json",
			success: function(data){
				if(data.result == 1) {
					window.location.href = window.location.href;
				} else {
					$.howl ({
					  type: 'info'
					  , title: 'Notification'
					  , content: data.error
					  , sticky: $(this).data ('sticky')
					  , lifetime: 7500
					  , iconCls: $(this).data ('icon')
					});	
				}
			}
	});								

	return false;
}

function deleteitemModal(campaign, media) {
	$('#campaigncode').val(campaign);
	$('#mediacode').val(media);
	$('#deleteitemModal').modal('show');
	return false;
}

function statusitemModal(campaign, media, status) {
	$('#statuscampaigncode').val(campaign);
	$('#statuscode').val(media);
	$('#status').val(status);
	$('#statusItemModal').modal('show');
	return false;
}

function deletesubitem() {
		
		var campaigncode 	= $('#campaigncode').val();
		var mediacode 			= $('#mediacode').val();
		
		$.ajax({
				type: "GET",
				url: "media.php",
				data: "delete_code="+mediacode+"&code="+campaigncode,
				dataType: "json",
				success: function(data){
					if(data.result == 1) {
						if(typeof oTable != 'undefined') {
							$('#deleteitemModal').modal('hide');
							oTable.fnDraw();
						} else {
							window.location.href = window.location.href;
						}
					} else {
						
						$('#deleteitemModal').modal('hide');
						
						$.howl ({
						  type: 'danger'
						  , title: 'Error Message'
						  , content: data.error
						  , sticky: $(this).data ('sticky')
						  , lifetime: 7500
						  , iconCls: $(this).data ('icon')
						});					
					}
				}
		});
		
		return false;
	}
	
	function changeItemStatus() {
	
		var campaign	= $('#statuscampaigncode').val();
		var media 		= $('#statuscode').val();	
		var status			= $('#status').val();
		
		$.ajax({
				type: "GET",
				url: "/campaign/media.php?code="+campaign+"&media="+media,
				data: "status_code="+media+"&status="+status,
				dataType: "json",
				success: function(data){
					if(data.result == 1) {
						window.location.href = window.location.href;
					} else {
						$('#statusItemModal').modal('hide');
						$.howl ({
						  type: 'info'
						  , title: 'Notification'
						  , content: data.error
						  , sticky: $(this).data ('sticky')
						  , lifetime: 7500
						  , iconCls: $(this).data ('icon')
						});	
					}
				}
		});								

		return false;		
	}
</script>
{/literal}
<!-- Modal -->
<div class="modal fade" id="statusItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Change Status</h4>
			</div>
			<div class="modal-body">Are you sure you want to change this item's status?</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:changeItemStatus();">Change Item Status</button>
				<input type="hidden" id="statuscampaigncode" name="statuscampaigncode" value="" />
				<input type="hidden" id="statusmediacode" name="statusmediacode" value="" />
				<input type="hidden" id="statuscode" name="statuscode" value=""/>
				<input type="hidden" id="status" name="status" value=""/>
			</div>
		</div>
	</div>
</div>
<!-- modal -->

<!-- Modal -->
<div class="modal fade" id="deleteitemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Item</h4>
			</div>
			<div class="modal-body">Are you sure you want to delete this item?</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:deletesubitem();">Delete Item</button>
				<input type="hidden" id="campaigncode" name="campaigncode" value=""/>
				<input type="hidden" id="mediacode" name="mediacode" value=""/>
				<input type="hidden" id="mediacode" name="mediacode" value=""/>
			</div>
		</div>
	</div>
</div>
<!-- modal -->
</html>