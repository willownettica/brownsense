<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
		<h2 class="content-header-title">Administrators</h2>
		<ol class="breadcrumb">
			<li><a href="/">Home</a></li>	
			<li><a href="#">Administration</a></li>	
			<li><a href="/admin/view/">Admins</a></li>
			<li><a href="/admin/view/details.php?code={$tempData.admin_code}">{$tempData.admin_name} {$tempData.admin_surname}</a></li>
			<li class="active">Comms</li>
		</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					Comms list
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="#" method="POST" data-validate="parsley" class="form parsley-form" enctype="multipart/form-data">			
				<p>Below is a list of communications sent to the administrator <b>{$tempData.admin_name}</b>.</p>
				<table class="table table-bordered">	
					<thead>
						<tr>
							<td>Sent</td>
							<td>Subject</td>
							<td>Message</td>
							<td>Output</td>
						</tr>
					</thead>
					<tbody>
					{foreach from=$commData item=item}
						<tr>
							<td>{$item._comm_added}</td>			
							<td>{$item._comm_subject}</td>
							<td>{if $item._comm_type eq 'SMS'}{$item._comm_message}{else}<a href="/mailer/view/{$item._comm_code}" target="_blank">Click to view mail</a>{/if}</td>				
							<td><span class="{if $item._comm_sent eq 1}success{else}error{/if}">{$item._comm_output|default:"No result recorded"}</span></td>
						</tr>			     
					{foreachelse}
						<tr>
							<td align="center" colspan="3">There are currently no items</td>
						</tr>					
					{/foreach}
					</tbody>					  
				</table>
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col -->		
		<div class="col-sm-3">
			<div class="list-group">  
				<a href="/admin/view/" class="list-group-item">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/admin/view/details.php?code={$tempData.admin_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a> 
				<a class="list-group-item" href="/admin/view/role.php?code={$tempData.admin_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp; Roles
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>				
				<a class="list-group-item" href="/admin/view/comm.php?code={$tempData.admin_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Comms
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>				
			</div> <!-- /.list-group -->
		</div>		
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
</html>