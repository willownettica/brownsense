<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';

/* Check for login */
require_once 'includes/auth.php';

/* objects. */
require_once 'class/item.php';
require_once 'class/link.php';

$itemObject		= new class_item();
$linkObject		= new class_link();

/* Check posted data. */
if(isset($_GET['delete_tag'])) {

	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success				= NULL;
	$linkcode				= trim($_GET['delete_tag']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {	
		$data					= array();
		$data['link_deleted']	= 1;
		
		$where		= array();
		$where[]	= $linkObject->getAdapter()->quoteInto('link_code = ?', $linkcode);
		$where[]	= $linkObject->getAdapter()->quoteInto("link_child_type = ?", 'TAG');
		$where[]	= $linkObject->getAdapter()->quoteInto('link_parent_code = ?', $zfsession->identity);
		$where[]	= $linkObject->getAdapter()->quoteInto("link_parent_type = ?", 'MEMBER');

		$success	= $linkObject->update($data, $where);	
		
		if($success) {
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}

	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(isset($_POST['add_code'])) {

	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 1;	
	$formValid				= true;
	$success				= NULL;
	$tagcode	= '';

	if(isset($_POST['category_code']) && trim($_POST['category_code']) != '') {
		if(isset($_POST['tag_code']) && trim($_POST['tag_code']) != '') {
			$tempData = $linkObject->checkExists('MEMBER', $zfsession->identity, 'TAG', trim($_POST['tag_code']));
			
			if($tempData) {
				$errorArray['error']	= 'This skill has already been linked to you account.';
				$errorArray['result']	= 0;		
			} else {
				$tagcode = trim($_POST['tag_code']);
			}
		} else if((isset($_POST['tag_name']) && trim($_POST['tag_name']) != '')) {
			/* Check if item exists, if not, add on. */
			$existData = $itemObject->exists(trim($_POST['tag_name']), trim($_POST['category_code']), 'TAG', 'MEMBER');
			
			if($existData) {
				$errorArray['error']	= 'This tag already exists, please add from the drop down after writing it.';
				$errorArray['result']	= 0;					
			} else {
				/* Add this item, it does not exist. */
				$item 	= array();				
				$item['item_parent']			= trim($_POST['category_code']);		
				$item['item_type']				= 'TAG';
				$item['item_name']				= strtolower(trim($_POST['tag_name']));	
				$item['item_config_reference']	= 'MEMBER';	

				$tagcode	= $itemObject->insert($item);

				if(!$tagcode) {
					$errorArray['error']	= 'We were unable to add the new skill, please try again.';
					$errorArray['result']	= 0;						
				}
			}
		} else {
			$errorArray['error']	= 'Please add or select skill from drop down.';
			$errorArray['result']	= 0;						
		}
	} else {
		$errorArray['error']	= 'Please select a category from the drop down.';
		$errorArray['result']	= 0;
	}
	
	if($errorArray['error']  == '' && $errorArray['result']  == 1 ) {
		$data 	= array();				
		$data['link_parent_type']	= 'MEMBER';		
		$data['link_parent_code']	= $zfsession->identity;	
		$data['link_child_type']		= 'TAG';	
		$data['link_child_code']		= $tagcode;	
		
		$success 	= $linkObject->insert($data);
		
		if($success) {
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not link, please try again.';
			$errorArray['result']	= 0;				
		}		
	}

	echo json_encode($errorArray);
	exit;
}

$tagData 			= $linkObject->getByParent('MEMBER', $zfsession->identity, 'TAG');
$parentSelect	= $itemObject->selectParents('CATEGORY');

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>	
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/account/"><?php echo $zfsession->memberData['member_name']; ?></a></li>
						<li><a href="#">My skillset</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-8">
				<h3 class="heading-1"><span>Account</span></h3>	
				<ul class="nav nav-tabs">
					<li><a href="/account/">Person Information</a></li>
					<li class="active"><a href="/account/tag.php">My skillset</a></li>					
					<li><a href="/account/document.php">My Documents</a></li>
					<li><a href="/account/link.php">Link with old Email Account</a></li>
				</ul>
				<p style="clear: both;">&nbsp;&nbsp;&nbsp;&nbsp;</p>					
					<p>Below is a list of skills sets you have or/and qualified in, this will make people who search for you find you better.</p><br />
					<form method="post" action="/account/tag.php" enctype="multipart/form-data">
						<div class="row clearfix">
							<!--Form Group-->
							<div class="form-group col-md-12 col-xs-12">
								<div class="text-right">								
									  <table class="table table-striped table-bordered" >
											<thead>
												<tr>
													<th>Industry / services and/or product</th>
													<th></th>
												</tr>
											</thead>							
										   <tbody>
										   <?php 
												if($tagData) {
													foreach($tagData as $item) { 
										   ?>
										  <tr>
											<td align="left"><?php echo $item['parentname']; ?> / <?php echo $item['item_name']; ?></td>
											<td>
											<button value="Delete" class="btn btn-danger" onclick="deleteModal('<?php echo $item['link_code']; ?>', 'delete_tag'); return false;">Delete</button>
											</td>
										  </tr>
										  <?php 
													}
												} else {
										  ?>
											<tr><td colspan="2">No skillset have been added yet</td></tr>
										  <?php } ?>
										  </tbody>			  
										</table> 										
								</div>
								<div class="clear: both"></div>	
								<label class="field-label">Select industry of your skills</label>
								<select name="category_code" id="category_code">
									<option value=""> --------------- </option>
									<?php 
										foreach($parentSelect as $key => $value) {
											echo "<option value='$key'>$value</option>";
										}
									?>
								</select>		
								<!--Form Group-->
								<div id="tag_div" style="display: none;">	
									<label><span class="category_name normal-font theme_color" ></span> industry skillset.</label><br />
									<p>If it already exists, select from the drop down menu.</p>
									<p>Please add your skills if you would like people to search for you, ONLY. e.g. "programmer", "doctor", "accountant", "lawyer"</p><br />
									<input type="text" name="tag_name" id="tag_name" value=""  style="text-transform: lowercase;" placeholder="Services and products you sell, add one at a time please" />
									<input type="hidden" id="tag_code" name="tag_code" value="" />
								</div>	
									<br />
									<button type="button" class="theme-btn btn-style-four" onclick="addTag(); return false;">Add Service / Product i offer</button><br /><br />
									<div class="alert alert-danger tag_fail" style="display: none; clear: both;">
										<strong>Oh snap!</strong><br /><p id="tag_fail_message"></p>
									</div>
									<div class="alert alert-info tag_submitting" style="display: none; clear: both;">
										<strong>Heads up!</strong> Submitting, please wait....
									</div>									
							</div>
						</div>
					</form>
					<br />
			</div>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" language="javascript">
	$(document).ready(function(){
		$("#category_code").change(function() {
			var category_code	= $('#category_code :selected').val();		
			var category_name	= $("#category_code :selected").text();
			
			if(category_code != '') {
				$('.category_name').html(category_name);
				$('#tag_div').show();
				$('#tag_div').css('visibility', 'visible');
				autocomplete();
			} else {
				$('#tag_div').hide();
				$('#tag_div').css('visibility', 'none');
			}
		});
	});
	function autocomplete() {
		$("#tag_name").autocomplete({
		  source: function(request, response) {
			$.getJSON("/feeds/tag.php?type=TAG&reference=MEMBER", { term: $("#tag_name").val(), parent: $('#category_code :selected').val() }, response);
		  },
		  minLength: 2,
		  select: function(event, ui){
			if(ui.item.id == '') {
				$('#tag_name').html('');
				$('#tag_code').val('');					
				$('#selectedtag').html('No skill to be added / selected yet');
			} else {
				$('#tag_name').html(ui.item.value);
				$('#tag_code').val(ui.item.id);
				$('#selectedtag').html(ui.item.value);
			}
			$('#tag_name').val('');		
		  }
		});
	}
	function addTag() {
		
		$('.tag_submitting').show();
		$('.tag_success').hide();
		$('.tag_fail').hide();
		
		var categorycode	= $('#category_code').val();
		var tagcode	 		= $('#tag_code').val();
		var tagname		 	= $('#tag_name').val();
		
		$.ajax({
			type: "POST",
			url: "tag.php",
			data: "add_code=1&category_code="+categorycode+"&tag_code="+tagcode+"&tag_name="+tagname,
			dataType: "json",
			success: function(data){
				if(data.result == 1) {
					window.location.href = window.location.href;
				} else {
					$('.tag_fail').show();
					$('.tag_success').hide();
					$('#tag_fail_message').html(data.error);
				}
				$('.tag_submitting').hide();
			}
		});

		return false;
	}	
	function deleteitemModal(media) {
		$('#mediacode').val(media);
		$('#deleteitemModal').modal('show');
		return false;
	}

	function deletesubitem() {
		
		$('.tag_d_submitting').show();
		$('.tag_d_submitting').css('visibility', 'visible');
		
		$('.tag_d_success').hide();
		$('.tag_d_success').css('visibility', 'hidden');
		
		$('.tag_d_fail').hide();
		$('.tag_d_fail').css('visibility', 'hidden');
		
		var mediacode	= $('#mediacode').val();
		
		$.ajax({
			type: "GET",
			url: "tag.php",
			data: "delete_code="+mediacode,
			dataType: "json",
			success: function(data){
				if(data.result == 1) {
					window.location.href = window.location.href;
				} else {
					$('.tag_d_fail').show();
					$('.tag_d_fail').css('visibility', 'visible');
					$('.tag_d_success').hide();
					$('.tag_d_success').css('visibility', 'hidden');
					$('#tag_d_fail_message').html(data.error);
				}
				$('.tag_d_submitting').hide();
				$('.tag_d_submitting').css('visibility', 'hidden');
			}
		});
		return false;
	}
	
	function deleteModal(item, parameter) {
			$('#deleteitem').val(item);
			$('#deleteparameter').val(parameter);
			$('#deleteModal').modal('show');
			return false;
	}


	function deleteItem() {
		
		$('.delete_submit').show();
		$('.delete_fail').hide();
		
		var deleteitem			= $('#deleteitem').val();
		var deleteparameter	= $('#deleteparameter').val();

		$.ajax({
			type: "GET",
			url: "/account/tag.php",
			data: deleteparameter+"="+deleteitem,
			dataType: "json",
			success: function(data){
				if(data.result == 1) {
					window.location.href = window.location.href;
				} else {
					$('.delete_fail').show();
					$('#delete_fail_message').html(data.error);
				}
				$('.delete_submit').hide();
			}
		});
		
		return false;
	}	
</script>
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Item</h4>
			</div>
			<div class="modal-body">
				Are you sure you want to delete this item?
				<div class="alert alert-danger delete_fail" style="display: none; clear: both;">
					<strong>Oh snap!</strong><br /><p id="delete_fail_message"></p>
				</div>
				<div class="alert alert-info delete_submit" style="display: none; clear: both;">
					<strong>Heads up!</strong> Submitting, please wait....
				</div>				
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:deleteItem();">Delete Item</button>
				<input type="hidden" id="deleteitem" name="deleteitem" value=""/>
				<input type="hidden" id="deleteparameter" name="deleteparameter" value=""/>
			</div>
		</div>
	</div>
</div>
<!-- modal -->
</body>
</html>
