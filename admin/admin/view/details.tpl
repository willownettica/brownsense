<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
	<h2 class="content-header-title">Administrators</h2>
	<ol class="breadcrumb">
	<li><a href="/">Home</a></li>	
	<li><a href="/admin/view/">Administration</a></li>
	<li><a href="/admin/view/">Admins</a></li>
	<li><a href="/admin/view/">{if isset($tempData)}{$tempData.admin_name}{else}Add an admin{/if}</a></li>
	<li class="active">Details</li>
	</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					{if isset($tempData)}{$tempData.admin_name}{else}Add a admin{/if}
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="/admin/view/details.php{if isset($tempData)}?code={$tempData.admin_code}{/if}" method="POST" data-validate="parsley" class="form parsley-form"> 				
				<div class="form-group">
                  <label for="admin_name">Name</label>
                  <input type="text" id="admin_name" name="admin_name" class="form-control" data-required="true" value="{$tempData.admin_name}" />
					{if isset($errorArray.admin_name)}<span class="error">{$errorArray.admin_name}</span>{/if}					  
                </div>
				<div class="form-group">
                  <label for="admin_surname">Surname</label>
                  <input type="text" id="admin_surname" name="admin_surname" class="form-control" data-required="true" value="{$tempData.admin_surname}" />
					{if isset($errorArray.admin_surname)}<span class="error">{$errorArray.admin_surname}</span>{/if}					  
                </div>					
                <div class="form-group">
                  <label for="admin_email">Email address</label>
                  <input type="text" id="admin_email" name="admin_email" class="form-control" value="{$tempData.admin_email}" />
				{if isset($errorArray.admin_email)}<span class="error">{$errorArray.admin_email}</span>{/if}					  
                </div>
                <div class="form-group">
                  <label for="admin_cellphone">Cellphone number</label>
                  <input type="text" id="admin_cellphone" name="admin_cellphone" class="form-control" value="{$tempData.admin_cellphone}" />
				{if isset($errorArray.admin_cellphone)}<span class="error">{$errorArray.admin_cellphone}</span>{/if}					  
                </div>				
                <div class="form-group"><button type="submit" class="btn btn-primary">Validate and Submit</button></div>
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col -->
		<div class="col-sm-3">
			<div class="list-group">  
				<a class="list-group-item" href="/admin/view/">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				{if isset($tempData)}					
				<a class="list-group-item" href="/admin/view/details.php?code={$tempData.admin_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a> 
				<a class="list-group-item" href="/admin/view/role.php?code={$tempData.admin_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp; Roles
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/admin/view/comm.php?code={$tempData.admin_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Comms
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>			
				{/if}
			</div> <!-- /.list-group -->
        </div>			
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
</html>
