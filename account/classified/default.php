<?php 
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/auth.php'; 

require_once 'class/classified.php';

$classifiedObject 	= new class_classified();

/* Check posted data. */
if(isset($_GET['delete_classified'])) {

	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success				= NULL;
	$classified				= trim($_GET['delete_classified']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {	
		$data					= array();
		$data['classified_deleted']	= 1;
		
		$where		= array();
		$where[]	= $classifiedObject->getAdapter()->quoteInto('classified_code = ?', $classified);
		$where[]	= $classifiedObject->getAdapter()->quoteInto("member_code = ?", $zfsession->identity);

		$success	= $classifiedObject->update($data, $where);	
		
		if($success) {
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}

	echo json_encode($errorArray);
	exit;
}

$classifiedData = $classifiedObject->getByMember();

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>	
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/account/"><?php echo $zfsession->memberData['member_name']; ?></a></li>
						<li><a href="/account/classified/">My Classifieds</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">			
			<div class="col-md-12 col-sm-8">
				<h2>My classifieds</h2><br />				
				<p>Below is a list of all classifieds that you have added and advertised.</p><br />
				<button value="Add new classified" class="btn btn-warning fr" style="float: right;" onclick="location.href='/account/classified/details.php'">Add new classified</button><br />			
				  <table class="table">
						<thead>
							<tr>
								<th>Type</th>
								<th>Subject</th>
								<th>Status</th>
								<th>Reason</th>
								<th></th>
							</tr>
						</thead>
					   <tbody>
					   <?php 
							if($classifiedData) {
								foreach($classifiedData as $item) {
						?>
							<tr class="<?php if($item['vet_class'] != '') { echo $item['vet_class'];  } ?>">
								<td align="left" width="5%"><?php echo $item['type_name']; ?></td>
								<td align="left"><a href="/account/classified/details.php?code=<?php echo $item['classified_code']; ?>"><?php echo $item['classified_subject']; ?></a></td>
								<td align="left" width="10%"><?php if($item['vet_name'] != '') { echo $item['vet_name'];  } else { ?>Not vetted<?php } ?></td>
								<td align="left"><?php if($item['vet_name'] != '') { echo $item['vet_reason']; } else { ?>N / A<?php } ?></td>
								<td width="5%">
									<button value="Delete" class="btn btn-danger" onclick="deleteModal('<?php echo $item['classified_code']; ?>', '<?php echo $item['classified_code']; ?>', 'delete_classified'); return false;" style="float: right;">Delete</button>											
								</td>
							</tr>
					  <?php 
								}
							} else {
						?>
						<tr><td colspan="5">No classified have been added yet</td></tr>
					  <?php } ?>
					  </tbody>
					</table>		
				<br />
			</div>
		</div>			
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
<script type="text/javascript" language="javascript">

function deleteModal(parent, item, parameter) {
	$('#deleteparent').val(parent);
	$('#deleteitem').val(item);
	$('#deleteparameter').val(parameter);
	$('#deleteModal').modal('show');
	return false;
}

function deleteItem() {
	
	$('.delete_submit').show();
	$('.delete_fail').hide();
	
	var deleteparent		= $('#deleteparent').val();
	var deleteitem			= $('#deleteitem').val();
	var deleteparameter	= $('#deleteparameter').val();

	$.ajax({
		type: "GET",
		url: "?code="+deleteparent,
		data: deleteparameter+"="+deleteitem,
		dataType: "json",
		success: function(data){
			if(data.result == 1) {
				window.location.href = window.location.href;
			} else {
				$('.delete_fail').show();
				$('#delete_fail_message').html(data.error);
			}
			$('.delete_submit').hide();
		}
	});
	
	return false;
}
</script>
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Item</h4>
			</div>
			<div class="modal-body">
				Are you sure you want to delete this item?
				<div class="alert alert-danger delete_fail" style="display: none; clear: both;">
					<strong>Oh snap!</strong><br /><p id="delete_fail_message"></p>
				</div>
				<div class="alert alert-info delete_submit" style="display: none; clear: both;">
					<strong>Heads up!</strong> Submitting, please wait....
				</div>				
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:deleteItem();">Delete Item</button>
				<input type="hidden" id="deleteparent" name="deleteparent" value=""/>
				<input type="hidden" id="deleteitem" name="deleteitem" value=""/>
				<input type="hidden" id="deleteparameter" name="deleteparameter" value=""/>
			</div>
		</div>
	</div>
</div>
<!-- modal -->

</body>
</html>
