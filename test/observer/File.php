<?php
/**
 * logTime checks as to when last it took for a code to process
 *
 * Get the current time
 * Make compare it with the last added time
 *
 * @since 2.9.0
 *
 * @param string $lasttime
 * @return string
 */
function logTime( $log, $initialstarttime, $lasttime, &$logHTML ) {
	
	$currenttime	= microtime(true);
	if(isset($_GET['debug'])) {
		$time 			= round(($currenttime - $lasttime), 3);
		$timelapsed 	= round(($lasttime - $initialstarttime), 3);
		$timeFormat 	= str_replace('.000000', '', date("H:i:s.u", $currenttime));
		
		$class = "";
		if($time < 0.2) {
			/* Do not styl. Its fine. */
			$class = 'success';
		} else if($time > 0.2 && $time < 0.8) {
			/* This is worrying. */
			$class = 'warning';
		} else if($time > 0.8) {
			$class = 'error';
		}
		$logHTML .= "<tr class='$class'><td style='padding-left: 30px;'>$timeFormat</td><td style='padding-left: 30px;'>$timelapsed</td><td style='padding-left: 30px;'>$time</td><td style='padding-left: 30px;'>:</td><td style='padding-left: 30px;'>$log</td></tr>";
	}
	return $currenttime;
}

function logFile(&$logHTML, $reference, $initialstarttime, $file) {
	if(isset($_GET['debug'])) {
		/* CSS. */
		$CSS = '.error{background-color:#b90000;}.success{background-color:green;}.warning{background-color:#FF9933;}';
		
		/* Create HTML content. */
		$logHTML = "<html><head><title>$reference</title><style type='text/css'>$CSS</style></head><body>$file<br /><br />This script took: ".round((microtime(true) - $initialstarttime), 3)." to run<br /><table cellspacing='0' cellpadding='0'>$logHTML</table></body></html>";
		/* Make sure we have a directory. */
		$directory 	= $_SERVER['DOCUMENT_ROOT']."/logs/".date('Y-m-d').'/';
		$file		= $directory.date('Y-m-d H.i.s').'.html';	
		/* Double check if folder is tehre. */
		if(!is_dir($directory)) mkdir($directory, 0777, true); 
		/* Create the file to write the error on. */
		touch($file);
		/* Log all the script errors. */
		error_log($logHTML, 3, $file);
	}
}


$logHTML 			= '';
$initialstarttime	= microtime(true);

$starttime = logTime( 'STARTING.....<br />------------------', $initialstarttime, $initialstarttime, $logHTML);
$helper = new CurlHelper(API_URL);
$starttime = logTime( 'API LINK: '.API_URL, $initialstarttime, $starttime, $logHTML);
$starttime = logTime( __LINE__.': Create Object: $helper',$initialstarttime, $starttime, $logHTML);
-- 
?>