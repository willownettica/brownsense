<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/* Check for login */
require_once 'includes/auth.php';

/* objects. */
require_once 'class/advert.php';

$advertObject = new class_advert();

if (isset($_GET['code']) && trim($_GET['code']) != '') {

	$code = trim($_GET['code']);

	$advertData = $advertObject->getByCode($code);

	if($advertData) {
		$smarty->assign('advertData', $advertData);
	} else {
		header('Location: /advert/product/');
		exit;
	}
}

/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;
	
	if(isset($_POST['advert_name']) && trim($_POST['advert_name']) == '') {
		$errorArray['advert_name'] = 'Name is required';
		$formValid = false;		
	}
	
	if(isset($_POST['advert_type']) && trim($_POST['advert_type']) == '') {
		$errorArray['advert_type'] = 'Type is required';
		$formValid = false;		
	}

	if(isset($_POST['advert_height']) && (int)trim($_POST['advert_height']) == 0) {
		$errorArray['advert_height'] = 'Height of this advert is required.';
		$formValid = false;		
	}

	if(isset($_POST['advert_width']) && (int)trim($_POST['advert_width']) == 0) {
		$errorArray['advert_width'] = 'Width of this advert is required.';
		$formValid = false;		
	}
	
	if(count($errorArray) == 0 && $formValid == true) {
	
		$data 	= array();						
		$data['advert_name']		= trim($_POST['advert_name']);		
		$data['advert_height']	= trim($_POST['advert_height']);
		$data['advert_width']		= trim($_POST['advert_width']);
		$data['advert_type']		= trim($_POST['advert_type']);
		
		if(isset($advertData)) {
			$where		= $advertObject->getAdapter()->quoteInto('advert_code = ?', $advertData['advert_code']);
			$success	= $advertObject->update($data, $where);
			$success	= $advertData['advert_code'];
		} else {
			$success = $advertObject->insert($data);
		}

		header('Location: /advert/product/');	
		exit;
	}
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

$smarty->display('advert/product/details.tpl');
?>