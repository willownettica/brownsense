<?php

require_once '_comm.php';

//custom account item class as account table abstraction
class class_market extends Zend_Db_Table_Abstract
{
   //declare table variables
    protected $_name 			= 'market';
	protected $_primary 		= 'market_code';
	public 			$_comm		= null;
	
	function init()	{
		$this->_comm	= new class_comm();
	}
	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 
	 public function insert(array $data)
    {
        // add a timestamp
        $data['market_added'] 		= date('Y-m-d H:i:s');        
		$data['market_code']			= $this->createCode();

		return parent::insert($data);	
    }
	
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where)
    {
        // add a timestamp
        $data['market_updated'] = date('Y-m-d H:i:s');
        
        return parent::update($data, $where);
    }
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getByBranch($branch) {
		$select = $this->_db->select()
			->from(array('market' => 'market'))
			->joinInner(array('company' => 'company'), 'company.company_code = market.company_code', array())
			->joinInner(array('vet' => 'vet'), "vet.vet_item_type = 'COMPANY' and vet.vet_item_code = company.company_code", array())
			->joinInner(array('item' => 'item'), "item.item_code = vet.item_code", array())
			->where('company_deleted = 0 and vet_deleted = 0 and item_deleted = 0 and item_config_status = 1')
			->where('market.branch_code = ?', $branch)
			->where('market_deleted = 0');

		$result = $this->_db->fetchAll($select);
		return ($result == false) ? false : $result = $result;					   
	}
	/**
	 * get job by job market Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code) {
		$select = $this->_db->select()
			->from(array('market' => 'market'))
			->joinInner(array('event' => 'event'), "market.branch_code = event.branch_code and period_code = date_format(event_date_start, '%m-%Y') and market_deleted = 0")
			->joinInner(array('company' => 'company'), "company.company_code = market.company_code")
			->joinLeft(array('link' => 'link'), "link.link_parent_type = 'MARKET' and link.link_parent_code = market_code and link_child_type = 'EVENT' and link_child_code = event_code")
			->joinLeft(array('vet' => 'vet'), "vet.vet_item_type = 'LINK' and vet_item_code = link_code and vet_active = 1 and vet_deleted = 0")
			->joinLeft(array('item' => 'item'), "item.item_code = vet.item_code and item_deleted = 0")
			->where("market_deleted = 0 and event_deleted = 0 and company_deleted = 0 and market_code = ?", $code);
		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}

	public function getSearch($start, $length, $filter = array()) {

		$where 		= 'company_deleted = 0';
		$eventcode	= '';
		$csv 			= 0;

		if(count($filter) > 0) {	
			for($i = 0; $i < count($filter); $i++) { 
				if(isset($filter[$i]['filter_event']) && trim($filter[$i]['filter_event']) != '') {
					$eventcode = $filter[$i]['filter_event'];
					$this->sanitize($eventcode);
				} else if(isset($filter[$i]['filter_search']) && trim($filter[$i]['filter_search']) != '') {
					$array = explode(" ",trim($filter[$i]['filter_search']));					
					if(count($array) > 0) {
						$where .= " and (";
						for($s = 0; $s < count($array); $s++) {
							$text = $array[$s];
							$this->sanitize($text);
							$where .= "lower(concat_ws(company_name, member_name, tag_name)) like lower('%$text%')";
							if(($s+1) != count($array)) {
								$where .= ' or ';
							}
						}
					}
					$where .= ")";
				} else if(isset($filter[$i]['filter_csv']) && (int)trim($filter[$i]['filter_csv']) == 1) {
					$csv = 1;
				} else if(isset($filter[$i]['filter_item']) && trim($filter[$i]['filter_item']) != '') {
					$itemcode = $filter[$i]['filter_item'];
					$this->sanitize($itemcode);
					if((int)$itemcode == 1) {
						$where .= " and market.item_code = '' ";
					} else {
						$where .= " and market.item_code = '$itemcode' ";
					}
					
				}
			}
		}

		$vet = $this->_db->select()	
				->from(array('vet' => 'vet'))
				->joinLeft(array('item' => 'item'), 'item.item_code = vet.item_code', array('item_name', 'item_config_color', 'item_config_class', 'item_config_status'))	
				->where("vet_deleted = 0 and item_deleted = 0 and item_config_status = 1")
				->where('vet_item_type = ?', 'COMPANY')
				->order('vet_added desc')
				->group('vet_item_code');

		$tag = $this->_db->select()	
					->from(array('link' => 'link'), array('link_parent_code', "group_concat(item.item_name separator ',') as tag_name"))
					->joinLeft(array('item' => 'item'), 'item.item_code = link.link_child_code', array())	
					->where("link_parent_type = 'COMPANY' and link_child_type = 'TAG'")
					->group('link_parent_code');

		$market = $this->_db->select()
					->from(array('market' => 'market'), array('company_code'))
					->joinInner(array('event' => 'event'), "market.branch_code = event.branch_code and period_code = date_format(event_date_start, '%m-%Y') and market_deleted = 0", array())
					->joinLeft(array('link' => 'link'), "link.link_parent_type = 'MARKET' and link.link_parent_code = market_code and link_child_type = 'EVENT' and link_child_code = event_code", array())
					->joinLeft(array('vet' => 'vet'), "vet.vet_item_type = 'LINK' and vet_item_code = link_code and vet_active = 1 and vet_deleted = 0", array('vet_reason'))
					->joinLeft(array('item' => 'item'), "item.item_code = vet.item_code and item_deleted = 0", array('item_code', 'item_name', 'item_config_status', 'item_config_class', 'item_config_color'))
					->where("market_deleted = 0 and event_deleted = 0 and event_code = '$eventcode'");

		$select = $this->_db->select()
			->from(array('company' => 'company'), array('company_name', 'company_contact_email', 'company_contact_number', 'company_contact_name'))
			->joinInner(array('member' => 'member'), 'member.member_code = company.member_code', array('member_name', 'social_facebook_id'))
			->joinInner(array('vet' => $vet), 'vet.vet_item_code = company.company_code', array())			
			->joinInner(array('tag' => $tag), 'tag.link_parent_code = company.company_code', array('tag.tag_name'))				
			->joinInner(array('market' => $market), 'market.company_code = company.company_code')
			->where($where);

		if($csv) {
			$result = $this->_db->fetchAll($select);
			return ($result == false) ? false : $result = $result;	
		} else {
			$result_count = $this->_db->fetchRow("select count(*) as query_count from ($select) as query");
			$result = $this->_db->fetchAll($select . " limit $start, $length");
			return ($result === false) ? false : $result = array('count'=>$result_count['query_count'],'displayrecords'=>count($result),'records'=>$result);	
		}
	}

	public function getLinked($company) {

		$select = $this->_db->select()
			->from(array('link' => 'link'), array('link_code'))
			->joinInner(array('event' => 'event'), "link.link_child_type = 'EVENT' and event.event_code = link.link_child_code",  array('event_code', "concat(event_name, ' - ', date_format(event_date_start, '%D %b %Y')) event_name"))
			->joinInner(array('market' => 'market'), "link.link_parent_type = 'MARKET' and market.market_code = link.link_parent_code")
			->where('link_deleted = 0 and market_deleted = 0 and market_active = 1 and company_code = ?', $company)
			->where('(event_date_start >= date(now() - interval 3 month)) and event_deleted = 0');

		$result = $this->_db->fetchAll($select);
		return ($result == false) ? false : $result = $result;				
	}
	
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getMarket($company, $branch, $period = '') {
		if($period != '') {
				$select = $this->_db->select()
					->from(array('market' => 'market'))
					->where('market_deleted = 0 and market_active = 1 and market.company_code = ?', $company)
					->where('branch_code = ?', $branch)
					->where('period_code = ?', $period)
					->where('market_active = 1')
					->limit(1);
				$result = $this->_db->fetchRow($select);
			} else {
				$select = $this->_db->select()
					->from(array('market' => 'market'))
					->where('market_deleted = 0 and market_active = 1 and market.company_code = ?', $company)
					->where('branch_code = ?', $branch)
					->where('market_active = 1');
				$result = $this->_db->fetchAll($select);
			}
		return ($result == false) ? false : $result = $result;					   
	}

	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getCode($code)
	{
		$select = $this->_db->select()	
					->from(array('market' => 'market'))	
					->where('market_code = ?', $code)
					->limit(1);

	   $result = $this->_db->fetchRow($select);	
        return ($result == false) ? false : $result = $result;					   
	}

	function createCode() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '1234567890';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Number[rand(0,$count)];
		}
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<4;$i++){
			$code .= $Number[rand(0,$count)];
		}		
		/* First check if it exists or not. */
		$marketCheck = $this->getCode($code);
		
		if($marketCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}

    function sanitize(&$string) { $string = preg_replace("/[^a-zA-Z0-9_]+/", "", $string);}
	
    function sanitizeArray(&$array) 
    {        
		for($i = 0; $i < count($array); $i++) {
			$array[$i] = preg_replace("/[^a-zA-Z0-9_]+/", "", $array[$i]);
		}
    }	
}
?>
