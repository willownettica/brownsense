<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/*** Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';
/**
 * Check for login
 */

require_once 'includes/auth.php';

require_once 'class/market.php';
require_once 'class/company.php';
require_once 'class/event.php';
require_once 'class/link.php';
require_once 'class/item.php';
require_once 'class/vet.php';

$marketObject		= new class_market();
$companyObject	= new class_company();
$eventObject			= new class_event();
$linkObject			= new class_link();
$itemObject			= new class_item();
$vetObject			= new class_vet();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$eventData = $eventObject->getByCode($code);
	
	if($eventData) {
		$smarty->assign('eventData', $eventData);
	} else {
		header('Location: /event/list/');
		exit;		
	}
} else {
	header('Location: /event/list/');
	exit;		
}

if(isset($_GET['action']) && trim($_GET['action']) == 'search') {

	$filter		= array();
	$filter[] 	= array('filter_branch' => $eventData['branch_code']);
	$filter[] 	= array('filter_period' => date('m-Y', strtotime($eventData['event_date_start'])));
	$filter[] 	= array('filter_event' => $eventData['event_code']);

	$csv			= 0;

	$start 		= isset($_REQUEST['iDisplayStart']) ? $_REQUEST['iDisplayStart'] : 0;
	$length 	= isset($_REQUEST['iDisplayLength']) ? $_REQUEST['iDisplayLength'] : 20;
	
	if(isset($_REQUEST['filter_search']) && trim($_REQUEST['filter_search']) != '') $filter[] = array('filter_search' => trim($_REQUEST['filter_search']));
	if(isset($_REQUEST['filter_item']) && trim($_REQUEST['filter_item']) != '') $filter[] = array('filter_item' => trim($_REQUEST['filter_item']));
	if(isset($_REQUEST['filter_csv']) && trim($_REQUEST['filter_csv']) != '') { $filter[] = array('filter_csv' => (int)trim($_REQUEST['filter_csv'])); $csv = (int)trim($_REQUEST['filter_csv']); } 

	if(!$csv) {
	
		$marketData = $marketObject->getSearch($start, $length, $filter);
		
		$markets = array();
		
		if($marketData) {
			for($i = 0; $i < count($marketData['records']); $i++) {
				$item = $marketData['records'][$i];

				$markets[$i] = array(
					($item['social_facebook_id'] == '' ? '<img src="/images/no-image.jpg" width="50px" />' : '<a href="http://facebook.com/'.$item['social_facebook_id'].'" target="_blank"><img src="https://graph.facebook.com/'.$item['social_facebook_id'].'/picture?width=300" width="50px" /></a>'),
					'<a href="/company/list/details.php?code='.$item['company_code'].'" target="_blank" id="company_name_'.$item['company_code'].'">'.$item['company_name'].'</a>',
					$item['tag_name'],
					'<b>'.($item['item_name'] != '' ? "<span title='".$item['vet_reason']."' alt='".$item['vet_reason']."' style='color: ".$item['item_config_color']."'>".$item['item_name']."</span>" : 'Not vetted').'</b>',
					'<button type="button" class="btn" onclick="vendorHistoryModal(\''.$item['company_code'].'\'); return false;" value="History">History</button>',
					'<button type="button" class="btn" onclick="vetVendorModal(\''.$item['company_code'].'\'); return false;" value="Vet">Vet</button>');
			}
		}

		if($marketData) {
			$response['sEcho'] = $_REQUEST['sEcho'];
			$response['iTotalRecords'] = $marketData['displayrecords'];		
			$response['iTotalDisplayRecords'] = $marketData['count'];
			$response['aaData']	= $markets;
		} else {
			$response['result'] 	= false;
			$response['message']	= 'There are no items to show.';			
		}

		echo json_encode($response);
		die();
	
	} else {
		$marketData = $marketObject->getSearch($start, $length, $filter);
		
		$row = "Out come, reason, Company, Contact Name, Contact Email, Contact Number, Services / Products\r\n";
		
		if($marketData) {

			for($i = 0; $i < count($marketData); $i++) {

				$item = $marketData[$i];

				$vendors[$i] = array(
					str_replace(',', ' and ', $item['item_name']),
					str_replace(',', ' and ', $item['vet_reason']),
					str_replace(',', ' and ', $item['company_name']),
					str_replace(',', ' and ', $item['company_contact_name']),
					str_replace(',', ' and ', $item['company_contact_email']), 
					str_replace(',', ' and ', $item['company_contact_number']), 			
					str_replace(',', ' and ', $item['tag_name'])
				);
			}
			
			foreach ($vendors as $data) {
				$row .= implode(', ', $data)."\r\n";
			}			
		}
		
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=".$eventData['event_name'].'_'.date('Y-m-d').".csv");
		Header("Cache-Control: private, must-revalidate" ); // HTTP/1.1
		Header("Pragma: private" ); // HTTP/1.0
	   
		print($row);
		exit;	
	}
}

/* Check posted data. */
if(isset($_GET['historycompany'])) {
	
	$errorArray					= array();
	$errorArray['error']		= '';
	$errorArray['records']	= '';
	$errorArray['result']		= 1;
	$companycode				= trim($_GET['historycompany']);

	$eventHistoryData = $eventObject->getVendorHistory($eventData['branch_code'], $companycode);

	if($eventHistoryData) {
		$html = '';
		for($i = 0; $i < count($eventHistoryData); $i++) {
			$item = $eventHistoryData[$i];
			$outcome = $item['item_name'] == '' ? '<td>Not Attended</td>' : '<td style="color: '.$item['item_config_color'].';">'.$item['item_name'].'</td>';
			$html .= "<tr><td>".$item['event_name']."</td>$outcome</tr>";
		}
		$errorArray['html']	= $html;
	} else {
		$errorArray['html']	= '<tr><td colspan="2">No events</td></tr>';	
	}

	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(isset($_GET['vetvendormarket'])) {
	
	$errorArray					= array();
	$errorArray['error']		= '';
	$errorArray['records']	= array();
	$errorArray['result']		= 0;
	$companycode				= trim($_GET['vetvendormarket']);

	$marketData = $marketObject->getMarket($companycode, $eventData['branch_code'], date('m-Y', strtotime($eventData['event_date_start'])));

	if($marketData) {
		$errorArray['result']		= 1;
		$errorArray['records']	= $marketData;
	} else {
		$errorArray['error']		= 'The selected company has not applied for markets in this event\'s branch.';
	}

	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(isset($_GET['vetvendor'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 1;
	$marketcode				= trim($_GET['marketcode']);
	$reason					= isset($_GET['reason']) && trim($_GET['reason']) != '' ? trim($_GET['reason']) : '';
	$itemcode					= isset($_GET['status']) && trim($_GET['status']) != '' ? trim($_GET['status']) : '';
	
	$marketData = $marketObject->getByCode($marketcode);

	if(!$marketData) {
		$errorArray['error']	= 'Company application does not exist.';
		$errorArray['result']	= 0;	
	}
	
	if($reason == '') {
		$errorArray['error']	= 'Please add a reason for your decision';
		$errorArray['result']	= 0;	
	}
	
	if($itemcode == '') {
		$errorArray['error']	= 'Please selected the outcome of the vetting';
		$errorArray['result']	= 0;	
	}
	
	if($errorArray['error']  == '' && $errorArray['result']  == 1 ) {
		$linkcode = '';
		/* Check if it already exists, then add the vetting result. */
		$linkData = $linkObject->checkExists('MARKET', $marketData['market_code'], 'EVENT', $eventData['event_code']);
		/* If it does not exist, create a new link. */
		if(!$linkData) {
			/* Check if already linked. */
			$data 								= array();
			$data['link_parent_type'] 	= 'MARKET';
			$data['link_parent_code'] 	= $marketData['market_code'];
			$data['link_child_type'] 		= 'EVENT';
			$data['link_child_code'] 		= $eventData['event_code'];
			$linkcode = $linkObject->insert($data);
		} else {
			$linkcode = $linkData['link_code'];
		}

		$data 							= array();
		$data['item_code'] 		= $itemcode;
		$data['vet_reason'] 		= $reason;
		$data['vet_item_type'] 	= 'LINK';
		$data['vet_item_code']	= $linkcode;

		$success = $vetObject->insert($data);
		
		if($success) {
			/* Send email to notify of vetting result. */
			$marketData = $marketObject->getByCode($marketcode);

			if($marketData) {
				
				if((int)$marketData['item_config_status'] == 1) {
					$templateData = $marketObject->_comm->_template->getTemplate('VET_MARKET_SUCCESS', 'EMAIL');
				} else if((int)$marketData['item_config_status'] == 0) {
					$templateData = $marketObject->_comm->_template->getTemplate('VET_MARKET_FAIL', 'EMAIL');
				}

				if($templateData) {
	 
					$recipient	= array();
					$recipient['recipient_code'] 		= $marketData['market_code'];
					$recipient['recipient_name'] 		= $marketData['company_name'];
					$recipient['recipient_cellphone'] 	= $marketData['company_contact_number'];
					$recipient['recipient_type'] 			= 'MARKET';
					$recipient['recipient_email'] 		= $marketData['company_contact_email'];
					$recipient['recipient_event'] 		= $marketData['event_name'];
					$recipient['recipient_reference'] 	= $marketData['company_contact_email'];
					$recipient['recipient_address'] 	= $marketData['event_address'];
					$recipient['recipient_message'] 	= $marketData['vet_reason'];

					$recipient['recipient_bankname'] 	= $marketData['event_bank_name'];
					$recipient['recipient_banktype'] 		= $marketData['event_bank_type'];
					$recipient['recipient_bankcode'] 		= $marketData['event_bank_code'];
					$recipient['recipient_bankaccount'] 	= $marketData['event_bank_account'];

					$success = $marketObject->_comm->sendEmail($recipient, $templateData);
					
					if(!$success) {	
						$errorArray['error']	= 'Email has not been sent, please contact the web administrator.';
						$errorArray['result']	= 0;							
					}
				} else {
					$errorArray['error']	= 'Email has not been sent, please contact the web administrator.';
					$errorArray['result']	= 0;										
				}
			}
		}
	}
	
	echo json_encode($errorArray);
	exit;
}

$itemData = $itemObject->selectParents('VET', 'MARKET');
if($itemData) $smarty->assign('itemData', $itemData);

$smarty->display('event/list/market.tpl');

?>