<!-- Favicon -->
<link rel="icon" type="image/png" href="/favicon.png" />
<link rel="icon" type="image/png" href="/favicon.png" />
<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="/css/bootstrap.min.css" />
<!-- Custom Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,300i,400,400i,500,700,700i,900" rel="stylesheet" />
<!-- Icon CSS -->
<link href="/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<!-- Theme CSS -->
<link href="/css/ts.css" rel="stylesheet" />
<link href="/library/javascript/slick/slick.css" rel="stylesheet" />
<link href="/library/javascript/lity/lity.min.css" rel="stylesheet" />
<link href="/css/style.css" rel="stylesheet" />
<link href="/css/magnific-popup.css" rel="stylesheet" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" href="/css/justifiedGallery.min.css" />
