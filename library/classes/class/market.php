<?php

require_once 'class/company.php';

//custom account item class as account table abstraction
class class_market extends Zend_Db_Table_Abstract {
   //declare table variables
    protected $_name		= 'market';
	protected $_primary	= 'market_code';
	
	public 			$_member		= null;
	public 			$_company		= null;
	
	function init()	{
		global $zfsession;
		$this->_member	= isset($zfsession->identity) && $zfsession->identity != '' ? $zfsession->identity : '';
		$this->_company	= new class_company();
	}
	/** 
	 * Insert the database record 
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 

	public function insert(array $data) {
        // add a timestamp
        $data['market_added']		= date('Y-m-d H:i:s');
        $data['market_code']			= $this->createCode();

		return parent::insert($data);	
    }
	
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where) {
        // add a timestamp
         $data['market_updated'] = date('Y-m-d H:i:s');
        
        return parent::update($data, $where);
    }
	/**
	 * get job by job branch Id
 	 * @param string job id
     * @return object
	 */
	public function remove($where) {
		return $this->_db->delete('market', $where);
	}
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getByPrimary($company, $branch, $period) {
		$select = $this->_db->select()
			->from(array('market' => 'market'))
			->joinInner(array('company' => 'company'), 'company.company_code = market.company_code')
			->joinInner(array('vet' => 'vet'), "vet.vet_item_type = 'COMPANY' and vet.vet_item_code = company.company_code")
			->joinInner(array('item' => 'item'), "item.item_code = vet.item_code")
			->where('company_deleted = 0 and vet_deleted = 0 and item_deleted = 0 and item_config_status = 1')
			->where('market_deleted = 0 and market.branch_code = ?', $branch)
			->where('period_code = ?', $period)
			->where('company.company_code = ?', $company);

		$result = $this->_db->fetchRow($select);
		return ($result == false) ? false : $result = $result;					   
	}	
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getMarket($branch, $period) {
		$select = $this->_db->select()
			->from(array('market' => 'market'))
			->joinInner(array('company' => 'company'), 'company.company_code = market.company_code')
			->joinInner(array('vet' => 'vet'), "vet.vet_item_type = 'COMPANY' and vet.vet_item_code = company.company_code")
			->joinInner(array('item' => 'item'), "item.item_code = vet.item_code")
			->where('company_deleted = 0 and vet_deleted = 0 and item_deleted = 0 and item_config_status = 1')
			->where('market_deleted = 0 and market.branch_code = ?', $branch)
			->where('period_code = ?', $period);

		$result = $this->_db->fetchAll($select);
		return ($result == false) ? false : $result = $result;					   
	}
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getByCompany($company) {
		$select = $this->_db->select()
			->from(array('market' => 'market'))
			->joinInner(array('company' => 'company'), 'company.company_code = market.company_code', array())
			->joinInner(array('vet' => 'vet'), "vet.vet_item_type = 'COMPANY' and vet.vet_item_code = company.company_code", array())
			->joinInner(array('item' => 'item'), "item.item_code = vet.item_code", array())
			->where('company_deleted = 0 and vet_deleted = 0 and item_deleted = 0 and item_config_status = 1')
			->where('market_deleted = 0');

		$result = $this->_db->fetchAll($select);
		return ($result == false) ? false : $result = $result;					   
	}
	
	public function getBranches($company) {
		$select = $this->_db->select()
			->from(array('market' => 'market'), array('branch_code'))
			->where('market_deleted = 0 and market_active = 1 and market.company_code = ?', $company);

		$result = $this->_db->fetchCol($select);
		return ($result == false) ? false : $result;	
	}
	
	public function getPeriods($company) {
		$select = $this->_db->select()->distinct()
			->from(array('market' => 'market'), array('period_code'))
			->where('market_deleted = 0 and market_active = 1 and company_code = ?', $company)
			->where('market_active = 1');

		$result = $this->_db->fetchCol($select);
		return ($result == false) ? false : $result;	
	}
	
	function _search($array, $searched) {
	  if (empty($searched) || empty($array)) { 
		return '';
	  } 

	  foreach ($array as $key => $value) { 
		$exists = true; 
		foreach ($searched as $skey => $svalue) { 
		  $exists = ($exists && IsSet($array[$key][$skey]) && $array[$key][$skey] == $svalue); 
		} 
		if($exists){ return $key; } 
	  } 

	  return ''; 
	}
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getVendor() {

		$companyData = $this->_company->getByMember($this->_member);

		$return = array();

		if($companyData) {
			for($i = 0; $i < count($companyData); $i++) {
				$tempData = $this->getBranches($companyData[$i]['company_code']);
				$return = $tempData === false ? $return : array_merge($tempData, $return); 
			}
		}
		return $return;		
	}
	
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function exists($company, $branch, $period) {
		$select = $this->_db->select()
			->from(array('market' => 'market'))
			->where('market_deleted = 0 and market_active = 1 and company_code = ?', $company)
			->where('branch_code = ?', $branch)
			->where('period_code = ?', $period)
			->limit(1);

		$result = $this->_db->fetchRow($select);
		return ($result == false) ? false : $result = $result;					   
	}
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getByCode($code) {
		$select = $this->_db->select()
			->from(array('market' => 'market'))
			->joinInner(array('company' => 'company'), 'company.company_code = market.company_code')
			->joinInner(array('vet' => 'vet'), "vet.vet_item_type = 'COMPANY' and vet.vet_item_code = company.company_code")
			->joinInner(array('item' => 'item'), "item.item_code = vet.item_code")
			->where('company_deleted = 0 and vet_deleted = 0 and item_deleted = 0 and item_config_status = 1')
			->where('market_deleted = 0 and market_code = ?', $code);

	   $result = $this->_db->fetchRow($select);	
       return ($result == false) ? false : $result = $result;					   
	}
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getCode($code) {
		$select = $this->_db->select()	
						->from(array('market' => 'market'))	
					   ->where('market_code = ?', $code)
					   ->limit(1);

	   $result = $this->_db->fetchRow($select);	
       return ($result == false) ? false : $result = $result;					   
	}
	
	function createCode() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '23456789';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Number[rand(0,$count)];
		}
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<4;$i++){
			$code .= $Number[rand(0,$count)];
		}		
		/* First check if it exists or not. */
		$marketCheck = $this->getCode($code);
		
		if($marketCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}
}
?>