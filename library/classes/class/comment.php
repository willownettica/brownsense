<?php

//custom account comment class as account table abstraction
class class_comment extends Zend_Db_Table_Abstract
{
   //declare table variables
    protected $_name 		= 'comment';
	protected $_primary 	= 'comment_code';
	
	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 
	 public function insert(array $data)
    {
        // add a timestamp
        $data['comment_added'] 		= date('Y-m-d H:i:s');        
		$data['comment_code']			= $this->createCode();

		return parent::insert($data);	
    }
	
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where) {
        // add a timestamp
        $data['comment_updated'] = date('Y-m-d H:i:s');
        
        return parent::update($data, $where);
    }

	/**
	 * get job by job comment Id
 	 * @param string job id
     * @return object
	 */
	public function getByTypeCode($type, $code) {
		
		$vet = $this->_db->select()	
					->from(array('vet' => 'vet'))
					->joinLeft(array('item' => 'item'), 'item.item_code = vet.item_code', array('item_name', 'item_config_color', 'item_config_class', 'item_config_status'))
					->where("item.item_config_status = 1 and vet_active = 1 and item_deleted = 0 and vet_deleted = 0")
					->where('vet_item_type = ?', 'COMMENT')
					->order('vet_added desc')
					->group('vet_item_code');
					
		$select = $this->_db->select()
					->from(array('comment' => 'comment'))
					->joinLeft(array('rate' => 'rate'), "rate.rate_item_code = comment.comment_code and rate_item_type = 'COMMENT' and rate_deleted = 0")
					->joinLeft(array('vet' => $vet), 'vet.vet_item_code = comment.comment_code')
					->joinLeft(array('member' => 'member'), 'member.member_code = comment.member_code and member_deleted = 0')
					->joinLeft(array('media' => 'media'), "media.media_item_code = member.member_code and media.media_item_type = 'MEMBER' and media.media_primary = '1' and media_deleted = 0", array('media_path', 'media_code', 'media_ext'))
					->where('comment_item_type = ?', $type)
					->where('comment_item_code = ?', $code)
					->where('comment_deleted = 0 and vet.item_config_status = 1');

		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get job by job comment Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code) {

		$vet = $this->_db->select()	
					->from(array('vet' => 'vet'))
					->joinLeft(array('item' => 'item'), 'item.item_code = vet.item_code', array('item_name', 'item_config_color', 'item_config_class', 'item_config_status'))
					->where("item.item_config_status = 1 and vet_active = 1 and item_deleted = 0 and vet_deleted = 0")
					->where('vet_item_type = ?', 'COMMENT')
					->where('vet_item_code = ?', $code)
					->order('vet_added desc')
					->group('vet_item_code');
					
		$select = $this->_db->select()	
					->from(array('comment' => 'comment'))
					->joinLeft(array('rate' => 'rate'), "rate.rate_item_code = comment.comment_code and rate_item_type = 'COMMENT' and rate_deleted = 0")
					->joinLeft(array('vet' => $vet), 'vet.vet_item_code = comment.comment_code')
					->where('comment_code = ?', $code)
					->where('comment_deleted = 0 and vet.item_config_status = 1');

		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}

	/**
	 * get job by job comment Id
 	 * @param string job id
     * @return object
	 */
	public function getCode($code) {

		$select = $this->_db->select()	
					->from(array('comment' => 'comment'))
					->where('comment_code = ?', $code);

		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}
	
	function createCode() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '1234567890';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Number[rand(0,$count)];
		}
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<4;$i++){
			$code .= $Number[rand(0,$count)];
		}		
		/* First check if it exists or not. */
		$commentCheck = $this->getCode($code);
		
		if($commentCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}
	
	public function validateEmail($string) {
		if(!filter_var($string, FILTER_VALIDATE_EMAIL)) {
			return '';
		} else {
			return trim($string);
		}
	}
	
	public function validateNumber($string) {
		if(preg_match('/^0[0-9]{9}$/', $this->onlyNumber(trim($string)))) {
			return $this->onlyNumber(trim($string));
		} else {
			return '';
		}
	}
	
	public function validateDate($string) {
		if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $string)) {
			if(date('Y-m-d', strtotime($string)) != $string) {
				return '';
			} else {
				return $string;
			}
		} else {
			return '';
		}
	}	
	
	public function onlyNumber($string) {
		/* Remove some weird charactors that windows dont like. */
		$string = strtolower($string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("�", "", $string);	
		$string = str_replace('___' , '' , $string);
		$string = str_replace('__' , '' , $string);	 
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("�", "", $string);	
		$string = str_replace("–", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("#", "", $string);	
		$string = str_replace("$", "", $string);	
		$string = str_replace("@", "", $string);	
		$string = str_replace("!", "", $string);	
		$string = str_replace("&", "", $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(':' , '' , $string);		
		$string = str_replace('[' , '' , $string);		
		$string = str_replace(']' , '' , $string);		
		$string = str_replace('|' , '' , $string);		
		$string = str_replace('\\' , '' , $string);		
		$string = str_replace('%' , '' , $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);	
		$string = str_replace('-' , '' , $string);	
		$string = str_replace('+27' , '0' , $string);	
		$string = str_replace('(0)' , '' , $string);	
		
		$string = preg_replace('/^00/', '0', $string);
		$string = preg_replace('/^27/', '0', $string);
		
		$string = preg_replace('!\s+!',"", strip_tags($string));
		
		return $string;
				
	}		
}
?>