<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/* Check for login */
require_once 'includes/auth.php';
/* objects. */
require_once 'class/item.php';

$itemObject 				= new class_item();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$itemData = $itemObject->getByCode($code);

	if($itemData) {
		$smarty->assign('itemData', $itemData);
	} else {
		header('Location: /admin/document/');
		exit;
	}
}

/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;
	
	if(isset($_POST['item_name']) && trim($_POST['item_name']) == '') {
		$errorArray['item_name'] = 'Name is required';
		$formValid = false;		
	}

	if(isset($_POST['item_config_reference']) && trim($_POST['item_config_reference']) == '') {
		$errorArray['item_config_reference'] = 'Reference is required';
		$formValid = false;		
	}
	
	if(count($errorArray) == 0 && $formValid == true) {
	
		$data 	= array();
		$data['item_name']					= trim($_POST['item_name']);
		$data['item_config_reference']	= trim($_POST['item_config_reference']);
		$data['item_type']						= 'DOCUMENT';			
		$data['item_config_date']			= (int)trim($_POST['item_config_date']);

		if(isset($itemData)) {
			$where		= array();
			$where[]	= $itemObject->getAdapter()->quoteInto('item_code = ?', $itemData['item_code']);
			$success	= $itemObject->update($data, $where);
			$success	= $itemData['item_code'];
		} else {
			$success = $itemObject->insert($data);
		}

		header('Location: /admin/document/');	
		exit;
	}
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

$smarty->display('admin/document/details.tpl');

?>