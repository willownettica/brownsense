<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/* Check for login */
require_once 'includes/auth.php';

/* objects. */
require_once 'class/branch.php';
require_once 'class/link.php';

$branchObject	= new class_branch();
$linkObject		= new class_link();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$branchData = $branchObject->getByCode($code);

	if($branchData) {
		$smarty->assign('branchData', $branchData);
		
		$adminListData = $linkObject->getByParent('BRANCH', $code, 'ADMIN');
		if($adminListData) $smarty->assign('adminListData', $adminListData);
	} else {
		header('Location: /admin/branch/');
		exit;
	}
} else {
	header('Location: /admin/branch/');
	exit;
}

/* Check posted data. */
if(isset($_GET['delete_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success					= NULL;
	$linkcode					= trim($_GET['delete_code']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {	
		$data	= array();
		$data['link_deleted'] = 1;
		
		$where		= array();
		$where[]	= $linkObject->getAdapter()->quoteInto('link_code = ?', $linkcode);
		$where[]	= $linkObject->getAdapter()->quoteInto("link_child_type = ?", 'ADMIN');
		$where[]	= $linkObject->getAdapter()->quoteInto('link_parent_code = ?', $branchData['branch_code']);
		$where[]	= $linkObject->getAdapter()->quoteInto("link_parent_type = ?", 'BRANCH');
		
		$success	= $linkObject->update($data, $where);	
		
		if(is_numeric($success) && $success > 0) {		
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	
	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;

	if(isset($_POST['admin_code']) && trim($_POST['admin_code']) == '') {
		$errorArray['admin_code'] = 'Area location is required';
		$formValid = false;		
	} else {
		$tempData = $linkObject->checkExists('BRANCH', $branchData['branch_code'], 'ADMIN', trim($_POST['admin_code']));
		
		if($tempData) {
			$errorArray['admin_code'] = 'This has already been added.';
			$formValid = false;	
		}
	}

	if(count($errorArray) == 0 && $formValid == true) {
		$data 	= array();				
		$data['link_parent_type']	= 'BRANCH';		
		$data['link_parent_code']	= $branchData['branch_code'];	
		$data['link_child_type']		= 'ADMIN';	
		$data['link_child_code']		= trim($_POST['admin_code']);	
		
		$success 	= $linkObject->insert($data);

		header('Location: /admin/branch/admin.php?code='.$code);	
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

$smarty->display('admin/branch/admin.tpl');

?>