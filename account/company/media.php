<?php 
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/auth.php'; 

require_once 'class/company.php';
require_once 'class/media.php';
require_once 'class/File.php';

$companyObject 	= new class_company();
$mediaObject 		= new class_media();
$fileObject				= new File(array('jpg', 'jpeg', 'png'));

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$companyData = $companyObject->getByMember($zfsession->identity, $code);

	if(!$companyData) {
		header('Location: /account/company/');
		exit;
	}
} else {
	header('Location: /account/company/');
	exit;
}

/* Check posted data. */
if(isset($_GET['delete_image'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true; 
	$success					= NULL;
	$itemcode					= trim($_GET['delete_image']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {	
		$data	= array();
		$data['media_deleted'] = 1; 
		
		$where		= array();
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_code = ?', $itemcode);
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_item_code = ?', $companyData['company_code']);
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_item_type = ?', 'COMPANY');
		
		$success	= $mediaObject->update($data, $where);	
		 
		if(is_numeric($success) && $success > 0) {		
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	
	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(isset($_GET['status_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;
	$data 						= array();
	$formValid				= true;
	$success					= NULL;
	$itemcode					= trim($_GET['status_code']);
	
	if($errorArray['error']  == '') {
		
		$itemData = $mediaData = $mediaObject->getCode($itemcode);
		
		if($itemData) {
			$success = $mediaObject->updatePrimary($itemData['media_category'], $itemData['media_item_type'], $companyData['company_code'], $itemcode);			
		}
		
		if(is_numeric($success)) {		
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not update, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	
	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(isset($_POST['uploadimage'])) {

	$errorArray					= array();
	$errorArray['message'] 	= array();
	$data 							= array();
	$formValid					= true;
	$success						= NULL;

	if(isset($_FILES['mediafile']) && count($_FILES['mediafile']['name']) > 0 && (isset($_FILES['mediafile']['name']) && trim($_FILES['mediafile']['name']) != '')) {
		/* Check validity of the CV. */
		if((int)$_FILES['mediafile']['size'] != 0 && trim($_FILES['mediafile']['name']) != '') {
			/* Check if its the right file. */
			$ext = $fileObject->file_extention($_FILES['mediafile']['name']); 

			if($ext != '') {
				$checkExt = $fileObject->getValidateExtention('mediafile', $ext);

				if(!$checkExt) {
					$errorArray['message'][] = 'Invalid file type something funny with the file format';
					$formValid = false;						
				} else {
					list($width, $height) = getimagesize($_FILES['mediafile']['tmp_name']);					

					if(((int)$width >= 374) && ((int)$height >= 278)) {
						/* Do absolutely nothing. */
					} else {
						$errorArray['message'][] = 'The width of the image needs to be greater than 374px and the height greater than 278px';
						$formValid = false;														
					}
				}
			} else {
				$errorArray['message'][] = 'Invalid file type';
				$formValid = false;									
			}
		} else {			
			switch((int)$_FILES['mediafile']['error']) {
				case 1 : $errorArray['message'][] = 'The uploaded file exceeds the maximum upload file size, should be less than 1M'; $formValid = false; break;
				case 2 : $errorArray['message'][] = 'File size exceeds the maximum file size'; $formValid = false; break;
				case 3 : $errorArray['message'][] = 'File was only partically uploaded, please try again'; $formValid = false; break;
				case 4 : $errorArray['message'][] = 'No file was uploaded'; $formValid = false; break;
				case 6 : $errorArray['message'][] = 'Missing a temporary folder'; $formValid = false; break;
				case 7 : $errorArray['message'][] = 'Faild to write file to disk'; $formValid = false; break;
			}
		}
	} else {
		$errorArray['message'][] = 'No file was uploaded';
		$formValid = false;									
	}

	if(count($errorArray['message']) == 0 && $formValid == true) {
		if(isset($_FILES['mediafile']) && count($_FILES['mediafile']['name']) > 0 && (isset($_FILES['mediafile']['name']) && trim($_FILES['mediafile']['name']) != '')) {
			$data = array();
			$data['media_code']			= $mediaObject->createCode();
			$data['media_item_code']	= $companyData['company_code'];
			$data['media_item_type']	= 'COMPANY';
			$data['media_category']		= 'IMAGE';

			$ext 			= strtolower($fileObject->file_extention($_FILES['mediafile']['name']));					
			$filename	= $data['media_code'].'.'.$ext;
			$directory	= $_SERVER['DOCUMENT_ROOT'].'/media/company/'.$companyData['company_code'].'/'.$data['media_code'];

			$file			= $directory.'/'.$filename;

			if(!is_dir($directory)) mkdir($directory, 0777, true);

			/* Create files for this product type. */
			foreach($fileObject->image as $item) {
				/* Change file name. */
				$newfilename = str_replace($filename, $item['code'].$filename, $file);
				/* Resize image. */
				$fileObject->resize_crop_image($item['width'], $item['height'], $_FILES['mediafile']['tmp_name'], $newfilename);
			}

			$data['media_path']		= '/media/company/'.$companyData['company_code'].'/'.$data['media_code'].'/';
			$data['media_filename']	= trim($_FILES['mediafile']['name']);
			$data['media_ext']			= '.'.$ext ;

			/* Check for other medias. */
			$primary = $mediaObject->getPrimary('IMAGE', 'COMPANY', $companyData['company_code']);		

			if($primary) {
				$data['media_primary']	= 0;
			} else {
				$data['media_primary']	= 1;
			}

			$success	= $mediaObject->insert($data);

			$mediaObject->updatePrimary(array('IMAGE'), 'COMPANY', $companyData['company_code'], $success);		
		}
		
		if(count($errorArray['message']) == 0 && $formValid == true) {
			/* All have been successfully uploaded. */
			$profileimageadded = 1;
		}
	}

	/* if we are here there are errors. */
	if(count($errorArray['message']) > 0) {
		$profileimagemessage = implode("<br />",$errorArray['message']);
	}
}

$mediaData 		= $mediaObject->getByReference(array('IMAGE'), 'COMPANY', $companyData['company_code']);

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/account/"><?php echo $zfsession->memberData['member_name']; ?></a></li>
						<li><a href="/account/company/">My Companies</a></li>
						<li><a href="/account/company/details.php?code<?php echo $companyData['company_code']; ?>"><?php echo $companyData['company_name']; ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">			
		<div class="row">
			<div class="col-md-12 col-sm-8">
				<h3 class="heading-1"><span><?php echo $companyData['company_name']; ?></span></h3>	
				<ul class="nav nav-tabs">
					<li><a href="/account/company/details.php?code=<?php echo $companyData['company_code']; ?>">Details</a></li>
					<li><a href="/account/company/tags.php?code=<?php echo $companyData['company_code']; ?>">Products / Services</a></li>
					<li class="active"><a href="/account/company/media.php?code=<?php echo $companyData['company_code']; ?>">Images</a></li>
					<li><a href="/account/company/document.php?code=<?php echo $companyData['company_code']; ?>">Documents</a></li>
					<li><a href="/account/company/vendor.php?code=<?php echo $companyData['company_code']; ?>">Become a vendor</a></li>
					<li><a href="/account/company/enquiry.php?code=<?php echo $companyData['company_code']; ?>">Enquiries</a></li>
				</ul>
				<p style="clear: both;">&nbsp;&nbsp;&nbsp;&nbsp;</p>			
				<p>Below is a list of all your company images, please also remember to select your company logo or profile pic.</p><br />
				<form method="post" action="/account/company/media.php?code=<?php echo $companyData['company_code']; ?>" enctype="multipart/form-data">
					<div class="row clearfix">								
						<div class="form-group col-md-12 col-xs-12">
							<table class="table table-bordered">	
								<thead>
									<tr>
										<td>Preview</td>
										<td></td>
										<td></td>
									</tr>
								</thead>
								<tbody class="popup-gallery">
									<?php 
										if($mediaData) {
											foreach($mediaData as $item) {
									?>
										<tr>
											<td>
												<a  title="<?php echo $companyData['company_name']; ?>" alt="<?php echo $companyData['company_name']; ?>" href="<?php echo $item['media_path']; ?>big_<?php echo $item['media_code'].$item['media_ext']; ?>" target="_blank">
													<img src="<?php echo $item['media_path']; ?>tny_<?php echo $item['media_code'].$item['media_ext']; ?>" title="<?php echo $companyData['company_name']; ?>" alt="<?php echo $companyData['company_name']; ?>" />
												</a>
											</td>
											<td>						
												<?php if($item['media_primary'] == 0) { ?>
													<button value="Select logo" class="btn btn-danger" onclick="statusitemModal('<?php echo $companyData['company_code']; ?>', '<?php echo $item['media_code']; ?>', '1' ); return false;">Select logo</button>
												<?php } else { ?>
												<b>My Logo</b>
												<?php } ?>
											</td>
											<td>
												<?php if($item['media_primary'] == 0) { ?>
													<button value="Delete" class="btn btn-danger" onclick="deleteModal('<?php echo $companyData['company_code']; ?>', '<?php echo $item['media_code']; ?>', 'delete_image'); return false;">Delete</button>
												<?php } else { ?>
													<b>The logo</b>
												<?php } ?>
											</td>
										</tr>	
									<?php 
											}
										} else {
									?>
										<tr>
											<td align="center" colspan="4">There are currently no logo added</td>
										</tr>
									<?php } ?>
								</tbody>					  
							</table>
						</div>
						<!--Form Group-->
						<div class="col-md-12 col-xs-12">
							<input type="file" name="mediafile" id="mediafile" /><br />
							<input type="hidden" id="uploadimage" name="uploadimage" value="1" />
							<p class="required">Only .jpg, .png or .jpeg extensions allowed.</p>
							<p class="required">More than 374px (width) and 278px (height).</p>
							<p class="required">Your image must be square shaped.</p>
						</div>								
						<!--Form Group-->
						<div class="form-group col-md-12 col-xs-12">
							<br />
								<button type="submit" class="theme-btn btn-style-four">Upload</button>
								<input type="hidden" id="uploadimage" name="uploadimage" value="1" />
							<div class="clear: both"></div><br />
							<?php if(isset($profileimageadded)) { ?>
							<div class="alert alert-success" style=" clear: both;">
								<strong>Well done!</strong><br />Your image has been successfully added.
							</div>
							<?php } ?>
							<?php if(isset($profileimagemessage)) { ?>
							<div class="alert alert-danger" style=" clear: both;">
								<strong>Oh snap!</strong><br /><?php echo $profileimagemessage; ?>
							</div>
							<?php } ?>										
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>

<script type="text/javascript" language="javascript">
	jQuery(document).ready(function() {
		$('.popup-gallery').magnificPopup({
			delegate: 'a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] 
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
				return item.el.attr('title') + '<small>'+item.el.attr('alt')+'</small>';
				}
			}
		});		
	});

	function statusitemModal(company, media, status) {
		$('#statuscompanycode').val(company);
		$('#statuscode').val(media);
		$('#status').val(status);
		$('#statusItemModal').modal('show');
		return false;
	}
	
	function changeItemStatus() {
	
		var company	= $('#statuscompanycode').val();
		var media 		= $('#statuscode').val();	
		var status			= $('#status').val();
		
		$.ajax({
				type: "GET",
				url: "?code="+company+"&media="+media,
				data: "status_code="+media+"&status="+status,
				dataType: "json",
				success: function(data){
					window.location.href = window.location.href;
				}
		});								

		return false;		
	}
	
	function deleteModal(parent, item, parameter) {
			$('#deleteparent').val(parent);
			$('#deleteitem').val(item);
			$('#deleteparameter').val(parameter);
			$('#deleteModal').modal('show');
			return false;
	}


	function deleteItem() {
		
		$('.delete_submit').show();
		$('.delete_fail').hide();
		
		var deleteparent		= $('#deleteparent').val();
		var deleteitem			= $('#deleteitem').val();
		var deleteparameter	= $('#deleteparameter').val();

		$.ajax({
			type: "GET",
			url: "?code="+deleteparent,
			data: deleteparameter+"="+deleteitem,
			dataType: "json",
			success: function(data){
				if(data.result == 1) {
					window.location.href = window.location.href;
				} else {
					$('.delete_fail').show();
					$('#delete_fail_message').html(data.error);
				}
				$('.delete_submit').hide();
			}
		});
		
		return false;
	}
</script>
<!-- Modal -->
<div class="modal fade" id="statusItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Select Logo</h4>
			</div>
			<div class="modal-body">Are you sure you want to make this picture <?php echo $companyData['company_name']; ?> logo image?</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:changeItemStatus();">Select</button>
				<input type="hidden" id="statuscompanycode" name="statuscompanycode" value="" />
				<input type="hidden" id="statusmediacode" name="statusmediacode" value="" />
				<input type="hidden" id="statuscode" name="statuscode" value=""/>
				<input type="hidden" id="status" name="status" value=""/>
			</div>
		</div>
	</div>
</div>
<!-- modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Item</h4>
			</div>
			<div class="modal-body">
				Are you sure you want to delete this item?
				<div class="alert alert-danger delete_fail" style="display: none; clear: both;">
					<strong>Oh snap!</strong><br /><p id="delete_fail_message"></p>
				</div>
				<div class="alert alert-info delete_submit" style="display: none; clear: both;">
					<strong>Heads up!</strong> Submitting, please wait....
				</div>				
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:deleteItem();">Delete Item</button>
				<input type="hidden" id="deleteparent" name="deleteparent" value=""/>
				<input type="hidden" id="deleteitem" name="deleteitem" value=""/>
				<input type="hidden" id="deleteparameter" name="deleteparameter" value=""/>
			</div>
		</div>
	</div>
</div>
<!-- modal -->
</body>
</html>
