<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/**
 * Standard includes
 */
require_once 'config/database.php';

require_once 'includes/auth.php';
/* objects. */
require_once 'class/media.php';
require_once 'class/File.php';
require_once 'class/item.php';

$mediaObject 		= new class_media();
$fileObject 			= new File(array('png', 'jpg', 'jpeg', 'pdf', 'xls', 'txt', 'docx', 'doc', 'csv', 'zip', 'xlsx'));
$itemObject			= new class_item();

/* Check posted data. */
if(isset($_GET['type_code'])) {

	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success					= NULL;
	$itemcode					= trim($_GET['type_code']);

	$itemData = $itemObject->getByCode($itemcode);
	
	echo json_encode(array('item_config_date' => $itemData['item_config_date']));
	exit;
}

if(isset($_GET['delete_document'])) {

	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success				= NULL;
	$itemcode				= trim($_GET['delete_document']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {

		$data	= array();
		$data['media_deleted'] = 1;
		
		$where		= array();
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_code = ?', $itemcode);
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_item_code = ?', $zfsession->identity);
		$where[]	= $mediaObject->getAdapter()->quoteInto("media_item_type = ?", 'MEMBER');
		
		$success	= $mediaObject->update($data, $where);	
		
		if(is_numeric($success) && $success > 0) {		
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	
	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(count($_FILES) > 0) {

	$documentError	= array();
	$data 		= array();
	$formValid	= true;
	$success	= NULL;
	
	if(isset($_POST['mediadescription']) && trim($_POST['mediadescription']) == '') {
		$documentError[] = 'Please add the file name or description';
	}
	
	if(isset($_POST['item_code']) && trim($_POST['item_code']) == '') {
		$documentError[] = 'Type of document is required';
	} else { 
		
		$tempItem = $itemObject->getByCode(trim($_POST['item_code']));
		
		if($tempItem) {
			if((int)$tempItem['item_config_date'] == 1) {
				if(isset($_POST['media_date_start']) && trim($_POST['media_date_start']) == '') {
					$documentError[] = 'A valid from date is required for this document type';
				}
				if(isset($_POST['media_date_end']) && trim($_POST['media_date_end']) == '') {
					$documentError[] = 'An exipiry date is required for this document type';
				}				
			}
		} else {
			$documentError[] = 'Type of document is required';
		}
	}
	
	if(isset($_FILES['mediafile']) && count($_FILES['mediafile']['name']) > 0 && (isset($_FILES['mediafile']['name']) && trim($_FILES['mediafile']['name']) != '')) {
		/* Check validity of the CV. */
		if((int)$_FILES['mediafile']['size'] != 0 && trim($_FILES['mediafile']['name']) != '') {
			/* Check if its the right file. */
			$ext = $fileObject->file_extention($_FILES['mediafile']['name']); 

			if($ext != '') {
				$checkExt = $fileObject->getValidateExtention('mediafile', $ext);

				if(!$checkExt) {
					$documentError[] = 'Invalid file type something funny with the file format';
				}
			} else {
				$documentError[] = 'Invalid file type';
			}
		} else {
			switch((int)$_FILES['mediafile']['error']) {
				case 1 : $documentError[] = 'The uploaded file exceeds the maximum upload file size, should be less than 1M'; break;
				case 2 : $documentError[] = 'File size exceeds the maximum file size'; break;
				case 3 : $documentError[] = 'File was only partically uploaded, please try again'; break;
				case 4 : $documentError[] = 'No file was uploaded'; break;
				case 6 : $documentError[] = 'Missing a temporary folder'; break;
				case 7 : $documentError[] = 'Faild to write file to disk'; break;
			}
		}
	} else {
		$errorArray['mediafile'] = 'No file was uploaded';
		$formValid = false;									
	}

	if(count($documentError) == 0) {

		if(isset($_FILES['mediafile']) && count($_FILES['mediafile']['name']) > 0 && (isset($_FILES['mediafile']['name']) && trim($_FILES['mediafile']['name']) != '')) {
			$data = array();
			$data['media_code']			= $mediaObject->createCode();
			$data['media_item_code']	= $zfsession->identity;
			$data['media_item_type']	= 'MEMBER';
			$data['item_code']				= trim($_POST['item_code']);
			$data['media_date_start']	= trim($_POST['media_date_start']);
			$data['media_date_end']	= trim($_POST['media_date_end']);
			$data['media_description']	= trim($_POST['mediadescription']);
			$data['media_category']		= 'DOCUMENT';

			$ext 		= strtolower($fileObject->file_extention($_FILES['mediafile']['name']));					
			$filename	= $data['media_code'].'.'.$ext;
			$directory	= $_SERVER['DOCUMENT_ROOT'].'/media/document/'.$data['media_code'];

			$file		= $directory.'/'.$filename;

			if(!is_dir($directory)) mkdir($directory, 0777, true); 
			/* Create new file and rename it. */ 
			file_put_contents($file, file_get_contents($_FILES['mediafile']['tmp_name']));

			$data['media_path']		= '/media/document/'.$data['media_code'].'/';
			$data['media_filename']	= trim($_FILES['mediafile']['name']);
			$data['media_ext']			= '.'.$ext ;

			$success	= $mediaObject->insert($data);
		}
	
		header('Location: /account/document.php');
		exit;
	}

	$documentError = implode("<br />",$documentError);

}

$documentData 	= $mediaObject->getByReference(array('DOCUMENT'), 'MEMBER', $zfsession->identity);
$documentSelect 	= $itemObject->selectParents('DOCUMENT', 'MEMBER');

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>	
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/account/"><?php echo $zfsession->memberData['member_name']; ?></a></li>
						<li><a href="#">My documents</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-8">
				<h3 class="heading-1"><span>Account</span></h3>	
				<ul class="nav nav-tabs">
					<li><a href="/account/">Person Information</a></li>
					<li><a href="/account/tag.php">My skillset</a></li>					
					<li class="active"><a href="/account/document.php">My Documents</a></li>
					<li><a href="/account/link.php">Link with old Email Account</a></li>
				</ul>
				<p style="clear: both;">&nbsp;&nbsp;&nbsp;&nbsp;</p>					
					<p>Below is where you can add your qualifications, CV and other documents that support your skillset.</p><br />
					<form method="post" action="/account/document.php" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-12">
								<table class="table table-striped table-bordered" >
									<thead>
										<tr>
											<td>Type of Document</td>
											<td>Description</td>
											<td>Start Date</td>
											<td>End Date</td>
											<td>Download</td>
											<td></td>
										</tr>
									</thead>
									<tbody>
									   <?php 
											if($documentData) {
												foreach($documentData as $item) { 
									   ?>
											<tr>
												<td><?php echo $item['item_name']; ?></td>							
												<td><?php echo $item['media_description']; ?></td>	
												<td><?php echo $item['media_date_start']; ?></td>	
												<td><?php echo $item['media_date_end']; ?></td>	
												<td>
													<a href="<?php echo $item['media_path']; ?><?php echo $item['media_code']; ?><?php echo $item['media_ext']; ?>" target="_blank">
														Download
													</a>
												</td>
												<td>
													<button value="Delete" class="btn btn-danger" onclick="deleteModal('<?php echo $item['media_code']; ?>', 'delete_document'); return false;">Delete</button>															
												</td>
											</tr>	
										  <?php 
														}
													} else {
											  ?>
												<tr><td colspan="6">No documents uploaded have been added yet</td></tr>
											  <?php } ?>					
									</tbody>					  
								</table>	
							</div>				
						</div>
						<div class="clear: both"></div>
						<p style="color: #42210b; font-weight: bold;">Add a new document below:</p><br />
						<div class="row">
							<div class="col-md-6">
								<label>File type</label>
								<select id="item_code" name="item_code">
									<option value=""> --------------- </option>
									<?php 
										foreach($documentSelect as $key => $value) {
											echo "<option value='$key'>$value</option>";
										}
									?>
								</select>	
							</div>
							<div class="col-md-6 item_config_name">
								<label>File description /  name</label>
								<input type="text" id="mediadescription" name="mediadescription" />
							</div>							
						</div>
						<div class="row">
							<div class="col-md-6 item_config_date">
								<label>Valid From</label>
								<input type="text" id="media_date_start" name="media_date_start" placeholder="Format being: YYYY-MM-DD" />
							</div>
							<div class="col-md-6 item_config_date">
								<label>Exirey Date</label>
								<input type="text" id="media_date_end" name="media_date_end" placeholder="Format being: YYYY-MM-DD" />
							</div>							
						</div>	
						<div class="row">
							<div class="col-md-12">
								<p class="theme_color">Allowed files are .png, .jpg, .jpeg, .pdf, .xlsx, .xls, .txt, .docx, .doc, .csv and .zip</p>
								<p>Files uploaded must be less than 1 MB<p><br />						
								<input type="file" id="mediafile" name="mediafile" />
								<br />
							</div>							
						</div>					
						<div class="row">
							<div class="col-md-12">
								<button type="submit" class="theme-btn btn-style-four">Upload document</button><br /><br />
								<input type="hidden" value="1" name="document_add" id="document_add" />
								<?php if(isset($documentError) && $documentError != '') { ?>										
								<div class="alert alert-danger">
									<strong>Oh snap!</strong><br /><p id="tag_fail_message"><?php echo $documentError; ?></p>
								</div>
								<?php } ?>	
							</div>							
						</div>								
					</form>
					<br />
			</div>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" language="javascript">
	$(document).ready(function(){
		
		getType();

		$( "#item_code" ).change(function() {
			getType();
		});
		
		$( "#media_date_start" ).datepicker({
			defaultDate: "+1w",
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1,
			onClose: function( selectedDate ) {
				$( "#media_date_end" ).datepicker( "option", "minDate", selectedDate );
			}
		});

		$( "#media_date_end" ).datepicker({
			defaultDate: "+1w",
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1,
			onClose: function( selectedDate ) {
				$( "#media_date_start" ).datepicker( "option", "maxDate", selectedDate );
			}
		});
	});

	function getType() {

		var typecode = $('#item_code').val();
		
		if(typecode != '') {
			$.ajax({
				type: "GET",
				url: "document.php",
				data: "type_code="+typecode,
				dataType: "json",
				success: function(data){
					if(data.item_config_date == 0) {
						$('.item_config_date').hide();
						$('.item_config_date').css('visibility', 'none');
					} else {
						$('.item_config_date').show();
						$('.item_config_date').css('visibility', 'visible');
					}
				}
			});
		} else {
			$('.item_config_date').hide();
		}
		$('#mediadescription').val($('#item_code :selected').text());
	}

	function deleteModal(item, parameter) {
		$('#deleteitem').val(item);
		$('#deleteparameter').val(parameter);
		$('#deleteModal').modal('show');
		return false;
	}

	function deleteItem() {
		
		$('.delete_submit').show();
		$('.delete_fail').hide();
		
		var deleteitem			= $('#deleteitem').val();
		var deleteparameter	= $('#deleteparameter').val();

		$.ajax({
			type: "GET",
			url: "/account/document.php",
			data: deleteparameter+"="+deleteitem,
			dataType: "json",
			success: function(data){
				if(data.result == 1) {
					window.location.href = window.location.href;
				} else {
					$('.delete_fail').show();
					$('#delete_fail_message').html(data.error);
				}
				$('.delete_submit').hide();
			}
		});
		
		return false;
	}	
</script>
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Item</h4>
			</div>
			<div class="modal-body">
				Are you sure you want to delete this item?
				<div class="alert alert-danger delete_fail" style="display: none; clear: both;">
					<strong>Oh snap!</strong><br /><p id="delete_fail_message"></p>
				</div>
				<div class="alert alert-info delete_submit" style="display: none; clear: both;">
					<strong>Heads up!</strong> Submitting, please wait....
				</div>				
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:deleteItem();">Delete Item</button>
				<input type="hidden" id="deleteitem" name="deleteitem" value=""/>
				<input type="hidden" id="deleteparameter" name="deleteparameter" value=""/>
			</div>
		</div>
	</div>
</div>
<!-- modal -->
</body>
</html>
