<?php 

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/auth.php'; 

require_once 'class/company.php';
require_once 'class/event.php';
require_once 'class/comment.php';
require_once 'class/media.php';
require_once 'class/market.php';
require_once 'class/branch.php';
require_once 'class/link.php';

$companyObject 	= new class_company();
$eventObject 		= new class_event();
$commentObject 	= new class_comment();
$mediaObject 		= new class_media();
$marketObject 		= new class_market();
$branchObject 		= new class_branch();
$linkObject			= new class_link();

if (isset($_GET['code']) && trim($_GET['code']) != '') {

	$code = trim($_GET['code']);

	$eventData = $eventObject->getByCode($code);

	if(!$eventData) {
		header('Location: /');
		exit;
	}

	$mediaData		= $mediaObject->getByReference(array('IMAGE'), 'EVENT', $code);

	$commentData 	= $commentObject->getByTypeCode('EVENT', $code);
	
} else {
	header('Location: /');
	exit;
}

/* Check posted data. */
if(isset($_REQUEST['market'])) {

	$errorArray					= array();
	$errorArray['message'] 	= array();
	$data 							= array();
	$company						= array();
	$branch						= array();
	
	if(isset($_POST['company_code'])) {
		$errorArray['message'][] = 'Please select a company';
	} else if(trim($_REQUEST['company_code']) == '') {
		$errorArray['message'][] = 'Please select a company';
	} else {
		$company = explode(",",$_REQUEST['company_code']);

		if(count($company) == 0) {
			$errorArray['message'][] = 'Please select a company';	
		}
	}

	if(isset($_POST['branch_code'])) {
		$errorArray['message'][] = 'Please select a branch';
	} else if(trim($_REQUEST['branch_code']) == '') {
		$errorArray['message'][] = 'Please select a branch';
	} else {
		$branch = explode(",",$_REQUEST['branch_code']);
		
		if(count($branch) == 0) {
			$errorArray['message'][] = 'Please select a branch';	
		}
	}
	
	if(count($errorArray['message']) == 0) {
		
		for($c = 0; $c < count($company); $c++) {				
			/* Add the new branches. */
			for($b = 0; $b < count($branch); $b++) {
				$check = $marketObject->exists($company[$c], $branch[$b]);
				
				if(!$check) {
					$data 							= array();
					$data['company_code']	= $company[$c];
					$data['branch_code']		= $branch[$b];
					
					$marketObject->insert($data);
				}
			}
			/* Remove branches that have been exluded. */
			$branchesData = $marketObject->getBranches($company[$c]);	

			$remove = array_values(array_diff($branchesData,$branch));

			for($d = 0; $d < count($remove); $d++) {
				$marketObject->remove($company[$c], $remove[$d]);
			}
		}
	}
	/* if we are here there are errors. */
	if(count($errorArray['message']) > 0) {
		$errorArray['message'] = implode("<br />",$errorArray['message']);
	} else {
		$errorArray['message'] = '';
	}
	
	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray					= array();
	$errorArray['message'] 	= array();
	$data 							= array();

	if(isset($_POST['comment_text']) && trim($_POST['comment_text']) == '') {
		$errorArray['message'][] = 'Please enter a comment.';
	}

	if(count($errorArray['message']) == 0) {

		$data 	= array();
		$data['comment_item_type']	= 'EVENT';
		$data['member_code']			= $zfsession->memberData['member_code'];
		$data['comment_item_code']	= $eventData['event_code'];		
		$data['comment_text']			= trim($_POST['comment_text']);
		$data['comment_name']			= $zfsession->memberData['member_name'].' '.$zfsession->memberData['member_surname'];
		$data['comment_number']		= $zfsession->memberData['member_cellphone'];
		$data['comment_email']			= $zfsession->memberData['member_email'];
		
		$commentadded = $commentObject->insert($data);
	}

	/* if we are here there are errors. */
	if(count($errorArray['message']) > 0) {
		$commentmessage = implode("<br />",$errorArray['message']);
	}
}

$companyPairs 	= $companyObject->pairs($zfsession->identity);
$linkData 			= $linkObject->getByChild('EVENT', $eventData['event_code'], 'MARKET');

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<?php if($eventData['event_map_latitude'] != '') { ?>
	<script type="text/javascript">
	var map;
	var marker;

	function initMap()
	{
		var opts = {'center': new google.maps.LatLng(<?php echo $eventData['event_map_latitude']; ?>, <?php echo $eventData['event_map_longitude']; ?>), 'zoom':14, 'mapTypeId': google.maps.MapTypeId.SATELLITE }
		map = new google.maps.Map(document.getElementById('mapdiv'),opts);
		
		
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(<?php echo $eventData['event_map_latitude']; ?>, <?php echo $eventData['event_map_longitude']; ?>),
			map: map
		});
	}

	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQULf4QMovya_unNUL1-kMhI_FxkZib7I&callback=initMap" async defer></script>
<?php } ?>
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/market/">Market</a></li>						
						<li><a href="#"><?php echo $eventData['event_name']; ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-7 dual-posts padding-bottom-30">
				<div class="blog-single-head margin-top-25">
					<h2><?php echo $eventData['event_name']; ?></h2>
					<div class="meta"><span class="date"><?php echo date('d F Y H:i A', strtotime($eventData['event_date_start'])); ?></span></div>
					<?php 
						if($zfsession->company) { 
							if(in_array($eventData['branch_code'], $zfsession->vendor)) {
								?>
								<div class="alert alert-success">
									You have applied to be a vendor in this event's branch.
								</div>
								<?php
							} else {
					?>
					<button type="button" class="background_green" onclick="/account/company/">Use your company to become a vendor</button><br /><br />
					<?php 
							}
						} else {
						?>
						<div class="alert alert-danger">
							You need a company to be a vendor in an event.
						</div>						
						<?php
						} 
					?>
					<div style="background-color: #f5f5f5; padding: 20px;">
						<p><strong class="text_brown ">Physical Address</strong><br /><?php echo $eventData['event_address']; ?></p>
						<hr />
						<p><?php echo $eventData['event_page']; ?></p><br />						
					</div>
					<?php if($eventData['event_map_latitude'] != '') { ?>
					<br />
					<p><strong class="text_brown ">Map Location</strong><br /><br />
					<div id="mapdiv" style="height: 400px; width: 100%;"><!-- Location of event. --></div>
					<?php } ?>					
				</div>
				<br />
				<?php if($linkData) { ?>
				<h2>Vendors</h2>
				<br />
				<div class="row">	
				<?php foreach($linkData as $link) { ?>				
				<div class="col-md-4 col-sm-4">
					<div class="layout_2--item row">
						<div class="col-md-6">
							<div class="thumb">
								<div class="icon-24 video2"></div>
								<a href="/company/<?php echo $link['company_code']; ?>">
									<?php if($link['media_code'] != '') { ?>
									<img src="<?php echo $link['media_path'].'tmb_'.$link['media_code'].$link['media_ext']; ?>" class="img-responsive" alt="<?php echo $link['company_name']; ?>" title="<?php echo $link['company_name']; ?>" />
									<?php  } else { ?>
									<img src="/images/no-image.jpg" class="img-responsive" alt="<?php echo $link['company_name']; ?>" title="<?php echo $link['company_name']; ?>" />
									<?php } ?>
								</a>
							</div>
						</div>
						<div class="col-md-6">
							<h4><a href="/company/<?php echo $link['company_code']; ?>"><?php echo $link['company_name']; ?></a></h4>
							<div class="meta"><span class="date"><?php echo substr($link['tag_name'],0,150).'...'; ?></span></div>
						</div>
					</div>
				</div>
				<?php } ?>				
				</div>
				<br />
				<?php } ?>
				<?php if($mediaData) { ?>
				<h2>Media</h2>
				<br />
				<div class="row">
					<div class="col-md-12 col-sm-8">
						<div id="mygallery" class="popup-gallery">
							<?php foreach($mediaData as $media) { ?>
								<a href="<?php echo $media['media_path'].'big_'.$media['media_code'].$media['media_ext']; ?>" alt="<?php echo $eventData['event_name']; ?>" title="<?php echo $eventData['event_name']; ?>">
									<img src="<?php echo $media['media_path'].'tmb_'.$media['media_code'].$media['media_ext']; ?>" class="img-responsive" alt="<?php echo $eventData['event_name']; ?>" title="<?php echo $eventData['event_name']; ?>" />
								</a>
							<?php } ?>
						</div>
					</div>
				</div>	
				<?php } ?>
				<br />
				<h2 id="commentsdiv">Comment</h2>
				<p>If you have had an engagement or any form of business with <strong class="text_green"><?php echo $eventData['event_name']; ?></strong> with this business, please tell us about it us how it threated you.</p>
				<br />
				<form class="post-comment-form" action="/event/<?php echo $eventData['event_code']; ?>#commentsdiv" method="POST">
					<div class="row">	
						<div class="col-md-12">
							<label>Your comment</label>
							<textarea id="comment_text" name="comment_text" style="margin-bottom: 5px;"></textarea>
						</div>							
					</div>
					<br />
					<?php if(isset($commentadded)) { ?>
					<br />
					<div class="alert alert-success" style=" clear: both;">
						<strong>Well done!</strong><br />Your message has been successfully added, it will first need to be vetted before it can be made public on this site. Please note that we may use your contact details to make sure all this information is correct.
					</div>
					<?php } ?>
					<?php if(isset($commentmessage)) { ?>
					<div class="alert alert-danger" style=" clear: both;">
						<strong>Oh snap!</strong><br /><?php echo $commentmessage; ?>
					</div>
					<?php } ?>						
					<button type="submit" value="1">Comment</button>
				</form>
				<hr />
				<?php if($commentData) { ?>
				<p class="text_brown">Below is a list of ratings that have been done <?php echo $eventData['event_name']; ?>.</p>
				<br />
				<div class="comments-list margin-bottom-20">
					<?php 
						for($i = 0; $i < count($commentData); $i++) { 
							$comment = $commentData[$i];
					?>
					<div class="comment-content">
						<img src="https://graph.facebook.com/<?php echo $comment['social_facebook_id']; ?>/picture?width=49" />
						<h5><b><?php echo $comment['comment_name']; ?></b></h5>
						<p><?php echo $comment['comment_text']; ?></p>
					</div>
					<?php } ?>			
				</div>
				<?php } else { ?>
				<div class="alert alert-danger">
					<strong>Oh snap!</strong><br />No ratings have been done for this event yet.
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
<script type="text/javascript" language="javascript">
jQuery(document).ready(function() {

	jQuery("#mygallery").justifiedGallery({
		rowHeight : 150,
		captions : false, 
		margins : 6
	});

	$('.popup-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] 
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function(item) {
			return item.el.attr('title') + '<small>We are the change we have been waiting for.</small>';
			}
		}
	});		
});
</script>
</body>
</html>
