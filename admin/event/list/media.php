<?php

ini_set('memory_limit','365M');
ini_set('max_execution_time', '600');

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/**
 * Standard includes
 */
require_once 'config/database.php';
require_once 'config/smarty.php';

require_once 'includes/auth.php';

/* objects. */
require_once 'class/event.php';
require_once 'class/media.php';
require_once 'class/File.php';

$eventObject		= new class_event();
$mediaObject 	= new class_media();
$fileObject 		= new File(array('png', 'jpg', 'jpeg'));

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code 		= trim($_GET['code']);
	
	$eventData = $eventObject->getByCode($code);	

	if($eventData) {
		
		$smarty->assign('eventData', $eventData);

		$mediaData = $mediaObject->getByReference(array('IMAGE'), 'EVENT', $code);

		if($mediaData) {
			$smarty->assign('mediaData', $mediaData);
		}
	} else {
		header('Location: /event/list/');
		exit;	
	}
} else {
	header('Location: /event/list/');
	exit;	 
}

/* Check posted data. */
if(isset($_GET['delete_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true; 
	$success					= NULL;
	$itemcode					= trim($_GET['delete_code']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {	
		$data	= array();
		$data['media_deleted'] = 1; 
		
		$where		= array();
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_code = ?', $itemcode);
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_item_code = ?', $eventData['event_code']);
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_item_type = ?', 'EVENT');
		
		$success	= $mediaObject->update($data, $where);	
		
		if(is_numeric($success) && $success > 0) {		
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	
	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(isset($_GET['update_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 1;	
	$formValid				= true;
	$success					= NULL;
	$itemcode					= trim($_GET['update_code']);
	
	if(isset($_GET['media_description']) && trim($_GET['media_description']) == '') {
		$errorArray['error']	= 'Please add a description.';
		$errorArray['result']	= 0;		
	}

	if($errorArray['error']  == '' && $errorArray['result']  == 1 ) {

		$data	= array();
		$data['media_description']	= trim($_GET['media_description']);

		$where		= array();
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_code = ?', $itemcode);
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_item_code = ?', $eventData['event_code']);

		$success	= $mediaObject->update($data, $where);	

		if(is_numeric($success) && $success > 0) {
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}

	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(isset($_GET['status_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;
	$data 						= array();
	$formValid				= true;
	$success					= NULL;
	$itemcode					= trim($_GET['status_code']);
	
	if($errorArray['error']  == '') {
		
		$itemData = $mediaData = $mediaObject->getCode($itemcode);
		
		if($itemData) {
			$success = $mediaObject->updatePrimary($itemData['media_category'], $itemData['media_item_type'], $eventData['event_code'], $itemcode);			
		}
		
		if(is_numeric($success)) {		
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not update, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	
	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(count($_FILES) > 0) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;

	if(isset($_POST['mediadescription']) && trim($_POST['mediadescription']) == '') {
		$errorArray['mediadescription'] = 'Media description is required';
		$formValid = false;
	}

	if(isset($_FILES['mediafiles']['name']) && count($_FILES['mediafiles']['name']) > 0) {
		for($i = 0; $i < count($_FILES['mediafiles']['name']); $i++) {
			/* Check validity of the CV. */
			if((int)$_FILES['mediafiles']['size'][$i] != 0 && trim($_FILES['mediafiles']['name'][$i]) != '') {
				/* Check if its the right file. */
				$ext = $fileObject->file_extention($_FILES['mediafiles']['name'][$i]); 

				if($ext != '') {
					$checkExt = $fileObject->getValidateExtention('mediafiles', $ext, $i);

					if(!$checkExt) {
						$errorArray['mediafiles'] = 'Invalid file type something funny with the file format';
						$formValid = false;						
					} else if($_FILES['mediafiles']['size'][$i] > 1048576) {
						$errorArray['mediafiles'] = '<b>'.$_FILES['mediafiles']['name'][$i].'</b> is more than 1MB big.';
						$formValid = false;	
					}
				} else {
					$errorArray['mediafiles'] = 'Invalid file type';
					$formValid = false;									
				}
			} else {			
				switch((int)$_FILES['mediafiles']['error'][$i]) {
					case 1 : $errorArray['mediafiles'] = 'The uploaded file exceeds the maximum upload file size, should be less than 1M'; $formValid = false; break;
					case 2 : $errorArray['mediafiles'] = 'File size exceeds the maximum file size'; $formValid = false; break;
					case 3 : $errorArray['mediafiles'] = 'File was only partically uploaded, please try again'; $formValid = false; break;
					case 4 : $errorArray['mediafiles'] = 'No file was uploaded'; $formValid = false; break;
					case 6 : $errorArray['mediafiles'] = 'Missing a temporary folder'; $formValid = false; break;
					case 7 : $errorArray['mediafiles'] = 'Faild to write file to disk'; $formValid = false; break;
				}
			}
		}
	} else {
		$errorArray['mediafiles'] = 'No file was uploaded';
		$formValid = false;									
	}

	if(count($errorArray) == 0 && $formValid == true) {
		/* Add file if there are any. */
		if(isset($_FILES['mediafiles']) && count($_FILES['mediafiles']['name']) > 0) {
			for($i = 0; $i < count($_FILES['mediafiles']['name']); $i++) {
			
				if(isset($_FILES['mediafiles']['size'][$i])) {
				
					if((int)$_FILES['mediafiles']['size'][$i] != 0 && trim($_FILES['mediafiles']['name'][$i]) != '') {

						$data = array();
						$data['media_code']			= $mediaObject->createCode();
						$data['media_item_code']	= $eventData['event_code'];
						$data['media_item_type']	= 'EVENT';
						$data['media_description']	= trim($_POST['mediadescription']);
						$data['media_category']		= 'IMAGE';

						$ext 			= strtolower($fileObject->file_extention($_FILES['mediafiles']['name'][$i]));					
						$filename	= $data['media_code'].'.'.$ext;
						
						$directory	= $globalPath.'/media/event/'.$data['media_code'];

						$file			= $directory.'/'.$filename;

						if(!is_dir($directory)) mkdir($directory, 0777, true);

						/* Create files for this product type. */
						foreach($fileObject->image as $item) {
							/* Change file name. */
							$newfilename = str_replace($filename, $item['code'].$filename, $file);
							/* Resize image. */
							$fileObject->resize_crop_image($item['width'], $item['height'], $_FILES['mediafiles']['tmp_name'][$i], $newfilename);
						}						

						$data['media_path']		= '/media/event/'.$data['media_code'].'/';
						$data['media_filename']	= trim($_FILES['mediafiles']['name'][$i]);
						$data['media_ext']			= '.'.$ext ;

						/* Check for other medias. */
						$primary = $mediaObject->getPrimary('IMAGE', 'EVENT', $eventData['event_code']);		
						
						if($primary) {
							$data['media_primary']	= 0;
						} else {
							$data['media_primary']	= 1;
						}

						$success	= $mediaObject->insert($data);
					}
				}
			}
		}
	
		header('Location: /event/list/media.php?code='.$eventData['event_code']);
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);
}

/* Display the template */	
$smarty->display('event/list/media.tpl');
$mediaObject = $errorArray = $data = $eventData = $primary = $ext = $newfilename = $uploadObject = $item = $i = $filename = $file = $directory = $fileObject = $formValid = $checkExt = null;
unset($mediaObject, $errorArray, $data, $eventData, $primary, $ext, $newfilename, $uploadObject, $item, $i, $filename, $file, $directory, $fileObject, $formValid, $checkExt);
?>