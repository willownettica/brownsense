<?php

require_once 'class/market.php';

//custom account item class as account table abstraction
class class_branch extends Zend_Db_Table_Abstract
{
   //declare table variables
    protected $_name 		= 'branch';
	protected $_primary 	= 'branch_code';
	public $_market			= null;
	
	function init()	{
		global $zfsession;
		$this->_member	= $zfsession->identity;
		$this->_market	= new class_market();
	}

	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 
	public function insert(array $data) {
        // add a timestamp
        $data['branch_added'] = date('Y-m-d H:i:s');        
		$data['branch_code']	= $this->createCode();
		
		return parent::insert($data);
    }
	
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where)
    {
        // add a timestamp
        $data['branch_updated'] = date('Y-m-d H:i:s');
        
        return parent::update($data, $where);
    }

	/**
	 * get job by job branch Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code) {	
		$select = $this->_db->select()	
					->from(array('branch' => 'branch'))	
					->joinLeft(array('parent' => 'branch'), 'parent.branch_code = branch.parent_code and parent.branch_deleted = 0', array("parent.branch_name as parent_name", 'parent.branch_code as parent_code'))
					->where('branch.branch_deleted = 0')
					->where('branch.branch_code = ?', $code)
					->limit(1);
       
	   $result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;

	}

	/**
	 * get job by job branch Id
 	 * @param string job id
     * @return object
	 */
	public function getAll() {	
		$select = $this->_db->select()	
					->from(array('branch' => 'branch'))	
					->joinLeft(array('parent' => 'branch'), 'parent.branch_code = branch.parent_code and parent.branch_deleted = 0', array("parent.branch_name as parent_name", 'parent.branch_code as parent_code'))
					->where('branch.branch_deleted = 0');

	   $result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get job by job branch Id
 	 * @param string job id
     * @return object
	 */
	public function getByLevels($level) {
		$select = $this->_db->select()
			->from(array('branch' => 'branch'), array('branch_code', 'branch_name'))
			->joinLeft(array('parent' => 'branch'), 'parent.branch_code = branch.parent_code and parent.branch_deleted = 0', array("parent.branch_name as parent_name", 'parent.branch_code as parent_code'), array())
			->where("branch.branch_level = ?", $level)
			->where('branch.branch_deleted = 0');

		$result = $this->_db->fetchAll($select);
		return ($result == false) ? false : $result = $result;
	}
	
	public function pairs(array $level, $code = null) {
		if($code == null) {
			$select = $this->select()
					->from(array('branch' => 'branch'), array('branch.branch_code', "branch_name"))
					->where("branch_deleted = 0 and branch_active = 1")
					->where("branch_level in(".implode(',',$level).")")
					->order('branch.branch_name asc');
		} else {
			$select = $this->select()
					->from(array('branch' => 'branch'), array('branch.branch_code', "branch_name"))
					->where("branch_deleted = 0 and branch_active = 1")
					->where('branch_code != ?', $code)
					->where("branch_level in(".implode(',',$level).")")
					->order('branch.branch_name asc');
		}

		$result = $this->_db->fetchPairs($select);
		return ($result == false) ? false : $result = $result;
	}

	function table($level, $columns = 3){
		
		$array = $this->getByLevels($level);
		$marketData = $this->_market->getVendor();
		// start table
		$html = '<table width="100%">';

		$html .= '<tr>';
		foreach( $array as $key=>$value){
			if((($key % $columns) == 0) && $key != 0 && $key != 1) {
				$html .= '</tr><tr>';
			}
			$html .= '<td><input type="checkbox" value="'.$value['branch_code'].'" class="market_branch_code" name="market_branch_code[]" id="market_branch_code[]" '.(in_array($value['branch_code'], $marketData) ? 'checked' : '').' />&nbsp;&nbsp;&nbsp;' . $value['branch_name'] . '</td>';
		}
		$html .= '<tr>';
		// finish table and return it
		$html .= '</table>';
		return $html;
	}

	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getCode($reference)
	{
		$select = $this->_db->select()	
				->from(array('branch' => 'branch'))	
				->where('branch_code = ?', $reference)
				->limit(1);

	   $result = $this->_db->fetchRow($select);	
        return ($result == false) ? false : $result = $result;					   
	}

	function createCode() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '1234567890';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<1;$i++){
			$code .= $Number[rand(0,$count)];
		}
		
		/* Last alphabet. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Number[rand(0,$count)];
		}	
		
		/* First check if it exists or not. */
		$itemCheck = $this->getCode($code);
		
		if($itemCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}	

	public function validateEmail($string) {
		if(!filter_var($string, FILTER_VALIDATE_EMAIL)) {
			return '';
		} else {
			return trim($string);
		}
	}
	
	public function validateNumber($string) {
		if(preg_match('/^0[0-9]{9}$/', $this->onlyContactNumber(trim($string)))) {
			return $this->onlyContactNumber(trim($string));
		} else {
			return '';
		}
	}
	
	public function onlyContactNumber($string) {
		/* Remove some weird charactors that windows dont like. */
		$string = strtolower($string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("�", "", $string);	
		$string = str_replace('___' , '' , $string);
		$string = str_replace('__' , '' , $string);	
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("�", "", $string);	
		$string = str_replace("–", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("#", "", $string);	
		$string = str_replace("$", "", $string);	
		$string = str_replace("@", "", $string);	
		$string = str_replace("!", "", $string);	
		$string = str_replace("&", "", $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(':' , '' , $string);		
		$string = str_replace('[' , '' , $string);		
		$string = str_replace(']' , '' , $string);		
		$string = str_replace('|' , '' , $string);		
		$string = str_replace('\\' , '' , $string);		
		$string = str_replace('%' , '' , $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);	
		$string = str_replace('-' , '' , $string);	
		$string = str_replace('+27' , '0' , $string);	
		$string = str_replace('(0)' , '' , $string);	
		
		$string = preg_replace('/^00/', '0', $string);
		$string = preg_replace('/^27/', '0', $string);
		
		$string = preg_replace('!\s+!',"", strip_tags($string));
		
		return $string;
				
	}	
}
?>