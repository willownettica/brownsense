<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/*** Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/**
 * Check for login
 */
require_once 'includes/auth.php';

require_once 'class/comment.php';
require_once 'class/vet.php';
require_once 'class/item.php';

$commentObject	= new class_comment();
$vetObject 			= new class_vet();
$itemObject 			= new class_item();

if(isset($_GET['vetcomment'])) {

	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 1;	

	$commentcode	= trim($_GET['commentcode']);
	$itemcode			= trim($_GET['itemcode']);

	if($commentcode == '') {
		$errorArray['error']	= 'Add a comment.';
		$errorArray['result']	= 0;
	}
	
	if($itemcode == '') {
		$errorArray['error']	= 'Select a comment';
		$errorArray['result']	= 0;
	}
	
	if($errorArray['error']  == '' && $errorArray['result']  == 1) {

		$data 							= array();				
		$data['item_code']			= $itemcode;
		$data['vet_reason']		= '';
		$data['vet_item_type']	= 'COMMENT';
		$data['vet_item_code']	= $commentcode;

		$success = $vetObject->insert($data);

		if($success) {		
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}

	echo json_encode($errorArray);
	exit;
}

if(isset($_GET['action']) && trim($_GET['action']) == 'search') {

	$filter	= array();
	$csv		= 0;
	$start 	= isset($_REQUEST['iDisplayStart']) ? $_REQUEST['iDisplayStart'] : 0;
	$length	= isset($_REQUEST['iDisplayLength']) ? $_REQUEST['iDisplayLength'] : 20;
	
	if(isset($_REQUEST['filter_search']) && trim($_REQUEST['filter_search']) != '') $filter[] = array('filter_search' => trim($_REQUEST['filter_search']));
	if(isset($_REQUEST['filter_item']) && trim($_REQUEST['filter_item']) != '') $filter[] = array('filter_item' => trim($_REQUEST['filter_item']));
	if(isset($_REQUEST['filter_csv']) && trim($_REQUEST['filter_csv']) != '') { $filter[] = array('filter_csv' => (int)trim($_REQUEST['filter_csv'])); $csv = (int)trim($_REQUEST['filter_csv']); }
	
	$comments 	= array();
	
	$commentData = $commentObject->getSearch(array('EVENT', 'GALLERY'), $start, $length, $filter);

	if(!$csv) {
		if($commentData) {
			for($i = 0; $i < count($commentData['records']); $i++) {
				$item = $commentData['records'][$i];
				$comments[$i] = array(
					($item['social_facebook_id'] == '' ? '<img src="/images/no-image.jpg" width="110px" />' : '<a href="http://facebook.com/'.$item['social_facebook_id'].'" target="_blank"><img src="https://graph.facebook.com/'.$item['social_facebook_id'].'/picture?width=300" width="150px" /></a>'),
					('<b>'.$item['comment_item_type'].'</b><br /><br /><span style="font-weight: bold; color: '.$item['item_config_color'].'" id="comment_'.$item['comment_code'].'">'.$item['comment_text'].'</span>'),
					($item['item_name'] != '' ? '<span style="color: '.$item['item_config_color'].'">'.$item['item_name'].'</span>' : 'Not vetted'),
					'<button value="Vet" class="btn btn-info" onclick="vetCommentModal(\''.$item['comment_code'].'\', \''.$item['comment_item_type'].'\'); return false;">Vet</button>'
				);
			}
		}

		if($commentData) {
			$response['sEcho'] = $_REQUEST['sEcho'];
			$response['iTotalRecords'] = $commentData['displayrecords'];		
			$response['iTotalDisplayRecords'] = $commentData['count'];
			$response['aaData']	= $comments;
		} else {
			$response['result'] 	= false;
			$response['message']	= 'There are no items to show.';			
		}

		echo json_encode($response);
		die();
	} else {
		$row = "Date, Name, Email, Vetted Result, Type, Message\r\n";
		if($commentData) {
			for($i = 0; $i < count($commentData); $i++) {
				$item = $commentData[$i];
				$comments[$i] = array(
						$item['comment_added'],
						preg_replace( "/\r|\n/", "", str_replace(',', '', $item['comment_name'])),
						preg_replace( "/\r|\n/", "", str_replace(',', '', $item['comment_email'])),
						preg_replace( "/\r|\n/", "", str_replace(',', '', $item['item_name'])), 
						$item['comment_item_type'],
						preg_replace( "/\r|\n/", "", str_replace(',', '', $item['comment_text']))
				);
			}

			foreach ($comments as $data) {
				$row .= implode(', ', $data)."\r\n";
			}			
		}

		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=comments_report_".date('Y-m-d').".csv");
		Header("Cache-Control: private, must-revalidate" ); // HTTP/1.1
		Header("Pragma: private" ); // HTTP/1.0
	   
		print($row);
		exit;
	}
}

if(isset($_GET['itemcode'])) {

	$itemData = $itemObject->selectParents('VET', 'COMMENT');

	$html = "<label for='item_code'>Vet</label><select  class='form-control' id='item_code' name='item_code'>";
	while ($value = current($itemData)) {
		$html .= '<option value="'.key($itemData).'">'.$value.'</option>';
		next($itemData);
	}
	$html .= '</select>';
	echo $html;
	exit;
}

$itemData = $itemObject->selectParents('VET', 'COMMENT');
if($itemData) $smarty->assign('itemData', $itemData);

$smarty->display('comment/default.tpl');
?>