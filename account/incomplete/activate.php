<?php 
/* Add this on all pages on top. */
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/iauth.php';

/* Check if the account has been completed, active and approved by administrators. */
if((int)trim($zfsession->memberData['member_active']) == 1) {
	/* Completed but not activated. */
	header('Location: /account/incomplete/status.php');
	exit;
}

/* objects. */
require_once 'class/member.php';

$memberObject = new class_member();

/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray	= array();
	$formValid	= true;
	$success		= NULL;

	if(isset($_POST['member_email']) && trim($_POST['member_email']) != '') {
		if($memberObject->validateEmail(trim($_POST['member_email'])) == '') {
			$errorArray[] = 'A valid email address is required';
			$formValid = false;				
		} else {
			$temp = isset($zfsession->memberData) ? $zfsession->memberData['member_code'] : null;
			
			$tempData = $memberObject->getByEmail(trim($_POST['member_email']), $temp);
			
			if($tempData) {
				$errorArray[] = 'Selected email address is already being used';
				$formValid = false;
			}
		}
	} else {
		$errorArray[] = 'Your email address is required';
		$formValid = false;		
	}
	
	if(count($errorArray) == 0 && $formValid == true) {
		/* Email needs to be activated, since its new or changed. */
		$data = array();
		$data['member_active'] 		= 0;
		$data['member_hashcode']	= md5(trim($_POST['member_email']).date('Y-m-d h:i:s'));
		
		$where		= $memberObject->getAdapter()->quoteInto('member_code = ?', $zfsession->memberData['member_code']);
		$success	= $memberObject->update($data, $where);
			
		$message = 'We have sent you an email to <b>'.trim($_POST['member_email']).'</b>, in order to activate your account, and proof this is your email address.<br />';
		
		$mailData = $memberObject->getByCode($zfsession->memberData['member_code']);
		
		if($mailData) {
			$templateData = $memberObject->_comm->_template->getTemplate('REGISTRATION_ACTIVATE', 'EMAIL');

			if($templateData) {

				$recipient	= array();
				$recipient['recipient_code'] 		= $mailData['member_code'];
				$recipient['recipient_name'] 		= $mailData['member_name'];
				$recipient['recipient_surname'] 	= $mailData['member_surname'];
				$recipient['recipient_cellphone'] 	= $mailData['member_cellphone'];
				$recipient['recipient_type'] 			= 'MEMBER';
				$recipient['recipient_email'] 		= $mailData['member_email'];
				$recipient['recipient_password'] 	= $mailData['member_password'];
				$recipient['recipient_hashcode'] 	= $mailData['member_hashcode'];

				$memberObject->_comm->sendEmail($recipient, $templateData);

			}
		}
	}
}

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<link rel="stylesheet" href="/css/jquery-ui-1.12.1.css">
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/account/">Account</a></li>
						<li><a href="#"><?php echo $zfsession->memberData['member_name']; ?></a></li>
						<li><a href="/account/incomplete/">Incomplete Profile</a></li>
						<li><a href="#">Activate your account</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8">		
				<form class="post-comment-form"action="/account/incomplete/activate.php" method="POST" >	
						<div class="alert alert-warning reg_member_fail" style="">
							<strong>Notice!</strong>
							<br>
							<p>If you have not received your activation email, please make sure you gave us the correct email address to send to, please check below to confirm so:						
							</p>
						</div>				
					<?php 
						if(isset($errorArray) && count($errorArray) != 0) {
						?>
						<div class="alert alert-danger reg_member_fail" style="">
							<strong>Oh snap! There were some issue with your submission, please see below:</strong>
							<br>
							<p>
							<?php
							for($i = 0; $i < count($errorArray); $i++) {
							 echo $errorArray[$i].'<br />'; 
							} 
							 ?>								
							</p>
						</div>
						<?php
						}
						if(isset($message)) {
						?>
						<div class="alert alert-success reg_member_fail" style="">
							<strong>Success!</strong>
							<br>
							<p>
							<?php echo $message; ?>
							</p>
						</div>							
						<?php
						}
					?>
					<div class="row">
						<div class="col-md-12">
							<label>Your Current Email</label>
							<input type="text" id="member_email" name="member_email" value="<?php echo $zfsession->memberData['member_email']; ?>" />
						</div>
					</div>		
					<br />
					<button type="submit">Resend Activation Email</button>
					<br />
				</form>			
			</div>
			<aside class="col-md-4 col-sm-4">
			
			<div class="side-widget margin-bottom-30">
				<h3 class="heading-1"><span>Facebook profile picture</span></h3>
				<img src="https://graph.facebook.com/<?php echo $zfsession->memberData['social_facebook_id']; ?>/picture?width=300">
				</div>
			</aside>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>

<?php require_once 'includes/javascript.php'; ?>
</body>
</html>
