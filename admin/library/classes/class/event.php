<?php

//custom account item class as account table abstraction
class class_event extends Zend_Db_Table_Abstract
{
   //declare table variables
    protected $_name 	= 'event';
	public $_primary		= 'event_code';
	
	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 
	 public function insert(array $data)
    {
        // add a timestamp
        $data['event_added'] 	= date('Y-m-d H:i:s');
		$data['event_code'] 	= $this->createCode();
		
		return parent::insert($data);
    }
	
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where)
    {
        // add a timestamp        
		$data['event_updated'] = date('Y-m-d H:i:s');
		
        return parent::update($data, $where);
    }

	/**
	 * get job by job event Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code)
	{		
		$select = $this->_db->select()	
					->from(array('event' => 'event'))
					->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = event.areapost_code')
					->joinLeft(array('branch' => 'branch'), 'branch.branch_code = event.branch_code')
					->where('event_deleted = 0 and branch_deleted = 0')
					->where('event.event_code = ?', $code)
					->limit(1);

		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}
	
	public function getVendorHistory($branch, $company, $limit = 10) {
		$select = $this->_db->select()
					->from(array('event' => 'event'), array('event_code', 'event_name'))
					->joinInner(array('branch' => 'branch'), 'branch.branch_code = event.branch_code', array())
					->joinLeft(array('market' => 'market'), "market.branch_code = event.branch_code and company_code = '$company' and period_code = date_format(event_date_start, '%m-%Y') and market_deleted = 0", array('market_code'))
					->joinLeft(array('link' => 'link'), "link.link_parent_type = 'MARKET' and link.link_parent_code = market_code and link_child_type = 'EVENT' and link_child_code = event_code", array())
					->joinLeft(array('vet' => 'vet'), "vet.vet_item_type = 'LINK' and vet_item_code = link_code and vet_active = 1 and vet_deleted = 0", array('vet_reason'))
					->joinLeft(array('item' => 'item'), "item.item_code = vet.item_code and item_deleted = 0", array('item_name', 'item_config_status', 'item_config_class', 'item_config_color'))
					->where('event_deleted = 0 and branch_deleted = 0')
					->where('event.branch_code = ?', $branch)
					->order('event_date_start desc')
					->limit($limit);

		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}
	/**
	 * get job by job event Id
 	 * @param string job id
     * @return object
	 */
	public function getAll() {
		$select = $this->_db->select()	
					->from(array('event' => 'event'))
					->joinLeft(array('branch' => 'branch'), 'branch.branch_code = event.branch_code')
					->where('event_deleted = 0 and branch_deleted = 0')
					->order('event_date_start desc');

		$result = $this->_db->fetchAll($select);
		return ($result == false) ? false : $result = $result;
	}
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getCode($code)
	{
		$select = $this->_db->select()	
					->from(array('event' => 'event'))	
					   ->where('event_code = ?', $code)
					   ->limit(1);

	   $result = $this->_db->fetchRow($select);	
        return ($result == false) ? false : $result = $result;					   
	}
	
	function createCode() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '1234567890';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<1;$i++){
			$code .= $Number[rand(0,$count)];
		}
		
		/* Last alphabet. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Number[rand(0,$count)];
		}	
		
		/* First check if it exists or not. */
		$itemCheck = $this->getCode($code);
		
		if($itemCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}
	
    function sanitize(&$string) { $string = preg_replace("/[^a-zA-Z0-9_]+/", "", $string);}
	
    function sanitizeArray(&$array) {
		for($i = 0; $i < count($array); $i++) {
			$array[$i] = preg_replace("/[^a-zA-Z0-9_]+/", "", $array[$i]);
		}
    } 	
}
?>