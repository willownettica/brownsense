<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/**
 * Standard includes
 */
require_once 'config/database.php';
require_once 'config/smarty.php';

require_once 'includes/auth.php';

/* objects. */
require_once 'class/member.php';
require_once 'class/_comm.php';

$memberObject	= new class_member();
$commObject 	= new class_comm();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$memberData = $memberObject->getByCode($code);

	if($memberData) {
		$smarty->assign('memberData', $memberData);

		$commData = $commObject->getByRecipient('MEMBER', $code);

		if($commData) {
			$smarty->assign('commData', $commData);
		}	

	} else {
		header('Location: /member/');
		exit;	
	}
} else {
	header('Location: /member/');
	exit;	
}

/* Display the template */	
$smarty->display('member/comm.tpl');
?>