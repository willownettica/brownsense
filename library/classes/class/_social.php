<?php

require_once 'class/media.php';

//custom account item class as account table abstraction
class class_social extends Zend_Db_Table_Abstract
{
   //declare table variables
    protected $_name 			= '_social';
	protected $_primary 		= '_social_id';
	protected $_media			= null; 
	
	function init()	{
		$this->_media	= new class_media();
	}
	
	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 
	 public function insert(array $data)
    {
        // add a timestamp
        $data['_social_added'] = date('Y-m-d H:i:s');

		return parent::insert($data);
    }
	
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where)
    {
        // add a timestamp
        $data['_social_updated'] = date('Y-m-d H:i:s');
        
        return parent::update($data, $where);
    }
	
	/**
	 * get job by job _social Id
 	 * @param string job id
     * @return object
	 */
	public function getById($id) {	
		$select = $this->_db->select()	
					->from(array('_social' => '_social'))
					->where('_social_deleted = 0')
					->where('_social_id = ?', $id)
					->limit(1);
       
		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}
	
	public function getSearch($start, $length, $search = '')
	{
		if($search == '') {
			$select = $this->_db->select()	
					->from(array('_social' => '_social'))	
					->joinLeft(array('media' => 'media'), "media.media_category = 'IMAGE' and media.media_item_type = 'SOCIAL' and media.media_primary = 1 and media.media_item_code = _social._social_id", array('media_code', 'media_path', 'media_ext'))
					->where('_social._social_deleted = 0')
					->order('_social._social_added desc');
		} else {
			$select = $this->_db->select()	
				->from(array('_social' => '_social'))	
				->joinLeft(array('media' => 'media'), "media.media_category = 'IMAGE' and media.media_item_type = 'SOCIAL' and media.media_primary = 1 and media.media_item_code = _social._social_id", array('media_code', 'media_path', 'media_ext'))
				->where('_social._social_deleted = 0')
				->where("lower(_social_message) like lower(?)", "%$search%")
				->order("LOCATE('$search', _social_message)");			
		}

		$result_count = $this->_db->fetchRow("select count(*) as query_count from ($select) as query");

		$result = $this->_db->fetchAll($select . " limit $start, $length");
		
		if($result) {
			for($i = 0; $i < count($result); $i++) {
				if($result[$i]['_social_item_type'] != 'IMAGE') {
					$temp = $this->_media->getByCode($result[$i]['_social_item_code']);
					if($temp) {
						$result[$i] = array_merge($result[$i], $temp);
					}
				}
			}
		}

		return ($result === false) ? false : $result = array('count'=>$result_count['query_count'],'displayrecords'=>count($result),'records'=>$result);	
	}
	
	public function post($limit = 10)
	{

			$select = $this->_db->select()	
					->from(array('_social' => '_social'))	
					->joinLeft(array('media' => 'media'), "media.media_category = 'IMAGE' and media.media_code = _social._social_item_code", array('media_code', 'media_path', 'media_ext'))
					->where("_social._social_deleted = 0 and (_social._social_sent_result is null or _social._social_sent_result = '')")
					->order('_social._social_added desc');

		$result = $this->_db->fetchAll($select);
		
		if($result) {
			for($i = 0; $i < count($result); $i++) {
				if($result[$i]['_social_item_type'] != 'IMAGE') {
					$temp = $this->_media->getByCode($result[$i]['_social_item_code']);
					if($temp) {
						$result[$i] = array_merge($result[$i], $temp);
					}
				}
			}
		}

        return ($result == false) ? false : $result = $result;
	}
	
	function createBit($longurl){

		$url = "http://api.bit.ly/shorten?version=2.0.1&longUrl=$longurl&login=willownettica&apiKey=R_7e4f545822114a9d9e6ace21904c57e1&format=json&history=1"; 
		
		$s = curl_init();  
		curl_setopt($s,CURLOPT_URL, $url);  
		curl_setopt($s,CURLOPT_HEADER,false);  
		curl_setopt($s,CURLOPT_RETURNTRANSFER,1);  
		$result = curl_exec($s);  
		curl_close( $s );  

		$obj = json_decode($result, true); 

		return $obj["results"]["$longurl"]["shortUrl"];  
	}
	
	public function random($limit = 10)
	{

			$select = $this->_db->select()	
					->from(array('_social' => '_social'))	
					->where("_social._social_deleted = 0 and (_social._social_sent_result is not null or _social._social_sent_result != '')")
					->order('RAND()');

		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}
	
}
?>