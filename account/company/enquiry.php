<?php 
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/auth.php'; 
/* objects. */
require_once 'class/company.php';
require_once 'class/enquiry.php';

$enquiryObject	= new class_enquiry();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$companyData = $companyObject->getByMember($zfsession->identity, $code);

	if(!$companyData) {
		header('Location: /account/company/');
		exit;
	}
} else {
	header('Location: /account/company/');
	exit;
}

$enquiryData	= $enquiryObject->getByTypeCode('COMPANY', $companyData['company_code']);

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<link rel="stylesheet" href="/css/jquery-ui-1.12.1.css">
	<link href="/library/javascript/tagsinput/bootstrap-tagsinput.css" rel="stylesheet" />
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/account/"><?php echo $zfsession->memberData['member_name']; ?></a></li>
						<li><a href="/account/company/">My Companies</a></li>
						<li><a href="/account/company/details.php?code<?php echo $companyData['company_code']; ?>"><?php echo $companyData['company_name']; ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">			
		<div class="row">
			<div class="col-md-12 col-sm-8">
				<h3 class="heading-1"><span><?php echo $companyData['company_name']; ?></span></h3>	
				<ul class="nav nav-tabs">
					<li><a href="/account/company/details.php?code=<?php echo $companyData['company_code']; ?>">Details</a></li>
					<li><a href="/account/company/tags.php?code=<?php echo $companyData['company_code']; ?>">Products / Services</a></li>
					<li><a href="/account/company/media.php?code=<?php echo $companyData['company_code']; ?>">Images</a></li>
					<li><a href="/account/company/document.php?code=<?php echo $companyData['company_code']; ?>">Documents</a></li>
					<li><a href="/account/company/vendor.php?code=<?php echo $companyData['company_code']; ?>">Become a vendor</a></li>
					<li class="active"><a href="/account/company/enquiry.php?code=<?php echo $companyData['company_code']; ?>">Enquiries</a></li>
				</ul>
				<p style="clear: both;">&nbsp;&nbsp;&nbsp;&nbsp;</p>			
				<p>Below is a list of enquiries that have been sent to the company <b><?php echo $companyData['company_name']; ?></b>.</p><br />
				<form method="post" action="/account/" enctype="multipart/form-data">
					<div class="row clearfix">
						<!--Form Group-->
						<div class="form-group col-md-12 col-xs-12">
							<div class="text-right">								
									<table class="table table-striped table-bordered" >
										<thead>
											<tr>
												<th>Date sent</th>
												<th>Member</th>
												<th>Message</th>
											</tr>
										</thead>							
									   <tbody>
									   <?php 
											if($enquiryData) {
												foreach($enquiryData as $item) { 
									   ?>
									  <tr>
										<td align="left" width="10%"><?php echo date('Y-m-d', strtotime($item['enquiry_added'])); ?></td>
										<td align="left" width="20%"><?php echo $item['enquiry_name']; ?><br /><?php echo $item['enquiry_email']; ?></td>
										<td align="left"><?php echo $item['enquiry_message']; ?></td>
									  </tr>
									  <?php 
												}
											} else {
									  ?>
										<tr><td colspan="3">No services or products have been added yet</td></tr>
									  <?php } ?>
									  </tbody>			  
									</table> 										
							</div>	
							<br />									
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
</body>
</html>
