<?php

require_once '_comm.php';

//custom account enquiry class as account table abstraction
class class_enquiry extends Zend_Db_Table_Abstract
{
   //declare table variables
    protected $_name 			= 'enquiry';
	protected $_primary 		= 'enquiry_code';
	public 		 $_comm			= null;

	function init()	{
		$this->_comm	= new class_comm();
	}
	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 
	 public function insert(array $data)
    {
        // add a timestamp
        $data['enquiry_added'] 		= date('Y-m-d H:i:s');        
		$data['enquiry_code']			= $this->createCode();

		$success = parent::insert($data);	

		if($success) {

			$mailData = $this->getByCode($success);

			if($mailData) {
					
					if($mailData['enquiry_item_type'] == 'COMPANY') {
						$templateData = $this->_comm->_template->getTemplate('COMPANY_ENQUIRY', 'EMAIL');

						if($templateData) {

							$recipient	= array();
							$recipient['recipient_code'] 		= $mailData['enquiry_code'];
							$recipient['recipient_name'] 		= $mailData['company_contact_name'];
							$recipient['recipient_type'] 			= 'ENQUIRY';
							$recipient['recipient_email'] 		= $mailData['company_contact_email'];
							$recipient['recipient_message'] 	= $mailData['enquiry_message'];

							$recipient['enquiry_name'] 		= $mailData['enquiry_name'];						
							$recipient['enquiry_email'] 			= $mailData['enquiry_email'];
							$recipient['enquiry_number'] 		= $mailData['enquiry_number'];

							$recipient['company_name']		= $mailData['company_name'];
							
							
							$this->_comm->sendEmail($recipient, $templateData, array(0 => array('name' => $mailData['company_contact_name'], 'email' => $mailData['company_contact_email']), 1 => array('name' => $mailData['enquiry_name'], 'email' => $mailData['enquiry_email'])));
						}
					} else {
						$templateData = $this->_comm->_template->getTemplate('ENQUIRY', 'EMAIL');

						if($templateData) {

							$recipient	= array();
							$recipient['recipient_code'] 		= $mailData['enquiry_code'];
							$recipient['recipient_name'] 		= $mailData['enquiry_name'];
							$recipient['recipient_type'] 			= 'ENQUIRY';
							$recipient['recipient_email'] 		= $mailData['enquiry_email'];
							$recipient['recipient_message'] 	= $mailData['enquiry_message'];

							$recipient['enquiry_name'] 		= $mailData['enquiry_name'];						
							$recipient['enquiry_email'] 			= $mailData['enquiry_email'];
							$recipient['enquiry_number'] 		= $mailData['enquiry_number'];

							$this->_comm->sendEmail($recipient, $templateData, array(0 => array('name' => $mailData['enquiry_name'], 'email' => $mailData['enquiry_email']), 1 => array('name' => 'Information Officer', 'email' => 'info@brownsense.co.za')));
						}
					}
			} else {
				return false;
			}
			
			return $success;
		} else {
			return false;
		}
    }
	
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where)
    {
        // add a timestamp
        $data['enquiry_updated'] = date('Y-m-d H:i:s');
        
        return parent::update($data, $where);
    }

	/**
	 * get job by job enquiry Id
 	 * @param string job id
     * @return object
	 */
	public function getByTypeCode($type, $code) {
		
		$select = $this->_db->select()	
					->from(array('enquiry' => 'enquiry'))
					->joinLeft(array('company' => 'company'), 'company.company_code = enquiry.enquiry_item_code')
					->where('enquiry_item_type = ?', $type)
					->where('enquiry_item_code = ?', $code)
					->where('enquiry_deleted = 0 and company_deleted = 0')
					->order('enquiry_added desc');

		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get job by job enquiry Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code) {

		$select = $this->_db->select()	
					->from(array('enquiry' => 'enquiry'))
					->joinLeft(array('company' => 'company'), 'company.company_code = enquiry.enquiry_item_code and company_deleted = 0')
					->where('enquiry_code = ?', $code)
					->where('enquiry_deleted = 0');

		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}

	/**
	 * get job by job enquiry Id
 	 * @param string job id
     * @return object
	 */
	public function getCode($code) {

		$select = $this->_db->select()	
					->from(array('enquiry' => 'enquiry'))
					->where('enquiry_code = ?', $code);

		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}
	
	function createCode() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '1234567890';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Number[rand(0,$count)];
		}
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<4;$i++){
			$code .= $Number[rand(0,$count)];
		}		
		/* First check if it exists or not. */
		$enquiryCheck = $this->getCode($code);
		
		if($enquiryCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}
	
	public function validateEmail($string) {
		if(!filter_var($string, FILTER_VALIDATE_EMAIL)) {
			return '';
		} else {
			return trim($string);
		}
	}
	
	public function validateNumber($string) {
		if(preg_match('/^0[0-9]{9}$/', $this->onlyNumber(trim($string)))) {
			return $this->onlyNumber(trim($string));
		} else {
			return '';
		}
	}
	
	public function validateDate($string) {
		if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $string)) {
			if(date('Y-m-d', strtotime($string)) != $string) {
				return '';
			} else {
				return $string;
			}
		} else {
			return '';
		}
	}	
	
	public function onlyNumber($string) {
		/* Remove some weird charactors that windows dont like. */
		$string = strtolower($string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("�", "", $string);	
		$string = str_replace('___' , '' , $string);
		$string = str_replace('__' , '' , $string);	 
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("�", "", $string);	
		$string = str_replace("–", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("#", "", $string);	
		$string = str_replace("$", "", $string);	
		$string = str_replace("@", "", $string);	
		$string = str_replace("!", "", $string);	
		$string = str_replace("&", "", $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(':' , '' , $string);		
		$string = str_replace('[' , '' , $string);		
		$string = str_replace(']' , '' , $string);		
		$string = str_replace('|' , '' , $string);		
		$string = str_replace('\\' , '' , $string);		
		$string = str_replace('%' , '' , $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);	
		$string = str_replace('-' , '' , $string);	
		$string = str_replace('+27' , '0' , $string);	
		$string = str_replace('(0)' , '' , $string);	
		
		$string = preg_replace('/^00/', '0', $string);
		$string = preg_replace('/^27/', '0', $string);
		
		$string = preg_replace('!\s+!',"", strip_tags($string));
		
		return $string;
				
	}		
}
?>