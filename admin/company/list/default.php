<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/*** Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/**
 * Check for login
 */
require_once 'includes/auth.php';
require_once 'class/company.php';
require_once 'class/item.php';
 
$companyObject = new class_company();
$itemObject 		= new class_item();
 
 if(isset($_GET['delete_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success					= NULL;
	$code						= trim($_GET['delete_code']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {
		$data	= array();
		$data['company_deleted'] = 1;
		
		$where 	= array();
		$where[] 	= $companyObject->getAdapter()->quoteInto('company_code = ?', $code);
		$success	= $companyObject->update($data, $where);	
		
		if(is_numeric($success) && $success > 0) {
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	 
	echo json_encode($errorArray);
	exit;
}

/* Setup Pagination. */
if(isset($_GET['action']) && trim($_GET['action']) == 'search') {

	$filter	= array();
	$csv		= 0;
	$start 	= isset($_REQUEST['iDisplayStart']) ? $_REQUEST['iDisplayStart'] : 0;
	$length	= isset($_REQUEST['iDisplayLength']) ? $_REQUEST['iDisplayLength'] : 20;
	
	if(isset($_REQUEST['filter_search']) && trim($_REQUEST['filter_search']) != '') $filter[] = array('filter_search' => trim($_REQUEST['filter_search']));
	if(isset($_REQUEST['filter_item']) && trim($_REQUEST['filter_item']) != '') $filter[] = array('filter_item' => trim($_REQUEST['filter_item']));
	if(isset($_REQUEST['filter_csv']) && trim($_REQUEST['filter_csv']) != '') { $filter[] = array('filter_csv' => (int)trim($_REQUEST['filter_csv'])); $csv = (int)trim($_REQUEST['filter_csv']); }
	
	$companys 	= array();
	
	if(!$csv) {
		
		$companyData = $companyObject->getSearch($start, $length, $filter);

		if($companyData) {
			for($i = 0; $i < count($companyData['records']); $i++) {
				$item = $companyData['records'][$i];

				$companys[$i] = array(
					($item['item_name'] == '' ? '<span class="error">Not vetted</span>' : '<b style="color: '.$item['item_config_color'].';"><span alt="'.$item['vet_reason'].'" title="'.$item['vet_reason'].'">'.$item['item_name'].'</span></b>'),
					date('Y-m-d', strtotime($item['company_added'])),											
					'<a href="/company/list/details.php?code='.$item['company_code'].'" style="'.($item['item_config_color'] == '' ? 'error' : 'color: '.$item['item_config_color']).'">'.$item['company_name'].'</a>',
					$item['company_services'],
					'<button value="" class="btn btn-danger" onclick="vetModal(\''.$item['company_code'].'\', \'company\', \''.$item['company_name'].'\', \'default\'); return false;">Vet</button>',
					'<button value="Delete" class="btn btn-danger" onclick="deleteModal(\''.$item['company_code'].'\', \'\', \'default\'); return false;">Delete</button>');
			}
		}

		if($companyData) {
			$response['sEcho'] = $_REQUEST['sEcho'];
			$response['iTotalRecords'] = $companyData['displayrecords'];		
			$response['iTotalDisplayRecords'] = $companyData['count'];
			$response['aaData']	= $companys;
		} else {
			$response['result'] 	= false;
			$response['message']	= 'There are no items to show.';			
		}

		echo json_encode($response);
		die();
	} else {
		
		$companyData = $companyObject->getSearch($start, $length, $filter);
		
		$row = "Registration Date, Status, Message, Company Name, Registration Number, Contact Name, Contact Email, Contact Number, Company Physical Address, Company Postal Address, Servicdes / Products, Twitter Account\r\n";
		if($companyData) {
			for($i = 0; $i < count($companyData); $i++) {
				$item = $companyData[$i];
				$companys[$i] = array(
						$item['company_added'],
						($item['item_name'] == '' ? 'Not Vetted' : $item['item_name']),
						($item['vet_reason'] == '' ? 'N / A' : preg_replace( "/\r|\n/", "", str_replace(',', '', $item['vet_reason']))),
						preg_replace( "/\r|\n/", "", str_replace(',', '', $item['company_name'])),
						preg_replace( "/\r|\n/", "", str_replace(',', '', $item['company_registration_number'])),
						preg_replace( "/\r|\n/", "", str_replace(',', '', $item['company_contact_name'])), 
						$item['company_contact_email'], 				
						$item['company_contact_number'], 
						preg_replace( "/\r|\n/", "", str_replace(',', '', $item['company_address_physical'])), 
						preg_replace( "/\r|\n/", "", str_replace(',', '', $item['company_address_postal'])), 
						preg_replace( "/\r|\n/", "", str_replace(',', '', $item['company_services'])), 
						$item['company_social_twitter'], 
				);
			}
			
			foreach ($companys as $data) {
				$row .= implode(', ', $data)."\r\n";
			}			
		}

		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=company_report_".date('Y-m-d').".csv");
		Header("Cache-Control: private, must-revalidate" ); // HTTP/1.1
		Header("Pragma: private" ); // HTTP/1.0
	   
		print($row);
		exit;		
	}
}

$itemData = $itemObject->selectParents('VET', 'COMPANY');
if($itemData) $smarty->assign('itemData', $itemData);

$smarty->display('company/list/default.tpl');
?>