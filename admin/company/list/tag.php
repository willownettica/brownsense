<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/* Check for login */
require_once 'includes/auth.php';

/* objects. */
require_once 'class/company.php';
require_once 'class/item.php';
require_once 'class/link.php';

$companyObject	= new class_company();
$itemObject			= new class_item();
$linkObject			= new class_link();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$companyData = $companyObject->getByCode($code);

	if($companyData) {
		$smarty->assign('companyData', $companyData);
		
		$tagData = $linkObject->getByParent('COMPANY', $code, 'TAG');
		if($tagData) $smarty->assign('tagData', $tagData);

	} else {
		header('Location: /company/list/');
		exit;
	}
} else {
	header('Location: /company/list/');
	exit;
}

/* Check posted data. */
if(isset($_GET['tag_select_code'])) {

	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success				= NULL;
	$category				= trim($_GET['tag_select_code']);
	$html = "";
	
	$children = $itemObject->getChildren($category, 'TAG');
	
	if($children) {		
		for($i = 0; $i < count($children); $i++) {
			$html .= "<option value='".$children[$i]['item_code']."'>".$children[$i]['item_name']."</option>";
		}		
	} else {
		$html = '<option value=""> No tags found for this category </option>';
	}

	echo $html;
	exit;
}

/* Check posted data. */
if(isset($_GET['delete_code'])) {

	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success				= NULL;
	$linkcode				= trim($_GET['delete_code']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {	
		$data	= array();
		$data['link_deleted'] = 1;
		
		$where		= array();
		$where[]	= $linkObject->getAdapter()->quoteInto('link_code = ?', $linkcode);
		$where[]	= $linkObject->getAdapter()->quoteInto("link_child_type = ?", 'TAG');
		$where[]	= $linkObject->getAdapter()->quoteInto('link_parent_code = ?', $companyData['company_code']);
		$where[]	= $linkObject->getAdapter()->quoteInto("link_parent_type = ?", 'COMPANY');
		
		$success	= $linkObject->update($data, $where);	
		
		if(is_numeric($success) && $success > 0) {		
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}

	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;

	if(isset($_POST['tag_code']) && trim($_POST['tag_code']) == '') {
		$errorArray['tag_code'] = 'Tag is required';
		$formValid = false;		
	} else {
		$tempData = $linkObject->checkExists('COMPANY', $companyData['company_code'], 'TAG', trim($_POST['tag_code']));
		
		if($tempData) {
			$errorArray['tag_code'] = 'This has already been added.';
			$formValid = false;	
		}
	}

	if(count($errorArray) == 0 && $formValid == true) {
		$data 	= array();				
		$data['link_parent_type']	= 'COMPANY';		
		$data['link_parent_code']	= $companyData['company_code'];	
		$data['link_child_type']		= 'TAG';	
		$data['link_child_code']		= trim($_POST['tag_code']);	
		
		$success 	= $linkObject->insert($data);

		header('Location: /company/list/tag.php?code='.$code);	
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

$parentSelect = $itemObject->selectParents('CATEGORY');
if($parentSelect) $smarty->assign('parentSelect', $parentSelect);

$smarty->display('company/list/tag.tpl');

?>