<?php

class Sun implements SplSubject {
	
	public 			$day				= true;			// Its either day or night, true or false.
	public 			$hour			= 0;				// It starts at midnight, the hour is from 0 to 23.
	public 			$lapsed			= 0;				// Global time lapsed in hours. 
    protected 	$observers	= array();	// Observers of the sun's state to be notified of any change.

	function onDayStart() {
		$this->day = true;
		echo '<br />DAY START (0)<br /><br />';
		return $this->day;
	}
	
	function onDayEnd() {
		$this->day = false;
		echo '<br />DAY END (24)<br /><br />';
		return $this->day;
	}
	
	public function onHourChange() {
		
		if(($this->hour >= 0 && $this->hour <= 12)) {
			if($this->day) {
				$this->onDayEnd();
			}
		}
		
		if(($this->hour >= 13 && $this->hour <= 24)) {
			if(!$this->day) {
				$this->onDayStart();				
			}
		}
		
		echo 'HOUR CHANGE ('.$this->hour.')<br />';
		
		return false;
	}
		
    public function attach(SplObserver $observer)
    {
        $this->observers[] = $observer;
    }

    public function detach(SplObserver $observer)
    {
        $index = array_search($observer, $this->observers);

        if (false !== $index) {
            unset($this->observers[$index]);
        }
    }

    public function notify()
    {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }
	
	public function addHour() {
		
		if($this->hour == 24) {
			$this->hour = 0;
		}
		$this->hour += 1;
		$this->lapsed	+= 1;
		$this->onHourChange();
		$this->notify();
	}
}
?>