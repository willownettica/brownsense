<?php 
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/auth.php'; 

/* objects. */
require_once 'class/enquiry.php';

$enquiryObject 		= new class_enquiry();

if(isset($_GET['send_message'])) {

	$errorArray				= array();
	$errorArray['error']	= array();
	$errorArray['result']	= 1;	

	if(isset($_GET['enquiry_name']) && trim($_GET['enquiry_name']) == '') {
		$errorArray['error'][]	= 'Please add your name';
		$errorArray['result']	= 0;	
	}
	
	if(isset($_GET['enquiry_email']) && trim($_GET['enquiry_email']) != '') {
		if($enquiryObject->validateEmail(trim($_GET['enquiry_email'])) == '') {
			$errorArray['error'][]	= 'Please add a valid email address';
			$errorArray['result']	= 0;				
		}
	} else {
		$errorArray['error'][]	= 'Please add an email address';
		$errorArray['result']	= 0;	
	}
	
	if(isset($_GET['enquiry_message']) && trim($_GET['enquiry_message']) == '') {
		$errorArray['error'][]	= 'Please add your message';
		$errorArray['result']	= 0;	
	}
	
	if(count($errorArray['error']) == 0 && $errorArray['result']	== 1) {

		$data 	= array();						
		$data['member_code']		= $zfsession->identity;
		$data['enquiry_name']		= trim($_GET['enquiry_name']);	
		$data['enquiry_email']		= trim($_GET['enquiry_email']);
		$data['enquiry_item_type']	= 'ENQUIRY';
		$data['enquiry_message']	= trim($_GET['enquiry_message']);	

		$success							= $enquiryObject->insert($data);

		if(!$success) {
			$errorArray['error'][]	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}		
	}

	$errorArray['error'] = implode("<br />",$errorArray['error']);

	echo json_encode($errorArray);
	exit;
}

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<link rel="stylesheet" href="/css/jquery-ui-1.12.1.css">
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8">
				<h3 class="heading-1"><span>Contact Us</span></h3>
				<br />
				<p>If you have any questions in regards to the site or want to know more about a certain subject or even want to just praise us, use the below form to send us a message.</p>
				<br />
				<form class="post-comment-form"action="/contact.php" method="POST" >		
					<div class="row">
						<div class="col-md-6">
							<label>Your Name</label>
							<input type="text" id="enquiry_name" name="enquiry_name" value="" />
						</div>
						<div class="col-md-6">
							<label>Your Email</label>
							<input type="text" id="enquiry_email" name="enquiry_email" value="" />
						</div>
					</div>	
					<div class="row">
						<div class="col-md-12">
							<label>Message</label>
							<textarea id="enquiry_message" name="enquiry_message"></textarea>
						</div>
					</div>
					<div class="alert alert-success enquiry_success" style="display: none; clear: both;">
						<strong>Well done!</strong><br />Your message has been successfully sent to our information officer, we will get back to you as soon as possible.
					</div>
					<div class="alert alert-danger enquiry_fail" style="display: none; clear: both;">
						<strong>Oh snap!</strong><br /><p id="enquiry_fail_message"></p>
					</div>
					<div class="alert alert-info enquiry_submitting" style="display: none; clear: both;">
						<strong>Heads up!</strong> Submitting, please wait....
					</div>					
					<button type="button" onclick="sendEnquiry(); return false;">Send</button><br /><br />
				</form>			
			</div>
			<aside class="col-md-4 col-sm-4">			
				<div class="side-widget margin-bottom-30">
					<h3 class="heading-1"><span>Benefits of joining</span></h3>
					<ul class="trending-text">
						<li>
							<em>1</em>
							<p><a href="#">Free advertising</a> <span>You will not only be able to advertise your company / companies, but also your skills, upload your CV, certificate, etc.</span></p>
						</li>
						<li>
							<em>2</em>
							<p><a href="#">Access to classifieds</a> <span>Free posting of any services you offer or need, you will also be able to see all the services/products needed by other brownies on our classifieds.</span></p>
						</li>
						<li>
							<em>3</em>
							<p><a href="#">Rate services by other brownies</a> <span>If a brownie gives you a service or a product, you will be able to rate their company in regards to your experience with them, company with highest rating goes up on our search.</span></p>
						</li>
						<li>
							<em>4</em>
							<p><a href="#">Apply to be a market vendor</a> <span>In all the markets that we have in the difference cities, you can apply to be a vendor in any of them and advertise your goods and services further.</span></p>
						</li>
					</ul>
				</div>
			</aside>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
<script type="text/javascript" language="javascript">
	function sendEnquiry() {

		$('.enquiry_submitting').show();
		$('.enquiry_submitting').css('visibility', 'visible');
		$('.enquiry_success').hide();
		$('.enquiry_success').css('visibility', 'hidden');
		$('.enquiry_fail').hide();
		$('.enquiry_fail').css('visibility', 'hidden');
		
		$.ajax({
			type: "GET",
			url: "?send_message=1",
			data: "enquiry_name="+$('#enquiry_name').val()+"&enquiry_email="+$('#enquiry_email').val()+"&enquiry_message="+$('#enquiry_message').val(),
			dataType: "json",
			success: function(data){
				if(data.result == 1) {
					$('.enquiry_success').show();	
					$('.enquiry_success').css('visibility', 'visible');
					$('.enquiry_fail').hide();
					$('.enquiry_fail').css('visibility', 'hidden');
					document.getElementById("contact-form").reset();
				} else {
					$('.enquiry_fail').show();
					$('.enquiry_fail').css('visibility', 'visible');
					$('.enquiry_success').hide();
					$('.enquiry_success').css('visibility', 'hidden');
					$('#enquiry_fail_message').html(data.error);
				}
				$('.enquiry_submitting').hide();
				$('.enquiry_submitting').css('visibility', 'hidden');
			}
		});
		
		return false;
	}
</script>
</body>
</html>
