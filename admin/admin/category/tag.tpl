<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
	<h2 class="content-header-title">Categories</h2>
	<ol class="breadcrumb">
		<li><a href="/">Home</a></li>	
		<li><a href="/admin/category/">Administration</a></li>
		<li><a href="/admin/category/">Category</a></li>
		<li><a href="#">{$itemData.item_name}</a></li>
		<li class="active">Tags</li>
	</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					{$itemData.item_name}
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="/admin/category/tag.php?code={$itemData.item_code}" method="POST" data-validate="parsley" class="form parsley-form">
              <table 
                class="table table-striped table-bordered" 
              >
					<thead>
						<tr>
							<th>Name</th>
							<th></th>
						</tr>
					</thead>							
				   <tbody>
				  {foreach from=$tagData item=item}
				  <tr>
					<td align="left">{$item.item_name}</td>
					<td>
						<button value="Delete" class="btn btn-danger" onclick="deleteitemModal('{$itemData.item_code}', '{$item.item_code}'); return false;">Delete</button>
					</td>
				  </tr>
				{foreachelse}
				<tr><td colspan="6">No tags have been added yet</td></tr>
				  {/foreach}
				  </tbody>
                </table>          
				<p>Below is where you add a tag for the category.</p>
				<div class="form-group">
                  <label for="tag_name">Tag</label>
                  <input type="text" id="tag_name" name="tag_name" class="form-control" value="" />
				  {if isset($errorArray.tag_name)}<span class="error">{$errorArray.tag_name}</span>{/if}					  
                </div>				
                <div class="form-group"><button type="submit" class="btn btn-primary">Validate and Submit</button></div>
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col --> 
		<div class="col-sm-3">
			<div class="list-group">  
				<a class="list-group-item" href="/admin/category/">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				{if isset($itemData)}					
				<a class="list-group-item" href="#">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/admin/category/tag.php?code={$itemData.item_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Administrators
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>			
				{/if}
			</div> <!-- /.list-group -->
        </div>			
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
{literal}
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	/* Area auto complete. */
	$( "#admin_name" ).autocomplete({
		source: "/feeds/tag.php",
		minLength: 2,
		select: function( event, ui ) {
			if(ui.item.id == '') {
				$('#admin_name').html('');
				$('#admin_code').val('');					
			} else {
				$('#admin_name').html(ui.item.value);
				$('#admin_code').val(ui.item.id);	
			}
			$('#admin_name').val('');										
		}
	});
});

function deleteitemModal(admin, media) {
	$('#admincode').val(admin);
	$('#mediacode').val(media);
	$('#deleteitemModal').modal('show');
	return false;
}

function deletesubitem() {
		
		var admincode 	= $('#admincode').val();
		var mediacode 			= $('#mediacode').val();
		
		$.ajax({
				type: "GET",
				url: "tag.php",
				data: "delete_code="+mediacode+"&code="+admincode,
				dataType: "json",
				success: function(data){
					if(data.result == 1) {
						window.location.href = window.location.href;
					} else {
						
						$('#deleteitemModal').modal('hide');
						
						$.howl ({
						  type: 'danger'
						  , title: 'Error Message'
						  , content: data.error
						  , sticky: $(this).data ('sticky')
						  , lifetime: 7500
						  , iconCls: $(this).data ('icon')
						});					
					}
				}
		});
		
		return false;
	}
	
</script>
{/literal}
<!-- Modal -->
<div class="modal fade" id="deleteitemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Item</h4>
			</div>
			<div class="modal-body">Are you sure you want to delete this item?</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:deletesubitem();">Delete Item</button>
				<input type="hidden" id="admincode" name="admincode" value=""/>
				<input type="hidden" id="mediacode" name="mediacode" value=""/>
				<input type="hidden" id="mediacode" name="mediacode" value=""/>
			</div>
		</div>
	</div>
</div>
<!-- modal -->
</body>
</html>
