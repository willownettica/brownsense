<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
	<h2 class="content-header-title">Branches</h2>
	<ol class="breadcrumb">
		<li><a href="/">Home</a></li>	
		<li><a href="/admin/view/">Administration</a></li>
		<li><a href="/admin/product/">Branches</a></li>
		<li><a href="#">{if isset($productData)}{$productData._product_name}{else}Add a _product{/if}</a></li>
		<li class="active">Details</li>
	</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					{if isset($productData)}{$productData._product_name}{else}Add a _product{/if}
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="/admin/product/details.php{if isset($productData)}?code={$productData._product_code}{/if}" method="POST" data-validate="parsley" class="form parsley-form">
                <div class="form-group">
					<label for="_product_type">Type</label>
					<select id="_product_type" name="_product_type" class="form-control">
						<option value="1" {if $productData._product_type eq '1'}selected{/if}> Continent </option>
						<option value="2" {if $productData._product_type eq '2'}selected{/if}> Country </option>
						<option value="3" {if $productData._product_type eq '3'}selected{/if}> Province / State </option>
						<option value="4" {if $productData._product_type eq '4'}selected{/if}> City / Town </option>
					</select>
					{if isset($errorArray._product_type)}<span class="error">{$errorArray._product_type}</span>{/if}					  
                </div>			  
                <div class="form-group">
                  <label for="_product_name">Name</label>
                  <input type="text" id="_product_name" name="_product_name" class="form-control" data-required="true" value="{$productData._product_name}" />
				{if isset($errorArray._product_name)}<span class="error">{$errorArray._product_name}</span>{/if}					  
                </div>
                <div class="form-group">
                  <label for="mailbok_code">Mailbok Subscription Code</label>
                  <input type="text" id="mailbok_code" name="mailbok_code" class="form-control" data-required="true" value="{$productData.mailbok_code}" />
				{if isset($errorArray.mailbok_code)}<span class="error">{$errorArray.mailbok_code}</span>{/if}					  
                </div>				
                <div class="form-group">
					<label for="parent_code">Parent Branch</label>
					<select id="parent_code" name="parent_code" class="form-control">
						<option value=""> --------------- </option>
						{html_options options=$_productPairs selected=$productData.parent_code}
					</select>
					{if isset($errorArray.parent_code)}<span class="error">{$errorArray.parent_code}</span>{/if}					  
                </div>					
                <div class="form-group"><button type="submit" class="btn btn-primary">Validate and Submit</button></div>
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col --> 
		<div class="col-sm-3">
			<div class="list-group">  
				<a class="list-group-item" href="/admin/product/">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				{if isset($productData)}					
				<a class="list-group-item" href="#">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/admin/product/admin.php?code={$productData._product_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Administrators
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>			
				{/if}
			</div> <!-- /.list-group -->
        </div>			
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
</html>
