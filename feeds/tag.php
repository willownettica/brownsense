<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/*** Standard includes */
require_once 'config/database.php';

require_once 'class/item.php';

$itemObject	= new class_item();
$list		= array();	

if(isset($_REQUEST['term'])) {

	$q			= strtolower(trim($_REQUEST['term'])); 
	$parent		= strtoupper(trim($_REQUEST['parent'])); 
	$type 		= strtoupper(trim($_REQUEST['type'])); 
	$reference	= strtoupper(trim($_REQUEST['reference']));

	$itemData	= $itemObject->search($q, $parent, $type, $reference);	

	if($itemData) {
		for($i = 0; $i < count($itemData); $i++) {
			$list[] = array(
				"id" 		=> $itemData[$i]["item_code"],
				"label" 	=> $itemData[$i]["item_name"],
				"value" 	=> $itemData[$i]["item_name"]
			);			
		}	
	}
}

if(count($list) > 0) {
	echo json_encode($list); 
	exit;
} else {
	echo json_encode(array()); 
	exit;
}

?>