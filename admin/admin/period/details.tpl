<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
	<h2 class="content-header-title">periods</h2>
	<ol class="breadcrumb">
		<li><a href="/">Home</a></li>	
		<li><a href="#">Administration</a></li>
		<li><a href="/admin/period/">Periods</a></li>
		<li><a href="#">{if isset($periodData)}{$periodData.period_name}{else}Add a period{/if}</a></li>
		<li class="active">Details</li>
	</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					{if isset($periodData)}{$periodData.period_name}{else}Add a period{/if}
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="/admin/period/details.php{if isset($periodData)}?code={$periodData.period_code}{/if}" method="POST" data-validate="parsley" class="form parsley-form">
                <div class="form-group">
                  <label for="period_type">Type</label>
                  <input type="text" id="0" name="period_type" class="form-control" data-required="true" value="{$periodData.period_type}" />
					{if isset($errorArray.period_type)}<span class="error">{$errorArray.period_type}</span>{else}<em>COMPANY, EVENT, MARKET, etc..</em>{/if}					  
                </div>
                <div class="form-group">
                  <label for="period_reference">Reference</label>
                  <select id="period_reference" name="period_reference" class="form-control" >
					<option value="ONCEOFF">Once off</option>
					<option value="DAY1">1 Day</option>
					<option value="DAYS3">3 Days</option>
					<option value="DAYS7">7 Days</option>
					<option value="MONTH1">1 Month</option>
					<option value="MONTHS3">3 Months</option>
					<option value="MONTHS6">6 Months</option>
					<option value="MONTHS9">9 Months</option>
					<option value="MONTHS12">12 Months</option>
				  </select>
					{if isset($errorArray.period_reference)}<span class="error">{$errorArray.period_reference}</span>{/if}					  
                </div>
                <div class="form-group">
                  <label for="period_name">Name</label>
                  <input type="text" id="period_name" name="period_name" class="form-control" data-required="true" value="{$periodData.period_name}" />
					{if isset($errorArray.period_name)}<span class="error">{$errorArray.period_name}</span>{/if}					  
                </div>				
                <div class="form-group">
                  <label for="period_date_start">Start Date</label>
                  <input type="text" id="period_date_start" name="period_date_start" class="form-control" data-required="true" value="{$periodData.period_date_start}" />
					{if isset($errorArray.period_date_start)}<span class="error">{$errorArray.period_date_start}</span>{else}<em>Start date of the period</em>{/if}					  
                </div>
                <div class="form-group">
                  <label for="period_date_end">End Date</label>
                  <input type="text" id="period_date_end" name="period_date_end" class="form-control" data-required="true" value="{$periodData.period_date_end}" />
					{if isset($errorArray.period_date_end)}<span class="error">{$errorArray.period_id}</span>{else}<em>End date of the period</em>{/if}
                </div>				
                <div class="form-group"><button type="submit" class="btn btn-primary">Validate and Submit</button></div>
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col --> 
		<div class="col-sm-3">
			<div class="list-group">  
				<a class="list-group-item" href="/admin/period/">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
			</div> <!-- /.list-group -->
        </div>			
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
{literal}
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$( "#period_date_start" ).datepicker({
		defaultDate: "+1w",
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1
	});

	$( "#period_date_end" ).datepicker({
		defaultDate: "+1w",
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1
	});
});
</script>
{/literal}
</html>
