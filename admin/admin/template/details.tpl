<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
	<h2 class="content-header-title">Templates</h2>
	<ol class="breadcrumb">
		<li><a href="/">Home</a></li>	
		<li><a href="/admin/template/">Administration</a></li>
		<li><a href="/admin/template/">Templates</a></li>
		<li><a href="#">{if isset($templateData)}{$templateData.template_name}{else}Add a template{/if}</a></li>
		<li class="active">View</li>
	</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					{if isset($templateData)}{$templateData.template_name}{else}Add a template{/if}
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="/admin/template/details.php{if isset($templateData)}?code={$templateData.template_code}{/if}" method="POST" enctype="multipart/form-data" data-validate="parsley" class="form parsley-form">
                <div class="form-group">
                  <label for="template_name">Name:</label>
                  <input type="text" id="template_name" name="template_name" class="form-control" data-required="true" value="{$templateData.template_name}" />
				{if isset($errorArray.template_name)}<span class="error">{$errorArray.template_name}</span>{/if}					  
                </div>		
                <div class="form-group">
                  <label for="template_subject">Subject:</label>
                  <input type="text" id="template_subject" name="template_subject" class="form-control" data-required="true" value="{$templateData.template_subject}" />
				{if isset($errorArray.template_subject)}<span class="error">{$errorArray.template_subject}</span>{/if}					  
                </div>					
                <div class="form-group">
                  <label for="template_type">Unique Code for the template:</label>
				<input type="text" id="template_type" name="template_type" value="{$templateData.template_type}"  class="form-control" data-required="true"/>
				{if isset($errorArray.template_type)}<span class="error">{$errorArray.template_type}</span>{/if}				
				<br />
				<span class="success">Examples include: REGISTER, ENQUIRY, PASSWORD, ADMIN_REGISTRATION</span>
                </div> 
                <div class="form-group">
                 <label for="template_category">Category this template is under:</label>
				<input type="text" id="template_category" name="template_category" value="{$templateData.template_category}"  class="form-control" data-required="true" />
				{if isset($errorArray.template_category)}<span class="error">{$errorArray.template_category}</span>{/if}					
				<br />
				<span class="success">Examples include: COMMUNICATE and BOOKING</span>
                </div> 				
                <div class="form-group">
					<label for="template_message">SMS:</label>
					<textarea rows="3" id="template_message" name="template_message" class="form-control">{$templateData.template_message}</textarea>
					{if isset($errorArray.template_message)}<br /><span class="error">{$errorArray.template_message}</span>{/if}					  
                </div>
                <div class="form-group">
					<label for="template_send_sms">Send SMS:</label><br />
					<input type="checkbox" id="template_send_sms" name="template_send_sms" value="1" {if $templateData.template_send_sms eq '1'}checked{/if} />
					<br />{if isset($errorArray.template_send_sms)}<span class="error">{$errorArray.template_send_sms}</span>{else}<em>Send SMS.</em>{/if}					  
                </div>
                <div class="form-group">
					<label for="template_send_email">Send Email:</label><br />
					<input type="checkbox" id="template_send_email" name="template_send_email" value="1" {if $templateData.template_send_email eq '1'}checked{/if} />
					<br />{if isset($errorArray.template_send_email)}<span class="error">{$errorArray.template_send_email}</span>{else}<em>Send Email.</em>{/if}					  
                </div>				
                <div class="form-group">
					<label for="htmlfile">Upload HTML File:</label>
					<input type="file" name="htmlfile" id="htmlfile" />
					{if isset($errorArray.htmlfile)}<br /><span class="error">{$errorArray.htmlfile}</span>{else}<em>Only .html and .htm allowed</em>{/if}					  
                </div>
                <div class="form-group">
					<label for="imagefiles">Upload image files:</label>
					<input type="file" name="imagefiles[]" id="imagefiles[]" multiple />
					{if isset($errorArray.imagefiles)}<br /><span class="error">{$errorArray.imagefiles}</span>{else}<em>Only .png, .jpeg, .gif and .jpg allowed</em>{/if}				  
                </div>
                <div class="form-group"><button type="submit" class="btn btn-primary">Validate and Submit</button></div>
                <div class="form-group">
				<h4>Note:</h4>
				To add peoples names on the mailer please add the following variables on the mailer: <br /><br />
				<table class="table">					
					<tr><td>[fullname]</td><td>=</td><td>Member Name and Surname</td></tr>
					<tr><td>[name]</td><td>=</td><td>Member Name</td></tr>
					<tr><td>[surname]</td><td>=</td><td>Member Surname</td></tr>
					<tr><td>[cellphone]</td><td>=</td><td>Member Cellphone</td></tr>
					<tr><td>[address]</td><td>=</td><td>Member Address</td></tr>
					<tr><td>[email]</td><td>=</td><td>Client email</td></tr>
					<tr><td>[password]</td><td>=</td><td>Password</td></tr>
					<tr><td>[tracking]</td><td>=</td><td>Code for tracking email opened by client</td></tr>
					<tr><td>[date]</td><td>=</td><td>Date sent out to client</td></tr>
					<tr><td>[reference]</td><td>=</td><td>Code of something</td></tr>
					<tr><td>[template]</td><td>=</td><td>Template code for images path</td></tr>
					{if isset($templateData) && $templateData.template_file neq ''}
					<tr>
						<td>View file</td>
						<td>=</td><td>
							<a href="template.php?code={$templateData.template_code}" target="_blank" >
								{$templateData.template_file}
							</a>
						</td>
					</tr>
					{/if}
				</table><br />				  
                </div>				
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col -->
		<div class="col-sm-3">
			<div class="list-group">  
				<a class="list-group-item" href="/admin/template/">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				{if isset($templateData)}					
				<a class="list-group-item" href="#">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>				
				{/if}
			</div> <!-- /.list-group -->
        </div>			
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
</html>
