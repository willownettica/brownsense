<?php 
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/auth.php'; 

require_once 'class/company.php';

$companyObject 	= new class_company();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$companyData = $companyObject->getByMember($zfsession->identity, $code);

	if(!$companyData) {
		header('Location: /account/');
		exit;
	}
}

if(isset($_REQUEST['save_company'])) {

	$errorArray					= array();
	$errorArray['error']		= array();
	$errorArray['result']		= 1;	
	$errorArray['success']	= '';	
	
	if(isset($_POST['areapost_code']) && trim($_POST['areapost_code']) == '') {
		$errorArray['error'][] = 'Area is required';
		$errorArray['result'] = false;		
	}

	if(isset($_POST['company_name']) && trim($_POST['company_name']) == '') {
		$errorArray['error'][] = 'Name is required'; 
		$errorArray['result'] = false;		
	}

	if(isset($_POST['company_contact_name']) && trim($_POST['company_contact_name']) == '') {
		$errorArray['error'][] = 'Contact name is required'; 
		$errorArray['result'] = false;		
	}

	if(isset($_POST['company_contact_email']) && trim($_POST['company_contact_email']) == '') {
		$errorArray['error'][] = 'Contact email is required'; 
		$errorArray['result'] = false;		
	} else if($companyObject->validateEmail(trim($_POST['company_contact_email'])) == ''){
		$errorArray['error'][] = 'Contact email needs to be valid'; 
		$errorArray['result'] = false;		
	}
	
	if(isset($_POST['company_contact_number']) && trim($_POST['company_contact_number']) == '') {
		$errorArray['error'][] = 'Contact number is required';
		$errorArray['result'] = false;		
	}
	
	if(isset($_POST['company_address_physical']) && trim($_POST['company_address_physical']) == '') {
		$errorArray['error'][] = 'Physical address is required';
		$errorArray['result'] = false;		
	}
	
	if(isset($_REQUEST['company_social_twitter']) && trim($_REQUEST['company_social_twitter']) != '') {
		if($companyObject->validateTwitter(trim($_REQUEST['company_social_twitter'])) == '') {
			$errorArray['error'][] = 'Your twitter handle was not found on twitter. Please check its spelling.';
			$errorArray['result'] = false;				
		}
	}

	if(count($errorArray['error']) == 0 && $errorArray['result'] == true) {

		$data 													= array();				
		$data['member_code']							= $zfsession->identity;	
		$data['areapost_code']							= trim($_POST['areapost_code']);	
		$data['company_name']							= trim($_POST['company_name']);	
		$data['company_registration_name']		= trim($_POST['company_registration_name']);	
		$data['company_registration_number']	= trim($_POST['company_registration_number']);	
		$data['company_registration_vat']			= trim($_POST['company_registration_vat']);
		$data['company_social_facebook']			= trim($_POST['company_social_facebook']);
		$data['company_social_twitter']				= trim($_POST['company_social_twitter']);
		$data['company_social_instagram']			= trim($_POST['company_social_instagram']);
		$data['company_social_linkedin']			= trim($_POST['company_social_linkedin']);
		$data['company_website']						= trim($_POST['company_website']);
		$data['company_address_physical']			= trim($_POST['company_address_physical']);
		$data['company_address_postal']			= trim($_POST['company_address_postal']);
		$data['company_contact_name']				= trim($_POST['company_contact_name']);
		$data['company_contact_email']				= trim($_POST['company_contact_email']);
		$data['company_contact_number']			= trim($_POST['company_contact_number']);
		$data['company_map_latitude']				= trim($_POST['company_map_latitude']);
		$data['company_map_longitude']			= trim($_POST['company_map_longitude']);
		$data['company_active']							= 0;

		if(isset($companyData)) {
			$where		= array();
			$where[]	= $companyObject->getAdapter()->quoteInto('member_code = ?', $zfsession->identity);
			$where[]	= $companyObject->getAdapter()->quoteInto('company_code = ?', $companyData['company_code']);
			$success	= $companyObject->update($data, $where);
			$errorArray['success']	= $companyData['company_code'];
		} else {
			$errorArray['success'] = $companyObject->insert($data);
		}	
	}
	
	$errorArray['error'] = implode("<br />",$errorArray['error']);
	
	echo json_encode($errorArray);
	exit;	
}

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<link rel="stylesheet" href="/css/jquery-ui-1.12.1.css">
	<script type="text/javascript">
	var map;
	var marker;

	function initMap()
	{
		var opts = {'center': new google.maps.LatLng(<?php if(isset($companyData['company_map_latitude']) && trim($companyData['company_map_latitude']) != '') { echo trim($companyData['company_map_latitude']); } else { echo "-33.9285481685662"; } ?>, <?php if(isset($companyData['company_map_longitude']) && trim($companyData['company_map_longitude']) != '') { echo trim($companyData['company_map_longitude']); } else { echo "18.42681884765625"; }  ?>), 'zoom':14, 'mapTypeId': google.maps.MapTypeId.SATELLITE }
		map = new google.maps.Map(document.getElementById('mapdiv'),opts);
		
		
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(<?php if(isset($companyData['company_map_latitude']) && trim($companyData['company_map_latitude']) != '') { echo trim($companyData['company_map_latitude']); } else { echo "-33.9285481685662"; }  ?>, <?php if(isset($companyData['company_map_longitude']) && trim($companyData['company_map_longitude']) != '') { echo trim($companyData['company_map_longitude']); } else { echo "18.42681884765625"; }  ?>),
			map: map
		});
		
		google.maps.event.addListener(map,'click',function(event) {
			
			if (marker) {
				marker.setMap(null);
				marker = null;
			}
			
			document.getElementById('company_map_latitude').value = event.latLng.lat();
			document.getElementById('company_map_longitude').value = event.latLng.lng();
			marker = new google.maps.Marker({
				position: event.latLng,
				map: map
			});
		});
	}
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQULf4QMovya_unNUL1-kMhI_FxkZib7I&callback=initMap" async defer></script>	
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/account/"><?php echo $zfsession->memberData['member_name']; ?></a></li>
						<li><a href="/account/company/">My Companies</a></li>
						<?php if(isset($companyData)) { ?>
						<li><a href="/account/company/details.php?code<?php echo $companyData['company_code']; ?>"><?php echo $companyData['company_name']; ?></a></li>
						<?php } else { ?>
						<li><a href="#">Add a company</a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">			
		<div class="row">
			<div class="col-md-12 col-sm-8">
				<h3 class="heading-1"><span><?php if(isset($companyData)) { echo $companyData['company_name']; } else { ?>Add a company<?php } ?></span></h3>
				<?php if(isset($companyData)) { ?>				
				<ul class="nav nav-tabs">
					<li class="active"><a href="/account/company/details.php?code=<?php echo $companyData['company_code']; ?>">Details</a></li>
					<li><a href="/account/company/tags.php?code=<?php echo $companyData['company_code']; ?>">Products / Services</a></li>
					<li><a href="/account/company/media.php?code=<?php echo $companyData['company_code']; ?>">Images</a></li>
					<li><a href="/account/company/document.php?code=<?php echo $companyData['company_code']; ?>">Documents</a></li>
					<li><a href="/account/company/vendor.php?code=<?php echo $companyData['company_code']; ?>">Become a vendor</a></li>
					<li><a href="/account/company/enquiry.php?code=<?php echo $companyData['company_code']; ?>">Enquiries</a></li>
				</ul>
				<p style="clear: both;">&nbsp;&nbsp;&nbsp;&nbsp;</p>	
				<?php } ?>				
				<div class="alert alert-info">
					<strong>Heads up!</strong> Please remember that each time you update your company, it will need to be vetted first and that may take less than 24 hours at least to allow the company. Even if you are doing a spelling check, it will be vetted. 
				</div>			
				<form class="post-comment-form"action="/account/comapny/<?php if(isset($companyData)) { echo '?code='.$companyData['company_code']; } ?>" method="POST" >
					<div class="row">
						<div class="col-md-4">
							<label class="text_red">Company name</label>
							<input type="text" id="company_name" name="company_name" value="<?php if(isset($companyData['company_code'])) { echo $companyData['company_name']; } ?>" placeholder="Your company name" />
						</div>
						<div class="col-md-4">
							<label>CIPC: Registered Business Name</label>
                            <input type="text" name="company_registration_name" id="company_registration_name" value="<?php if(isset($companyData['company_registration_name'])) { echo $companyData['company_registration_name']; } ?>" placeholder="Registered name of the company">
						</div>
						<div class="col-md-4">
							<label>CIPC: Registered Business Number</label>
							<input type="text" name="company_registration_number" id="company_registration_number" value="<?php if(isset($companyData['company_registration_number'])) { echo $companyData['company_registration_number']; } ?>" placeholder="Registered number of the company" />
						</div>							
					</div>
					<div class="row">					
						<div class="col-md-4">
							<label class="text_red">Business Contact: Full name</label>
							<input type="text" name="company_contact_name" id="company_contact_name" value="<?php if(isset($companyData['company_contact_name'])) { echo $companyData['company_contact_name']; } ?>" placeholder="Your business contact name">
						</div>
						<div class="col-md-4">
							<label class="text_red">Business Contact: Email address</label>
							<input type="text" name="company_contact_email" id="company_contact_email" value="<?php if(isset($companyData['company_contact_email'])) { echo $companyData['company_contact_email']; } ?>" placeholder="Business contact email address">
						</div>
						<div class="col-md-4">
							<label class="text_red">Business Contact: Number</label>
                            <input type="text" name="company_contact_number" id="company_contact_number" value="<?php if(isset($companyData['company_contact_number'])) { echo $companyData['company_contact_number']; } ?>" placeholder="Business contact number">
						</div>						
					</div>
					<div class="row">
						<div class="col-md-4">
							<label>VAT Number</label>
							<input type="text" name="company_registration_vat" id="company_registration_vat" value="<?php if(isset($companyData['company_registration_vat'])) { echo $companyData['company_registration_vat']; } ?>" placeholder="Vat registration number">
						</div>
						<div class="col-md-4">
							<label>Company website</label>
							<input type="text" name="company_website" id="company_website" value="<?php if(isset($companyData['company_website'])) { echo $companyData['company_website']; } ?>" placeholder="Company website">	
						</div>
						<div class="col-md-4">
							<label class="text_red">Company location, city or town.</label>
							<input type="text" id="areapost_name" name="areapost_name" data-required="true" value="<?php if(isset($companyData['areapost_name'])) { echo $companyData['areapost_name']; } ?>" placeholder="Business location or area, add first 3 letters and select from the drop down." />
							<input type="hidden" id="areapost_code" name="areapost_code" value="<?php if(isset($companyData['areapost_code'])) { echo $companyData['areapost_code']; } ?>" />
						</div>	
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="text_red">Physical Address</label>
							<input type="text" id="company_address_physical" name="company_address_physical" data-required="true" value="<?php if(isset($companyData['company_address_physical'])) { echo $companyData['company_address_physical']; } ?>" placeholder="Business physical address" />
						</div>				
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="text_red">Postal Address</label>
							<input type="text" id="company_address_postal" name="company_address_postal" data-required="true" value="<?php if(isset($companyData['company_address_postal'])) { echo $companyData['company_address_postal']; } ?>" placeholder="usiness postal address" />
						</div>				
					</div>
					<div class="row">
						<div class="col-md-4">
							<label>Social Media: Facebook account / page</label>
                            <input type="text" name="company_social_facebook" id="company_social_facebook" value="<?php if(isset($companyData['company_social_facebook'])) { echo $companyData['company_social_facebook']; } ?>" placeholder="Company's facebook social media account or page">
						</div>
						<div class="col-md-4">
							<label>Social Media: Twitter account handle</label>
                            <input type="text" name="company_social_twitter" id="company_social_twitter" value="<?php if(isset($companyData['company_social_twitter'])) { echo $companyData['company_social_twitter']; } ?>" placeholder="Company's twitter social media account">
						</div>	
						<div class="col-md-4">
							<label>Social Media: Instagram handle</label>
                            <input type="text" name="company_social_instagram" id="company_social_instagram" value="<?php if(isset($companyData['company_social_instagram'])) { echo $companyData['company_social_instagram']; } ?>" placeholder="Company's instagram social media account">
						</div>							
					</div>
					<div class="row">
						<div class="col-md-12">
							<label>Social Media: LinkedIn account URL</label>
                            <input type="text" name="company_social_linkedin" id="company_social_linkedin" value="<?php if(isset($companyData['company_social_linkedin'])) { echo $companyData['company_social_linkedin']; } ?>" placeholder="Company's linkedIn social media account">
						</div>			
					</div>
					<div class="row">
						<div class="col-md-9">
							<div id="mapdiv" style="height: 400px; width: 100%;"></div>
						</div>
						<div class="col-md-3">
							<p style="font-size: 13px;">Select by simply clicking on the map the location of your business, this will help your potential clients to find your place of business a lot better.</p><br />							
							<label>Latitude</label><br />
							<input type="text" name="company_map_latitude" id="company_map_latitude" value="<?php if(isset($companyData['company_map_latitude'])) { echo $companyData['company_map_latitude']; } ?>" placeholder="" disabled />
							<br />
							<label>Longitude</label><br />
							<input type="text" name="company_map_longitude" id="company_map_longitude" value="<?php if(isset($companyData['company_map_longitude'])) { echo $companyData['company_map_longitude']; } ?>" placeholder="" disabled />
						
						</div>							
					</div>	
					<br />					
					<div class="alert alert-success company_success" style="display: none; clear: both;">
						<strong>Well done!</strong><br />Your message has been successfully sent to our information officer, we will get back to you as soon as possible.
					</div>
					<div class="alert alert-danger company_fail" style="display: none; clear: both;">
						<strong>Oh snap!</strong><br /><p id="company_fail_message"></p>
					</div>
					<div class="alert alert-info company_submitting" style="display: none; clear: both;">
						<strong>Heads up!</strong> Submitting, please wait....
					</div>				
					<button type="button" onclick="saveCompany(); return false;">Save Company</button>
					<br /><br />
				</form>
			</div>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" language="javascript">

	$(document).ready(function(){

		$( "#areapost_name" ).autocomplete({
			source: "/feeds/areapost.php",
			minLength: 2,
			select: function( event, ui ) {
				if(ui.item.id == '') {
					$('#areapost_name').html('');
					$('#areapost_code').val('');					
				} else {
					$('#areapost_name').html(ui.item.value);
					$('#areapost_code').val(ui.item.id);	
				}
				$('#areapost_name').val('');										
			}
		});
	});

	function saveCompany() {

		$('.company_submitting').show();
		$('.company_success').hide();
		$('.company_fail').hide();
		     
		$.ajax({
			type: "POST",
			url: "?save_company=1<?php if(isset($companyData)) { echo '&code='.$companyData['company_code']; } ?>",
			data: "areapost_code="+$('#areapost_code').val()+"&company_name="+$('#company_name').val()+"&company_registration_name="+$('#company_registration_name').val()+"&company_registration_number="+$('#company_registration_number').val()+"&company_registration_vat="+$('#company_registration_vat').val()+"&company_social_facebook="+$('#company_social_facebook').val()+"&company_social_twitter="+$('#company_social_twitter').val()+"&company_social_instagram="+$('#company_social_instagram').val()+"&company_social_linkedin="+$('#company_social_linkedin').val()+"&company_website="+$('#company_website').val()+"&company_address_physical="+$('#company_address_physical').val()+"&company_address_postal="+$('#company_address_postal').val()+"&company_contact_name="+$('#company_contact_name').val()+"&company_contact_email="+$('#company_contact_email').val()+"&company_contact_number="+$('#company_contact_number').val()+"&company_map_latitude="+$('#company_map_latitude').val()+"&company_map_longitude="+$('#company_map_longitude').val(),
			dataType: "json",
			success: function(data){
				if(data.result == 1) {
						// window.location.href = '/account/company/tags.php?code='+data.success;
				} else {
					$('.company_fail').show();
					$('.company_success').hide();
					$('#company_fail_message').html(data.error);
				}
				$('.company_submitting').hide();
			}
		});
		
		return false;
	}

	function deleteModal(parent, item, parameter) {
			$('#deleteparent').val(parent);
			$('#deleteitem').val(item);
			$('#deleteparameter').val(parameter);
			$('#deleteModal').modal('show');
			return false;
	}


	function deleteItem() {
		
		$('.delete_submit').show();
		$('.delete_fail').hide();
		
		var deleteparent		= $('#deleteparent').val();
		var deleteitem			= $('#deleteitem').val();
		var deleteparameter	= $('#deleteparameter').val();

		$.ajax({
			type: "GET",
			url: "?code="+deleteparent,
			data: deleteparameter+"="+deleteitem,
			dataType: "json",
			success: function(data){
				if(data.result == 1) {
					window.location.href = window.location.href;
				} else {
					$('.delete_fail').show();
					$('#delete_fail_message').html(data.error);
				}
				$('.delete_submit').hide();
			}
		});
		
		return false;
	}
</script>
<?php if(isset($companyData)) { ?>
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Item</h4>
			</div>
			<div class="modal-body">
				Are you sure you want to delete this item?
				<div class="alert alert-danger delete_fail" style="display: none; clear: both;">
					<strong>Oh snap!</strong><br /><p id="delete_fail_message"></p>
				</div>
				<div class="alert alert-info delete_submit" style="display: none; clear: both;">
					<strong>Heads up!</strong> Submitting, please wait....
				</div>				
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:deleteItem();">Delete Item</button>
				<input type="hidden" id="deleteparent" name="deleteparent" value=""/>
				<input type="hidden" id="deleteitem" name="deleteitem" value=""/>
				<input type="hidden" id="deleteparameter" name="deleteparameter" value=""/>
			</div>
		</div>
	</div>
</div>
<!-- modal -->
<?php } ?>
</body>
</html>
