<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* standard config include. */
require_once 'config/database.php';

//include the Zend class for Authentification
require_once 'Zend/Session.php';
 
// Set up the namespace
$zfsession	= new Zend_Session_Namespace('LOGIN_MEMBER');

// Check if logged in, otherwise redirect
if (!isset($zfsession->identity) || is_null($zfsession->identity) || $zfsession->identity == '') {
	/* Do nothing. */
	$page = explode('/',$_SERVER['REQUEST_URI']);
	$currentPage = isset($page[1]) && trim($page[1]) != '' ? trim($page[1]) : '';
	
	if($currentPage == 'account') {
		/* Redirect else where. */
		header('Location: /');
		exit;
	}
} else {

	require_once 'class/member.php';

	$memberObject 	= new class_member();
	//get user details by username
	$memberData = $memberObject->getByCode($zfsession->identity);

	if($memberData) {
		/* Administrator selected campaign. */
		$zfsession->memberData 	= $memberData;
		$zfsession->identity 			= $memberData['member_code'];

		unset($memberObject);	
	}
}

global $zfsession;

?>