<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');
/**
 * Standard includes
 */
require_once 'config/database.php';
require_once 'config/smarty.php';
require_once 'class/admin.php';

$adminObject	= new class_admin();
$list			= array();	

if(isset($_REQUEST['term'])) {

	$q				= strtolower(trim($_REQUEST['term'])); 
	$adminListData	= $adminObject->search($q);
	
	if($adminListData) {
		for($i = 0; $i < count($adminListData); $i++) {
			$list[] = array(
				"id" 		=> $adminListData[$i]["admin_code"],
				"label" 	=> $adminListData[$i]["admin_name"].' '.$adminListData[$i]["admin_surname"].' - '.$adminListData[$i]['admin_email'].' - '.$adminListData[$i]['admin_cellphone'].' in '.$adminListData[$i]['areapost_name'],
				"value" 	=> $adminListData[$i]["admin_name"].' '.$adminListData[$i]["admin_surname"].' - '.$adminListData[$i]['admin_email'].' - '.$adminListData[$i]['admin_cellphone'].' in '.$adminListData[$i]['areapost_name']
			);			
		}	
	}
	
}

if(count($list) > 0) {
	echo json_encode($list); 
	exit;
} else {
	echo json_encode(array('id' => '', 'label' => 'no results')); 
	exit;
}
exit;
?>