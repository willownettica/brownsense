<?php

//custom account item class as account table abstraction
class class_period extends Zend_Db_Table_Abstract {
   //declare table variables
    protected $_name		= 'period';
	protected $_primary	= 'period_code';

	/** 
	 * Insert the database record 
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 

	public function insert(array $data) {
        // add a timestamp
        $data['period_added']		= date('Y-m-d H:i:s');
        $data['period_code']			= $this->createCode();
		
		$periodData = $this->getActive($data['period_type'], $data['period_reference']);

		if($periodData) {
			/* Increase id to the latest one. */
			$data['period_id'] 		= (int)$periodData['period_id']+1;
			/* Update previous item. */
			$udata 							= array();
			$udata['period_active']	= 0;

			$where 	= array();
			$where[]	= $this->getAdapter()->quoteInto('period_code = ?', $periodData['period_code']);
			$this->update($udata, $where);
		}
		return parent::insert($data);	
    }

	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where) {
        // add a timestamp
         $data['period_updated'] = date('Y-m-d H:i:s');
        
        return parent::update($data, $where);
    }
	
	/**
	 * get job by job branch Id
 	 * @param string job id
     * @return object
	 */
	public function remove($period) {	
		
		$data 						= array();
		$data['period_active']	= 0;

		$where 	= array();
		$where[]	= $this->getAdapter()->quoteInto('period_code = ?', $period);
		return $this->update($data, $where);

	}
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getActive($type, $reference) {
		$select = $this->_db->select()
			->from(array('period' => 'period'))
			->where('period_deleted = 0 and period_active = 1 and period.period_type = ?', $type)
			->where('period_reference = ?', $reference)
			->limit(1);

		$result = $this->_db->fetchRow($select);
		return ($result == false) ? false : $result = $result;					   
	}

	public function pairs($type, $reference) {
		$select = $this->_db->select()
			->from(array('period' => 'period'))
			->where('period_deleted = 0 and period_active = 1 and period.period_type = ?', $type)
			->where('period_reference = ?', $reference)
			->order('period.period_name asc');

		$result = $this->_db->fetchPairs($select);
		return ($result == false) ? false : $result = $result;
	}	
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getByCode($code) {
		$select = $this->_db->select()
			->from(array('period' => 'period'))
			->where('period_deleted = 0 and period.period_code = ?', $code)
			->limit(1);

		$result = $this->_db->fetchRow($select);
		return ($result == false) ? false : $result = $result;					   
	}
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getAll() {
		$select = $this->_db->select()
			->from(array('period' => 'period'))
			->where('period_deleted = 0')
			->order(array('period_type desc', 'period_id'));

		$result = $this->_db->fetchAll($select);
		return ($result == false) ? false : $result = $result;					   
	}	
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getCode($code) {
		$select = $this->_db->select()	
						->from(array('period' => 'period'))	
					   ->where('period_code = ?', $code)
					   ->limit(1);

	   $result = $this->_db->fetchRow($select);	
       return ($result == false) ? false : $result = $result;					   
	}
	
	function createCode() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '23456789';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Number[rand(0,$count)];
		}
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<4;$i++){
			$code .= $Number[rand(0,$count)];
		}		
		/* First check if it exists or not. */
		$periodCheck = $this->getCode($code);
		
		if($periodCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}
}
?>