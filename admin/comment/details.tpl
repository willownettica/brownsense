<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
	<h2 class="content-header-title">Event</h2>
	<ol class="breadcrumb">
	<li><a href="/">Home</a></li>
	<li><a href="#">Event</a></li>
	<li><a href="#">{if isset($eventData)}{$eventData.event_name}{else}Add a event{/if}</a></li>
	<li class="active">Details</li>
	</ol>
	</div>	
      <div class="row">
        <div class="col-sm-8">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					{if isset($eventData)}{$eventData.event_name}{else}Add a event{/if}
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
				{if $eventData.event_active eq '1'}
					<div class="alert alert-success">
						<strong>Active</strong> It will be searchable and be visible to everyone on the website.
					</div>
				{else}
					<div class="alert alert-danger">
						<strong>Non-Active</strong> The event will not be visible on the website nor will it be searchable, only on the member account will it be seen when updating it.
					</div>
				{/if}
              <form id="validate-basic" action="/event/vendors/details.php{if isset($eventData)}?code={$eventData.event_code}{/if}" method="POST" data-validate="parsley" class="form parsley-form">
                <div class="form-group">
                  <label for="member_name" class="error">Member</label>
                  <input type="text" id="member_name" name="member_name" class="form-control" value="" />
				  <input type="hidden" id="member_code" name="member_code" value="{$eventData.member_code}" /><br />
				  <span id="selectedmember">{if !isset($eventData)}No member selected yet{else}{$eventData.member_name} {$eventData.member_surname} ( {$eventData.member_email} ){/if}</span>
				  {if isset($errorArray.member_code)}<span class="error">{$errorArray.member_code}</span>{/if}					  
                </div>			  
                <div class="form-group">
                  <label for="areapost_name" class="error">Your area / town / city</label>
                  <input type="text" id="areapost_name" name="areapost_name" class="form-control" value="" />
				  <input type="hidden" id="areapost_code" name="areapost_code" value="{$eventData.areapost_code}" /><br />
				  <span id="selectedareapost">{$eventData.areapost_name|default:"No area selected yet"}</span>
				  {if isset($errorArray.areapost_code)}<span class="error">{$errorArray.areapost_code}</span>{/if}					  
                </div>					
                <div class="form-group">
                  <label for="event_name" class="error">Name</label>
                  <input type="text" id="event_name" name="event_name" class="form-control" data-required="true" value="{$eventData.event_name}" />
				{if isset($errorArray.event_name)}<span class="error">{$errorArray.event_name}</span>{/if}					  
                </div>
                <div class="form-group"> 
                  <label for="event_address_physical" class="error">Address: Physical</label>
                  <textarea id="event_address_physical" name="event_address_physical" class="form-control"data-required="true" >{$eventData.event_address_physical}</textarea>
				{if isset($errorArray.event_address_physical)}<span class="error">{$errorArray.event_address_physical}</span>{/if}					  
                </div>
                <div class="form-group">
                  <label for="event_address_postal">Address: Postal</label>
                  <textarea id="event_address_postal" name="event_address_postal" class="form-control">{$eventData.event_address_postal}</textarea>
				{if isset($errorArray.event_address_postal)}<span class="error">{$errorArray.event_address_postal}</span>{/if}					  
                </div>
                <div class="form-group">
                  <label for="event_contact_name" class="error">Contact: Full Name</label>
                  <input type="text" id="event_contact_name" name="event_contact_name" class="form-control" data-required="true" value="{$eventData.event_contact_name}" />
				{if isset($errorArray.event_contact_name)}<span class="error">{$errorArray.event_contact_name}</span>{/if}					  
                </div>	
                <div class="form-group">
                  <label for="event_contact_email" class="error">Contact: Email address</label>
                  <input type="text" id="event_contact_email" name="event_contact_email" class="form-control" data-required="true" value="{$eventData.event_contact_email}" />
				{if isset($errorArray.event_contact_email)}<span class="error">{$errorArray.event_contact_email}</span>{/if}					  
                </div>	
                <div class="form-group">
                  <label for="event_contact_number" class="error">Contact: Cellphone / Telephone number</label>
                  <input type="text" id="event_contact_number" name="event_contact_number" class="form-control" data-required="true" value="{$eventData.event_contact_number}" />
				{if isset($errorArray.event_contact_number)}<span class="error">{$errorArray.event_contact_number}</span>{/if}					  
                </div>					
                <div class="form-group">
                  <label for="event_registration_name">CIPC: Registration Name</label>
                  <input type="text" id="event_registration_name" name="event_registration_name" class="form-control" value="{$eventData.event_registration_name}" />
				{if isset($errorArray.event_registration_name)}<span class="error">{$errorArray.event_registration_name}</span>{/if}					  
                </div>				
                <div class="form-group">
                  <label for="event_registration_number">CIPC: Registration Number</label>
                  <input type="text" id="event_registration_number" name="event_registration_number" class="form-control" value="{$eventData.event_registration_number}" />
				{if isset($errorArray.event_registration_number)}<span class="error">{$errorArray.event_registration_number}</span>{/if}					  
                </div>
                <div class="form-group">
                  <label for="event_registration_vat">CIPC: VAT Number</label>
                  <input type="text" id="event_registration_vat" name="event_registration_vat" class="form-control" value="{$eventData.event_registration_vat}" />
				{if isset($errorArray.event_registration_vat)}<span class="error">{$errorArray.event_registration_vat}</span>{/if}					  
                </div>
                <div class="form-group">
                  <label for="event_social_facebook">Social Media: Facebook</label>
                  <input type="text" id="event_social_facebook" name="event_social_facebook" class="form-control" value="{$eventData.event_social_facebook}" />
				{if isset($errorArray.event_social_facebook)}<span class="error">{$errorArray.event_social_facebook}</span>{/if}					  
                </div>
                <div class="form-group">
                  <label for="event_social_twitter">Social Media: Twitter</label>
                  <input type="text" id="event_social_twitter" name="event_social_twitter" class="form-control" value="{$eventData.event_social_twitter}" />
				{if isset($errorArray.event_social_twitter)}<span class="error">{$errorArray.event_social_twitter}</span>{/if}					  
                </div>	
                <div class="form-group">
                  <label for="event_social_instagram">Social Media: Instagram</label>
                  <input type="text" id="event_social_instagram" name="event_social_instagram" class="form-control" value="{$eventData.event_social_instagram}" />
				{if isset($errorArray.event_social_instagram)}<span class="error">{$errorArray.event_social_instagram}</span>{/if}					  
                </div>				
                <div class="form-group">
                  <label for="event_social_linkedin">Social Media: LinkedIn</label>
                  <input type="text" id="event_social_linkedin" name="event_social_linkedin" class="form-control" value="{$eventData.event_social_linkedin}" />
				{if isset($errorArray.event_social_linkedin)}<span class="error">{$errorArray.event_social_linkedin}</span>{/if}					  
                </div>	
                <div class="form-group">
                  <label for="event_website">Website</label>
                  <input type="text" id="event_website" name="event_website" class="form-control" value="{$eventData.event_website}" />
				{if isset($errorArray.event_website)}<span class="error">{$errorArray.event_website}</span>{/if}					  
                </div>				
                <div class="form-group"><button type="submit" class="btn btn-primary">Validate and Submit</button></div>
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col --> 
		<div class="col-sm-4">
			<div class="list-group">  
				<a class="list-group-item" href="/event/vendors/">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				{if isset($eventData)}					
				<a class="list-group-item" href="#">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/event/vendors/media.php?code={$eventData.event_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Media
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/event/vendors/tag.php?code={$eventData.event_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Tag(s)
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/event/vendors/document.php?code={$eventData.event_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Document(s)
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/event/vendors/market.php?code={$eventData.event_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Comments
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>					
				{/if}
			</div> <!-- /.list-group -->
			<div class="portlet">
				<div class="portlet-header">
				  <h3>
					<i class="fa fa-tasks"></i>
					Vet event
				  </h3>
				</div> <!-- /.portlet-header -->
				<div class="portlet-content">
				  <form id="validate-basic" action="/event/vendors/details.php?code={$eventData.event_code}" method="POST" data-validate="parsley" class="form parsley-form">
					<div class="form-group">  
					  <label for="item_code">Status</label>
					  <select data-required="true" class="form-control parsley-validated" name="item_code" id="item_code">
						{html_options options=$itemData}
					  </select>
					</div>
					<div class="form-group">
					  <label for="vet_reason">Reasons</label>
					  <textarea class="form-control parsley-validated" rows="5" cols="10" id="vet_reason" name="vet_reason" data-required="true"></textarea>
					</div>
					<div class="form-group">
					  <button class="btn btn-primary" type="submit">Vet</button>
					</div>
				  </form>
				  <p>Below is the history of vetting for this event.</p>
              <table class="table">
					<thead>
						<tr>
							<th data-sortable="true">Date</th>
							<th data-sortable="true">Status</th>
						</tr>
					</thead>							
				   <tbody>
				  {foreach from=$vetData item=item}
				  <tr class="{$item.item_config_class}">
					<td align="left">{$item.vet_added|date_format}</td>
					<td align="left" alt="{$item.vet_reason}" title="{$item.vet_reason}">{$item.item_name}</td>
				  </tr>
				{foreachelse}
				<tr><td colspan="2">No vetting has occurred yet.</td></tr>
				  {/foreach}
				  </tbody>
                </table>
				
				</div> <!-- /.portlet-content -->
			  </div>
        </div>			
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
{literal}
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	/* Area auto complete. */
	$( "#areapost_name" ).autocomplete({
		source: "/feeds/areapost.php",
		minLength: 2,
		select: function( event, ui ) {
			if(ui.item.id == '') {
				$('#areapost_name').html('');
				$('#areapost_code').val('');					
			} else {
				$('#areapost_name').html(ui.item.value);
				$('#areapost_code').val(ui.item.id);
				$('#selectedareapost').html(ui.item.value);
			}
			$('#areapost_name').val('');										
		}
	});
	/* Area auto complete. */
	$( "#member_name" ).autocomplete({
		source: "/feeds/member.php",
		minLength: 2,
		select: function( event, ui ) {
			if(ui.item.id == '') {
				$('#member_name').html('');
				$('#member_code').val('');					
				$('#selectedmember').html('')
			} else {
				$('#member_name').html(ui.item.value);
				$('#member_code').val(ui.item.id);	
				$('#selectedmember').html(ui.item.value);				
			}
			$('#member_name').val('');
		}
	});
});
</script>
{/literal}
</html>
