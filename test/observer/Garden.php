<?php
class Garden implements SplObserver {

	public	$flowerObjects	= array();
	public 	$state			= false;
	
	function __construct( $number_of_flowers = 10) {
		/* Make sure that an integer is the parameter. */
		if(is_int($number_of_flowers)) {
			for($i = 0; $i < $number_of_flowers; $i++) {
				$this->flowerObjects[$i] 		= new Flower();
				$this->flowerObjects[$i]->name	= 'FLOWER-'.$i;
			}
		} else {
			echo 'THE GARDEN NEEDS TO HAVE FLOWERS, HOW MANY ARE IN IT?. <br />';		
		}
	}

	public function setState($state) {
		$this->state = $state;		
	}
	
    public function update(SplSubject $subject)
    {
		if($subject->day) {
			$this->setState(true);
		} else {
			$this->setState(false);
		}
    }	
}
?>