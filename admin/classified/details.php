<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';
/* Check for login */
require_once 'includes/auth.php';
/* objects. */
require_once 'class/classified.php';
require_once 'class/branch.php';
require_once 'class/item.php';

$classifiedObject	= new class_classified();
$branchObject 		= new class_branch();
$itemObject			= new class_item();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$classifiedData = $classifiedObject->getByCode($code);

	if($classifiedData) {
		$smarty->assign('classifiedData', $classifiedData);
	} else {
		header('Location: /classified/');
		exit;
	}
}

/* Check posted data. */
if(isset($_GET['getsection'])) {
	
	$categorycode	= trim($_GET['category_code']);
	$html 				= '';

	if($categorycode  != '') {
		
		$sectionData = $itemObject->getChildren($categorycode, 'TAG');
		
		if($sectionData) {
			for($i = 0; $i < count($sectionData); $i++) {
				$selected = isset($classifiedData) && $classifiedData['section_code'] == $sectionData[$i]['item_code'] ? 'selected' : '';
				$html .= "<option value='".$sectionData[$i]['item_code']."' $selected >".$sectionData[$i]['item_name']."</option>";
			}
		} else {
			$html .= "<option value=''>No sections for this category</option>";
		}
	} else {
		$html .= "<option value=''>No sections for this category</option>";
	}
	
	echo $html;
	exit;
}

/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;

	if(isset($_POST['classified_subject']) && trim($_POST['classified_subject']) == '') {
		$errorArray['classified_subject'] = 'Subject is required';
		$formValid = false;		
	}
	
	if(isset($_POST['section_code']) && trim($_POST['section_code']) == '') {
		$errorArray['section_code'] = 'Section is required';
		$formValid = false;		
	}
	
	if(isset($_POST['type_code']) && trim($_POST['type_code']) == '') {
		$errorArray['type_code'] = 'Type is required';
		$formValid = false;		
	}

	if(isset($_POST['areapost_code']) && trim($_POST['areapost_code']) == '') {
		$errorArray['areapost_code'] = 'Location is required';
		$formValid = false;		
	}

	if(isset($_POST['classified_expire']) && trim($_POST['classified_expire']) == '') {
		$errorArray['classified_expire'] = 'Expiry date is required';
		$formValid = false;		
	} else {
		if($classifiedObject->validateDate(trim($_POST['classified_expire'])) == '') {
			$errorArray['classified_expire'] = 'Valid expiry date is required';
			$formValid = false;				
		}
	}

	if(isset($_POST['classified_message']) && strlen(trim($_POST['classified_message'])) < 20) {
		$errorArray['classified_message'] = 'Messge is required and must be more than 20 characters at least.';
		$formValid = false;		
	}
	
	if(count($errorArray) == 0 && $formValid == true) {

		$data 									= array();				
		$data['classified_subject']		= trim($_POST['classified_subject']);		
		$data['section_code']				= trim($_POST['section_code']);
		$data['type_code']					= trim($_POST['type_code']);
		$data['member_code']			= trim($_POST['member_code']);
		$data['areapost_code']			= trim($_POST['areapost_code']);
		$data['classified_expire']		= trim($_POST['classified_expire']);
		$data['classified_message']	= trim($_POST['classified_message']);
		
		if(isset($classifiedData)) {
			$where		= $classifiedObject->getAdapter()->quoteInto('classified_code = ?', $classifiedData['classified_code']);
			$success	= $classifiedObject->update($data, $where);
			$success	= $classifiedData['classified_code'];
		} else {
			$success = $classifiedObject->insert($data);
		}
		
		header('Location: /classified/media.php?code='.$success);	
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

$itemPairs = $itemObject->selectParents('CLASSIFIED');
if($itemPairs) $smarty->assign('itemPairs', $itemPairs);

$categoryPairs = $itemObject->selectType('CATEGORYCLASS');
if($categoryPairs) $smarty->assign('categoryPairs', $categoryPairs);

$smarty->display('classified/details.tpl');

?>