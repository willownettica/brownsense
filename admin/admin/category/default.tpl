<!DOCitem html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
		<h2 class="content-header-title">Category</h2>
		<ol class="breadcrumb">
			<li><a href="/">Home</a></li>
			<li><a href="/">Administration</a></li>
			<li><a href="/">Category</a></li>
			<li class="active">View</li>
		</ol>
	</div>	  
	<div class="row">
        <div class="col-md-12">
		<button class="btn btn-secondary fr" item="button" onclick="link('/admin/category/details.php'); return false;">Add a new item</button><br/ ><br />
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-hand-o-up"></i>
                item List
              </h3>
            </div> <!-- /.portlet-header -->			  
            <div class="portlet-content">           			
              <div class="table-responsive">
              <table 
                class="table table-striped table-bordered table-hover table-highlight" 
                data-provide="datatable" 
                data-display-rows="30"
                data-info="true"
                data-search="true"
                data-length-change="false"
                data-paginate="true"
              >
					<thead>
						<tr>
							<th data-sortable="true">Added</th>
							<th data-sortable="true">Name</th>
							<th></th>							
						</tr>
					</thead>							
				   <tbody>
				  {foreach from=$itemData item=item}
				  <tr>
					<td>{$item.item_added|date_format}</td>
					<td align="left"><a href="/admin/category/details.php?code={$item.item_code}">{$item.item_name}</a></td>
					<td>
						<button onclick="deleteModal('{$item.item_code}', '', 'default'); return false;" class="btn btn-danger">Delete</button>
					</td>
				  </tr>
				{foreachelse}
				<tr><td colspan="6">No categories have been added yet</td></tr>
				  {/foreach}
				  </tbody>
                </table>
              </div> <!-- /.table-responsive -->
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col -->	
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
</html>