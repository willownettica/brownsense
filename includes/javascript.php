<!-- jQuery -->
<script src="/library/javascript/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/library/javascript/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="/library/javascript/slick/slick.min.js"></script>
<script src="/library/javascript/socialShare.min.js"></script>
<script src="/library/javascript/jquery.simpleWeather.min.js"></script>
<script src="/library/javascript/lity/lity.min.js"></script>

<!-- Theme JavaScript -->
<script src="/library/javascript/main.js"></script>

<!-- Mailchimp Js -->
<script src="/library/javascript/mc/jquery.ketchup.all.min.js"></script>
<script src="/library/javascript/mc/main.js"></script>
<script src="/library/javascript/jquery.magnific-popup.min.js"></script>
<script src="/library/javascript/jquery.justifiedGallery.min.js"></script>	