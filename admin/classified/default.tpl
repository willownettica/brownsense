<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
		<h2 class="content-header-title">Classifieds</h2>
		<ol class="breadcrumb">
		<li><a href="/">Home</a></li>
		<li class="active"><a href="#">Classifieds</a></li>
		</ol>
	</div>	  
	<div class="row">
        <div class="col-md-12">
		<button class="btn btn-secondary fr" type="button" onclick="link('/classified/details.php'); return false;">Add a new Classified</button><br/ ><br />
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-hand-o-up"></i>
                Classifieds
              </h3>
            </div> <!-- /.portlet-header -->	
			<div class="portlet-content">
			<div class="form-group">
			  <label for="filter_search">Search for classified</label>
			  <input type="text" id="filter_search" name="filter_search" class="form-control" value="" />
			</div>	
				<div class="form-group">
				  <label for="filter_item">Select by vetting outcome</label>
				  <select id="filter_item" name="filter_item" class="form-control">
					<option value=""> --- Show All --- </option>
					<option value="-1"> Unvetted </option>
					{html_options options=$itemvetData}
				  </select>
				</div>				
			<div class="form-group">
				<button type="button" onclick="getRecords(); return false;" class="btn btn-primary">Search</button>
				<button type="button" onclick="csv(); return false;" class="btn">Download CSV</button>
			</div>
			</div>
            <div class="portlet-content">
				<div id="tableContent" align="center">
					<!-- Start Content Table -->
					<div class="content_table">
						<img src="/images/ajax_loader.gif" />
					 </div>
					 <!-- End Content Table -->	
				</div>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col -->	
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
{literal}
<script type="text/javascript">
$(document).ready(function() {
	getRecords();
});

function getRecords() {
	var html				= '';
	var filter_search	= $('#filter_search').val() != 'undefined' ? $('#filter_search').val() : '';
	var filter_item		= $('#filter_item').val() != 'undefined' ? $('#filter_item').val() : '';
	
	/* Clear table contants first. */			
	$('#tableContent').html('');
	
	$('#tableContent').html('<table cellpadding="0" cellspacing="0" width="100%" border="0" class="display" id="dataTable"><thead><tr><th>Days passed</th><th>Status</th><th></th><th>Subject</th><th></th><th></th></tr></thead><tbody id="classifiedbody"><tr><td colspan="6" align="center"><img src="/images/ajax_loader.gif" /></td></tr></tbody></table>');	
		
	oTable = $('#dataTable').dataTable({
		"bJQueryUI": true,
		"aoColumns" : [
			{ sWidth: "10%" },
			{ sWidth: "10%" },
			{ sWidth: "8%" },
			{ sWidth: "40%" },
			{ sWidth: "5%" },
			{ sWidth: "5%" }
		],
		"sPaginationType": "full_numbers",							
		"bSort": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayStart": 0,
		"iDisplayLength": 20,				
		"bLengthChange": false,									
		"bProcessing": true,
		"bServerSide": true,		
		"sAjaxSource": "?action=search&csv=0&filter_search="+filter_search+"&filter_item="+filter_item,
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			$.getJSON( sSource, aoData, function (json) {
				if (json.result === false) {
					$('#classifiedbody').html('<tr><td colspan="6" align="center">No results</td></tr>');											
				}
				fnCallback(json)
			});
		},
		"fnDrawCallback": function(){
		}
	});
	return false;
}

function csv() {
	var filter_search 		= $('#filter_search').val() != 'undefined' ? $('#filter_search').val() : '';
	var filter_item			= $('#filter_item').val() != 'undefined' ? $('#filter_item').val() : '';
	window.location.href 	= "/classified/?action=search&csv=1&filter_search="+filter_search+"&filter_item="+filter_item;
	return false;
}
</script>
{/literal}
</body>
</html>