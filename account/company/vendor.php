<?php 
/* Add this on all pages on top. */
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/auth.php';

/* objects. */
require_once 'class/market.php';
require_once 'class/company.php';
require_once 'class/branch.php';

$marketObject 		= new class_market();
$companyObject 	= new class_company();
$branchObject 		= new class_branch();


if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$companyData = $companyObject->getByMember($zfsession->identity, $code);

	if(!$companyData) {
		header('Location: /account/company/');
		exit;
	}
} else {
	header('Location: /account/company/');
	exit;
}

$marketData = $marketObject->getByCompany($code);

/* Check posted data. */
if(isset($_REQUEST['market'])) {

	$errorArray					= array();
	$errorArray['message'] 	= array();
	$data 							= array();
	$branch						= array();
	$period							= array();

	if(isset($_POST['branch_code'])) {
		$errorArray['message'][] = 'Please select a branch';
	} else if(trim($_REQUEST['branch_code']) == '') {
		$errorArray['message'][] = 'Please select a branch';
	} else {
		$branch = explode(",",$_REQUEST['branch_code']);
		
		if(count($branch) == 0) {
			$errorArray['message'][] = 'Please select a branch';	
		}
	}
	
	if(isset($_POST['period_code'])) {
		$errorArray['message'][] = 'Please select a market month';
	} else if(trim($_REQUEST['period_code']) == '') {
		$errorArray['message'][] = 'Please select a market month';
	} else {
		$period = explode(",",$_REQUEST['period_code']);
		
		if(count($period) == 0) {
			$errorArray['message'][] = 'Please select a period';	
		}
	}
	
	if(count($errorArray['message']) == 0) {
		/* Delete everything first. */
		$where 	= array();
		$where[] 	= $marketObject->getAdapter()->quoteInto('company_code = ?', $code);
		$marketObject->remove($where);
		/* Add the new branches. */
		for($b = 0; $b < count($branch); $b++) {
			for($c = 0; $c < count($period); $c++) {
				$check = $marketObject->exists($companyData['company_code'], $branch[$b], $period[$c]);
				if(!$check) {
					$data 							= array();
					$data['company_code']	= $companyData['company_code'];
					$data['branch_code']		= $branch[$b];
					$data['period_code']		= $period[$c];

					$marketObject->insert($data);
				}
			}
		}
	}
	
	/* if we are here there are errors. */
	if(count($errorArray['message']) > 0) {
		$errorArray['message'] = implode("<br />",$errorArray['message']);
	} else {
		$errorArray['message'] = '';
	}
	
	echo json_encode($errorArray);
	exit;
}

$periodList = $marketObject->getPeriods($companyData['company_code']);

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<link rel="stylesheet" href="/css/jquery-ui-1.12.1.css" />
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/account/"><?php echo $zfsession->memberData['member_name']; ?></a></li>
						<li><a href="/account/company/">My Companies</a></li>
						<li><a href="/account/company/details.php?code<?php echo $companyData['company_code']; ?>"><?php echo $companyData['company_name']; ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-8">
				<h3 class="heading-1"><span><?php echo $companyData['company_name']; ?></span></h3>	
				<ul class="nav nav-tabs">
					<li><a href="/account/company/details.php?code=<?php echo $companyData['company_code']; ?>">Details</a></li>
					<li><a href="/account/company/tags.php?code=<?php echo $companyData['company_code']; ?>">Products / Services</a></li>
					<li><a href="/account/company/media.php?code=<?php echo $companyData['company_code']; ?>">Images</a></li>
					<li><a href="/account/company/document.php?code=<?php echo $companyData['company_code']; ?>">Documents</a></li>
					<li class="active"><a href="/account/company/vendor.php?code=<?php echo $companyData['company_code']; ?>">Become a vendor</a></li>
					<li><a href="/account/company/enquiry.php?code=<?php echo $companyData['company_code']; ?>">Enquiries</a></li>
				</ul>
				<p style="clear: both;">&nbsp;&nbsp;&nbsp;&nbsp;</p>
				<form class="post-comment-form"action="#" method="POST" >		
					<p>Below is where you select branches you would like your company <span  class="text_brown"><?php echo $companyData['company_name']; ?></span> to be vendor in, this is to simply notify our administrators that you are interested so you do not have to apply every month.</p> 
					<br />
					<label>Select branches you would like to be a vendor in:</label>
					<br />
					<?php echo $branchObject->table(4, 7); ?>  	
					<p class="clear">&nbsp;&nbsp;&nbsp;</p>
					<label class="text_red">Months you are applying for</label><br />
					<?php 
					$months = array();
					array_push($months, date('F')) ;
					for ($i=1; $i<=  12 - date('m'); $i++ ){
						array_push($months, date('F', strtotime("+$i months"))) ;
					}
					for($i = 0; $i < count($months); $i++) {
						$month = date('m', strtotime($months[$i])).'-'.date('Y');
					?>
					<input value="<?php echo $month; ?>" <?php if($periodList != false && in_array($month, $periodList)) { echo 'checked'; } ?> class="period_code" name="period_code[]" id="period_code[]" type="checkbox" />&nbsp;&nbsp;&nbsp;<?php echo $months[$i].' '.date('Y'); ?>&nbsp;&nbsp;&nbsp; <br />
					<?php 
					}
					?><br />
					<div class="alert alert-success market_success" style="display: none; clear: both;">
						<strong>Well done!</strong><br />You have successfully applied to be a vendor in the selected branches with the selected company/companies. The administrators will notify you each month, for the next six months.
					</div>
					<div class="alert alert-danger market_fail" style="display: none; clear: both;">
						<strong>Oh snap!</strong><br /><p id="market_fail_message"></p>
					</div>
					<div class="alert alert-info market_submitting" style="display: none; clear: both;">
						<strong>Heads up!</strong> Submitting, please wait....
					</div>
					<button type="button" onclick="marketApply(); return false;">Apply</button>
					<br /><br />
				</form>				
			</div>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
<script type="text/javascript">
	function marketApply() {

		$('.market_submitting').show();
		$('.market_submitting').css('visibility', 'visible');
		$('.market_success').hide();
		$('.market_success').css('visibility', 'hidden');
		$('.market_fail').hide();
		$('.market_fail').css('visibility', 'hidden');
		
		var branch_code = []; 
		var inputElements = document.getElementsByClassName('market_branch_code');
		for(var i=0; inputElements[i]; ++i){
			  if(inputElements[i].checked){
				   branch_code.push(inputElements[i].value);
			  }
		}

		var period_code = []; 
		var inputElements = document.getElementsByClassName('period_code');
		for(var i=0; inputElements[i]; ++i){
			  if(inputElements[i].checked){
				   period_code.push(inputElements[i].value);
			  }
		}
		
		$.ajax({
			type: "GET",
			url: "?market=1",
			data: "code=<?php echo $companyData['company_code']; ?>&branch_code="+branch_code+"&period_code="+period_code,
			dataType: "json",
			success: function(data){
				if(data.message == '') {
					$('.market_success').show();	
					$('.market_success').css('visibility', 'visible');
					$('.market_fail').hide();
					$('.market_fail').css('visibility', 'hidden');
				} else {
					$('.market_fail').show();
					$('.market_fail').css('visibility', 'visible');
					$('.market_success').hide();
					$('.market_success').css('visibility', 'hidden');
					$('#market_fail_message').html(data.message);
				}
				$('.market_submitting').hide();
				$('.market_submitting').css('visibility', 'hidden');
			}
		});
		
		return false;
	}
</script>
</body>
</html>
