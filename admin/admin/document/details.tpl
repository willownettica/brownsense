<!DOCitem html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
	<h2 class="content-header-title">Categories</h2>
	<ol class="breadcrumb">
		<li><a href="/">Home</a></li>
		<li><a href="/">Administration</a></li>
		<li><a href="/">Document</a></li>
		<li><a href="#">{if isset($itemData)}{$itemData.item_name}{else}Add a category{/if}</a></li>
		<li class="active">Details</li>
	</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
				{if isset($itemData)}{$itemData.item_name}{else}Add a category{/if}
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="/admin/document/details.php{if isset($itemData)}?code={$itemData.item_code}{/if}" method="POST" data-validate="parsley" class="form parsley-form">
                <div class="form-group">
					<label for="item_name">Name</label>
					<input type="text" id="item_name" name="item_name" class="form-control" data-required="true" value="{$itemData.item_name}" />
					{if isset($errorArray.item_name)}<span class="error">{$errorArray.item_name}</span>{/if}					  
                </div>
                <div class="form-group">
					<label for="item_config_reference">Type</label>
					<input type="text" id="item_config_reference" name="item_config_reference" class="form-control" data-required="true" value="{$itemData.item_config_reference}" />
					{if isset($errorArray.item_config_reference)}<span class="error">{$errorArray.item_config_reference}</span>{/if}					  
                </div>				
                <div class="form-group">
					<label for="item_config_date">Config: Requires Date / Date Range</label>
					<select id="item_config_date" name="item_config_date" class="form-control">
						<option value="0" {if $itemData.item_config_date eq '0'}selected{/if}> No </option>
						<option value="1" {if $itemData.item_config_date eq '1'}selected{/if}> Yes </option>
					</select>
					{if isset($errorArray.item_config_date)}<span class="error">{$errorArray.item_config_date}</span>{/if}					  
                </div>					
                <div class="form-group"><button type="submit" class="btn btn-primary">Validate and Submit</button></div>
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col -->
		<div class="col-sm-3">
			<div class="list-group">  
				<a class="list-group-item" href="/admin/document">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				{if isset($itemData)}					
				<a class="list-group-item" href="#">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>				
				{/if}
			</div> <!-- /.list-group -->
        </div>			
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
</html>
