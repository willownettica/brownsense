<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/*** Standard includes */
require_once 'config/smarty.php';
/* Check for login */
require_once 'includes/auth.php';

/* objects. */
require_once 'class/advert.php';

$advertObject = new class_advert();

if(isset($_GET['delete_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success					= NULL;
	$code						= trim($_GET['delete_code']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {
		$data	= array();
		$data['advert_deleted'] = 1;
		
		$where 	= array();
		$where[] 	= $advertObject->getAdapter()->quoteInto('advert_code = ?', $code);
		$success	= $advertObject->update($data, $where);	
		
		if(is_numeric($success) && $success > 0) {
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	
	echo json_encode($errorArray);
	exit;
}

$advertData = $advertObject->getAll();

if($advertData) $smarty->assign('advertData', $advertData);

$smarty->display('advert/product/default.tpl');
?>