<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

error_reporting(E_ALL);

/**
 * Standard includes
 */
require_once 'config/database.php';
require_once 'includes/iauth.php';

global $zfsession;

// Clear the identity from the session
$zfsession = $zfsession->identity = $zfsession->memberData  = null;
unset($zfsession->identity, $zfsession->memberData, $zfsession);

//redirect to login page
header('Location: /');
exit;
?>