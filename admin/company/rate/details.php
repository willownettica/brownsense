<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/* Check for login */
require_once 'includes/auth.php';

/* objects. */
require_once 'class/company.php';
require_once 'class/vet.php';
require_once 'class/item.php';
require_once 'class/_comm.php';

$companyObject	= new class_company();
$vetObject 			= new class_vet();
$itemObject 			= new class_item();
$commObject 			= new class_comm();

if (isset($_GET['code']) && trim($_GET['code']) != '') {

	$code = trim($_GET['code']);
	
	$companyData = $companyObject->getByCode($code);

	if($companyData) {

		$smarty->assign('companyData', $companyData);
		
		$vetData = $vetObject->getAll('COMPANY', $companyData['company_code']);
		if($vetData) $smarty->assign('vetData', $vetData);

	} else {
		header('Location: /company/list/');
		exit;
	}
}

/* Check posted data. */
if(count($_POST) > 0 && isset($_POST['areapost_code'])) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;
	
	if(isset($_POST['member_code']) && trim($_POST['member_code']) == '') {
		$errorArray['member_code'] = 'Member is required';
		$formValid = false;		
	}
	
	if(isset($_POST['areapost_code']) && trim($_POST['areapost_code']) == '') {
		$errorArray['areapost_code'] = 'Area is required';
		$formValid = false;		
	}

	if(isset($_POST['company_name']) && trim($_POST['company_name']) == '') {
		$errorArray['company_name'] = 'Name is required'; 
		$formValid = false;		
	}

	if(isset($_POST['company_contact_name']) && trim($_POST['company_contact_name']) == '') {
		$errorArray['company_contact_name'] = 'Contact name is required'; 
		$formValid = false;		
	}

	if(isset($_POST['company_contact_email']) && trim($_POST['company_contact_email']) == '') {
		$errorArray['company_contact_email'] = 'Contact email is required'; 
		$formValid = false;		
	} else if($companyObject->validateEmail(trim($_POST['company_contact_email'])) == ''){
		$errorArray['company_contact_email'] = 'Contact email needs to be valid'; 
		$formValid = false;		
	}
	
	if(isset($_POST['company_contact_number']) && trim($_POST['company_contact_number']) == '') {
		$errorArray['company_contact_number'] = 'Contact number is required';
		$formValid = false;		
	}

	if(count($errorArray) == 0 && $formValid == true) {
	
		$data 	= array();				
		$data['member_code']							= trim($_POST['member_code']);		
		$data['areapost_code']							= trim($_POST['areapost_code']);	
		$data['company_name']							= trim($_POST['company_name']);	
		$data['company_registration_name']		= trim($_POST['company_registration_name']);	
		$data['company_registration_number']	= trim($_POST['company_registration_number']);	
		$data['company_registration_vat']			= trim($_POST['company_registration_vat']);
		$data['company_social_facebook']			= trim($_POST['company_social_facebook']);
		$data['company_social_twitter']				= trim($_POST['company_social_twitter']);
		$data['company_social_instagram']			= trim($_POST['company_social_instagram']);
		$data['company_social_linkedin']			= trim($_POST['company_social_linkedin']);
		$data['company_website']						= trim($_POST['company_website']);
		$data['company_address_physical']			= trim($_POST['company_address_physical']);
		$data['company_address_postal']			= trim($_POST['company_address_postal']);
		$data['company_contact_name']				= trim($_POST['company_contact_name']);
		$data['company_contact_email']				= trim($_POST['company_contact_email']);
		$data['company_contact_number']			= trim($_POST['company_contact_number']);
		
		if(isset($companyData)) {
			$where		= $companyObject->getAdapter()->quoteInto('company_code = ?', $companyData['company_code']);
			$success	= $companyObject->update($data, $where);
			$success	= $companyData['company_code'];
		} else {
			$success = $companyObject->insert($data);
		}

		header('Location: /company/list/media.php?code='.$success);	
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}


if(count($_POST) > 0 && isset($_POST['item_code'])) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;

	if(isset($_POST['item_code']) && trim($_POST['item_code']) == '') {
		$errorArray['item_code'] = 'Please select a status';
		$formValid = false;		
	}

	if(isset($_POST['vet_reason']) && trim($_POST['vet_reason']) == '') {
		$errorArray['vet_reason'] = 'Please add a reason.';
		$formValid = false;		
	}

	if(count($errorArray) == 0 && $formValid == true) {

		$data 	= array();				
		$data['item_code']			= trim($_POST['item_code']);		
		$data['vet_reason']		= trim($_POST['vet_reason']);	
		$data['vet_item_type']	= 'COMPANY';	
		$data['vet_item_code']	= $companyData['company_code'];	

		$success = $vetObject->insert($data);
		
		if($success) {

			$itemData = $itemObject->getByCode(trim($_POST['item_code']));

			if($itemData) {
				$data	= array();
				$data['company_active']	= $itemData['item_config_status'];

				$where		= $companyObject->getAdapter()->quoteInto('company_code = ?', $companyData['company_code']);
				$success	= $companyObject->update($data, $where);
				
				/* Email company for vetting. */
				$templateData = $commObject->_template->getTemplate('VET_NOTIFY', 'EMAIL');

				if($templateData) {

					$recipient	= array();
					$recipient['recipient_code'] 		= $companyData['company_code'];
					$recipient['recipient_name'] 		= $companyData['company_name'];
					$recipient['recipient_type'] 		= 'COMPANY';
					$recipient['recipient_email'] 		= $companyData['member_email'];
					$recipient['recipient_vet_type'] 	= 'company';
					$recipient['recipient_message'] 	= 'The company has been vetted and the result is: <span style="color: '.$itemData['item_config_color'].'"><b>'.$itemData['item_name'].'</b></span>, reasons being: <br /><br /><b><i>'.trim($_POST['vet_reason']).'</i></b><br /><br />. '.((int)$itemData['item_config_status'] == 1 ? '<b style="color: green;">The company will be visible on the website and searchable</b>' : '<b style="color: red">Please action accordingly</b>');

					$commObject->sendEmail($recipient, $templateData);
				}			
			}
		}

		header('Location: /company/list/details.php?code='.$companyData['company_code']);	
		exit;
	}
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

$itemData = $itemObject->selectParents('VET', 'COMPANY');
if($itemData) $smarty->assign('itemData', $itemData);

$smarty->display('company/list/details.tpl');

?>