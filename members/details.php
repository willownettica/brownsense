<?php 

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/auth.php'; 

/* objects. */
require_once 'class/member.php';
require_once 'class/media.php';
require_once 'class/enquiry.php';

$memberObject	= new class_member();
$mediaObject 		= new class_media();
$enquiryObject 		= new class_enquiry();

if (isset($_GET['code']) && trim($_GET['code']) != '') {

	$code = trim($_GET['code']);

	$memberData = $memberObject->getByCode($code);

	if(!$memberData) {
		header('Location: /');
		exit;
	}

	$mediaData		= $mediaObject->getByReference(array('IMAGE'), 'CLASSIFIED', $code);

} else {
	header('Location: /');
	exit;
}

if(isset($_GET['send_message'])) {

	$errorArray				= array();
	$errorArray['error']	= array();
	$errorArray['result']	= 1;	
	
	if(isset($_GET['enquiry_message']) && trim($_GET['enquiry_message']) == '') {
		$errorArray['error'][]	= 'Please add your message';
		$errorArray['result']	= 0;	
	}

	if(trim($zfsession->memberData['member_email']) == '') {
		$errorArray['error'][]	= 'You have not added an email address, please update your account by adding one.';
		$errorArray['result']	= 0;	
	}
	
	 if($zfsession->identity == $memberData['member_code']) { 
		$errorArray['error'][]	= 'You cannot enquir on your own post.';
		$errorArray['result']	= 0;	
	 }

	if(count($errorArray['error']) == 0 && $errorArray['result']	== 1) {

		$data 	= array();				
		$data['member_code']		= $zfsession->identity;	
		$data['enquiry_name']		= $zfsession->memberData['member_name'].' '.$zfsession->memberData['member_surname'];
		$data['enquiry_email']		= $zfsession->memberData['member_email'];	
		$data['enquiry_number']	= $zfsession->memberData['member_cellphone'];	
		$data['enquiry_message']	= trim($_GET['enquiry_message']);	
		$data['enquiry_item_type']	= 'CLASSIFIED';	
		$data['enquiry_item_code']	= $memberData['member_code'];

		$success							= $enquiryObject->insert($data);

		if(!$success) {
			$errorArray['error'][]	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}		
	}

	$errorArray['error'] = implode("<br />",$errorArray['error']);

	echo json_encode($errorArray);
	exit;
}
$memberLatest = $memberObject->similar($memberData['category_code'], $code);
?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<link rel="stylesheet" href="/css/jquery-ui-1.12.1.css">
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/members/">Memberss</a></li>
						<li><a href="/members/"><?php echo $memberData['category_name']; ?></a></li>						
						<li><a href="/members/"><?php echo $memberData['section_name']; ?></a></li>						
						<li><a href="#"><?php echo $memberData['member_subject']; ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-7 dual-posts padding-bottom-30">
				<div class="blog-single-head margin-top-25">
					<h2><?php echo $memberData['member_subject']; ?></h2>
					<div class="meta">
						<span class="author">by <?php echo $memberData['member_name']; ?> <?php echo $memberObject->humanTime($memberData['member_added']); ?></span>
					</div>
					<div style="background-color: #f5f5f5; padding: 10px;">
						<p><strong class="text_brown ">Type</strong><br /><?php echo $memberData['type_name']; ?></p><br />				
						<p><strong class="text_brown ">Province</strong><br /><?php echo $memberData['demarcation_name']; ?></p><br />	
						<p><strong class="text_brown ">Expiry date</strong><br /><?php echo date('d M Y', strtotime($memberData['member_expire'])); ?></p><br />	
					</div>
					<br >
					<?php echo $memberData['member_message']; ?>
				</div>
				<br />
				<?php if($mediaData) { ?>
				<h2>Media</h2>
				<br />
				<div class="row">
					<div class="col-md-12 col-sm-8">
						<div id="mygallery" class="popup-gallery">
							<?php foreach($mediaData as $media) { ?>
								<a href="<?php echo $media['media_path'].'big_'.$media['media_code'].$media['media_ext']; ?>" alt="<?php echo $memberData['member_subject']; ?>" title="<?php echo $memberData['member_subject']; ?>">
									<img src="<?php echo $media['media_path'].'tmb_'.$media['media_code'].$media['media_ext']; ?>" class="img-responsive" alt="<?php echo $memberData['member_subject']; ?>" title="<?php echo $memberData['member_subject']; ?>" />
								</a>
							<?php } ?>
						</div>
					</div>
				</div>	
				<?php } ?>
				<br />
			</div>
			<!-- // CATEGORY -->
			<aside class="col-md-4 col-sm-4">	
				<?php if($zfsession->identity != $memberData['member_code']) { ?>
				<div class="side-widget margin-bottom-30">
				<h3 class="heading-1"><span>Send enquiry</span></h3>
				<p>Are you interested in this member or would like to learn more about it? Please use the below form to send <strong class="text_brown "><?php echo $memberData['member_subject']; ?></strong> a message. The message will go to <strong class="text_brown "><?php echo $memberData['member_name']; ?></strong>.</p><br />
				<form class="post-comment-form"action="/members/<?php echo $memberData['member_code']; ?>" method="GET">		
					<div class="row">
						<div class="col-md-12">
							<label>Message</label>
							<textarea id="enquiry_message" name="enquiry_message" placeholder="Your message" style="margin-bottom: 5px;"></textarea>
							<span class="tiny_explain">Send this member a message.</span>
						</div>							
					</div>					
					<br />		
					<div class="clear: both"></div>
					<div class="alert alert-success enquiry_success" style="display: none; clear: both;">
						<strong>Well done!</strong><br />Your message has been successfully sent out to the business owners, they will get back to you as soon as possible.
					</div>
					<div class="alert alert-danger enquiry_fail" style="display: none; clear: both;">
						<strong>Oh snap!</strong><br /><p id="enquiry_fail_message"></p>
					</div>
					<div class="alert alert-info enquiry_submitting" style="display: none; clear: both;">
						<strong>Heads up!</strong> Submitting, please wait....
					</div>					
					<button type="button" onclick="sendMessage(); return false;">Send message</button>
				</form>
				</div>
				<?php } ?>
				<?php if($memberLatest) { ?>
				<div class="side-widget margin-bottom-30">
				<h3 class="heading-1"><span>Similar Posts</span></h3>
					<ul class="trending padding-top-30 padding-bottom-15">
						<?php foreach($memberLatest as $member) { ?>
						<li>
							<div class="thumb">
								<img src="<?php echo $member['media_path'].'tmb_'.$member['media_code'].$member['media_ext']; ?>" class="img-responsive" alt="<?php echo $member['member_subject']; ?>" title="<?php echo $member['member_subject']; ?>" />
							</div>
							<h4><a href="/members/<?php echo $member['member_code']; ?>"><?php echo $member['member_subject']; ?></a></h4>
							<div class="meta"><span class="date"><?php echo $memberObject->humanTime($member['member_added']); ?></span></div>
						</li>
						<?php } ?>
					</ul>
				</div>
				<?php } ?>
			</aside>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
<script type="text/javascript" language="javascript">

	$(document).ready(function() {
	
		jQuery("#mygallery").justifiedGallery({
			rowHeight : 150,
			captions : false, 
			margins : 6
		});
		
		$('.popup-gallery').magnificPopup({
			delegate: 'a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] 
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
				return item.el.attr('title') + '<small><?php echo $memberData['member_subject']; ?></small>';
				}
			}
		});
	});

	function sendMessage() {

		$('.enquiry_submitting').show();
		$('.enquiry_success').hide();
		$('.enquiry_fail').hide();
		
		$.ajax({
			type: "GET",
			url: "?send_message=1",
			data: "enquiry_message="+$('#enquiry_message').val(),
			dataType: "json",
			success: function(data){
				if(data.result == 1) {
					$('.enquiry_success').show();	
					$('.enquiry_fail').hide();
					document.getElementById("contact-form").reset();
				} else {
					$('.enquiry_fail').show();
					$('.enquiry_success').hide();
					$('#enquiry_fail_message').html(data.error);
				}
				$('.enquiry_submitting').hide();
			}
		});
		
		return false;
	}
</script>
</body>
</html>
