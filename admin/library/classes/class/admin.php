<?php

require_once '_comm.php';

//custom account item class as account table abstraction
class class_admin extends Zend_Db_Table_Abstract
{
   //declare table variables
    protected 	$_name 		= 'admin';
	protected 	$_primary		= 'admin_code';
	
	public 			$_comm		= null;
	
	function init()	{
		global $zfsession;
		$this->_comm	= new class_comm();
	}

	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 
	 public function insert(array $data) {
        // add a timestamp
        $data['admin_added']			= date('Y-m-d H:i:s');
		$data['admin_code']			= $this->createCode();
		$data['admin_password']	= $this->createPassword();

		$success = parent::insert($data);	

		if($success) {

			$mailData = $this->getByCode($success);

			if($mailData) {
				
				if($mailData['admin_email'] != '') {
					$templateData = $this->_comm->_template->getTemplate('REGISTRATION_ADMIN', 'EMAIL');

					if($templateData) {

						$recipient	= array();
						$recipient['recipient_code'] 		= $mailData['admin_code'];
						$recipient['recipient_name'] 		= $mailData['admin_name'];
						$recipient['recipient_cellphone'] 	= $mailData['admin_cellphone'];
						$recipient['recipient_type'] 		= 'ADMIN';
						$recipient['recipient_email'] 		= $mailData['admin_email'];
						$recipient['recipient_password'] 	= $mailData['admin_password'];

						$this->_comm->sendEmail($recipient, $templateData);
					}
				}
				
				if($mailData['admin_cellphone'] != '') {
					$templateData = null;
					$templateData = $this->_comm->_template->getTemplate('REGISTRATION_ADMIN', 'SMS');

					if($templateData) {

						$recipient	= array();
						$recipient['recipient_code'] 		= $mailData['admin_code'];
						$recipient['recipient_name'] 		= $mailData['admin_name'];
						$recipient['recipient_type'] 			= 'ADMIN';
						$recipient['recipient_cellphone'] 	= $mailData['admin_cellphone'];
						$recipient['recipient_email'] 		= $mailData['admin_email'];
						$recipient['recipient_password'] 	= $mailData['admin_password'];

						$this->_comm->sendSMS($recipient, $templateData);
					}
				}
			} else {
				return false;
			}
			
			return $success;
		} else {
			return false;
		}
		
    }

	
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where)
    {
        // add a timestamp
        $data['admin_updated']	= date('Y-m-d H:i:s');		

        return parent::update($data, $where);
    }
	
	/**
	 * get job by job admin Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code)
	{
		$select = $this->_db->select()	
					->from(array('admin' => 'admin'))
					->where('admin.admin_code = ?', $code)
					->where('admin.admin_deleted = 0')
					->limit(1); 

		$result = $this->_db->fetchRow($select);
		
		if($result) {
			require_once 'class/role.php';
			
			$roleObject = new class_role();
			
			$roleData = $roleObject->listRoles($result['admin_code'], 'PAGE');
			
			if($roleData) {
				$result['pages'] = $roleData;
			} else {
				$result['pages'] = array();
			}
		}
		return ($result == false) ? false : $result = $result;
	}	
	
	public function getAll()
	{

		$select = $this->_db->select()	
					->from(array('admin' => 'admin'))
					->where('admin.admin_deleted = 0')			
					->order('admin_name desc');						

		$result = $this->_db->fetchAll($select);
		return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get all admins ordered by column name
	 * example: $collection->getAlladmins('admin_title');
	 * @param string $order
     * @return object
	 */
	public function checkLogin($username = '', $password= '') {
		$select = $this->_db->select()	
						->from(array('admin' => 'admin'))	
						->where('admin_email = ?', $username)
						->where('admin_deleted = 0')
						->where('admin_password = ?', $password);

		$result = $this->_db->fetchRow($select);
		return ($result == false) ? false : $result = $result;
	}

	public function search($query, $limit = 20) {

		$select = $this->_db->select()
						->from(array('admin' => 'admin'), array('admin.admin_code', 'admin.admin_cellphone', 'admin.admin_email', 'admin_name', 'admin.admin_surname'))	
					   ->where("concat(admin.admin_name, admin_surname, admin_cellphone) like lower(?)", "%$query%")
					   ->limit($limit)
					   ->order("LOCATE('$query', concat(admin.admin_name, admin_surname, admin_cellphone))");

	   $result = $this->_db->fetchAll($select);	
        return ($result == false) ? false : $result = $result;					

	}

	/**
	 * get job by job participant Id
 	 * @param string job id
     * @return object
	 */
	public function getByCell($cell, $code = null) {
	
		if($code == null) {
			$select = $this->_db->select()	
						->from(array('admin' => 'admin'))						
						->where('admin_cellphone = ?', $cell)						
						->where('admin_deleted = 0')
						->limit(1);
       } else {
			$select = $this->_db->select()	
						->from(array('admin' => 'admin'))
						->where('admin_cellphone = ?', $cell)
						->where('admin_code != ?', $code)
						->where('admin_deleted = 0')
						->limit(1);		
	   }
	   
		$result = $this->_db->fetchRow($select);
		return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get job by job admin Id
 	 * @param string job id
     * @return object
	 */
	public function getByEmail($email, $code = null) {
	
		if($code == null) {
			$select = $this->_db->select()	
						->from(array('admin' => 'admin'))							
						->where('admin_email = ?', $email)
						->where('admin_deleted = 0')
						->limit(1);
       } else {
			$select = $this->_db->select()	
						->from(array('admin' => 'admin'))													
						->where('admin_email = ?', $email)
						->where('admin_code != ?', $code)
						->where('admin_deleted = 0')
						->limit(1);		
	   }

	   $result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get job by job admin Id
 	 * @param string job id
     * @return object
	 */
	public function getByIDNumber($idnumber, $code = null) {
	
		if($code == null) {
			$select = $this->_db->select()	
						->from(array('admin' => 'admin'))							
						->where('admin_idnumber = ?', $idnumber)
						->where('admin_deleted = 0')
						->limit(1);
       } else {
			$select = $this->_db->select()	
						->from(array('admin' => 'admin'))	
						->where('admin_idnumber = ?', $idnumber)
						->where('admin_code != ?', $code)
						->where('admin_deleted = 0')
						->limit(1);		
	   }

	   $result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}
	
	function createPassword() {
		/* New code. */
		$password = "";
		$codeAlphabet = "abcdefghigklmnopqrstuvwxyz";
		$codeAlphabet .= "0123456789";
		
		$count = strlen($codeAlphabet) - 1;
		
		for($i=0;$i<6;$i++){
			$password .= $codeAlphabet[rand(0,$count)];
		}
		
		return $password;
	}	
	
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getCode($code)
	{
		$select = $this->_db->select()	
						->from(array('admin' => 'admin'))	
						->where('admin_code = ?', $code)
						->limit(1);

		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;					   
	}
	
	function createCode() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '1234567890';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<1;$i++){
			$code .= $Number[rand(0,$count)];
		}
		
		/* Last alphabet. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Number[rand(0,$count)];
		}	
		
		/* First check if it exists or not. */
		$itemCheck = $this->getCode($code);
		
		if($itemCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}
	 
	public function validateEmail($string) {
		if(!filter_var($string, FILTER_VALIDATE_EMAIL)) {
			return '';
		} else {
			return trim($string);
		}
	}
	
	public function validateNumber($string) { 
		if(preg_match('/^0[0-9]{9}$/', $this->onlyNumber(trim($string)))) {
			return $this->onlyNumber(trim($string));
		} else {
			return '';
		}
	}
	
	public function validateDate($string) {
		if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $string)) {
			if(date('Y-m-d', strtotime($string)) != $string) {
				return '';
			} else {
				return $string;
			}
		} else {
			return '';
		}
	}
	
	public function validateIdentityNumber($string) {
		if(!preg_match('/^([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-1])([0-9])([0-9])$/', trim($string))) {
			return '';
		} else {			
			/* Do a luhn check. */
			if(!$this->LuhnCheck(trim($string))) {
				return '';					
			}
			return trim($string);
		}
		return '';
	}

	# LUHN check
	function LuhnCheck($number) {
		settype($number, 'string');
		$sumTable = array(array(0,1,2,3,4,5,6,7,8,9),array(0,2,4,6,8,1,3,5,7,9));
		$sum = 0;
		$flip = 0;
		for ($i = strlen($number) - 1; $i >= 0; $i--) {
			$sum += $sumTable[$flip++ & 0x1][$number[$i]];
		}
		return $sum % 10 === 0;
	}

	public function onlyNumber($string) {

		/* Remove some weird charactors that windows dont like. */
		$string = strtolower($string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace("é", "", $string);
		$string = str_replace("è", "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("ë", "", $string);	
		$string = str_replace('___' , '' , $string);
		$string = str_replace('__' , '' , $string);	 
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace("é", "", $string);
		$string = str_replace("è", "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("ë", "", $string);	
		$string = str_replace("â€“", "", $string);	
		$string = str_replace("â", "", $string);	
		$string = str_replace("€", "", $string);	
		$string = str_replace("“", "", $string);	
		$string = str_replace("#", "", $string);	
		$string = str_replace("$", "", $string);	
		$string = str_replace("@", "", $string);	
		$string = str_replace("!", "", $string);	
		$string = str_replace("&", "", $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(':' , '' , $string);		
		$string = str_replace('[' , '' , $string);		
		$string = str_replace(']' , '' , $string);		
		$string = str_replace('|' , '' , $string);		
		$string = str_replace('\\' , '' , $string);		
		$string = str_replace('%' , '' , $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);	
		$string = str_replace('-' , '' , $string);	
		$string = str_replace('+27' , '0' , $string);	
		$string = str_replace('(0)' , '' , $string);	
		
		$string = preg_replace('/^00/', '0', $string);
		$string = preg_replace('/^27/', '0', $string);
		
		$string = preg_replace('!\s+!',"", strip_tags($string));
		
		return $string;
				
	}
}
?>