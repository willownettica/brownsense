<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/* Check for login */
require_once 'includes/auth.php';

/* objects. */
require_once 'class/company.php';
require_once 'class/comment.php';
require_once 'class/item.php';
require_once 'class/vet.php';

$companyObject	= new class_company();
$commentObject 	= new class_comment();
$itemObject			= new class_item();
$vetObject			= new class_vet();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$companyData = $companyObject->getByCode($code);

	if($companyData) {
		
		$smarty->assign('companyData', $companyData);
		
		$commentData = $commentObject->getByTypeCode('COMPANY', $code);
		if($commentData) $smarty->assign('commentData', $commentData);

		$itemData = $itemObject->selectParents('VET', 'COMPANY');
		if($itemData) $smarty->assign('itemData', $itemData);

	} else {
		header('Location: /company/list/');
		exit;
	}
} else {
	header('Location: /company/list/');
	exit;
}

/* Check posted data. */
if(isset($_GET['rate_comment'])) {

	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success					= NULL;
	$commentcode			= trim($_GET['rate_comment']);
	
	if(isset($_GET['vetcode']) && trim($_GET['vetcode']) == '') {
		$errorArray['error']	= 'Please select the vetting.';
		$errorArray['result']	= 0;	
	}
	
	if(isset($_GET['ratereason']) && trim($_GET['ratereason']) == '') {
		$errorArray['error']	= 'Please add a reason for this selected vetting.';
		$errorArray['result']	= 0;	
	}
	
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {

		$data	= array();
		$data['item_code'] 		= trim($_GET['vetcode']);
		$data['vet_item_type'] 	= 'COMMENT';
		$data['vet_item_code'] 	= $commentcode;
		$data['vet_reason']		= trim($_GET['ratereason']);

		$success	= $vetObject->insert($data);	

		if($success) {
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not add vet, please try again.';
			$errorArray['result']	= 0;				
		}
	}

	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(isset($_GET['delete_code'])) {

	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success				= NULL;
	$linkcode				= trim($_GET['delete_code']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {	
		$data	= array();
		$data['link_deleted'] = 1;
		
		$where		= array();
		$where[]	= $linkObject->getAdapter()->quoteInto('link_code = ?', $linkcode);
		$where[]	= $linkObject->getAdapter()->quoteInto("link_child_type = ?", 'TAG');
		$where[]	= $linkObject->getAdapter()->quoteInto('link_parent_code = ?', $companyData['company_code']);
		$where[]	= $linkObject->getAdapter()->quoteInto("link_parent_type = ?", 'COMPANY');
		
		$success	= $linkObject->update($data, $where);	
		
		if(is_numeric($success) && $success > 0) {		
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}

	echo json_encode($errorArray);
	exit;
}

$parentSelect = $itemObject->selectParents('CATEGORY');
if($parentSelect) $smarty->assign('parentSelect', $parentSelect);

$smarty->display('company/list/comment.tpl');

?>