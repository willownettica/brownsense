<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');
/**
 * Standard includes
 */
require_once 'config/database.php';
require_once 'config/smarty.php';
require_once 'class/demarcation.php';

$demarcationObject	= new class_demarcation();
$list						= array();	

if(isset($_REQUEST['term'])) {

	$q							= strtolower(trim($_REQUEST['term'])); 
	$demarcationData		= $demarcationObject->search($q);
	
	if($demarcationData) {
		for($i = 0; $i < count($demarcationData); $i++) {
			$list[] = array(
				"id" 		=> $demarcationData[$i]["demarcation_id"],
				"label" 	=> $demarcationData[$i]["demarcation_name"],
				"value" 	=> $demarcationData[$i]["demarcation_name"]
			);			
		}	
	}
	
}

if(count($list) > 0) {
	echo json_encode($list); 
	exit;
} else {
	echo json_encode(array('id' => '', 'label' => 'no results')); 
	exit;
}
exit;
?>