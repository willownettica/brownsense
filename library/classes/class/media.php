<?php

//custom account item class as account table abstraction
class class_media extends Zend_Db_Table_Abstract
{
   //declare table variables
    protected $_name 			= 'media';
	protected $_primary 		= 'media_code';
	
	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 
	 public function insert(array $data)
    {
        // add a timestamp
        $data['media_added'] 	= date('Y-m-d H:i:s');        
		
		return parent::insert($data);
		
    }
	
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where)
    {
        // add a timestamp
        $data['media_updated'] = date('Y-m-d H:i:s');
        
        return parent::update($data, $where);
    }
	
	/**
	 * get job by job media Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code) {	
		$select = $this->_db->select()	
					->from(array('media' => 'media'))
					->joinLeft(array('item' => 'item'), 'item.item_code = media.item_code and item_deleted = 0')
					->where('media_deleted = 0')
					->where('media_code = ?', $code)
					->limit(1);
       
	   $result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;

	}
	
	public function getByReference(array $category, $type, $reference, $limit = 100000)
	{
		
		$string = "'".implode("','",$category)."'";
		
		$select = $this->_db->select()	
					->from(array('media' => 'media'))
					->joinLeft(array('item' => 'item'), 'item.item_code = media.item_code and item_deleted = 0')
					->where("media_category in($string)")
					->where("media_item_type = ?", $type)
					->where("media_item_code = ?", $reference)
					->where('media_deleted = 0')
					->order('media_added desc')
					->limit($limit);					

		$result = $this->_db->fetchAll($select);		
		return ($result == false) ? false : $result = $result;
	}
	
	public function getPrimary($category, $type, $reference) {
		
		$select = $this->_db->select()	
					->from(array('media' => 'media'))
					->joinLeft(array('item' => 'item'), 'item.item_code = media.item_code and item_deleted = 0')
					->where("media_category = ?", $category)
					->where("media_item_type = ?", $type)
					->where("media_item_code = ?", $reference)
					->where('media_deleted = 0 and media_primary = 1')
					->order('media_primary')	
					->limit(1);

	   $result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;	
	}
	
	public function updatePrimary($category, $type, $reference, $code) {
		
		$item = $this->getPrimary($category, $type, $reference);

		if($item) {
			$data = array();
			$where = array();
			$data['media_primary'] = 0;
			
			$where[]	= $this->getAdapter()->quoteInto('media_category = ?', $category);
			$where[]	= $this->getAdapter()->quoteInto('media_item_type = ?', $type);
			$where[]	= $this->getAdapter()->quoteInto('media_item_code = ?', $reference);
			$success	= $this->update($data, $where);				
		}

		$data = array();
		$data['media_primary'] = 1;

		$where		= $this->getAdapter()->quoteInto('media_code = ?', $code);
		$success	= $this->update($data, $where);

		return $success;
	}

	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getCode($reference)
	{
		$select = $this->_db->select()	
					->from(array('media' => 'media'))	
					->where('media_code = ?', $reference)
					->limit(1);

	   $result = $this->_db->fetchRow($select);	
        return ($result == false) ? false : $result = $result;					   
	}

	function createCode() {
		/* New reference. */
		$reference = "";
		$codeAlphabet = "123456789";

		$count = strlen($codeAlphabet) - 1;
		
		for($i=0;$i<12;$i++){
			$reference .= $codeAlphabet[rand(0,$count)];
		}
		
		/* First check if it exists or not. */
		$itemCheck = $this->getCode($reference);
		
		if($itemCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $reference;
		}
	}

	public function validateDate($string) {
		if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $string)) {
			if(date('Y-m-d', strtotime($string)) != $string) {
				return '';
			} else {
				return $string;
			}
		} else {
			return '';
		}
	}	
}
?>