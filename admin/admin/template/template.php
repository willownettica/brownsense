<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';
/* Check for login */
require_once 'includes/auth.php';
/* objects. */
require_once 'class/template.php';

$templateObject	= new class_template();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$templateData = $templateObject->getByCode($code);

	if($templateData) {
		$smarty->assign('templateData', $templateData);
	} else {
		header('Location: /admin/template/');
		exit;
	}
} else {
	header('Location: /admin/template/');
	exit;
}


$file = $templateData['template_file'];

$html = file_get_contents($file);
/*
$html = str_replace('[fullname]', 'Mzimhle Mosiwe', $html);
$html = str_replace('[name]', 'Mzimhle', $html);
$html = str_replace('[password]', 'd38di3d', $html);
$html = str_replace('[surname]', 'Mosiwe', $html);
$html = str_replace('[cellphone]', '0735640764', $html);
$html = str_replace('[address]', 'No. 213, Denvor Street, Cape Town', $html);
$html = str_replace('[email]', 'mzimhle@nolali.co.za', $html);
$html = str_replace('[template]', 'http://www.brownsense.co.za/media/templates/'.$templateData['template_code'].'/images/', $html);		
$html = str_replace('[date]', '01 January 2016', $html);
$html = str_replace('[broswer]', '<a style="text-decoration: none; color: black;" href="#">View mail on browser</a>', $html);
*/
echo $html;
exit;
?>