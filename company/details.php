<?php 

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/auth.php'; 

/* objects. */
require_once 'class/company.php';
require_once 'class/media.php';
require_once 'class/event.php';
require_once 'class/enquiry.php';
require_once 'class/comment.php';
require_once 'class/rate.php';
require_once 'class/File.php';
require_once 'class/link.php';

$companyObject	= new class_company();
$mediaObject 		= new class_media();
$eventObject 		= new class_event();
$enquiryObject 		= new class_enquiry();
$commentObject 	= new class_comment();
$rateObject 			= new class_rate();
$linkObject			= new class_link();
$fileObject				= new File(array('jpg', 'jpeg', 'png', 'gif', 'txt', 'pdf', 'doc', 'docx'));

if (isset($_GET['code']) && trim($_GET['code']) != '') {

	$code = trim($_GET['code']);

	$companyData = $companyObject->getByCode($code);

	if(!$companyData) {
		header('Location: /');
		exit;
	}

	$mediaData		= $mediaObject->getByReference(array('IMAGE'), 'COMPANY', $code);

	$eventData		= $eventObject->upcoming(3);

	$commentData 	= $commentObject->getByTypeCode('COMPANY', $code);

	if($commentData) {
		for($i = 0; $i < count($commentData); $i++) {
			$commentData[$i]['media'] = $mediaObject->getByReference(array('DOCUMENT'), 'COMMENT', $commentData[$i]['comment_code']);
		}
	}

} else {
	header('Location: /');
	exit;
}

if(isset($_GET['send_message'])) {

	$errorArray				= array();
	$errorArray['error']	= array();
	$errorArray['result']	= 1;	
	
	if(isset($_GET['enquiry_message']) && trim($_GET['enquiry_message']) == '') {
		$errorArray['error'][]	= 'Please add your message';
		$errorArray['result']	= 0;	
	}
	
	if(count($errorArray['error']) == 0 && $errorArray['result']	== 1) {

		$data 	= array();						
		$data['enquiry_name']		= $zfsession->memberData['member_name'].' '.$zfsession->memberData['member_surname'];
		$data['enquiry_email']		= $zfsession->memberData['member_email'];	
		$data['enquiry_number']	= $zfsession->memberData['member_cellphone'];	
		$data['enquiry_message']	= trim($_GET['enquiry_message']);	
		$data['enquiry_item_type']	= 'COMPANY';	
		$data['enquiry_item_code']	= $companyData['company_code'];

		$success							= $enquiryObject->insert($data);

		if(!$success) {
			$errorArray['error'][]	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}		
	}

	$errorArray['error'] = implode("<br />",$errorArray['error']);

	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(count($_POST) > 0 && isset($memberData)) {

	$errorArray					= array();
	$errorArray['message'] 	= array();
	$data 							= array();
	$formValid					= true;
	$success						= NULL;

	if(isset($_POST['rate_number']) && ((int)trim($_POST['rate_number']) == 0 or (int)trim($_POST['rate_number']) > 10)) {
		$errorArray['message'][] = 'Please select the rating which must be less than 10.';
		$formValid = false;
	}

	if(isset($_POST['comment_text']) && trim($_POST['comment_text']) == '') {
		$errorArray['message'][] = 'Please enter the description of work that was done by this company.';
		$formValid = false;
	}

	if(isset($_FILES['mediafiles']['name']) && count($_FILES['mediafiles']['name']) > 0) {
		for($i = 0; $i < count($_FILES['mediafiles']['name']); $i++) {
			/* Check validity of the CV. */
			if((int)$_FILES['mediafiles']['size'][$i] != 0 && trim($_FILES['mediafiles']['name'][$i]) != '') {
				/* Check if its the right file. */
				$ext = $fileObject->file_extention($_FILES['mediafiles']['name'][$i]); 

				if($ext != '') {
					$checkExt = $fileObject->getValidateExtention('mediafiles', $ext, $i);

					if(!$checkExt) {
						$errorArray['message'][] = 'Invalid file type something funny with the file format';
						$formValid = false;						
					} else if($_FILES['mediafiles']['size'][$i] > 1048576) { 
						$errorArray['message'][] = '<b>'.$_FILES['mediafiles']['name'][$i].'</b> is more than 1MB big.';
						$formValid = false;	
					}
				} else {
					$errorArray['message'][] = 'Invalid file type';
					$formValid = false;									
				}
			} else {			
				switch((int)$_FILES['mediafiles']['error'][$i]) {
					case 1 : $errorArray['message'][] = 'The uploaded file exceeds the maximum upload file size, should be less than 1M'; $formValid = false; break;
					case 2 : $errorArray['message'][] = 'File size exceeds the maximum file size'; $formValid = false; break;
					case 3 : $errorArray['message'][] = 'File was only partically uploaded, please try again'; $formValid = false; break;
					//case 4 : $errorArray['message'][] = 'No file was uploaded'; $formValid = false; break;
					case 6 : $errorArray['message'][] = 'Missing a temporary folder'; $formValid = false; break;
					case 7 : $errorArray['message'][] = 'Faild to write file to disk'; $formValid = false; break;
				}
			}
		}
	}

	if(count($errorArray['message']) == 0 && $formValid == true) {

		$data 	= array();
		$data['comment_item_type']	= 'COMPANY';
		$data['member_code']			= $zfsession->memberData['member_code'];
		$data['comment_item_code']	= $companyData['company_code'];		
		$data['comment_text']			= trim($_POST['comment_text']);
		$data['comment_name']			= $zfsession->memberData['member_name'].' '.$zfsession->memberData['member_surname'];
		$data['comment_number']		= $zfsession->memberData['member_cellphone'];
		$data['comment_email']			= $zfsession->memberData['member_email'];
		
		$success = $commentObject->insert($data);
		
		if($success) {
			/* Add rating. */
			$rate = array();
			$rate['rate_item_type'] 	= 'COMMENT';
			$rate['rate_item_code'] 	= $success;
			$rate['rate_number']		= trim($_POST['rate_number']);
		
			$ratesuccess = $rateObject->insert($rate);
			
			if($ratesuccess) {
				/* Add file if there are any. */
				if(isset($_FILES['mediafiles']) && count($_FILES['mediafiles']['name']) > 0) {
					for($i = 0; $i < count($_FILES['mediafiles']['name']); $i++) {
					
						if(isset($_FILES['mediafiles']['size'][$i])) {
						
							if((int)$_FILES['mediafiles']['size'][$i] != 0 && trim($_FILES['mediafiles']['name'][$i]) != '') {
								
								$media 						= array();
								$media['media_code']	= $mediaObject->createCode();
								
								$ext 			= strtolower($fileObject->file_extention($_FILES['mediafiles']['name'][$i]));					
								$filename	= $media['media_code'].'.'.$ext;
								$directory	= $_SERVER['DOCUMENT_ROOT'].'/media/comment/'.$success.'/';
								$file			= $directory.$filename;

								if(!is_dir($directory)) mkdir($directory, 0777, true);

								if(!file_put_contents($file, file_get_contents($_FILES['mediafiles']['tmp_name'][$i]))) {
									$errorArray['message'][] = 'Comment and rating have been added, but the files or one of them have not been.';
									$formValid = false;
								} else {
									
									$media['media_item_type'] 	= 'COMMENT';
									$media['media_item_code'] 	= $success;
									$media['media_category'] 		= 'DOCUMENT';
									$media['media_filename'] 		= $_FILES['mediafiles']['name'][$i];
									$media['media_path'] 			= '/media/comment/'.$success.'/'.$filename;
									$media['media_ext'] 				= $ext;
									
									$mediasuccess = $mediaObject->insert($media);
									
									if(!$mediasuccess) {
										$errorArray['message'][] = 'We could not save the file <b>'.$_FILES['mediafiles']['name'][$i].'</b>, please contact administrator.';
										$formValid = false;
									}
								}
							}
						}
					}
				}
				
				if(count($errorArray['message']) == 0 && $formValid == true) {
					/* All have been successfully uploaded. */
					$commentadded = 1;
				}
			} else {
				$errorArray['message'][] = 'We could not add the rate number, please try again.';
				$formValid = false;
			}			
		} else {
			$errorArray['message'][] = 'We could not add the comment, please try again.';
			$formValid = false;
		}
		
	}

	/* if we are here there are errors. */
	if(count($errorArray['message']) > 0) {
		$commentmessage = implode("<br />",$errorArray['message']);
	}
}

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<link rel="stylesheet" href="/css/jquery-ui-1.12.1.css">
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/companies/">Companies</a></li>						
						<li><a href="#"><?php echo $companyData['company_name']; ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-7 dual-posts padding-bottom-30">
				<div class="blog-single-head margin-top-25">
					<h2><?php echo $companyData['company_name']; ?></h2>
					<div class="meta">
						<span class="author">by <?php echo $companyData['member_name'].' '.$companyData['member_surname']; ?></span>
						<span class="date"><?php echo date('d F Y', strtotime($companyData['company_added'])); ?></span>
						<span class="comments"><?php if((int)$companyData['comment_count'] == 0) { ?>0 Comments<?php } else { echo $companyData['comment_count']; ?> Comments, rating of <span class="text_green"><?php echo $companyData['rate_avg'].'%';} ?></span></span>
					</div>
					<div style="background-color: #f5f5f5; padding: 20px;">
						<p><strong class="text_brown ">Services</strong><br /><?php echo $companyData['tag_name']; ?></p><br />
						<?php if($companyData['company_contact_name'] != '') { echo '<p><strong class="text_brown">Contact Person</strong><br />'.$companyData['company_contact_name'].'</p><br />'; } ?>
						<?php if($companyData['company_address_physical'] != '') { echo '<p><strong class="text_brown">Physical Address</strong><br />'.$companyData['company_address_physical'].'</p><br />'; } ?>
						<?php if($companyData['company_address_postal'] != '') { echo '<p><strong class="text_brown">Postal Address</strong><br />'.$companyData['company_address_postal'].'</p><br />'; } ?>
						<?php if($companyData['company_registration_name'] != '') { echo '<p><strong class="text_brown">CIPC Registration Name</strong><br />'.$companyData['company_registration_name'].'</p><br />'; } ?>
						<?php if($companyData['company_registration_number'] != '') { echo '<p><strong class="text_brown">CIPC Registration Number</strong><br />'.$companyData['company_registration_number'].'</p><br />'; } ?>
						<?php if($companyData['company_registration_vat'] != '') { echo '<p><strong class="text_brown">VAT Number</strong><br />'.$companyData['company_registration_vat'].'</p><br />'; } ?>	
					</div>
				</div>
				<br />
				<?php if(count($mediaData) > 0) { ?>
				<h2>Media</h2>
				<br />
				<div class="row">
					<div class="col-md-12 col-sm-8">
						<div id="mygallery" class="popup-gallery">
							<?php foreach($mediaData as $media) { ?>
								<a href="<?php echo $media['media_path'].'big_'.$media['media_code'].$media['media_ext']; ?>" alt="<?php echo $companyData['company_name']; ?>" title="<?php echo $companyData['company_name']; ?>">
									<img src="<?php echo $media['media_path'].'tmb_'.$media['media_code'].$media['media_ext']; ?>" class="img-responsive" alt="<?php echo $companyData['company_name']; ?>" title="<?php echo $companyData['company_name']; ?>" />
								</a>
							<?php } ?>
						</div>
					</div>
				</div>	
				<?php } ?>
				<br />
				<h2>Rate me</h2>
				<br />
				<p>If you have had an engagement or any form of business with <strong class="text_green"><?php echo $companyData['company_name']; ?></strong> with this business, please tell us about it us how it threated you.</p>
				<br />
				<form class="post-comment-form"action="/company/<?php echo $companyData['company_code']; ?>" method="POST" enctype="multipart/form-data">
					<div class="row">	
						<div class="col-md-12">
							<label>Experience with this company</label>
							<textarea id="comment_text" name="comment_text" style="margin-bottom: 5px;"></textarea>
							<span class="tiny_explain">Add a reason for your comment or rating.</span>
						</div>							
					</div>
					<br />
					<div class="row">
						<div class="col-md-6">
							<label>Rate</label>
							<select id="rate_number" name="rate_number" style="margin-bottom: 2px !important;">
								<option value=""> ----------------- </option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
							<span class="tiny_explain">Rate this company's service out of 10.</span>
						</div>
						<div class="col-md-6">
							<label>Upload relevent documents</label>
							<input type="file" name="mediafiles[]" id="mediafiles[]" multiple />
							<strong class="tiny_explain">Please only add the files ending with  .jpg, .jpeg, .png, .gif, .txt, .pdf, .doc, .docx also, your files need to be less than 1MB.</strong>
						</div>						
					</div>
					<?php if(isset($commentadded)) { ?>
					<br />
					<div class="alert alert-success" style=" clear: both;">
						<strong>Well done!</strong><br />Your message has been successfully added, it will first need to be vetted before it can be made public on this site. Please note that we may use your contact details to make sure all this information is correct.
					</div>
					<?php } ?>
					<?php if(isset($commentmessage)) { ?>
					<br />
					<div class="alert alert-danger" style=" clear: both;">
						<strong>Oh snap!</strong><br /><?php echo $commentmessage; ?>
					</div>
					<?php } else { echo '<br />'; } ?>						

					<button type="submit" value="1">Rate</button>
				</form>
				<hr />
				<h3 class="heading-1"><span><?php echo (int)$companyData['comment_count']; ?> Comments / <?php echo $companyData['rate_avg'].'%';  ?> Rating</span></h3>
				<?php if($commentData) { ?>
				<p class="text_brown">Below is a list of ratings that have been done <?php echo $companyData['company_name']; ?>.</p>
				<br />
				<div class="comments-list margin-bottom-20">
					<?php 
						for($i = 0; $i < count($commentData); $i++) { 
							$comment = $commentData[$i];
					?>				
					<div class="comment-content">
						<img src="https://graph.facebook.com/<?php echo $comment['social_facebook_id']; ?>/picture?width=49" />
						<h5><b><?php echo $comment['comment_name']; ?></b> <span class="pull-right"><?php echo $comment['rate_number'].' / 10'; ?></span></h5>
						<p><?php echo $comment['comment_text']; ?></p>
					</div>
					<?php } ?>			
				</div>
				
				<?php } else { ?>
					<div class="alert alert-danger">
						<strong>Oh snap!</strong><br />No ratings have been done for this company yet.
					</div>
				<?php } ?>
			</div>
			<!-- // CATEGORY -->
			<aside class="col-md-4 col-sm-4">			
				<div class="side-widget margin-bottom-30">
				<h3 class="heading-1"><span>Send enquiry</span></h3>
				<p>Are you interested in this company or would like to learn more about it? Please use the below form to send <strong class="text_brown "><?php echo $companyData['company_name']; ?></strong> a message.</p><br />
				<form class="post-comment-form"action="/company/<?php echo $companyData['company_code']; ?>" method="GET">		
					<div class="row">
						<div class="col-md-12">
							<label>Message</label>
							<textarea id="enquiry_message" name="enquiry_message" placeholder="Your message" style="margin-bottom: 5px;"></textarea>
							<span class="tiny_explain">Send this company a message.</span>
						</div>							
					</div>					
					<br />		
					<div class="clear: both"></div>
					<div class="alert alert-success enquiry_success" style="display: none; clear: both;">
						<strong>Well done!</strong><br />Your message has been successfully sent out to the business owners, they will get back to you as soon as possible.
					</div>
					<div class="alert alert-danger enquiry_fail" style="display: none; clear: both;">
						<strong>Oh snap!</strong><br /><p id="enquiry_fail_message"></p>
					</div>
					<div class="alert alert-info enquiry_submitting" style="display: none; clear: both;">
						<strong>Heads up!</strong> Submitting, please wait....
					</div>					
					<button type="button" onclick="sendMessage(); return false;">Send message</button>
				</form>
				</div>
			</aside>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
<script type="text/javascript" language="javascript">

	$(document).ready(function() {
	
		jQuery("#mygallery").justifiedGallery({
			rowHeight : 150,
			captions : false, 
			margins : 6
		});
		
		$('.popup-gallery').magnificPopup({
			delegate: 'a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] 
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
				return item.el.attr('title') + '<small><?php echo $companyData['company_name']; ?></small>';
				}
			}
		});
	});

	function sendMessage() {

		$('.enquiry_submitting').show();
		$('.enquiry_success').hide();
		$('.enquiry_fail').hide();
		
		$.ajax({
			type: "GET",
			url: "?send_message=1",
			data: "enquiry_message="+$('#enquiry_message').val(),
			dataType: "json",
			success: function(data){
				if(data.result == 1) {
					$('.enquiry_success').show();	
					$('.enquiry_fail').hide();
					$('#enquiry_fail_message').hide();
					document.getElementById("contact-form").reset();
				} else {
					$('.enquiry_fail').show();
					$('.enquiry_success').hide();
					$('#enquiry_fail_message').html(data.error);
				}
				$('.enquiry_submitting').hide();
			}
		});
		
		return false;
	}
</script>
</body>
</html>
