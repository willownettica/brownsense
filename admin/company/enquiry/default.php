<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/*** Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/**
 * Check for login
 */
require_once 'includes/auth.php';

require_once 'class/enquiry.php';

$enquiryObject		= new class_enquiry();

if(isset($_GET['action']) && trim($_GET['action']) == 'search') {

	$filter			= array();
	$csv				= 0;
	
	$start 		= isset($_REQUEST['iDisplayStart']) ? $_REQUEST['iDisplayStart'] : 0;
	$length 	= isset($_REQUEST['iDisplayLength']) ? $_REQUEST['iDisplayLength'] : 20;
	
	if(isset($_REQUEST['filter_search']) && trim($_REQUEST['filter_search']) != '') $filter[] = array('filter_search' => trim($_REQUEST['filter_search']));
	if(isset($_REQUEST['filter_csv']) && trim($_REQUEST['filter_csv']) != '') { $filter[] = array('filter_csv' => (int)trim($_REQUEST['filter_csv'])); $csv = (int)trim($_REQUEST['filter_csv']); }

	$enquiryData = $enquiryObject->getSearch('COMPANY', $start, $length, $filter);
	
	if(!$csv) {
		
		$enquirys = array();
		
		if($enquiryData) {
			for($i = 0; $i < count($enquiryData['records']); $i++) {
				$item = $enquiryData['records'][$i];
				$enquirys[$i] = array(
					date('d M Y', strtotime($item['enquiry_added'])),
					$item['company_name'],			
					($item['social_facebook_id'] == '' ? '<img src="/images/no-image.jpg" width="50px" />' : '<a href="http://facebook.com/'.$item['social_facebook_id'].'" target="_blank"><img src="https://graph.facebook.com/'.$item['social_facebook_id'].'/picture?width=300" width="50px" /></a>'),					
					$item['enquiry_name'].'<br />'.$item['enquiry_email'].'<br />'.$item['enquiry_number'],
					$item['enquiry_message']
				);
			}
		}

		if($enquiryData) {
			$response['sEcho'] = $_REQUEST['sEcho'];
			$response['iTotalRecords'] = $enquiryData['displayrecords'];		
			$response['iTotalDisplayRecords'] = $enquiryData['count'];
			$response['aaData']	= $enquirys;
		} else {
			$response['result'] 	= false;
			$response['message']	= 'There are no items to show.';			
		}

		echo json_encode($response);
		die();
	
	} else {
		
		$row = "Sent, Company, Name, Email, Number, Message\r\n";
		
		if($enquiryData) {

			for($i = 0; $i < count($enquiryData); $i++) {

				$item = $enquiryData[$i];

				$vendors[$i] = array(
						str_replace(',', ' and ', date('d M Y', strtotime($item['enquiry_added']))),
						str_replace(',', ' and ', $item['company_name']),
						str_replace(',', ' and ', $item['enquiry_name']),
						str_replace(',', ' and ', $item['enquiry_email']),
						str_replace(',', ' and ', $item['enquiry_number']),
						str_replace(',', ' and ', $item['enquiry_message']));
			}
			
			foreach ($vendors as $data) {
				$row .= implode(', ', $data)."\r\n";
			}			
		}
		
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=company_enquiry_report_".date('Y-m-d').".csv");
		Header("Cache-Control: private, must-revalidate" ); // HTTP/1.1
		Header("Pragma: private" ); // HTTP/1.0
	   
		print($row);
		exit;	
	}
}

$smarty->display('company/enquiry/default.tpl');

?>