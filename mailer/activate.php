<?php 
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');
/**
 * Standard includes
 */
require_once 'config/database.php';

$array = explode('/',$_SERVER['REQUEST_URI']);

if (isset($array[3]) && trim($array[3]) != '') {

	/* objects. */
	require_once 'class/member.php';

	$memberObject	= new class_member();

	$code = trim($array[3]);

	$tempData = $memberObject->getByHash($code, 0);

	if($tempData) {

		$data = array();
		$data['member_active'] = 1;

		$where		= $memberObject->getAdapter()->quoteInto('member_code = ?', $tempData['member_code']);
		$memberObject->update($data, $where);

	} else {
		header('Location: /');
		exit;
	}
} else {
	header('Location: /');
	exit;
}

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<link rel="stylesheet" href="/css/jquery-ui-1.12.1.css">
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/account/">Account</a></li>
						<li><a href="#">Account Activated</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8">		
				<form class="post-comment-form"action="#" method="POST" >	
					<div class="alert alert-success">
						<strong>Success!</strong>
						<br />
						<p>Good day, <?php echo $tempData['member_name']; ?>, you have successfully activated your account.</p>
					</div>						
					<br />
				</form>			
			</div>
			<aside class="col-md-4 col-sm-4">
			<div class="side-widget margin-bottom-30">
				<h3 class="heading-1"><span>Facebook profile picture</span></h3>
				<img src="https://graph.facebook.com/<?php echo $tempData['social_facebook_id']; ?>/picture?width=300">
				</div>
			</aside>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
<script type="text/javascript">
$(document).ready(function () {
    window.setTimeout(function () {
        location.href = "/";
    }, 3000);
});
</script>
</body>
</html>