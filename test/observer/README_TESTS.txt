These are the tests I conducted for the app before sending:

The app is an OOP app which has the sun that runs on a 24 hour basis, it has observers that do certain actions depending on the sun's time, these are:
1. SunBird
2. Garden

The garden will feed on the flowers if the sun is up, if the garden is open and if the flower they are feeding on has any nector left.
There are only two parameters that can be put in by potentially a user or the program itself which would control the whole app:
1. Number of flowers in the Garden object.
2. Telling the SunBird as to which garden to feed in.

Test questions:

1. Would the application run from any folder in a website?
2. Is the number entered by the program for the number of flowers validated?
3. Are we sure that the garden given to the sunbird is a garden?
4. Does the garden have enough flowers for the sunbird to feed in?

For the above, i tested from the index.php file:

Test 1
=======
Ran the application from different folders from anywhere on my local machine, rand perfectly.

Test 2
=======

$gardenObject		= new Garden();
On the above the default parameter for initializing this object is the number of flowers in the garden, the user or app needs to add an integer only.
Tested if a string or any other datatype is added instead, the return was a readable error message on the screen:

$gardenObject		= new Garden('NotAnInteger');
Returns: THE GARDEN NEEDS TO HAVE FLOWERS, HOW MANY ARE IN IT?. 

Test 3:
=======

$sugarBirdObject	= new sugarBird($gardenObject);

The sugar bird needs a garden to feed on, so on opening of the application, it is assigned one, but this object assigned to it can also be given to it by a user or program, 
so it needs some vetting.

Tested by adding a null, string and other datatypes, there was validation and it returned the appropriate return and not fatal error.

$sugarBirdObject	= new sugarBird('NotGarden');
Returned: SUNBIRD NEEDS A GARDEN TO FEED IN

Test 3:
=======

When I put in zero flowers, to tell the program that this garden does not have any flowers, the program simply does not run.

$gardenObject		= new Garden(0);
Returns: EXIT (0)

==================================================================================================

P.S. 
Apologies for taking long in responding, I only started with this app on the second day as I had to do some work from home which was urgent.
Thank you for the consideration.
