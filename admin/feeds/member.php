<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');
/** 
 * Standard includes
 */
require_once 'config/database.php';
require_once 'config/smarty.php';
require_once 'class/member.php';

$memberObject	= new class_member();
$list						= array();

if(isset($_REQUEST['term'])) {
	
	$q				= strtolower(trim($_REQUEST['term'])); 
	$memberData		= $memberObject->search($q);	
	
	if($memberData) {
		for($i = 0; $i < count($memberData); $i++) {
			$list[] = array(
				"id" 		=> $memberData[$i]["member_code"],
				"label" 	=> $memberData[$i]["member_name"].' '.$memberData[$i]["member_surname"].' ( '.$memberData[$i]["member_email"].' )',
				"value" 	=> $memberData[$i]["member_name"].' '.$memberData[$i]["member_surname"].' ( '.$memberData[$i]["member_email"].' )'
			);			
		}	
	}
}

if(count($list) > 0) {

	echo json_encode($list); 
	exit;
	
} else {

	echo json_encode(array('id' => '', 'label' => 'no results')); 
	exit;
	
}

exit;

?>