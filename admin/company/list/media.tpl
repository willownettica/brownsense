<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]--> 
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8"> 
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
		<h2 class="content-header-title">Company</h2>
		<ol class="breadcrumb">
		<li><a href="/">Home</a></li>
		<li><a href="/company/list/">Company</a></li>
		<li><a href="/company/list/details.php?code={$companyData.company_code}">{$companyData.company_name}</a></li>
		<li class="active">Media</li>
		</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					Media list
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="/company/list/media.php?code={$companyData.company_code}" method="POST" data-validate="parsley" class="form parsley-form" enctype="multipart/form-data">
				<p>Below is a list of your media.</p>
				<table class="table table-bordered">	
					<thead>
						<tr>
							<td>Preview</td>
							<td>Descripton</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						{foreach from=$mediaData item=item}
							<tr>
								<td>
									<a href="http://www.brownsense.co.za{$item.media_path}/tmb_{$item.media_code}{$item.media_ext}" target="_blank">
										<img src="http://www.brownsense.co.za{$item.media_path}/tmb_{$item.media_code}{$item.media_ext}" width="120" />
									</a>
								</td>
								<td>
									<textarea id="media_description_{$item.media_code}" name="media_description_{$item.media_code}" rows="3" class="form-control">
										{$item.media_description}
									</textarea>
								</td>
								<td>
									<button value="Update" class="btn btn-danger" onclick="updateRecord('{$item.media_code}'); return false;">Update</button>
									<br /><br />							
									{if $item.media_primary eq '0'}
										<button value="Make Primary" class="btn btn-danger" onclick="statusitemModal('{$companyData.company_code}', '{$item.media_code}', '1' ); return false;">Primary</button>
									{else}
									<b>Primary</b>
									{/if}				
									<br /><br />
									{if $item.media_primary eq '0'}
										<button value="Delete" class="btn btn-danger" onclick="deleteitemModal('{$companyData.company_code}', '{$item.media_code}'); return false;">Delete</button>
									{else}
										<b>Primary</b>
									{/if}
								</td>
							</tr>	
							{foreachelse}	
							<tr>
								<td align="center" colspan="4">There are currently no items</td>
							</tr>								
						{/foreach}
					</tbody>					  
				</table>
				<p>Add images below</p>
                <div class="form-group">
					<label for="mediadescription">Image Description</label>
					<textarea id="mediadescription" name="mediadescription" rows="3" class="form-control">{$companyData.company_name} {$companyData.company_surname}</textarea>
					{if isset($errorArray.mediadescription)}<br /><span class="error">{$errorArray.mediadescription}</span>{/if}
                </div>				
                <div class="form-group">
					<label for="mediafile">Image Upload</label>
					<input type="file" id="mediafile" name="mediafile" />
					{if isset($errorArray.mediafile)}<br /><span class="error">{$errorArray.mediafile}</span>{/if}					  
					<br /><span>Allowed files are png, jpg, jpeg.</span>
                </div>					
                <div class="form-group">
					<button type="submit" class="btn btn-primary">Upload and Save</button>
				</div>
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col -->		
		<div class="col-sm-3">
			<div class="list-group">  
				<a href="/company/list/" class="list-group-item">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/company/list/details.php?code={$companyData.company_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/company/list/media.php?code={$companyData.company_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Media
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/company/list/tag.php?code={$companyData.company_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Tag(s)
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/company/list/document.php?code={$companyData.company_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Document(s)
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>	
				<a class="list-group-item" href="/company/list/comment.php?code={$companyData.company_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Comments
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>				
			</div> <!-- /.list-group -->
		</div>		
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
{literal}
<script type="text/javascript">
function updateRecord(code) {
	$.ajax({
			type: "GET",
			url: "media.php?code={/literal}{$companyData.company_code}{literal}",
			data: "update_code="+code+"&media_description="+$("#media_description_"+code).val(),
			dataType: "json",
			success: function(data){
				if(data.result == 1) {
					window.location.href = window.location.href;
				} else {
					$.howl ({
					  type: 'info'
					  , title: 'Notification'
					  , content: data.error
					  , sticky: $(this).data ('sticky')
					  , lifetime: 7500
					  , iconCls: $(this).data ('icon')
					});	
				}
			}
	});								

	return false;
}

function deleteitemModal(company, media) {
	$('#companycode').val(company);
	$('#mediacode').val(media);
	$('#deleteitemModal').modal('show');
	return false;
}

function statusitemModal(company, media, status) {
	$('#statuscompanycode').val(company);
	$('#statuscode').val(media);
	$('#status').val(status);
	$('#statusItemModal').modal('show');
	return false;
}

function deletesubitem() {
		
		var companycode 	= $('#companycode').val();
		var mediacode 			= $('#mediacode').val();
		
		$.ajax({
				type: "GET",
				url: "media.php",
				data: "delete_code="+mediacode+"&code="+companycode,
				dataType: "json",
				success: function(data){
					if(data.result == 1) {
						if(typeof oTable != 'undefined') {
							$('#deleteitemModal').modal('hide');
							oTable.fnDraw();
						} else {
							window.location.href = window.location.href;
						}
					} else {
						
						$('#deleteitemModal').modal('hide');
						
						$.howl ({
						  type: 'danger'
						  , title: 'Error Message'
						  , content: data.error
						  , sticky: $(this).data ('sticky')
						  , lifetime: 7500
						  , iconCls: $(this).data ('icon')
						});					
					}
				}
		});
		
		return false;
	}
	
	function changeItemStatus() {
	
		var company	= $('#statuscompanycode').val();
		var media 		= $('#statuscode').val();	
		var status			= $('#status').val();
		
		$.ajax({
				type: "GET",
				url: "/company/list/media.php?code="+company+"&media="+media,
				data: "status_code="+media+"&status="+status,
				dataType: "json",
				success: function(data){
					if(data.result == 1) {
						window.location.href = window.location.href;
					} else {
						$('#statusItemModal').modal('hide');
						$.howl ({
						  type: 'info'
						  , title: 'Notification'
						  , content: data.error
						  , sticky: $(this).data ('sticky')
						  , lifetime: 7500
						  , iconCls: $(this).data ('icon')
						});	
					}
				}
		});								

		return false;		
	}
</script>
{/literal}
<!-- Modal -->
<div class="modal fade" id="statusItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Change Status</h4>
			</div>
			<div class="modal-body">Are you sure you want to change this item's status?</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:changeItemStatus();">Change Item Status</button>
				<input type="hidden" id="statuscompanycode" name="statuscompanycode" value="" />
				<input type="hidden" id="statusmediacode" name="statusmediacode" value="" />
				<input type="hidden" id="statuscode" name="statuscode" value=""/>
				<input type="hidden" id="status" name="status" value=""/>
			</div>
		</div>
	</div>
</div>
<!-- modal -->

<!-- Modal -->
<div class="modal fade" id="deleteitemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Item</h4>
			</div>
			<div class="modal-body">Are you sure you want to delete this item?</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:deletesubitem();">Delete Item</button>
				<input type="hidden" id="companycode" name="companycode" value=""/>
				<input type="hidden" id="mediacode" name="mediacode" value=""/>
				<input type="hidden" id="mediacode" name="mediacode" value=""/>
			</div>
		</div>
	</div>
</div>
<!-- modal -->
</html>