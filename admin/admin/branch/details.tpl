<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
	<h2 class="content-header-title">Branches</h2>
	<ol class="breadcrumb">
		<li><a href="/">Home</a></li>	
		<li><a href="/admin/view/">Administration</a></li>
		<li><a href="/admin/branch/">Branches</a></li>
		<li><a href="#">{if isset($branchData)}{$branchData.branch_name}{else}Add a branch{/if}</a></li>
		<li class="active">Details</li>
	</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					{if isset($branchData)}{$branchData.branch_name}{else}Add a branch{/if}
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="/admin/branch/details.php{if isset($branchData)}?code={$branchData.branch_code}{/if}" method="POST" data-validate="parsley" class="form parsley-form">
                <div class="form-group">
                  <label for="branch_name">Name</label>
                  <input type="text" id="branch_name" name="branch_name" class="form-control" data-required="true" value="{$branchData.branch_name}" />
				{if isset($errorArray.branch_name)}<span class="error">{$errorArray.branch_name}</span>{/if}					  
                </div>
                <div class="form-group">
                  <label for="mailbok_code">Mailbok Subscription Code</label>
                  <input type="text" id="mailbok_code" name="mailbok_code" class="form-control" data-required="true" value="{$branchData.mailbok_code}" />
				{if isset($errorArray.mailbok_code)}<span class="error">{$errorArray.mailbok_code}</span>{/if}					  
                </div>				
                <div class="form-group">
					<label for="parent_code">Parent Branch</label>
					<select id="parent_code" name="parent_code" class="form-control">
						<option value=""> --------------- </option>
						{html_options options=$branchPairs selected=$branchData.parent_code}
					</select>
					{if isset($errorArray.parent_code)}<span class="error">{$errorArray.parent_code}</span>{/if}					  
                </div>	
                <div class="form-group">
					<label for="branch_level">Branch Level</label>
					<select id="branch_level" name="branch_level" class="form-control">
						<option value="1" {if $branchData.branch_level eq '1'}selected{/if}> Continent </option>
						<option value="2" {if $branchData.branch_level eq '2'}selected{/if}> Country </option>
						<option value="3" {if $branchData.branch_level eq '3'}selected{/if}> Province / State </option>
						<option value="4" {if $branchData.branch_level eq '4'}selected{/if}> City / Town </option>
					</select>
					{if isset($errorArray.branch_level)}<span class="error">{$errorArray.branch_level}</span>{/if}					  
                </div>				
                <div class="form-group"><button type="submit" class="btn btn-primary">Validate and Submit</button></div>
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col --> 
		<div class="col-sm-3">
			<div class="list-group">  
				<a class="list-group-item" href="/admin/branch/">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				{if isset($branchData)}					
				<a class="list-group-item" href="#">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/admin/branch/admin.php?code={$branchData.branch_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Administrators
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>			
				{/if}
			</div> <!-- /.list-group -->
        </div>			
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
</html>
