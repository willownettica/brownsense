<?php

require_once "Zend/Paginator.php";  
require_once '_comm.php';

//custom account item class as account table abstraction
class class_company extends Zend_Db_Table_Abstract
{
	//declare table variables
    protected $_name 			= 'company';
	protected $_primary 		= 'company_code';
	public 			$_comm		= null;
	public 			$_member	= null;
	
	function init()	{
		global $zfsession;
		$this->_comm	= new class_comm();
		$this->_member	= isset($zfsession->identity) && $zfsession->identity != '' ? $zfsession->identity : '';
	}
	
	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 
	 public function insert(array $data)
    {
        // add a timestamp
        $data['company_added'] = date('Y-m-d H:i:s');        
		$data['company_code']	= $this->createCode();
		
		$success = parent::insert($data);	

		if($success) {

			$companyData = $this->getCode($success);

			if($companyData) {
				/* Email admin for vetting. */
				$templateData = $this->_comm->_template->getTemplate('VET', 'EMAIL');

				if($templateData) {

					$recipient	= array();
					$recipient['recipient_code'] 		= '';
					$recipient['recipient_name'] 		= 'Administrator';
					$recipient['recipient_type'] 			= 'ADMIN';
					$recipient['recipient_email'] 		= 'admin@brownsense.co.za ';
					$recipient['recipient_vet_owner'] = $companyData['company_contact_name'];
					$recipient['recipient_vet_email'] 	= $companyData['company_contact_email'];
					$recipient['recipient_vet_name'] 	= $companyData['company_name'];
					$recipient['recipient_vet_type'] 	= 'company';

					$this->_comm->sendEmail($recipient, $templateData);
				}


			} else {
				return false;
			}
			
			return $data['company_code'];
		} else {
			return false;
		}
    }
	
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean 
	 */
    public function update(array $data, $where)
    {
        // add a timestamp
        $data['company_updated'] = date('Y-m-d H:i:s');
        
        return parent::update($data, $where);
    }
	
	/**
	 * get job by job company Id
 	 * @param string job id
     * @return object
	 */
	public function getList() {

		$vet = $this->_db->select()	
					->from(array('vet' => 'vet'))
					->joinLeft(array('item' => 'item'), 'item.item_code = vet.item_code', array('item_name', 'item_config_color', 'item_config_class', 'item_config_status', 'item_config_code', 'item_config_paid'))
					->where("item.item_config_status = 1 and vet_active = 1 and item_deleted = 0 and vet_deleted = 0")
					->where('vet_item_type = ?', 'COMPANY');	

		$select = $this->_db->select()	
				->from(array('company' => 'company'), array('company_code'))
				->joinLeft(array('member' => 'member'), 'member.member_code = company.member_code', array())
				->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = company.areapost_code', array())
				->joinLeft(array('vet' => $vet), 'vet.vet_item_code = company.company_code', array())					
				->where('company.company_deleted = 0 and member_deleted = 0 and item_config_status = 1 and member.member_code = ?', $this->_member);

		$result = $this->_db->fetchCol($select);
        return ($result == false) ? false : $result = $result;

	}
	
	/**
	 * get job by job company Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code) {

		$tag = $this->_db->select()	
					->from(array('link' => 'link'), array('link_parent_code', "group_concat(item.item_name separator ',') as tag_name"))
					->joinLeft(array('item' => 'item'), 'item.item_code = link.link_child_code', array())	
					->where("link_parent_type = 'COMPANY' and link_child_type = 'TAG'")
					->where('link_parent_code = ?', $code)
					->group('link_parent_code');	

		$vet = $this->_db->select()	
					->from(array('vet' => 'vet'))
					->joinLeft(array('item' => 'item'), 'item.item_code = vet.item_code', array('item_name', 'item_config_color', 'item_config_class', 'item_config_status', 'item_config_code', 'item_config_paid'))
					->where("item.item_config_status = 1 and vet_active = 1 and item_deleted = 0 and vet_deleted = 0")
					->where('vet_item_type = ?', 'COMPANY')
					->where('vet_item_code = ?', $code)
					->limit(1);

		$commentrate = $this->_db->select()	
					->from(array('comment' => 'comment'), array('comment_item_code', 'count(comment_code) as comment_count'))
					->joinLeft(array('rate' => 'rate'), "rate.rate_item_type = 'COMMENT' and rate_item_code = comment_code", array('round((AVG(rate_number)/10)*100) as rate_avg'))
					->joinLeft(array('vet' => 'vet'), "vet.vet_item_type = 'COMMENT' and vet_item_code = comment_code", array())
					->joinLeft(array('item' => 'item'), "item.item_code = vet.item_code", array())
					->where("item.item_config_status = 1 and vet_active = 1 and item_deleted = 0 and vet_deleted = 0")
					->where("rate_deleted = 0 and comment_deleted = 0 and comment_item_type = 'COMPANY' and comment_item_code = ?", $code)
					->group(array('comment_item_code'));	

		$select = $this->_db->select()	
				->from(array('company' => 'company'))
				->joinLeft(array('media' => 'media'), "media.media_item_code = company.company_code and media.media_item_type = 'COMPANY' and media.media_primary = '1' and media_deleted = 0", array('media_path', 'media_code', 'media_ext'))
				->joinLeft(array('tag' => $tag), 'tag.link_parent_code = company.company_code', array('tag_name'))
				->joinLeft(array('vet' => $vet), 'vet.vet_item_code = company.company_code')
				->joinLeft(array('commentrate' => $commentrate), 'commentrate.comment_item_code = company.company_code', array('ifnull(comment_count, 0) comment_count', 'ifnull(rate_avg, 0) rate_avg'))				
				->joinLeft(array('member' => 'member'), 'member.member_code = company.member_code')
				->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = company.areapost_code')
				->where('company.company_deleted = 0 and member_deleted = 0 and vet.item_config_status = 1 and vet.vet_active = 1 and company.company_code = ?', $code)
				->limit(1);

		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;

	}
	
	/**
	 * get job by job company Id
 	 * @param string job id
     * @return object
	 */
	public function getByMember($member, $code = '') {

		if($code == '') {
			
			$tag = $this->_db->select()	
						->from(array('link' => 'link'), array('link_parent_code', "group_concat(item.item_name separator ',') as tag_name"))
						->joinLeft(array('item' => 'item'), 'item.item_code = link.link_child_code', array())	
						->where("link_parent_type = 'COMPANY' and link_child_type = 'TAG'")
						->group('link_parent_code');	

			$vet = $this->_db->select()	
						->from(array('vet' => 'vet'))
						->joinLeft(array('item' => 'item'), 'item.item_code = vet.item_code', array('item_name', 'item_config_color', 'item_config_class', 'item_config_status', 'item_config_code', 'item_config_paid'))
						->where("vet_active = 1 and item_deleted = 0 and vet_deleted = 0")
						->where('vet_item_type = ?', 'COMPANY');	

			$select = $this->_db->select()	
					->from(array('company' => 'company'))
					->joinLeft(array('member' => 'member'), 'member.member_code = company.member_code')
					->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = company.areapost_code')
					->joinLeft(array('tag' => $tag), 'tag.link_parent_code = company.company_code')
					->joinLeft(array('vet' => $vet), 'vet.vet_item_code = company.company_code')					
					->where('company_deleted = 0 and member_deleted = 0 and member.member_code = ?', $member);
			
			$result = $this->_db->fetchAll($select);

		} else {
			
			$tag = $this->_db->select()	
						->from(array('link' => 'link'), array('link_parent_code', "group_concat(item.item_name separator ',') as tag_name"))
						->joinLeft(array('item' => 'item'), 'item.item_code = link.link_child_code', array())	
						->where("link_parent_type = 'COMPANY' and link_child_type = 'TAG'")
						->where('link_parent_code = ?', $code)
						->group('link_parent_code');	

			$vet = $this->_db->select()	
						->from(array('vet' => 'vet'))
						->joinLeft(array('item' => 'item'), 'item.item_code = vet.item_code', array('item_name', 'item_config_color', 'item_config_class', 'item_config_status', 'item_config_code', 'item_config_paid'))	
						->where("item.item_config_status = 1 and vet_active = 1 and item_deleted = 0 and vet_deleted = 0")
						->where('vet_item_code = ?', $code)
						->where('vet_item_type = ?', 'COMPANY');		

			$select = $this->_db->select()	
					->from(array('company' => 'company'))	
					->joinLeft(array('member' => 'member'), 'member.member_code = company.member_code')
					->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = company.areapost_code')
					->joinLeft(array('tag' => $tag), 'tag.link_parent_code = company.company_code')
					->joinLeft(array('vet' => $vet), 'vet.vet_item_code = company.company_code')					
					->where('company.company_deleted = 0 and member_deleted = 0 and member.member_code = ?', $member)
					->where('company.company_code = ?', $code)
					->limit(1);
			
			$result = $this->_db->fetchRow($select);
		}
		
        return ($result == false) ? false : $result = $result;

	}
	
	/**
	 * get job by job company Id
 	 * @param string job id
     * @return object
	 */
	public function _latest($limit = 2) {
	
		$tag = $this->_db->select()	
					->from(array('link' => 'link'), array('link_parent_code', "group_concat(item.item_name separator ',') as tag_name", "group_concat( distinct category.item_name separator ',') as category_name"))
					->joinLeft(array('item' => 'item'), 'item.item_code = link.link_child_code', array())
					->joinLeft(array('category' => 'item'), 'category.item_code = item.item_parent', array())
					->where("link_parent_type = 'COMPANY' and link_child_type = 'TAG' and link_deleted = 0 and item.item_deleted = 0 and item.item_active = 1")
					->group('link_parent_code');

		$vet = $this->_db->select()	
					->from(array('vet' => 'vet'))
					->joinLeft(array('item' => 'item'), 'item.item_code = vet.item_code', array('item_name', 'item_config_color', 'item_config_class', 'item_config_status', 'item_config_code', 'item_config_paid'))
					->where("item.item_config_status = 1 and vet_active = 1 and item_deleted = 0 and vet_deleted = 0")
					->where('vet_item_type = ?', 'COMPANY')
					->order('vet_added desc')
					->group('vet_item_code');

		$select = $this->_db->select()	
				->from(array('company' => 'company'))	
				->joinLeft(array('member' => 'member'), 'member.member_code = company.member_code')
				->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = company.areapost_code')
				->joinLeft(array('areapostregion' => 'areapostregion'), 'areapostregion.areapostregion_code = areapost.areapostregion_code')
				->joinLeft(array('tag' => $tag), 'tag.link_parent_code = company.company_code')
				->joinLeft(array('vet' => $vet), 'vet.vet_item_code = company.company_code')
				->joinLeft(array('media' => 'media'), "media.media_item_code = company.company_code and media.media_item_type = 'COMPANY' and media.media_primary = '1' and media_deleted = 0", array('media_path', 'media_code', 'media_ext'))					
				->where("company.company_deleted = 0 and member_deleted = 0 and company.company_address_physical is not null and company.company_address_physical != '' and item_config_status = 1")
				->where("tag.tag_name is not null and company_active = 1 and media.media_path != ''")
				->order('company_added desc')
				->limit($limit);
			
		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;

	}
	
	public function search($query, $limit = 20) {

		$select = $this->_db->select()
						->from(array('company' => 'company'), array('company.company_code', 'company.company_name'))		
					   ->where("company.company_name like lower(?)", "%$query%")
					   ->limit($limit)
					   ->order("LOCATE('$query', company.company_name)");

	   $result = $this->_db->fetchAll($select);	
        return ($result == false) ? false : $result = $result;					

	}

	public function pairs($member) {

		$select = $this->_db->select()
				->from(array('company' => 'company'), array('company.company_code', "company.company_name"))
				->where('member_code = ?', $member)
				->order('company.company_name asc');
					
        $result = $this->_db->fetchPairs($select);
        return ($result == false) ? false : $result = $result;		
	}
	
	public function paginate($filter = array(), $page = 1, $perPage = 10, $listedPages = 10, $scrollingStyle = 'Sliding') {
		
		$where = 'company_deleted = 0';

		if(count($filter) > 0) {	
			for($i = 0; $i < count($filter); $i++) { 
				if(isset($filter[$i]['filter_province']) && trim($filter[$i]['filter_province']) != '') {
					$province = $filter[$i]['filter_province'];
					$this->sanitize($province);
					$where .= " and areapostregion.demarcation_id = '$province'";
				} else if(isset($filter[$i]['filter_search']) && trim($filter[$i]['filter_search']) != '') {
					$array = explode(" ",trim($filter[$i]['filter_search']));					
					if(count($array) > 0) {
						$where .= " and (";
						for($i = 0; $i < count($array); $i++) {
							$text = $array[$i];
							$this->sanitize($text);
							$where .= "lower(concat(company.company_name, tag.tag_name, member.member_name, member.member_surname)) like lower('%$text%')";
							if(($i+1) != count($array)) {
								$where .= ' or ';
							}
						}
					}
					$where .= ")";
				} else if(isset($filter[$i]['filter_category']) && trim($filter[$i]['filter_category']) != '') {
					$category = $filter[$i]['filter_category'];
					$this->sanitize($category);
					$where .= " and find_in_set('$category',category_code) <> 0";
				}
			}
		}

		$tag = $this->_db->select()	
					->from(array('link' => 'link'), array('link_parent_code', "group_concat(item.item_name separator ',') as tag_name", "group_concat( distinct category.item_name separator ',') as category_name", "group_concat( distinct category.item_code separator ',') as category_code"))
					->joinLeft(array('item' => 'item'), 'item.item_code = link.link_child_code', array())
					->joinLeft(array('category' => 'item'), 'category.item_code = item.item_parent', array())
					->where("link_parent_type = 'COMPANY' and link_child_type = 'TAG' and link_deleted = 0 and item.item_deleted = 0 and item.item_active = 1")
					->group('link_parent_code');

		$commentrate = $this->_db->select()	
					->from(array('comment' => 'comment'), array('comment_item_code', 'count(comment_code) as comment_count'))
					->joinLeft(array('rate' => 'rate'), "rate.rate_item_type = 'COMMENT' and rate_item_code = comment_code", array('round((AVG(rate_number)/10)*100) as rate_avg'))
					->joinLeft(array('vet' => 'vet'), "vet.vet_item_type = 'COMMENT' and vet_item_code = comment_code", array())
					->joinLeft(array('item' => 'item'), "item.item_code = vet.item_code", array())
					->where("item.item_config_status = 1 and vet_active = 1 and item_deleted = 0 and vet_deleted = 0")
					->where("rate_deleted = 0 and comment_deleted = 0 and comment_item_type = 'COMPANY' ")
					->where("item.item_config_status = 1 and vet_active = 1 and item_deleted = 0 and vet_deleted = 0")
					->group(array('comment_item_code'));	
					
			$vet = $this->_db->select()	
						->from(array('vet' => 'vet'))
						->joinLeft(array('item' => 'item'), 'item.item_code = vet.item_code', array('item_name', 'item_config_color', 'item_config_class', 'item_config_status', 'item_config_code', 'item_config_paid'))
						->where("item.item_config_status = 1 and vet_active = 1 and item_deleted = 0 and vet_deleted = 0")
						->where('vet_item_type = ?', 'COMPANY');	
				
			$select = $this->_db->select()	
				->from(array('company' => 'company'))
				->joinLeft(array('member' => 'member'), 'member.member_code = company.member_code', array('member_name', 'member_surname'))
				->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = company.areapost_code', array('areapost_city', 'areapost_name'))
				->joinLeft(array('areapostregion' => 'areapostregion'), 'areapostregion.areapostregion_code = areapost.areapostregion_code', array())
				->joinLeft(array('tag' => $tag), 'tag.link_parent_code = company.company_code')
				->joinLeft(array('commentrate' => $commentrate), 'commentrate.comment_item_code = company.company_code', array('ifnull(comment_count, 0) comment_count', 'ifnull(rate_avg, 0) rate_avg'))
				->joinLeft(array('vet' => $vet), 'vet.vet_item_code = company.company_code')
				->joinLeft(array('media' => 'media'), "media.media_item_code = company.company_code and media.media_item_type = 'COMPANY' and media.media_primary = '1' and media_deleted = 0", array('media_path', 'media_code', 'media_ext'))					
				->where("company.company_deleted = 0 and member_deleted = 0 and (tag.tag_name != '' or tag.tag_name is not null) and vet.item_config_status = 1 and vet.vet_active = 1")
				->where($where);

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage($perPage);
		$paginator->setPageRange($listedPages);
		$paginator->setDefaultScrollingStyle($scrollingStyle);

		return $paginator;
	}
	
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getCode($reference)
	{
		$select = $this->_db->select()	
				->from(array('company' => 'company'))	
				->where('company_code = ?', $reference)
				->limit(1);

	   $result = $this->_db->fetchRow($select);	
        return ($result == false) ? false : $result = $result;					   
	}

	function createCode() {
		/* New code. */
		$code = "";
		 
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '1234567890';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<1;$i++){
			$code .= $Number[rand(0,$count)];
		}
		 
		/* Last alphabet. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)]; 
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Number[rand(0,$count)];
		}	
		
		/* First check if it exists or not. */
		$itemCheck = $this->getCode($code);
		
		if($itemCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}	

	public function validateTwitter($handler) {
		
		$handler = str_replace('@' , '' , $handler);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, 'https://twitter.com/users/username_available?username='.$handler);
		$result = curl_exec($ch);
		curl_close($ch);

		$obj = json_decode($result);

		if($obj->reason == 'taken') {
			return $handler;
		} else {
			return '';
		}
	}
	
	public function validateEmail($string) {
		if(!filter_var($string, FILTER_VALIDATE_EMAIL)) {
			return '';
		} else {
			return trim($string);
		}
	}
	
	public function validateNumber($string) {
		if(preg_match('/^0[0-9]{9}$/', $this->onlyContactNumber(trim($string)))) {
			return $this->onlyContactNumber(trim($string));
		} else {
			return '';
		}
	}
	
	public function clean($string) {
		/* Remove some weird charactors that windows dont like. */
		$string = strtolower($string);
		$string = str_replace('__' , ' ' , $string);
		$string = str_replace("�", "e", $string);
		$string = str_replace("�", "e", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", " ", $string);
		$string = str_replace("\\", " ", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", " ", $string);
		$string = str_replace(".", " ", $string);
		$string = str_replace("�", "e", $string);	
		$string = str_replace('___' , ' ' , $string);
		$string = str_replace('__' , ' ' , $string);	
		$string = str_replace('__' , ' ' , $string);
		$string = str_replace("�", "e", $string);
		$string = str_replace("�", "e", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", " ", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("�", "e", $string);	
		$string = str_replace("–", "", $string);	
		$string = str_replace("�", "a", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("#", "", $string);	
		$string = str_replace("$", "", $string);	
		$string = str_replace("@", "", $string);	
		$string = str_replace("!", "", $string);	
		$string = str_replace("&", "", $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(':' , '' , $string);		
		$string = str_replace('[' , '' , $string);		
		$string = str_replace(']' , '' , $string);		
		$string = str_replace('|' , '' , $string);		
		$string = str_replace('\\' , '' , $string);		
		$string = str_replace('%' , '' , $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace('__' , ' ' , $string);	
		$string = str_replace('-' , ' ' , $string);	

		return $string;
	}
	
	public function onlyContactNumber($string) {
		/* Remove some weird charactors that windows dont like. */
		$string = strtolower($string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("�", "", $string);	
		$string = str_replace('___' , '' , $string);
		$string = str_replace('__' , '' , $string);	
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("�", "", $string);	
		$string = str_replace("–", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("#", "", $string);	
		$string = str_replace("$", "", $string);	
		$string = str_replace("@", "", $string);	
		$string = str_replace("!", "", $string);	
		$string = str_replace("&", "", $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(':' , '' , $string);		
		$string = str_replace('[' , '' , $string);		
		$string = str_replace(']' , '' , $string);		
		$string = str_replace('|' , '' , $string);		
		$string = str_replace('\\' , '' , $string);		
		$string = str_replace('%' , '' , $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);	
		$string = str_replace('-' , '' , $string);	
		$string = str_replace('+27' , '0' , $string);	
		$string = str_replace('(0)' , '' , $string);	
		
		$string = preg_replace('/^00/', '0', $string);
		$string = preg_replace('/^27/', '0', $string);
		
		$string = preg_replace('!\s+!',"", strip_tags($string));
		
		return $string;
	}	
	
    function sanitize(&$string) 
    {        
        $string = preg_replace("/[^a-zA-Z0-9_]+/", "", $string);
    }
	
    function sanitizeArray(&$array) 
    {        
		for($i = 0; $i < count($array); $i++) {
			$array[$i] = preg_replace("/[^a-zA-Z0-9_]+/", "", $array[$i]);
		}
    } 		
}
?>