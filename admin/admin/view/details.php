<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/* Check for login */
require_once 'includes/auth.php';

/* objects. */
require_once 'class/admin.php';

$adminObject 		= new class_admin();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$tempData = $adminObject->getByCode($code);

	if($tempData) {
		$smarty->assign('tempData', $tempData);
	} else {
		header('Location: /admin/view/');
		exit;
	}
}

/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;

	if(isset($_POST['areapost_code']) && trim($_POST['areapost_code']) == '') {
		$errorArray['areapost_code'] = 'Area is required';
		$formValid = false;		
	}
	
	if(isset($_POST['admin_name']) && trim($_POST['admin_name']) == '') {
		$errorArray['admin_name'] = 'Name is required';
		$formValid = false;		
	}
	
	if(isset($_POST['admin_surname']) && trim($_POST['admin_surname']) == '') {
		$errorArray['admin_surname'] = 'Surname is required';
		$formValid = false;		
	}
	
	if(isset($_POST['admin_email']) && trim($_POST['admin_email']) != '') {
		if($adminObject->validateEmail(trim($_POST['admin_email'])) == '') {
			$errorArray['admin_email'] = 'Valid email is required';
			$formValid = false;				
		} else {
			$emailcode = isset($tempData['admin_code']) ? $tempData['admin_code'] : null;
			
			$emailData = $adminObject->getByEmail(trim($_POST['admin_email']), $emailcode);
			
			if($emailData) {
				$errorArray['admin_email'] = 'Email is already being used';
				$formValid = false;		
			}
		}
	}

	if(isset($_POST['admin_cellphone']) && trim($_POST['admin_cellphone']) != '') {
		if($adminObject->validateNumber(trim($_POST['admin_cellphone'])) == '') {
			$errorArray['admin_cellphone'] = 'Valid cellphone number is required';
			$formValid = false;				
		} else {

			$smscode = isset($tempData['admin_code']) ? $tempData['admin_code'] : null;

			$smsData = $adminObject->getByCell(trim($_POST['admin_cellphone']), $smscode);

			if($smsData) {
				$errorArray['admin_cellphone'] = 'Cellphone number is already being used';
				$formValid = false;		
			}
		}
	}

	if((isset($_POST['admin_cellphone']) && trim($_POST['admin_cellphone']) == '') && (isset($_POST['admin_email']) && trim($_POST['admin_email']) == '')) {
		$errorArray['admin_cellphone'] = 'Please at least add a cellphone number of email address.';
		$formValid = false;				
	}
	
	if(count($errorArray) == 0 && $formValid == true) {

		$data 	= array();				
		$data['admin_name']			= trim($_POST['admin_name']);		
		$data['admin_surname']		= trim($_POST['admin_surname']);		
		$data['admin_email']		= trim($_POST['admin_email']);	
		$data['admin_cellphone']	= trim($_POST['admin_cellphone']);			
		
		if(isset($tempData)) {
			$where		= $adminObject->getAdapter()->quoteInto('admin_code = ?', $tempData['admin_code']);
			$success	= $adminObject->update($data, $where);
			$success	= $tempData['admin_code'];
		} else {
			$success = $adminObject->insert($data);
		}

		header('Location: /admin/view/role.php?code='.$success);	
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

$smarty->display('admin/view/details.tpl');

?>