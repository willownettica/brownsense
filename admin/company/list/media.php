<?php

ini_set('memory_limit','365M');
ini_set('max_execution_time', '600');

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/**
 * Standard includes
 */
require_once 'config/database.php';
require_once 'config/smarty.php';

require_once 'includes/auth.php';

/* objects. */
require_once 'class/company.php';
require_once 'class/media.php';
require_once 'class/File.php';

$companyObject	= new class_company();
$mediaObject 		= new class_media();
$fileObject 			= new File(array('png', 'jpg', 'jpeg'));

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code 		= trim($_GET['code']);
	
	$companyData = $companyObject->getByCode($code);	

	if($companyData) {
		
		$smarty->assign('companyData', $companyData);

		$mediaData = $mediaObject->getByReference(array('IMAGE'), 'COMPANY', $code);

		if($mediaData) {
			$smarty->assign('mediaData', $mediaData);
		}
	} else {
		header('Location: /company/list/');
		exit;	
	}
} else {
	header('Location: /company/list/');
	exit;	 
}

/* Check posted data. */
if(isset($_GET['delete_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true; 
	$success					= NULL;
	$itemcode					= trim($_GET['delete_code']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {	
		$data	= array();
		$data['media_deleted'] = 1; 
		
		$where		= array();
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_code = ?', $itemcode);
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_item_code = ?', $companyData['company_code']);
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_item_type = ?', 'COMPANY');
		
		$success	= $mediaObject->update($data, $where);	
		
		if(is_numeric($success) && $success > 0) {		
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	
	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(isset($_GET['update_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 1;	
	$formValid				= true;
	$success					= NULL;
	$itemcode					= trim($_GET['update_code']);
	
	if(isset($_GET['media_description']) && trim($_GET['media_description']) == '') {
		$errorArray['error']	= 'Please add a description.';
		$errorArray['result']	= 0;		
	}

	if($errorArray['error']  == '' && $errorArray['result']  == 1 ) {

		$data	= array();
		$data['media_description']	= trim($_GET['media_description']);

		$where		= array();
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_code = ?', $itemcode);
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_item_code = ?', $companyData['company_code']);

		$success	= $mediaObject->update($data, $where);	

		if(is_numeric($success) && $success > 0) {
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}

	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(isset($_GET['status_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;
	$data 						= array();
	$formValid				= true;
	$success					= NULL;
	$itemcode					= trim($_GET['status_code']);
	
	if($errorArray['error']  == '') {
		
		$itemData = $mediaData = $mediaObject->getCode($itemcode);
		
		if($itemData) {
			$success = $mediaObject->updatePrimary($itemData['media_category'], $itemData['media_item_type'], $companyData['company_code'], $itemcode);			
		}
		
		if(is_numeric($success)) {		
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not update, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	
	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(count($_FILES) > 0) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;

	if(isset($_POST['mediadescription']) && trim($_POST['mediadescription']) == '') {
		$errorArray['mediadescription'] = 'Media description is required';
		$formValid = false;
	}

	if(isset($_FILES['mediafile']) && count($_FILES['mediafile']['name']) > 0 && (isset($_FILES['mediafile']['name']) && trim($_FILES['mediafile']['name']) != '')) {
		/* Check validity of the CV. */
		if((int)$_FILES['mediafile']['size'] != 0 && trim($_FILES['mediafile']['name']) != '') {
			/* Check if its the right file. */
			$ext = $fileObject->file_extention($_FILES['mediafile']['name']); 

			if($ext != '') {
				$checkExt = $fileObject->getValidateExtention('mediafile', $ext);

				if(!$checkExt) {
					$errorArray['mediafile'] = 'Invalid file type something funny with the file format';
					$formValid = false;						
				} else {
					list($width, $height) = getimagesize($_FILES['mediafile']['tmp_name']);					

					if(((int)$width >= 374) && ((int)$height >= 278)) {
						/* Do absolutely nothing. */
					} else {
						$errorArray['mediafile'] = 'The width of the image needs to be greater than 374px and the height greater than 278px';
						$formValid = false;														
					}
				}
			} else {
				$errorArray['mediafile'] = 'Invalid file type';
				$formValid = false;									
			}
		} else {			
			switch((int)$_FILES['mediafile']['error']) {
				case 1 : $errorArray['mediafile'] = 'The uploaded file exceeds the maximum upload file size, should be less than 1M'; $formValid = false; break;
				case 2 : $errorArray['mediafile'] = 'File size exceeds the maximum file size'; $formValid = false; break;
				case 3 : $errorArray['mediafile'] = 'File was only partically uploaded, please try again'; $formValid = false; break;
				case 4 : $errorArray['mediafile'] = 'No file was uploaded'; $formValid = false; break;
				case 6 : $errorArray['mediafile'] = 'Missing a temporary folder'; $formValid = false; break;
				case 7 : $errorArray['mediafile'] = 'Faild to write file to disk'; $formValid = false; break;
			}
		}
	} else {
		$errorArray['mediafile'] = 'No file was uploaded';
		$formValid = false;									
	}

	if(count($errorArray) == 0 && $formValid == true) {

		if(isset($_FILES['mediafile']) && count($_FILES['mediafile']['name']) > 0 && (isset($_FILES['mediafile']['name']) && trim($_FILES['mediafile']['name']) != '')) {
				$data = array();
				$data['media_code']			= $mediaObject->createCode();
				$data['media_item_code']	= $companyData['company_code'];
				$data['media_item_type']	= 'COMPANY';
				$data['media_description']	= trim($_POST['mediadescription']);
				$data['media_category']		= 'IMAGE';

				$ext 			= strtolower($fileObject->file_extention($_FILES['mediafile']['name']));					
				$filename	= $data['media_code'].'.'.$ext;
				$directory	= realpath(__DIR__.'/../../../public_html/').'/media/company/list/'.$data['media_code'];

				$file			= $directory.'/'.$filename;

				if(!is_dir($directory)) mkdir($directory, 0777, true); 
				/* Create files for this product type. */
				foreach($fileObject->image as $item) {
					/* Change file name. */
					$newfilename = str_replace($filename, $item['code'].$filename, $file);
					/* Starting with the big one, the resizing the rest from the big image. */
					if($item['code'] == 'orig_') {
						/* Create new file and rename it. */
						$uploadObject	= PhpThumbFactory::create($_FILES['mediafile']['tmp_name']);
						$uploadObject->cropFromCenter($item['width'], $item['height']);
						$uploadObject->save($newfilename);
					} else {
						/* Create new file and rename it. */ 
						$uploadObject	= PhpThumbFactory::create($directory.'/orig_'.$filename);
						$uploadObject->adaptiveResize($item['width'], $item['height']);
						$uploadObject->save($newfilename);
					}
				}

				$data['media_path']		= '/media/company/list/'.$data['media_code'].'/';
				$data['media_filename']	= trim($_FILES['mediafile']['name']);
				$data['media_ext']			= '.'.$ext ;

				/* Check for other medias. */
				$primary = $mediaObject->getPrimary('IMAGE', 'COMPANY', $companyData['company_code']);		
				
				if($primary) {
					$data['media_primary']	= 0;
				} else {
					$data['media_primary']	= 1;
				}

				$success	= $mediaObject->insert($data);
		}
	
		header('Location: /company/list/media.php?code='.$companyData['company_code']);
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);
}

/* Display the template */	
$smarty->display('company/list/media.tpl');
$mediaObject = $errorArray = $data = $companyData = $primary = $ext = $newfilename = $uploadObject = $item = $i = $filename = $file = $directory = $fileObject = $formValid = $checkExt = null;
unset($mediaObject, $errorArray, $data, $companyData, $primary, $ext, $newfilename, $uploadObject, $item, $i, $filename, $file, $directory, $fileObject, $formValid, $checkExt);
?>