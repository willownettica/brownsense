<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';
/* Check for login */
require_once 'includes/auth.php';
/* objects. */
require_once 'class/_social.php';
require_once 'class/media.php';
require_once 'class/File.php';

$socialObject 	= new class_social();
$mediaObject 	= new class_media();
$fileObject 		= new File(array('png', 'jpg', 'jpeg'));

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$socialData = $socialObject->getById($code);

	if($socialData) {
		$smarty->assign('socialData', $socialData);
	} else {
		header('Location: /social/');
		exit;
	}
}

/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;

	if(!isset($_POST['_social_message'])) {
		$errorArray['_social_message'] = 'Message is required';
		$formValid = false;		
	} else if((strlen(trim($_POST['_social_message'])) == 0) || (strlen(trim($_POST['_social_message'])) > 140)){
		$errorArray['_social_message'] = 'Message needs to be less than 140 characters.';
		$formValid = false;
	}

	if((int)$_FILES['mediafile']['size'] != 0 && trim($_FILES['mediafile']['name']) != '') {
		/* Check if its the right file. */
		$ext = $fileObject->file_extention($_FILES['mediafile']['name']); 

		if($ext != '') {
			$checkExt = $fileObject->getValidateExtention('mediafile', $ext);

			if(!$checkExt) {
				$errorArray['mediafile'] = 'Invalid file type something funny with the file format';
				$formValid = false;						
			} else if($_FILES['mediafile']['size'] > 1048576) {
				$errorArray['mediafile'] = '<b>'.$_FILES['mediafile']['name'].'</b> is more than 1MB big.';
				$formValid = false;	
			}
		} else {
			$errorArray['mediafile'] = 'Invalid file type';
			$formValid = false;									
		}
	} else {			
		switch((int)$_FILES['mediafile']['error']) {
			case 1 : $errorArray['mediafile'] = 'The uploaded file exceeds the maximum upload file size, should be less than 1M'; $formValid = false; break;
			case 2 : $errorArray['mediafile'] = 'File size exceeds the maximum file size'; $formValid = false; break;
			case 3 : $errorArray['mediafile'] = 'File was only partically uploaded, please try again'; $formValid = false; break;
			// case 4 : $errorArray['mediafile'] = 'No file was uploaded'; $formValid = false; break;
			case 6 : $errorArray['mediafile'] = 'Missing a temporary folder'; $formValid = false; break;
			case 7 : $errorArray['mediafile'] = 'Faild to write file to disk'; $formValid = false; break;
		}
	}

	if(count($errorArray) == 0 && $formValid == true) {

		$data 	= array();				
		$data['_social_message']	= trim($_POST['_social_message']);		

		if(isset($socialData)) {
			$where		= $socialObject->getAdapter()->quoteInto('_social_id = ?', $socialData['_social_id']);
			$success	= $socialObject->update($data, $where);
			$success	= $socialData['_social_id'];
		} else {
			$success = $socialObject->insert($data);
		}
		/* Add file if there are any. */
		if((int)$_FILES['mediafile']['size'] != 0 && trim($_FILES['mediafile']['name']) != '') {

			$data = array();
			$data['media_code']			= $mediaObject->createCode();
			$data['media_item_code']	= $success;
			$data['media_item_type']	= 'SOCIAL';
			$data['media_description']	= trim($_POST['_social_message']);
			$data['media_category']		= 'IMAGE';

			$ext 			= strtolower($fileObject->file_extention($_FILES['mediafile']['name']));					
			$filename	= $data['media_code'].'.'.$ext;
			$directory	= realpath(__DIR__.'/../../../public_html/').'/media/social/'.$data['media_code'];

			$file			= $directory.'/'.$filename;

			if(!is_dir($directory)) mkdir($directory, 0777, true);
			
			/* Create files for this product type. */
			foreach($fileObject->image as $item) {
				/* Change file name. */
				$newfilename = str_replace($filename, $item['code'].$filename, $file);
				/* Starting with the big one, the resizing the rest from the big image. */
				if($item['code'] == 'orig_') {
					/* Create new file and rename it. */
					$uploadObject	= PhpThumbFactory::create($_FILES['mediafile']['tmp_name']);
					$uploadObject->cropFromCenter($item['width'], $item['height']);
					$uploadObject->save($newfilename);
				} else {
					/* Create new file and rename it. */ 
					$uploadObject	= PhpThumbFactory::create($directory.'/orig_'.$filename);
					$uploadObject->adaptiveResize($item['width'], $item['height']);
					$uploadObject->save($newfilename);
				}
			}

			$data['media_path']		= '/media/social/'.$data['media_code'].'/';
			$data['media_filename']	= trim($_FILES['mediafile']['name']);
			$data['media_ext']			= '.'.$ext ;

			/* Check for other medias. */
			$primary = $mediaObject->getPrimary('IMAGE', 'SOCIAL', $success);		
			
			if($primary) {
				$data['media_primary']	= 0;
			} else {
				$data['media_primary']	= 1;
			}
			$success	= $mediaObject->insert($data);
		}

		header('Location: /social/details.php?code='.$success);	
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);

}

$smarty->display('social/details.tpl');

?>