<!-- FOOTER -->
<footer class="bg-dark footer1 padding-top-60">
	<div class="container">
		<div class="row margin-bottom-30">
			<div class="col-md-8 col-sm-8 margin-bottom-30 footer-info">
				<img src="/images/brownsense_footer.png" class="img-responsive" alt=""/>
				<p>BrownSense&trade; is a platform created to open access to greater markets for Black entrepreneurs. it is both a business-to-business, and a business-to-customer organization</p> 
				<span><i class="fa fa-map-marker"></i>Johannesburg, Gauteng, South Africa</span>
				<span><i class="fa fa-envelope"></i> <a href="mailto:info@brownsense.co.za">info@brownsense.co.za</a></span>
				<span><i class="fa fa-phone"></i> +27 61 382 2005</span>
			</div>
			<div class="col-md-4 col-sm-4 margin-bottom-30 footer-follow">
				<h5>Follow</h5>
				<div class="footer-social">
					<a href="https://www.facebook.com/brownsensesa/" target="_blank"><i class="fa fa-facebook"></i></a>
					<a href="https://twitter.com/brownsense" target="_blank"><i class="fa fa-twitter"></i></a>
					<a href="https://www.instagram.com/brownsensesa/" target="_blank"><i class="fa fa-instagram"></i></a>
					<a href="https://www.youtube.com/channel/UCRqleSTq54sAFY9ozelFB5g" target="_blank"><i class="fa fa-youtube-play"></i></a>
				</div>
			</div>
		</div>
		<!-- FOOTER COPYRIGHT -->
		<div class="footer-bottom">
			<div class="row">
				<div class="col-md-4 col-sm-12">
					<p>&copy; Copyright 2017 brownsense.co.za. All rights reserved.</p>
				</div>
				<div class="col-md-8 col-sm-12">
					<?php if(isset($zfsession->memberData)) { ?>
					<ul class="footer-links">
						<li><a href="/">Home</a></li>
						<li><a href="/companies/">Companies</a></li>
						<li><a href="/events/">Events</a></li>
						<li><a href="/privacy.php">Privacy Policy</a></li>
						<!-- <li><a href="#">Advertising</a></li> -->
						<li><a href="/contact.php">Contact Us</a></li>
					</ul>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- // FOOTER -->
<script>
/*
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86117197-1', 'auto');
  ga('send', 'pageview');
*/
</script>