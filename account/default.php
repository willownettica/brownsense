<?php 
/* Add this on all pages on top. */
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/auth.php';

/* objects. */
require_once 'class/member.php';
require_once 'class/branch.php';

$memberObject 	= new class_member();
$branchObject		= new class_branch();

if(isset($_REQUEST['save_member'])) {

	$errorArray					= array();
	$errorArray['error']		= array();
	$errorArray['result']		= 1;	
	$errorArray['success']	= '';	

	if(isset($_REQUEST['branch_code']) && trim($_REQUEST['branch_code']) == '') {
		$errorArray['error'][] = 'Branch is required';
		$errorArray['result'] = false;		
	}

	if(isset($_REQUEST['member_birthdate']) && trim($_REQUEST['member_birthdate']) == '') {
		$errorArray['error'][] = 'Birth date is required';
		$errorArray['result'] = false;		
	} else {
		if($memberObject->validateDate(trim($_REQUEST['member_birthdate'])) == '') {
			$errorArray['error'][] = 'Valid birth date is required';
			$errorArray['result'] = false;				
		}
	}
	
	if(isset($_REQUEST['member_email']) && trim($_REQUEST['member_email']) != '') {
		if($memberObject->validateEmail(trim($_REQUEST['member_email'])) == '') {
			$errorArray['error'][] = 'Valid email is required';
			$errorArray['result'] = false;				
		} else {
			
			$tempData = $memberObject->getByEmail(trim($_REQUEST['member_email']), $zfsession->identity);
			
			if($tempData) {
				$errorArray['error'][] = 'Email already being used';
				$errorArray['result'] = false;
			}
		}
	} else {
		$errorArray['error'][] = 'Email is required';
		$errorArray['result'] = false;		
	}
	
	if(isset($_REQUEST['member_cellphone']) && trim($_REQUEST['member_cellphone']) != '') {
		if($memberObject->validateNumber(trim($_REQUEST['member_cellphone'])) == '') {
			$errorArray['error'][] = 'Valid cellphone is required';
			$errorArray['result'] = false;				
		} else {
			
			$emailData = $memberObject->getByCell(trim($_REQUEST['member_cellphone']), $zfsession->identity);
			
			if($emailData) {
				$errorArray['error'][] = 'Cellphone already being used';
				$errorArray['result'] = false;
			}
		}
	} else {
		$errorArray['error'][] = 'Cellphone is required';
		$errorArray['result'] = false;		
	}
	
	if(isset($_REQUEST['member_social_twitter']) && trim($_REQUEST['member_social_twitter']) != '') {
		if($memberObject->validateTwitter(trim($_REQUEST['member_social_twitter'])) == '') {
			$errorArray['error'][] = 'Your twitter handle was not found on twitter. Please check its spelling.';
			$errorArray['result'] = false;				
		}
	}
	
	
	if(count($errorArray['error']) == 0 && $errorArray['result'] == true) {

		$data 	= array();
		$data['branch_code']					= trim($_REQUEST['branch_code']);	
		$data['member_email']				= trim($_REQUEST['member_email']);	
		$data['member_cellphone']			= trim($_REQUEST['member_cellphone']);
		$data['member_birthdate']			= trim($_REQUEST['member_birthdate']);
		$data['member_social_twitter']	= trim($_REQUEST['member_social_twitter']);
		
		$where		= $memberObject->getAdapter()->quoteInto('member_code = ?', $zfsession->identity);
		$success	= $memberObject->update($data, $where);
	}
	
	$errorArray['error'] = implode("<br />",$errorArray['error']);
	
	echo json_encode($errorArray);
	exit;	
}

$branchPairs 		= $branchObject->pairs(array(3));

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>	
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/account/"><?php echo $zfsession->memberData['member_name']; ?></a></li>
						<li><a href="#">Update profile</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-8">
				<h3 class="heading-1"><span>Account</span></h3>	
				<ul class="nav nav-tabs">
					<li class="active"><a href="/account/">Person Information</a></li>
					<li><a href="/account/tag.php">My skillset</a></li>					
					<li><a href="/account/document.php">My Documents</a></li>
					<li><a href="/account/link.php">Link with old Email Account</a></li>
				</ul>
				<p style="clear: both;">&nbsp;&nbsp;&nbsp;&nbsp;</p>					
				<p>Please complete your member details below. After which we will be able to vet your account and make sure that you are part of the brownsense group on facebook.</p>
				<form class="post-comment-form"action="#" method="POST" >				
					<div class="row">
						<div class="col-md-4">
							<label>Your Name</label>
							<input type="text" id="member_name" name="member_name" value="<?php echo $zfsession->memberData['member_name']; ?>" readonly disabled />
						</div>
						<div class="col-md-4">
							<label>Your branch</label>
							<select id="branch_code" name="branch_code">
								<option value=""> ------- </option>
								<?php 
									while ($value = current($branchPairs)) {
										echo '<option '.($zfsession->memberData['branch_code'] == key($branchPairs) ? 'selected' : '').' value="'.key($branchPairs).'">'.$value.'</option>';
										next($branchPairs);
									}
								?>
							</select>
						</div>
						<div class="col-md-4">
							<label>Your birth date</label>
							<input type="text" id="member_birthdate" name="member_birthdate" value="<?php echo $zfsession->memberData['member_birthdate']; ?>" placeholder="YYYY-MM-DD" />
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
						<div class="alert alert-danger">
							<strong>Note:</strong>
							<br>
							<p>
							If you change your email address, your account will be disabled and you will be sent an email to activate and confirm it.
							</p>
						</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-md-4">
							<label>Your Email</label>
							<input type="text" id="member_email" name="member_email" value="<?php echo $zfsession->memberData['member_email']; ?>" />
						</div>
						<div class="col-md-4">
							<label>Your Cellphone number</label>
							<input type="text" id="member_cellphone" name="member_cellphone" value="<?php echo $zfsession->memberData['member_cellphone']; ?>" placeholder="10 digit South African Cell number" />
						</div>
						<div class="col-md-4">
							<label>Your TWITTER handler</label>
							<input type="text" id="member_social_twitter" name="member_social_twitter" value="<?php echo $zfsession->memberData['member_social_twitter']; ?>" placeholder="ONLY the twitter handler." />
						</div>			
					</div>		
					<div class="alert alert-success member_success" style="display: none; clear: both;">
						<strong>Well done!</strong><br />Your details have been successfully updated.
					</div>
					<div class="alert alert-danger member_fail" style="display: none; clear: both;">
						<strong>Oh snap!</strong><br /><p id="member_fail_message"></p>
					</div>
					<div class="alert alert-info member_submitting" style="display: none; clear: both;">
						<strong>Heads up!</strong> Submitting, please wait....
					</div>					
					<button type="button" onclick="saveMember(); return false;">Update Profile</button>
					<br /><br />
				</form>
			</div>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" language="javascript">

	$(document).ready(function(){

		$( "#areapost_name" ).autocomplete({
			source: "/feeds/areapost.php",
			minLength: 2,
			select: function( event, ui ) {
				if(ui.item.id == '') {
					$('#areapost_name').html('');
					$('#areapost_code').val('');					
				} else {
					$('#areapost_name').html(ui.item.value);
					$('#areapost_code').val(ui.item.id);	
				}
				$('#areapost_name').val('');										
			}
		});
	});
	
	function saveMember() {

		$('.member_submitting').show();
		$('.member_submitting').css('visibility', 'visible');
		
		$('.member_success').hide();
		$('.member_success').css('visibility', 'hidden');
		
		$('.member_fail').hide();
		$('.member_fail').css('visibility', 'hidden');
		     
		$.ajax({
			type: "GET",
			url: "?save_member=1",
			data: "member_name="+$('#member_name').val()+"&branch_code="+$('#branch_code').val()+"&areapost_code="+$('#areapost_code').val()+"&member_email="+$('#member_email').val()+"&member_cellphone="+$('#member_cellphone').val()+"&member_birthdate="+$('#member_birthdate').val()+"&member_social_twitter="+$('#member_social_twitter').val(),
			dataType: "json",
			success: function(data){
				if(data.result == 1) {
					$('.member_success').show();	
					$('.member_success').css('visibility', 'visible');
					
					$('.member_fail').hide();
					$('.member_fail').css('visibility', 'hidden');
					window.location.href = '/account/tag.php';
				} else {
					$('.member_fail').show();
					$('.member_fail').css('visibility', 'visible');
					
					$('.member_success').hide();
					$('.member_success').css('visibility', 'hidden');
					$('#member_fail_message').html(data.error);
				}
				$('.member_submitting').hide();
				$('.member_submitting').css('visibility', 'hidden');
			}
		});
		
		return false;
	}
</script>
</body>
</html>
