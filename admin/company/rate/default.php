<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/*** Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/**
 * Check for login
 */
require_once 'includes/auth.php';
require_once 'class/comment.php';
require_once 'class/company.php';
require_once 'class/vet.php';
require_once 'class/item.php';
require_once 'class/_comm.php';

$commentObject	= new class_comment();
$companyObject	= new class_company();
$vetObject 			= new class_vet();
$itemObject 			= new class_item();
$commObject 		= new class_comm();

if(isset($_GET['itemcode']) && isset($_GET['itemmessage']) && isset($_GET['companycode']) && isset($_GET['commentcode'])) {

	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 1;	

	$item 		= trim($_GET['itemcode']);
	$message	= trim($_GET['itemmessage']);
	$code		= trim($_GET['companycode']);
	$comment	= trim($_GET['commentcode']);
	
	$commentData = $commentObject->getByCode($comment);
	
	if(!$commentData) {
		$errorArray['error']	= 'Comment does not exist.';
		$errorArray['result']	= 0;
	}
	
	if($errorArray['error']  == '' && $errorArray['result']  == 1) {

		$data 							= array();				
		$data['item_code']			= $item;
		$data['vet_reason']		= $message;
		$data['vet_item_type']	= 'COMMENT';
		$data['vet_item_code']	= $comment;

		$success = $vetObject->insert($data);

		if($success) {

			$itemData = $itemObject->getByCode($item);

			if($itemData) {
				/* Email company for vetting. */
				$templateData = $commObject->_template->getTemplate('VET_NOTIFY', 'EMAIL');

				if($templateData) {

					$recipient	= array();
					$recipient['recipient_code'] 		= $commentData['comment_code'];
					$recipient['recipient_name'] 		= $commentData['comment_name'];
					$recipient['recipient_type'] 			= 'COMMENT';
					$recipient['recipient_email'] 		= $commentData['comment_email'];
					$recipient['recipient_vet_type'] 	= 'comment';
					$recipient['recipient_message'] 	= 'The comment has been vetted and the result is: <span style="color: '.$itemData['item_config_color'].'"><b>'.$itemData['item_name'].'</b></span>, reasons being: <br /><br /><b><i>'.trim($_POST['vet_reason']).'</i></b><br /><br />. '.((int)$itemData['item_config_status'] == 1 ? '<b style="color: green;">The comment will be visible on the website</b>' : '<b style="color: red">Please action accordingly</b>');

					$commObject->sendEmail($recipient, $templateData);
				}			
			}
		}
		
		if($success) {		
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}

	echo json_encode($errorArray);
	exit;
}

if(isset($_GET['action']) && trim($_GET['action']) == 'search') {

	$filter	= array();
	$csv		= 0;
	$start 	= isset($_REQUEST['iDisplayStart']) ? $_REQUEST['iDisplayStart'] : 0;
	$length	= isset($_REQUEST['iDisplayLength']) ? $_REQUEST['iDisplayLength'] : 20;
	
	if(isset($_REQUEST['filter_search']) && trim($_REQUEST['filter_search']) != '') $filter[] = array('filter_search' => trim($_REQUEST['filter_search']));
	if(isset($_REQUEST['filter_item']) && trim($_REQUEST['filter_item']) != '') $filter[] = array('filter_item' => trim($_REQUEST['filter_item']));
	if(isset($_REQUEST['filter_csv']) && trim($_REQUEST['filter_csv']) != '') { $filter[] = array('filter_csv' => (int)trim($_REQUEST['filter_csv'])); $csv = (int)trim($_REQUEST['filter_csv']); }
	
	$comments = array();
	
	if(!$csv) {

		$commentData = $commentObject->getSearch('COMPANY', $start, $length, $filter);

		if($commentData) {
			for($i = 0; $i < count($commentData['records']); $i++) {
				$item = $commentData['records'][$i];
				$images = '';
				for($m = 0; $m < count($item['images']); $m++) {
						$image = $item['images'][$m];
						$images .= '<a href="'.$image['media_path'].'">'.($m+1).' -  '.$image['media_filename'].'</a><br />';
				}
				$comments[$i] = array(
					'<a href="/company/list/details.php?code='.$item['company_code'].'" target="_blank">'.$item['company_name'].'</a>',
					($item['social_facebook_id'] == '' ? '<img src="/images/no-image.jpg" width="50px" />' : '<a href="http://facebook.com/'.$item['social_facebook_id'].'" target="_blank"><img src="https://graph.facebook.com/'.$item['social_facebook_id'].'/picture?width=300" width="50px" /></a>'),
					'<b>Rating Given</b><br />'.$item['rate_number'].' / 10'.'<br /><b>Message</b><br />'.$item['comment_text'].'<br /><b>Documents</b><br />'.($images != '' ? $images : 'N / A'),
					($item['item_name'] != '' ? '<span style="color: '.$item['item_config_color'].'" alt="'.$item['vet_reason'].'" title="'.$item['vet_reason'].'">'.$item['item_name'].'</span>' : 'Not vetted'),
					'<button value="Vet" class="btn btn-info" onclick="vetCompanyModal(\''.$item['comment_code'].'\', \''.$item['company_code'].'\', \''.str_replace("'", "", $item['company_name']).'\'); return false;">Vet</button>');
			}
		}

		if($commentData) {
			$response['sEcho'] = $_REQUEST['sEcho'];
			$response['iTotalRecords'] = $commentData['displayrecords'];		
			$response['iTotalDisplayRecords'] = $commentData['count'];
			$response['aaData']	= $comments;
		} else {
			$response['result'] 	= false;
			$response['message']	= 'There are no items to show.';			
		}

		echo json_encode($response);
		die();

	} else {
		
		$commentData = $commentObject->getSearch('COMPANY', $start, $length, $filter);
		
		$row = "Vet, Vet Message, Company, Name, Email, Number, Rating out of 10, Comments\r\n";
		if($commentData) {
			for($i = 0; $i < count($commentData); $i++) {
				$item = $commentData[$i];
				$comments[$i] = array(						
						($item['item_name'] == '' ? 'Not Vetted' : $item['item_name']),
						($item['vet_reason'] == '' ? 'N / A' : preg_replace( "/\r|\n/", "", str_replace(',', '', $item['vet_reason']))),
						str_replace(',', ' ',$item['company_name']),
						str_replace(',', ' ',$item['comment_name']),
						str_replace(',', ' ',$item['comment_email']),
						str_replace(',', ' ',$item['comment_number']),
						str_replace(',', ' ',$item['rate_number']),
						str_replace(',', ' ',$item['comment_text'])
				);
			}
			
			foreach ($comments as $data) {
				$row .= implode(', ', $data)."\r\n";
			}			
		}
		
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=company_ratings_report_".date('Y-m-d').".csv");
		Header("Cache-Control: private, must-revalidate" ); // HTTP/1.1
		Header("Pragma: private" ); // HTTP/1.0
	   
		print($row);
		exit;		
	}
}

$itemData = $itemObject->selectParents('VET', 'COMMENT');
if($itemData) $smarty->assign('itemData', $itemData);

$smarty->display('company/rate/default.tpl');
?>