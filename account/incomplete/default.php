<?php 
/* Add this on all pages on top. */
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/iauth.php';

/* Check if the account has been completed, active and approved by administrators. */
if(trim($zfsession->memberData['member_linked']) != '') {
	/* Nothing has been completed, please complete site. */
	header('Location: /account/incomplete/details.php');
	exit;
}

/* objects. */
require_once 'class/member.php';
require_once 'class/branch.php';

$memberObject = new class_member();
$branchObject 	= new class_branch();

if(isset($_GET['password'])) {

	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success					= NULL;
		
	if(isset($_GET['sendPassword']) && trim($_GET['sendPassword']) != '') {
		if($memberObject->validateEmail(trim($_GET['sendPassword'])) == '') {
			$errorArray['error'] 	= 'A valid email address is required';
			$errorArray['result']	= 0;			
		} else {

			$mailData = $memberObject->getByEmail(trim($_GET['sendPassword']));
			
			if(!$mailData) {
				$errorArray['error'] 	= 'Email address does not exist';
				$errorArray['result']	= 0;
			}
		}
	} else {
		$errorArray['error'] 	= 'An email address is required';
		$errorArray['result']	= 0;
	}
	
	if(isset($mailData)) {

		$templateData = $memberObject->_comm->_template->getTemplate('REGISTRATION_ACTIVATE', 'EMAIL');

		if($templateData) {

			$recipient	= array();
			$recipient['recipient_code'] 		= $mailData['member_code'];
			$recipient['recipient_name'] 		= $mailData['member_name'];
			$recipient['recipient_surname'] 	= $mailData['member_surname'];
			$recipient['recipient_cellphone'] 	= $mailData['member_cellphone'];
			$recipient['recipient_type'] 			= 'MEMBER';
			$recipient['recipient_email'] 		= $mailData['member_email'];
			$recipient['recipient_password'] 	= $mailData['member_password'];
			$recipient['recipient_hashcode'] 	= $mailData['member_hashcode'];

			$memberObject->_comm->sendEmail($recipient, $templateData);

		} else {
			$errorArray['error'] 	= 'Template does not exist, please contact the administrator.';
			$errorArray['result']	= 0;
		}
	}

	echo json_encode($errorArray);
	exit;
}

if(count($_POST) > 0 && isset($_POST['member_linked'])) {
	
	if((int)trim($_POST['member_linked']) == 1) {

		$errorLoginArray	= array();
		$data 					= array();
		$formValid			= true;
		$success				= NULL;
		
		if(isset($_POST['member_email']) && trim($_POST['member_email']) == '') {
			$errorLoginArray[] = 'Your email address is required.';
			$formValid = false;		
		}

		if(isset($_POST['member_password']) && trim($_POST['member_password']) == '') {
			$errorLoginArray[] = 'Your password is required.';
			$formValid = false;		
		}
		
		if(count($errorLoginArray) == 0 && $formValid == true) {
			
			$memberLoginData	= $memberObject->login(trim($_POST['member_email']), trim($_POST['member_password']));
			
			$message					= '';
			
			if($memberLoginData) {
				/* Successfull log in. */
				$data = array();
				$data['member_name'] 		= $zfsession->memberData['member_name'];
				$data['social_facebook_id']	= $zfsession->memberData['social_facebook_id'];
				$data['member_linked']		= 1;

				$where		= $memberObject->getAdapter()->quoteInto('member_code = ?', $memberLoginData['member_code']);
				$memberObject->update($data, $where);
				/* Delete new account if all went well. */
				$newData = $memberObject->checkChange($memberLoginData['member_code'], $zfsession->memberData['social_facebook_id']);

				if($newData) {
					/* Successfull log in. */
					$data 								= array();
					$data['member_deleted']	= 1;

					$where		= $memberObject->getAdapter()->quoteInto('member_code = ?', $zfsession->memberData['member_code']);
					$memberObject->update($data, $where);
					/* Delete and change session id. */
					$zfsession->memberData	= $newData;
					$zfsession->identity 			= $newData['member_code'];

					header('Location: /');
					exit;

				} else {
					$errorLoginArray[] = 'Could not link your account, if this continues please contact administrator via our facebook group/page or email us on <a href="mailto:support@brownsense.co.za">support@brownsense.co.za</a>';
				}
			} else {
				$errorLoginArray[] = 'The email and/or password you entered could not be found.';
			}
		}
	} else if((int)trim($_POST['member_linked']) == 0) {
		/* Successfull log in. */
		$data 							= array();
		$data['member_linked']	= 0;

		$where		= $memberObject->getAdapter()->quoteInto('member_code = ?', $zfsession->memberData['member_code']);
		$memberObject->update($data, $where);

		header('Location: /account/incomplete/details.php');
		exit;		
	}
}

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<link rel="stylesheet" href="/css/jquery-ui-1.12.1.css">
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/account/">Account</a></li>
						<li><a href="#"><?php echo $zfsession->memberData['member_name']; ?></a></li>
						<li><a href="/account/incomplete/">Incomplete Profile</a></li>
						<li><a href="#">Link with old account</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8">
				<h3 class="heading-1"><span>Link facebook account with my old email account profile</span></h3>
				<p>If you have registered with us before using your email address in the old brownsense website, you can link your email profile with your facebook profile by giving us your log in credentials below</p>
				<form class="post-comment-form" name="linkedForm" id="linkedForm" action="/account/incomplete/" method="POST" >		
					<br />
					<?php if(isset($errorLoginArray) && count($errorLoginArray) != 0) { ?>
						<div class="alert alert-danger reg_member_fail" style="">
							<strong>Oh snap! There were some issue with your credentials</strong>
							<br>
							<p>
							<?php
							for($i = 0; $i < count($errorLoginArray); $i++) {
							 echo $errorLoginArray[$i].'<br />'; 
							} 
							 ?>								
							</p>
						</div>
					<?php } ?>						
					<div class="row">
						<div class="col-md-6">
							<label>Your Email</label>
							<input type="text" id="member_email" name="member_email" value="" placeholder="Email address you used to register" />
						</div>
						<div class="col-md-6">
							<label>Password</label>
							<input type="password" id="member_password" name="member_password" value="" placeholder="Password emailed after account activation" />
						</div>
					</div>		
					<button type="submit">Link account</button>&nbsp;&nbsp;&nbsp;&nbsp;
					<button onclick="noThanks(); return false;">No thanks, I never had an account</button>&nbsp;&nbsp;&nbsp;&nbsp;
					<button onclick="resentPassword(); return false;">I forgot my password</button>
					<br /><br />
					<input type="hidden" value="1" id="member_linked" name="member_linked" />
				</form>		
			</div>
			<aside class="col-md-4 col-sm-4">
				<div class="side-widget margin-bottom-30">
					<h3 class="heading-1"><span>Facebook profile picture</span></h3>
					<img src="https://graph.facebook.com/<?php echo $zfsession->memberData['social_facebook_id']; ?>/picture?width=300" />
				</div>
			</aside>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
<script type="text/javascript">
function noThanks() {
	$('#member_linked').val('0');
	document.forms.linkedForm.submit();
	return false;
}

function resentPassword() {
	$('#resentPassword').modal('show');
	return false;
}

function sendPassword() {
	$.ajax({
			type: "GET",
			url: "?sendPassword="+$('#password_email').val(),
			data: "password=1",
			dataType: "json",
			success: function(data){
				if(data.result == 1) {
					$('#passwordSuccess').html('You have been sent an email with your password, please check it out and log in with it.');
					$('#passwordSuccess').show();
					$('#passwordError').hide();
				} else {
					$('#passwordError').html(data.error);
					$('#passwordError').show();
					$('#passwordSuccess').hide();
				}
			}
		});	

	return false;
}
</script>
<!-- Modal -->
<div class="modal fade" id="resentPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">I forgot my password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<label>Your Email</label>
						<input type="text" id="password_email" name="password_email" value="" placeholder="Email address you registered with" />
					</div>
				</div>
				<p>Please add your email address above. Use the email address you used to register with before.</p>
				<p class="error" id="passwordError" name="passwordError" style="display: none; color: red; font-weight: bold;"></p>
				<p class="success" id="passwordSuccess" name="passwordSuccess" style="display: none; color: red; font-weight: bold;"></p>
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:sendPassword();">Resent Password</button>
			</div>
		</div>
	</div>
</div>
<!-- modal -->
</body>
</html>
