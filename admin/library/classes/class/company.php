<?php

//custom account item class as account table abstraction
class class_company extends Zend_Db_Table_Abstract
{
	//declare table variables
    protected $_name 		= 'company';
	protected $_primary 	= 'company_code';
	
	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 
	 public function insert(array $data)
    {
        // add a timestamp
        $data['company_added'] = date('Y-m-d H:i:s');        
		$data['company_code']	= $this->createCode();
		
		return parent::insert($data);
		
    }
	
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean 
	 */
    public function update(array $data, $where)
    {
        // add a timestamp
        $data['company_updated'] = date('Y-m-d H:i:s');
        
        return parent::update($data, $where);
    }
	
	/**
	 * get job by job company Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code) {	
		$select = $this->_db->select()	
				->from(array('company' => 'company'))	
				->joinLeft(array('member' => 'member'), 'member.member_code = company.member_code')
				->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = company.areapost_code')
				->joinLeft(array('vet' => 'vet'), "vet.vet_item_code = company.company_code and vet.vet_item_type = 'COMPANY' and vet_active = 1 and vet_deleted = 0")
				->joinLeft(array('item' => 'item'), "item.item_code = vet.item_code and item_deleted = 0")	
				->where('company.company_deleted = 0 and member_deleted = 0 and company_code = ?', $code)
				->limit(1);
		
		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;

	}

	/**
	 * get job by job company Id
 	 * @param string job id
     * @return object
	 */
	public function getAll() {	 
	
		$tag = $this->_db->select()	
					->from(array('link' => 'link'), array('link_parent_code', "group_concat(item.item_name separator ',') as company_services"))
					->joinLeft(array('item' => 'item'), 'item.item_code = link.link_child_code', array())	
					->where("link_parent_type = 'COMPANY' and link_child_type = 'TAG'")
					->group('link_parent_code');	

		$select = $this->_db->select()	
				->from(array('company' => 'company'))	
				->joinLeft(array('member' => 'member'), 'member.member_code = company.member_code')
				->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = company.areapost_code')
				->joinLeft(array('tag' => $tag), 'tag.link_parent_code = company.company_code')
				->joinLeft(array('vet' => 'vet'), "vet.vet_item_code = company.company_code and vet.vet_item_type = 'COMPANY' and vet_active = 1 and vet_deleted = 0")
				->joinLeft(array('item' => 'item'), "item.item_code = vet.item_code and item_deleted = 0")					
				->where('company.company_deleted = 0 and member_deleted = 0')
				->order('company.company_added desc');

	   $result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;

	}
	
	public function search($query, $limit = 20) {

		$select = $this->_db->select()
						->from(array('company' => 'company'), array('company.company_code', 'company.company_name'))		
					   ->where("company.company_name like lower(?)", "%$query%")
					   ->limit($limit)
					   ->order("LOCATE('$query', company.company_name)");

		$result = $this->_db->fetchAll($select);	
        return ($result == false) ? false : $result = $result;					

	}
	
	public function getSearch($start, $length, $filter = array()) {
	
		$where 		= 'company_deleted = 0';
		$csv 			= 0;

		if(count($filter) > 0) {	
			for($i = 0; $i < count($filter); $i++) { 
				if(isset($filter[$i]['filter_search']) && trim($filter[$i]['filter_search']) != '') {
					$array = explode(" ",trim($filter[$i]['filter_search']));					
					if(count($array) > 0) {
						$where .= " and (";
						for($s = 0; $s < count($array); $s++) {
							$text = $array[$s];
							$this->sanitize($text);
							$where .= "lower(concat_ws(company_name, company_services, company_contact_name, company_contact_email, company_contact_number)) like lower('%$text%')";
							if(($s+1) != count($array)) {
								$where .= ' or ';
							}
						}
					}
					$where .= ")";
				} else if(isset($filter[$i]['filter_csv']) && (int)trim($filter[$i]['filter_csv']) == 1) {
					$csv = 1;
				} else if(isset($filter[$i]['filter_item']) && trim($filter[$i]['filter_item']) != '') {
					$itemcode = $filter[$i]['filter_item'];
					$this->sanitize($itemcode);
					if((int)$itemcode == 1) {
						$where .= " and vet.item_code = '' ";
					} else {
						$where .= " and vet.item_code = '$itemcode' ";
					}
					
				}
			}
		}

		$tag = $this->_db->select()	
					->from(array('link' => 'link'), array('link_parent_code', "group_concat(item.item_name separator ',') as company_services"))
					->joinLeft(array('item' => 'item'), 'item.item_code = link.link_child_code', array())	
					->where("link_parent_type = 'COMPANY' and link_child_type = 'TAG'")
					->group('link_parent_code');

		$select = $this->_db->select()	
				->from(array('company' => 'company'))	
				->joinLeft(array('member' => 'member'), 'member.member_code = company.member_code', array())
				->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = company.areapost_code', array('areapost_name'))
				->joinLeft(array('tag' => $tag), 'tag.link_parent_code = company.company_code')	
				->joinLeft(array('vet' => 'vet'), "vet.vet_item_code = company.company_code and vet.vet_item_type = 'COMPANY' and vet_active = 1 and vet_deleted = 0", array('vet_reason'))
				->joinLeft(array('item' => 'item'), "item.item_code = vet.item_code and item_deleted = 0", array('item_name', 'item_config_status', 'item_config_color', 'item_config_class'))
				->where('company.company_deleted = 0 and member_deleted = 0')
				->where($where)
				->order('company.company_added desc');

		if($csv) {
			$result = $this->_db->fetchAll($select);
			return ($result == false) ? false : $result = $result;	
		} else {
			$result_count = $this->_db->fetchRow("select count(*) as query_count from ($select) as query");
			$result = $this->_db->fetchAll($select . " limit $start, $length");
			return ($result === false) ? false : $result = array('count'=>$result_count['query_count'],'displayrecords'=>count($result),'records'=>$result);	
		}
	}
	
	public function csv($search = '') {
		$tag = $this->_db->select()	
					->from(array('link' => 'link'), array('link_parent_code', "group_concat(item.item_name separator ',') as company_services"))
					->joinLeft(array('item' => 'item'), 'item.item_code = link.link_child_code', array())	
					->where("link_parent_type = 'COMPANY' and link_child_type = 'TAG'")
					->group('link_parent_code');	
				
		if($search == '') {
			$select = $this->_db->select()	
				->from(array('company' => 'company'))	
				->joinLeft(array('member' => 'member'), 'member.member_code = company.member_code')
				->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = company.areapost_code')
				->joinLeft(array('tag' => $tag), 'tag.link_parent_code = company.company_code')	
				->joinLeft(array('vet' => 'vet'), "vet.vet_item_code = company.company_code and vet.vet_item_type = 'COMPANY' and vet_active = 1 and vet_deleted = 0", array('vet_reason'))
				->joinLeft(array('item' => 'item'), "item.item_code = vet.item_code and item_deleted = 0", array('item_name', 'item_config_status', 'item_config_color', 'item_config_class'))			
				->where('company.company_deleted = 0 and member_deleted = 0')
				->order('company.company_added desc');
		} else {
			$select = $this->_db->select()	
				->from(array('company' => 'company'))	
				->joinLeft(array('member' => 'member'), 'member.member_code = company.member_code')
				->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = company.areapost_code')
				->joinLeft(array('tag' => $tag), 'tag.link_parent_code = company.company_code')	
				->joinLeft(array('vet' => 'vet'), "vet.vet_item_code = company.company_code and vet.vet_item_type = 'COMPANY' and vet_active = 1 and vet_deleted = 0", array('vet_reason'))
				->joinLeft(array('item' => 'item'), "item.item_code = vet.item_code and item_deleted = 0", array('item_name', 'item_config_status', 'item_config_color', 'item_config_class'))
				->where('company.company_deleted = 0 and member_deleted = 0')
				->where("lower(concat_ws(company_name, company_services, company_contact_name, company_contact_email, company_contact_number)) like lower(?)", "%$search%")
				->order("LOCATE('$search', company_name)");			
		}

		$result = $this->_db->fetchAll($select);

		if($result) {
			for($i = 0; $i < count($result); $i++) {
				$result[$i] = array_merge($result[$i], $this->_latest('COMPANY', $result[$i]['company_code']));
			}
		}		
		return ($result == false) ? false : $result = $result;		
	}

	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getCode($reference)
	{
		$select = $this->_db->select()	
				->from(array('company' => 'company'))	
				->where('company_code = ?', $reference)
				->limit(1);

	   $result = $this->_db->fetchRow($select);	
        return ($result == false) ? false : $result = $result;					   
	}

	function createCode() {
		/* New code. */
		$code = "";
		 
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '1234567890';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<1;$i++){
			$code .= $Number[rand(0,$count)];
		}
		 
		/* Last alphabet. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)]; 
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Number[rand(0,$count)];
		}	
		
		/* First check if it exists or not. */
		$itemCheck = $this->getCode($code);
		
		if($itemCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}	

	public function validateEmail($string) {
		if(!filter_var($string, FILTER_VALIDATE_EMAIL)) {
			return '';
		} else {
			return trim($string);
		}
	}
	
	public function validateNumber($string) {
		if(preg_match('/^0[0-9]{9}$/', $this->onlyContactNumber(trim($string)))) {
			return $this->onlyContactNumber(trim($string));
		} else {
			return '';
		}
	}
	
	public function onlyContactNumber($string) {
		/* Remove some weird charactors that windows dont like. */
		$string = strtolower($string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("�", "", $string);	
		$string = str_replace('___' , '' , $string);
		$string = str_replace('__' , '' , $string);	
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("�", "", $string);	
		$string = str_replace("–", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("#", "", $string);	
		$string = str_replace("$", "", $string);	
		$string = str_replace("@", "", $string);	
		$string = str_replace("!", "", $string);	
		$string = str_replace("&", "", $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(':' , '' , $string);		
		$string = str_replace('[' , '' , $string);		
		$string = str_replace(']' , '' , $string);		
		$string = str_replace('|' , '' , $string);		
		$string = str_replace('\\' , '' , $string);		
		$string = str_replace('%' , '' , $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);	
		$string = str_replace('-' , '' , $string);	
		$string = str_replace('+27' , '0' , $string);	
		$string = str_replace('(0)' , '' , $string);	
		
		$string = preg_replace('/^00/', '0', $string);
		$string = preg_replace('/^27/', '0', $string);
		
		$string = preg_replace('!\s+!',"", strip_tags($string));
		
		return $string;
	}	
	
    function sanitize(&$string) { $string = preg_replace("/[^a-zA-Z0-9_]+/", "", $string);}
	
    function sanitizeArray(&$array) 
    {        
		for($i = 0; $i < count($array); $i++) {
			$array[$i] = preg_replace("/[^a-zA-Z0-9_]+/", "", $array[$i]);
		}
    }		
}
?>