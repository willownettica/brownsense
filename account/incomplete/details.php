<?php 
/* Add this on all pages on top. */
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/iauth.php';

/* Check if the account has been completed, active and approved by administrators. */
if($zfsession->memberData['branch_code'] != '' && $zfsession->memberData['member_email'] != '' && $zfsession->memberData['member_cellphone'] != '' && $zfsession->memberData['member_birthdate'] != '') {
	/* Nothing has been completed, please complete site. */
	header('Location: /account/incomplete/activate.php');
	exit;
}

/* objects. */
require_once 'class/member.php';
require_once 'class/branch.php';

$memberObject = new class_member();
$branchObject 	= new class_branch();

/* Check posted data. */
if(count($_POST) > 0 && isset($_POST['member_profile'])) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;

	if(isset($_POST['member_birthdate']) && trim($_POST['member_birthdate']) == '') {
		$errorArray[] = 'Your birth date is required';
		$formValid = false;		
	} else {
		if($memberObject->validateDate(trim($_POST['member_birthdate'])) == '') {
			$errorArray[] = 'A valid birth date is required';
			$formValid = false;				
		}
	}
	
	if(isset($_POST['branch_code']) && trim($_POST['branch_code']) == '') {
		$errorArray[] = 'Your nearest branch or branch you are in is required';
		$formValid = false;		
	}
	
	if(isset($_POST['member_email']) && trim($_POST['member_email']) != '') {
		if($memberObject->validateEmail(trim($_POST['member_email'])) == '') {
			$errorArray[] = 'A valid email address is required';
			$formValid = false;				
		} else {
			$temp = isset($zfsession->memberData) ? $zfsession->memberData['member_code'] : null;
			
			$tempData = $memberObject->getByEmail(trim($_POST['member_email']), $temp);
			
			if($tempData) {
				$errorArray[] = 'Selected email address is already being used';
				$formValid = false;
			}
		}
	} else {
		$errorArray[] = 'Your email address is required';
		$formValid = false;		
	}
	
	if(isset($_POST['member_cellphone']) && trim($_POST['member_cellphone']) != '') {
		if($memberObject->validateNumber(trim($_POST['member_cellphone'])) == '') {
			$errorArray[] = 'Valid cellphone is required';
			$formValid = false;				
		} else {
			$temp = isset($zfsession->memberData) ? $zfsession->memberData['member_code'] : null;
			
			$tempData = $memberObject->getByCell(trim($_POST['member_email']), $temp);
			
			if($tempData) {
				$errorArray[] = 'Selected cellphone number is already being used';
				$formValid = false;
			}
		}
	} else {
		$errorArray[] = 'Your cellphone number is required';
		$formValid = false;		
	}
	
	if(!isset($_POST['member_policy']) || (!isset($_POST['member_policy']) && (int)trim($_POST['member_policy']) != 1)) {
		$errorArray[]	= 'You need to agree with our terms and conditions before you continue';
		$formValid 		= false;		
	}
	
	if(count($errorArray) == 0 && $formValid == true) {

		$data 										= array();	
		$data['branch_code']					= trim($_POST['branch_code']);
		$data['member_email']				= trim($_POST['member_email']);	
		$data['member_cellphone']			= trim($_POST['member_cellphone']);
		$data['member_birthdate']			= trim($_POST['member_birthdate']);
		$data['member_social_twitter']	= trim($_POST['member_social_twitter']);
		$data['member_policy']				= 1;

		$where		= $memberObject->getAdapter()->quoteInto('member_code = ?', $zfsession->memberData['member_code']);
		$success	= $memberObject->update($data, $where);
		$message = 'Your details have been updated.<br />';

		if($zfsession->memberData['member_email'] == '' || ($zfsession->memberData['member_email'] != $data['member_email'])) {
			/* Email needs to be activated, since its new or changed. */
			$email = array();
			$email['member_active'] 		= 0;
			$email['member_hashcode']	= md5($data['member_email'].date('Y-m-d h:i:s'));

			$where		= $memberObject->getAdapter()->quoteInto('member_code = ?', $zfsession->memberData['member_code']);
			$success	= $memberObject->update($email, $where);

			$message .= 'We have sent you an email to <b>'.$data['member_email'].'</b>, in order to activate your account, and proof this is your email address.<br />';

			$mailData = $memberObject->getByCode($zfsession->memberData['member_code']);

			if($mailData) {
				$templateData = $memberObject->_comm->_template->getTemplate('REGISTRATION_ACTIVATE', 'EMAIL');

				if($templateData) {

					$recipient	= array();
					$recipient['recipient_code'] 		= $mailData['member_code'];
					$recipient['recipient_name'] 		= $mailData['member_name'];
					$recipient['recipient_surname'] 	= $mailData['member_surname'];
					$recipient['recipient_cellphone'] 	= $mailData['member_cellphone'];
					$recipient['recipient_type'] 			= 'MEMBER';
					$recipient['recipient_email'] 		= $mailData['member_email'];
					$recipient['recipient_password'] 	= $mailData['member_password'];
					$recipient['recipient_hashcode'] 	= $mailData['member_hashcode'];

					$memberObject->_comm->sendEmail($recipient, $templateData);

				}
			}
		}
		header('Location: /');	
		exit;		
	}
}

$registerBranchPairs = $branchObject->pairs(array(3));

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<link rel="stylesheet" href="/css/jquery-ui-1.12.1.css">
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/account/">Account</a></li>
						<li><a href="#"><?php echo $zfsession->memberData['member_name']; ?></a></li>
						<li><a href="/account/incomplete/">Incomplete Profile</a></li>
						<li><a href="#">Profile details and/or email</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8">
				<h3 class="heading-1"><span>Update my profile</span></h3>
				<br />				
				<p>Please complete your member details below. After which we will be able to vet your account and make sure that you are part of the brownsense group on facebook.</p>
				<br />
				<form class="post-comment-form"action="/account/incomplete/details.php" method="POST" >		
					<?php 
						if(isset($errorArray) && count($errorArray) != 0) {
						?>
						<div class="alert alert-danger reg_member_fail" style="">
							<strong>Oh snap! There were some issue with your submission, please see below:</strong>
							<br>
							<p>
							<?php
							for($i = 0; $i < count($errorArray); $i++) {
							 echo $errorArray[$i].'<br />'; 
							} 
							 ?>								
							</p>
						</div>
						<?php
						}
						if(isset($message)) {
						?>
						<div class="alert alert-success reg_member_fail" style="">
							<strong>Successfully updated!</strong>
							<br>
							<p>
							<?php echo $message; ?>
							</p>
						</div>							
						<?php
						}
					?>
					<br />
					<div class="row">
						<div class="col-md-6">
							<label>Your Name</label>
							<input type="text" id="member_name" name="member_name" value="<?php echo $zfsession->memberData['member_name']; ?>" readonly disabled />
						</div>

						<div class="col-md-6">
							<label>Your branch</label>
							<select id="branch_code" name="branch_code">
								<option value=""> ------- </option>
								<?php 
									while ($value = current($registerBranchPairs)) {
										echo '<option '.($zfsession->memberData['branch_code'] == key($registerBranchPairs) ? 'selected' : '').' value="'.key($registerBranchPairs).'">'.$value.'</option>';
										next($registerBranchPairs);
									}
								?>   
							</select>						
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label>Your Email</label>
							<input type="text" id="member_email" name="member_email" value="<?php echo $zfsession->memberData['member_email']; ?>" />
						</div>

						<div class="col-md-6">
							<label>Your Cellphone number</label>
							<input type="text" id="member_cellphone" name="member_cellphone" value="<?php echo $zfsession->memberData['member_cellphone']; ?>" placeholder="10 digit South African Cell number" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label>Your birth date</label>
							<input type="text" id="member_birthdate" name="member_birthdate" value="<?php echo $zfsession->memberData['member_birthdate']; ?>" placeholder="YYYY-MM-DD" />
						</div>

						<div class="col-md-6">
							<label>Your TWITTER handler</label>
							<input type="text" id="member_social_twitter" name="member_social_twitter" value="<?php echo $zfsession->memberData['member_social_twitter']; ?>" placeholder="ONLY the handler." />
						</div>			
					</div>
					<div class="row">
						<div class="col-md-12">
							<label>Terms and Conditions</label>
							<input id="member_policy" name="member_policy" value="1" type="checkbox" <?php if((int)$zfsession->memberData['member_policy'] == 1) { ?>checked<?php }?> >	We will contact you to send your our weekly newsletter or share any relevant information with you. Please see our <a href="/privacy.php" target="_blank">Privacy Policy</a> 
						</div>		
					</div>		
					<br /><br />
					<button type="submit">Update My Details</button>
					<br /><br />
					<input type="hidden" value="1" id="member_profile" name="member_profile" />
				</form>			
			</div>
			<aside class="col-md-4 col-sm-4">
			
			<div class="side-widget margin-bottom-30">
				<h3 class="heading-1"><span>Facebook profile picture</span></h3>
				<img src="https://graph.facebook.com/<?php echo $zfsession->memberData['social_facebook_id']; ?>/picture?width=300">
				</div>
			</aside>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>

<?php require_once 'includes/javascript.php'; ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script type="text/javascript" language="javascript">

		$(document).ready(function(){

			$( "#areapost_name" ).autocomplete({
				source: "/feeds/areapost.php",
				minLength: 2,
				select: function( event, ui ) {
					if(ui.item.id == '') {
						$('#areapost_name').html('');
						$('#areapost_code').val('');					
					} else {
						$('#areapost_name').html(ui.item.value);
						$('#areapost_code').val(ui.item.id);	
					}
					$('#areapost_name').val('');										
				}
			});
		});
	</script>
</body>
</html>
