<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/auth.php'; 

/* objects. */
require_once 'class/member.php';
require_once 'class/areapost.php';
require_once 'class/item.php';

$memberObject 	= new class_member();
$areapostObject 	= new class_areapost();
$itemObject 			= new class_item();

$where			= array();
$parameter	= "";
$filters			= array();

if(isset($_REQUEST['filter_search']) && trim($_REQUEST['filter_search']) != '') $where[] = array('filter_search' => trim($_REQUEST['filter_search']));
if(isset($_REQUEST['filter_province']) && trim($_REQUEST['filter_province']) != '') $where[] = array('filter_province' => trim($_REQUEST['filter_province']));
if(isset($_REQUEST['filter_category']) && trim($_REQUEST['filter_category']) != '') $where[] = array('filter_category' => trim($_REQUEST['filter_category']));


if(count($where) > 0) {
	for($i = 0; $i < count($where); $i++) { 
		if(isset($where[$i]['filter_search']) && trim($where[$i]['filter_search']) != '') {
			$parameter .= "&filter_search=".$where[$i]['filter_search'];
			$filters['filter_search'] = $where[$i]['filter_search'];
		} else if(isset($where[$i]['filter_province']) && trim($where[$i]['filter_province']) != '') {
			$parameter .= "&filter_province=".$where[$i]['filter_province'];
			$filters['filter_province'] = $where[$i]['filter_province'];
		} else if(isset($where[$i]['filter_category']) && trim($where[$i]['filter_category']) != '') {
			$parameter .= "&filter_category=".$where[$i]['filter_category'];
			$filters['filter_category'] = $where[$i]['filter_category'];
		}
	}
}

$page 		= isset($_GET['page']) 		? $_GET['page'] 		: 1;
$perPage	= isset($_GET['perPage'])	? $_GET['perPage']	: 10;

$memberData = $memberObject->paginate($where, $page, $perPage);

$memberItems = $memberData->getCurrentItems();

if($memberItems) {
	$paginator = $memberData->setView()->getPages();
}

$provinceData	= $areapostObject->getProvinces();
$categoryPairs	= $itemObject->selectType('CATEGORY');

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<link rel="stylesheet" href="/css/jquery-ui-1.12.1.css">
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/members/">Members</a></li>						
						<li><a href="#">Search member</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<h2>Members</h2><br />
			<div class="col-md-12 col-sm-7">
					<h3 class="heading-1"><span>Filter</span></h3>		
					<form class="post-comment-form"action="/members/" method="GET">		
						<div class="row">
							<div class="col-md-3">
								<label>Search</label>
								<input type="text" id="filter_search" name="filter_search" value="<?php echo (isset($_REQUEST['filter_search']) && $_REQUEST['filter_search'] != '' ? $_REQUEST['filter_search'] : ''); ?>" style="margin-bottom: 2px !important;" />
							</div>
							<div class="col-md-3">
								<label>Province</label>
								<select id="filter_province" name="filter_province" style="margin-bottom: 2px !important;">
									<option value=""> ------------------- </option>
									<?php 
										while ($value = current($provinceData)) {
											echo '<option '.(isset($_REQUEST['filter_province']) &&  $_REQUEST['filter_province'] == key($provinceData) ? 'selected' : '').' value="'.key($provinceData).'">'.$value.'</option>';
											next($provinceData);
										}
									?>
								</select>
							</div>
							<div class="col-md-3">
								<label>Category</label>
								<select id="filter_category" name="filter_category" style="margin-bottom: 2px !important;">
									<option value=""> ------------------- </option>
									<?php 
										while ($value = current($categoryPairs)) {
											echo '<option '.(isset($_REQUEST['filter_category']) &&  $_REQUEST['filter_category'] == key($categoryPairs) ? 'selected' : '').' value="'.key($categoryPairs).'">'.$value.'</option>';
											next($categoryPairs);
										}
									?>
								</select>
							</div>
							<div class="col-md-3">
								<label>&nbsp;&nbsp;</label>
								<br />
								<button type="submit">Update Filter</button>&nbsp;&nbsp;&nbsp;&nbsp;
								<button type="button" onclick="window.location.href = '/members/'; return false;">Clear Search</button>
							</div>								
						</div>						

					</form>			
			</div>
		</div>
		<br /><br />
		<div class="row">		
			<div class="col-md-12 col-sm-7 dual-posts padding-bottom-30">				
				<p>Search members. The number of items found so far is <span class="text_brown"><?php echo $paginator->totalItemCount; ?></span>. <?php echo ((int)$paginator->totalItemCount > 0 ? 'Search has <span class="text_brown">'.$paginator->pageCount.'</span> pages.' : '') ; ?></p>
				<p><?php if(isset($_REQUEST['filter_search']) && $_REQUEST['filter_search'] != '') { ?>Searching for text: '<i><span class="normal-font text_brown"><?php echo $_REQUEST['filter_search']; ?></span></i>'.<?php } ?>
				<?php if(isset($_REQUEST['filter_province']) && $_REQUEST['filter_province'] != '') { ?>Results in <span class="normal-font text_brown"><?php echo $provinceData[$_REQUEST['filter_province']]; ?></span>.<?php } ?>		
				</p>					
				<br />
				<?php if($paginator->pageCount > 1) { ?>
				<!-- PAGINATION -->
				<ul class="pagination">
					<?php foreach($paginator->pagesInRange as $page) { ?>
					<li <?php echo ((int)$page == (int)$paginator->current ? 'class="active"' : ''); ?>><a href="/members/?page=<?php echo $page ?>&<?php echo $parameter; ?>"><?php echo $page ?></a></li>
					<?php } ?>
				</ul>
				<?php } ?>					 
				<hr class="l4">
				<div class="row">	
				<?php foreach($memberItems as $item) { ?>
				<div class="col-md-4 col-sm-4">
					<div class="layout_2--item row">
						<div class="col-md-6">
							<div class="thumb">
								<div class="icon-24 video2"></div>
								<a href="#">
									<?php if($item['social_facebook_id'] != '') { ?>									
									<img src="https://graph.facebook.com/<?php echo $item['social_facebook_id']; ?>/picture?width=300" width="150px"  class="img-responsive" />
									<?php } else { ?>
									<img src="/images/no-image.jpg" class="img-responsive" alt="<?php echo $item['member_name']; ?>" title="<?php echo $item['member_name']; ?>" />
									<?php } ?>
								</a>
							</div>
						</div>
						<div class="col-md-6">
							<h4><a href="#"><?php echo $item['member_name']; ?></a></h4>
							<div class="meta"><span class="date"><?php echo substr($item['tag_name'],0,150); ?></span></div>
						</div>
					</div>
				</div>
				<?php } ?>
				</div>
				<?php if($paginator->pageCount > 1) { ?>
				<!-- PAGINATION -->
				<ul class="pagination">
					<?php foreach($paginator->pagesInRange as $page) { ?>
					<li <?php echo ((int)$page == (int)$paginator->current ? 'class="active"' : ''); ?>><a href="/members/?page=<?php echo $page ?>&<?php echo $parameter; ?>"><?php echo $page ?></a></li>
					<?php } ?>
				</ul>
				<?php } ?>			
			</div>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	getSection();
	$("#filter_category").change(function() {
	  getSection();
	});
});

function getSection() {
	$.ajax({
		type: "GET",
		url: "?getsection=1<?php echo $parameter; ?>",
		data: "filter_category="+$('#filter_category :selected').val(),
		dataType: "html",
		success: function(data){				
			$('#sectioninput').html(data);
			addFilterText();
		}
	});								
	return false;
}


function addFilterText() {
<?php if (isset($filters['filter_category']) && $filters['filter_category'] != ''); { ?>
	var category = '<?php echo $filters['filter_category']; ?>';
	var html 		= ' Searching in category <span class="normal-font text_brown">'+$('#filter_category  :selected').text()+'</span>';
<?php 
	if(isset($filters['filter_section']) && is_array($filters['filter_section']) && count($filters['filter_section']) > 0) {
?>
		html 		+= ', in sections: ';
		var sections = [<?php echo "'".implode("','",$filters['filter_section'])."'"; ?>];		
		for(var i = 0; i < sections.length; i++) {
			if(i != 0) {
				html += ', ';
			}
			html += '<span class="normal-font text_brown">'+$('#'+sections[i]).html()+'</span>';			
		}		
<?php 	
	}
?>
html 		+= '.';
$('#sectionshtml').html(html);
<?php } ?>
return false;
}
</script>
</body>
</html>
