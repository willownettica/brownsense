<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* standard config include. */
require_once 'config/database.php';

//include the Zend class for Authentification
require_once 'Zend/Session.php';
 
// Set up the namespace
$zfsession	= new Zend_Session_Namespace('LOGIN_MEMBER');

$page 			= explode('/',$_SERVER['REQUEST_URI']);
$currentPage = isset($page[1]) && trim($page[1]) != '' ? trim($page[1]) : '';

// Check if logged in, otherwise redirect
if (!isset($zfsession->identity) || is_null($zfsession->identity) || $zfsession->identity == '') {
	if($currentPage != '') {
		header('Location: /');
		exit;
	}
} else {

	require_once 'class/member.php';
	require_once 'class/market.php';
	require_once 'class/company.php';
	
	$memberObject		= new class_member();
	$marketObject		= new class_market();
	$companyObject	= new class_company();
	
	//get user details by username
	$memberData = $memberObject->getByCode($zfsession->identity);

	if($memberData) {
		/* Administrator selected campaign. */
		$zfsession->memberData 	= $memberData;
		$zfsession->identity 			= $memberData['member_code'];
		$zfsession->vendor 			= $marketObject->getVendor();
		$zfsession->company 		= $companyObject->getList();
	}
	/* Check if the account has been completed, active and approved by administrators. */
	if(trim($memberData['member_linked']) == '') {
		/* Activated, completed but not approved yet. */
		header('Location: /account/incomplete/default.php');
		exit;
	} else if($memberData['branch_code'] == '' || $memberData['member_email'] == '' || $memberData['member_birthdate'] == '' || $memberData['member_birthdate'] == '') {
		/* Nothing has been completed, please complete site. */
		header('Location: /account/incomplete/');
		exit;
	} else if((int)trim($memberData['member_active']) == 0) {
		/* Completed but not activated. */
		header('Location: /account/incomplete/activate.php');
		exit;
	} else if((int)trim($memberData['item_config_status']) == 0) {
		/* Activated, completed but not approved yet. */
		header('Location: /account/incomplete/status.php');
		exit;
	}
}

global $zfsession;

?>