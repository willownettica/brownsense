<?php
class SugarBird implements SplObserver {
	
	public $gardenObject	= array();
	public $flowerObject		= null;
	public $flower				= null;
	
	function __construct($gardenObject) {
		if(is_a($gardenObject, 'Garden')) {
			$this->gardenObject = $gardenObject;
		} else {
			echo 'SUNBIRD NEEDS A GARDEN TO FEED IN<br />';
		}
	}
	
	public function getFlower() {		
		/* Check if the garden is open. */
		if($this->gardenObject->state) {
			/* Get a random flower */
			if(count($this->gardenObject->flowerObjects)) {				
				$this->flower		= $this->getFlowerKey();
				/* Get random key in order to get a flower in the garden. */
				$flowerObject	= $this->gardenObject->flowerObjects[$this->flower];
				echo $flowerObject->name.' ('.$flowerObject->nector.')<br />';
				/* Feed the bird. */
				if(!$this->feed($flowerObject)) {
					/* Remove this flower from the garden. */
					unset($this->gardenObject->flowerObjects[$this->flower]);
					/* If the bird was not fed on this flower, check another one */
					$this->getFlower();
				}
			}
		} else {
			echo 'GARDEN CLOSED<br />';
		}
 	}
	
	public function getFlowerKey() {		
		$key =	array_rand($this->gardenObject->flowerObjects);
		if($key != $this->flower) {
			return $key;
		} else {
			if(count($this->gardenObject->flowerObjects) != 1) {
				$this->getFlowerKey();
			}
		}
		return $key;
	}
	
	public function feed(Flower &$flowerObject) {
		/* Make sure that there is enough nector. */
		if(!$flowerObject->nector) {
			return false;
		}
		
		$flowerObject->nector -= 1;
		
		return true;
	}
	
    public function update(SplSubject $subject)
    {
		if($subject->day) {
			$this->getFlower(); 
		} else {
			echo 'SLEEP<br />';
		}
    }
}
?>