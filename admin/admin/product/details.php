<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/* Check for login */
require_once 'includes/auth.php';

/* objects. */
require_once 'class/_product.php';

$_productObject = new class_product();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$productData = $_productObject->getByCode($code);

	if($productData) {
		$smarty->assign('productData', $productData);
	} else {
		header('Location: /admin/product/');
		exit;
	}
}

/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;
	
	if(isset($_POST['_product_name']) && trim($_POST['_product_name']) == '') {
		$errorArray['_product_name'] = 'Name is required';
		$formValid = false;		
	}
	
	if(!isset($productData)) {
		if(isset($_POST['_product_type']) && trim($_POST['_product_type']) == '') {
			$errorArray['_product_type'] = 'Level is required';
			$formValid = false;		
		}
	}
	
	if(isset($_POST['_product_height']) && (int)trim($_POST['_product_height']) == 0) {
		$errorArray['_product_height'] = 'Height of this advert is required.';
		$formValid = false;		
	}

	if(isset($_POST['_product_width']) && (int)trim($_POST['_product_width']) == 0) {
		$errorArray['_product_width'] = 'Width of this advert is required.';
		$formValid = false;		
	}
	
	if(count($errorArray) == 0 && $formValid == true) {
	
		$data 	= array();						
		$data['_product_name']	= trim($_POST['_product_name']);		
		$data['_product_height']	= trim($_POST['_product_height']);
		$data['_product_width']	= trim($_POST['_product_width']);
		$data['_product_page']	= trim($_POST['_product_page']);
		$data['_product_url']		= trim($_POST['_product_url']);
		
		if(isset($productData)) {
			$where		= $_productObject->getAdapter()->quoteInto('_product_code = ?', $productData['_product_code']);
			$success	= $_productObject->update($data, $where);
			$success	= $productData['_product_code'];
		} else {
			
			$data['_product_type']		= trim($_POST['_product_type']);
			
			$success = $_productObject->insert($data);
		}

		header('Location: /admin/product/admin.php?code='.$success);	
		exit;
	}
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

$smarty->display('admin/product/details.tpl');

?>