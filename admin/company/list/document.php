<?php

ini_set('memory_limit','365M');
ini_set('max_execution_time', '600');

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/**
 * Standard includes
 */
require_once 'config/database.php';
require_once 'config/smarty.php';

require_once 'includes/auth.php';
/* objects. */
require_once 'class/company.php';
require_once 'class/media.php';
require_once 'class/File.php';
require_once 'class/item.php';

$companyObject	= new class_company();
$mediaObject 	= new class_media();
$fileObject 	= new File(array('png', 'jpg', 'jpeg', 'pdf', 'xls', 'txt', 'docx', 'doc', 'cvs', 'zip', 'xlsx'));
$itemObject		= new class_item();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code 		= trim($_GET['code']);
	
	$companyData = $companyObject->getByCode($code);	

	if($companyData) {
		
		$smarty->assign('companyData', $companyData);

		$mediaData = $mediaObject->getByReference(array('DOCUMENT'), 'COMPANY', $code);

		if($mediaData) {
			$smarty->assign('mediaData', $mediaData);
		}
	} else {
		header('Location: /company/list/');
		exit;	
	}
	
} else {
	header('Location: /company/list/');
	exit;	
}

/* Check posted data. */
if(isset($_GET['delete_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success				= NULL;
	$itemcode				= trim($_GET['delete_code']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {

		$data	= array();
		$data['media_deleted'] = 1;
		
		$where		= array();
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_code = ?', $itemcode);
		$where[]	= $mediaObject->getAdapter()->quoteInto('media_item_code = ?', $code);
		$where[]	= $mediaObject->getAdapter()->quoteInto("media_item_type = ?", 'COMPANY');
		
		$success	= $mediaObject->update($data, $where);	
		
		if(is_numeric($success) && $success > 0) {		
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	
	echo json_encode($errorArray);
	exit;
}
/* Check posted data. */
if(count($_FILES) > 0) {

	$errorArray	= array();
	$data 		= array();
	$formValid	= true;
	$success	= NULL;
	
	if(isset($_POST['item_code']) && trim($_POST['item_code']) == '') {
		$errorArray['item_code'] = 'Type of document is required';
		$formValid = false;
	} else { 
		
		$tempItem = $itemObject->getByCode(trim($_POST['item_code']));
		
		if($tempItem) {
			if((int)$tempItem['item_config_date'] == 1) {
				if(isset($_POST['media_date_start']) && trim($_POST['media_date_start']) == '') {
					$errorArray['media_date_start'] = 'A valid from date is required for this document type';
					$formValid = false;
				}
				if(isset($_POST['media_date_end']) && trim($_POST['media_date_end']) == '') {
					$errorArray['media_date_end'] = 'An exipiry date is required for this document type';
					$formValid = false;
				}				
			}
		} else {
			$errorArray['item_code'] = 'Type of document is required';
			$formValid = false;
		}
	}
	
	if(isset($_FILES['mediafile']) && count($_FILES['mediafile']['name']) > 0 && (isset($_FILES['mediafile']['name']) && trim($_FILES['mediafile']['name']) != '')) {
		/* Check validity of the CV. */
		if((int)$_FILES['mediafile']['size'] != 0 && trim($_FILES['mediafile']['name']) != '') {
			/* Check if its the right file. */
			$ext = $fileObject->file_extention($_FILES['mediafile']['name']); 

			if($ext != '') {
				$checkExt = $fileObject->getValidateExtention('mediafile', $ext);

				if(!$checkExt) {
					$errorArray['mediafile'] = 'Invalid file type something funny with the file format';
					$formValid = false;						
				}
			} else {
				$errorArray['mediafile'] = 'Invalid file type';
				$formValid = false;									
			}
		} else {			
			switch((int)$_FILES['mediafile']['error']) {
				case 1 : $errorArray['mediafile'] = 'The uploaded file exceeds the maximum upload file size, should be less than 1M'; $formValid = false; break;
				case 2 : $errorArray['mediafile'] = 'File size exceeds the maximum file size'; $formValid = false; break;
				case 3 : $errorArray['mediafile'] = 'File was only partically uploaded, please try again'; $formValid = false; break;
				case 4 : $errorArray['mediafile'] = 'No file was uploaded'; $formValid = false; break;
				case 6 : $errorArray['mediafile'] = 'Missing a temporary folder'; $formValid = false; break;
				case 7 : $errorArray['mediafile'] = 'Faild to write file to disk'; $formValid = false; break;
			}
		}
	} else {
		$errorArray['mediafile'] = 'No file was uploaded';
		$formValid = false;									
	}

	if(count($errorArray) == 0 && $formValid == true) {

		if(isset($_FILES['mediafile']) && count($_FILES['mediafile']['name']) > 0 && (isset($_FILES['mediafile']['name']) && trim($_FILES['mediafile']['name']) != '')) {
			$data = array();
			$data['media_code']			= $mediaObject->createCode();
			$data['media_item_code']	= $code;
			$data['media_item_type']	= 'COMPANY';
			$data['item_code']			= trim($_POST['item_code']);
			$data['media_date_start']	= trim($_POST['media_date_start']);
			$data['media_date_end']		= trim($_POST['media_date_end']);
			$data['media_description']	= trim($_POST['mediadescription']);
			$data['media_category']		= 'DOCUMENT';

			$ext 		= strtolower($fileObject->file_extention($_FILES['mediafile']['name']));					
			$filename	= $data['media_code'].'.'.$ext;
			$directory	= realpath(__DIR__.'/../../public_brownsense_manage/').'/media/document/'.$data['media_code'];

			$file		= $directory.'/'.$filename;

			if(!is_dir($directory)) mkdir($directory, 0777, true); 
			/* Create new file and rename it. */ 
			file_put_contents($file, file_get_contents($_FILES['mediafile']['tmp_name']));

			$data['media_path']		= '/media/document/'.$data['media_code'].'/';
			$data['media_filename']	= trim($_FILES['mediafile']['name']);
			$data['media_ext']		= '.'.$ext ;

			$success	= $mediaObject->insert($data);
		}
	
		header('Location: /company/list/document.php?code='.$code);
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);
}

$parentSelect = $itemObject->selectParents('DOCUMENT');
if($parentSelect) $smarty->assign('parentSelect', $parentSelect);

/* Display the template */	
$smarty->display('company/list/document.tpl');
$mediaObject = $errorArray = $data = $campaignData = $primary = $ext = $newfilename = $uploadObject = $item = $i = $filename = $file = $directory = $fileObject = $formValid = $checkExt = null;
unset($mediaObject, $errorArray, $data, $campaignData, $primary, $ext, $newfilename, $uploadObject, $item, $i, $filename, $file, $directory, $fileObject, $formValid, $checkExt);
?>