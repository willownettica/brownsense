<?php

//custom account item class as account table abstraction
class class_item extends Zend_Db_Table_Abstract
{
   //declare table variables
    protected $_name 			= 'item';
	protected $_primary 		= 'item_code';
	
	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 
	 public function insert(array $data)
    {
        // add a timestamp
        $data['item_added'] 		= date('Y-m-d H:i:s');        
		$data['item_code']			= $this->createCode();

		return parent::insert($data);
    }
	
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where)
    {
        // add a timestamp
        $data['item_updated'] = date('Y-m-d H:i:s');
        
        return parent::update($data, $where);
    }
	
	/**
	 * get job by job item Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code) {	
		$select = $this->_db->select()	
					->from(array('item' => 'item'))
					->where('item_deleted = 0')
					->where('item_code = ?', $code)
					->limit(1);
       
		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}

	/**
	 * get job by job item Id
 	 * @param string job id
     * @return object
	 */
	public function getParents($parenttype) {	
		$select = $this->_db->select()
				->from(array('item' => 'item'))
				->where('item.item_type = ?', $parenttype)
				->where("item.item_parent = '' or item.item_parent is null")
				->where('item.item_deleted = 0');

		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get job by job item Id
 	 * @param string job id
     * @return object
	 */
	public function getByType($type) {

		$select = $this->_db->select()
				->from(array('item' => 'item'))
				->where('item.item_type = ?', $type)
				->where('item.item_deleted = 0');

		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get job by job item Id
 	 * @param string job id
     * @return object
	 */
	public function selectType($type) {

		$select = $this->_db->select()
				->from(array('item' => 'item'), array('item.item_code', "item_name"))
				->where('item.item_type = ?', $type)
				->where('item.item_deleted = 0');

		$result = $this->_db->fetchPairs($select);
        return ($result == false) ? false : $result = $result;
	}	
	/**
	 * get job by job item Id
 	 * @param string job id
     * @return object
	 */
	public function getPrimary($type, $reference) {

		$select = $this->_db->select()
				->from(array('item' => 'item'))
				->where('item.item_type = ?', $type)
				->where('item.item_config_reference = ?', $reference)
				->where('item.item_config_primary = 1')
				->where('item.item_deleted = 0');

		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get job by job item Id
 	 * @param string job id
     * @return object
	 */
	public function selectParents($type, $reference = '') {
		
		if($reference != '') {
			$select = $this->_db->select()
					->from(array('item' => 'item'), array('item.item_code', "item_name"))
					->where('item.item_type = ?', $type)
					->where('item.item_config_reference = ?', $reference)
					->where("item.item_parent = '' or item.item_parent is null")
					->where('item.item_deleted = 0');
		} else {
			$select = $this->_db->select()
					->from(array('item' => 'item'), array('item.item_code', "item_name"))
					->where('item.item_type = ?', $type)
					->where("item.item_parent = '' or item.item_parent is null")
					->where('item.item_deleted = 0');
		}
		$result = $this->_db->fetchPairs($select);
        return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get job by job item Id
 	 * @param string job id
     * @return object
	 */
	public function getChildren($parentcode, $childrentype) {
		$select = $this->_db->select()
				->from(array('item' => 'item'))
				->where('item.item_parent = ?', $parentcode)
				->where('item.item_type = ?', $childrentype)				
				->where('item.item_deleted = 0');

		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getCode($reference)
	{
		$select = $this->_db->select()	
					->from(array('item' => 'item'))	
					->where('item_code = ?', $reference)
					->limit(1);

	   $result = $this->_db->fetchRow($select);	
        return ($result == false) ? false : $result = $result;					   
	}

	function createCode() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '1234567890';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Number[rand(0,$count)];
		}
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<4;$i++){
			$code .= $Number[rand(0,$count)];
		}		
		/* First check if it exists or not. */
		$itemCheck = $this->getCode($code);
		
		if($itemCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}	
}
?>