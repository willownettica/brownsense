<?php
/* Get the path of the code, make it its root for the classes. */
set_include_path(dirname(__FILE__));

require 'SugarBird.php';
require 'Sun.php';
require 'Flower.php';
require 'Garden.php';

$sunObject 			= new Sun;						/* The sun. */
$gardenObject		= new Garden();					/* My collection of flowers in the garden, defualt number is 10 of them. */
$sugarBirdObject	= new sugarBird($gardenObject);	/* The sugar bird. */

/* Observers of the sun, the garden is merely a collection of flowers. */
$sunObject->attach($gardenObject);
$sunObject->attach($sugarBirdObject);

/* Start the day. */
while(count($sugarBirdObject->gardenObject->flowerObjects) > 0) {
	/* 
		As the sun moves each hour, the observers will observe the time and act accordingly. 
		Since this is a OOP app, all flowers objects are created in the garden object.
		So in short, its the garden as well as the sugarbird that observe the sun as each hour passes.
	*/
	$sunObject->addHour();
}

/* We done, lets see how long it took. */
echo 'EXIT ('.$sunObject->lapsed.')';
?>