<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
	<h2 class="content-header-title">Advert</h2>
	<ol class="breadcrumb">
		<li><a href="/">Home</a></li>	
		<li><a href="#">Administration</a></li>
		<li><a href="/advert/product/">Advert</a></li>
		<li><a href="#">{if isset($advertData)}{$advertData.advert_type}{else}Add a advert{/if}</a></li>
		<li class="active">Details</li>
	</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					{if isset($advertData)}{$advertData.advert_type}{else}Add a advert{/if}
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="/advert/product/details.php{if isset($advertData)}?code={$advertData.advert_code}{/if}" method="POST" data-validate="parsley" class="form parsley-form">
                <div class="form-group">
					<label for="advert_type">Type</label>
					<select id="advert_type" name="advert_type" class="form-control">
						<option value="SQUARE" {if $advertData.advert_type eq 'SQUARE'}selected{/if}> SQUARE </option>
						<option value="BANNER" {if $advertData.advert_type eq 'BANNER'}selected{/if}> BANNER </option>
						<option value="RECTANGLE" {if $advertData.advert_type eq 'RECTANGLE'}selected{/if}> RECTANGLE </option>
					</select>
					{if isset($errorArray.advert_type)}<span class="error">{$errorArray.advert_type}</span>{/if}					  
                </div>
                <div class="form-group">
                  <label for="advert_name">Name</label>
                  <input type="text" id="advert_name" name="advert_name" class="form-control" data-required="true" value="{$advertData.advert_name}" />
				{if isset($errorArray.advert_name)}<span class="error">{$errorArray.advert_name}</span>{/if}					  
                </div>
                <div class="form-group">
                  <label for="advert_width">Width</label>
                  <input type="text" id="advert_width" name="advert_width" class="form-control" data-required="true" value="{$advertData.advert_width}" />
				{if isset($errorArray.advert_width)}<span class="error">{$errorArray.advert_width}</span>{/if}					  
                </div>
                <div class="form-group">
                  <label for="advert_height">Height</label>
                  <input type="text" id="advert_height" name="advert_height" class="form-control" data-required="true" value="{$advertData.advert_height}" />
				{if isset($errorArray.advert_height)}<span class="error">{$errorArray.advert_height}</span>{/if}					  
                </div>									
                <div class="form-group"><button type="submit" class="btn btn-primary">Validate and Submit</button></div>
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col --> 
		<div class="col-sm-3">
			<div class="list-group">  
				<a class="list-group-item" href="/advert/product/">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
			</div> <!-- /.list-group -->
        </div>			
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
</html>
