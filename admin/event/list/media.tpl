<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]--> 
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8"> 
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
		<h2 class="content-header-title">Events</h2>
		<ol class="breadcrumb">
		<li><a href="/">Home</a></li>
		<li><a href="/event/list/">Events</a></li>
		<li><a href="/event/list/details.php?code={$eventData.event_code}">{$eventData.event_name}</a></li>
		<li class="active">Media</li>
		</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					Media list
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="/event/list/media.php?code={$eventData.event_code}" method="POST" data-validate="parsley" class="form parsley-form" enctype="multipart/form-data">
				<p>Add images below</p>
                <div class="form-group">
					<label for="mediadescription">Image Description</label>
					<textarea id="mediadescription" name="mediadescription" rows="3" class="form-control">{$eventData.event_name} {$eventData.event_surname}</textarea>
					{if isset($errorArray.mediadescription)}<br /><span class="error">{$errorArray.mediadescription}</span>{/if}
                </div>
                <div class="form-group">
					<label for="mediafiles">Image Upload</label>
					<input type="file" id="mediafiles[]" name="mediafiles[]" multiple />
					{if isset($errorArray.mediafiles)}<br /><span class="error">{$errorArray.mediafiles}</span>{/if}					  
					<br /><span>Allowed files are png, jpg, jpeg.</span>
                </div>					
                <div class="form-group">
					<button type="submit" class="btn btn-primary">Upload and Save</button>
				</div>			  
				<p>Below is a list of your media.</p>
				<table class="table table-bordered">
					<tbody>
						{foreach from=$mediaData item=item}
							<tr>
								<td>
									<a href="http://brownsense.co.za{$item.media_path}/tmb_{$item.media_code}{$item.media_ext}" target="_blank">
										<img src="http://brownsense.co.za{$item.media_path}/tmb_{$item.media_code}{$item.media_ext}" width="120" />
									</a>
								</td>
								<td>
									<button value="Update" class="btn btn-danger" onclick="postModal('MEDIA', '{$item.media_code}'); return false;">Add to social</button>
								</td>
								<td>
									{if $item.media_primary eq '0'}
										<button value="Make Primary" class="btn btn-danger" onclick="statusitemModal('{$eventData.event_code}', '{$item.media_code}', '1' ); return false;">Primary</button>
									{else}
									<b>Primary</b>
									{/if}									
								</td>
								<td>
									{if $item.media_primary eq '0'}
										<button value="Delete" class="btn btn-danger" onclick="deleteitemModal('{$eventData.event_code}', '{$item.media_code}'); return false;">Delete</button>
									{else}
										<b>Primary</b>
									{/if}
								</td>
							</tr>	
							{foreachelse}	
							<tr>
								<td align="center" colspan="4">There are currently no items</td>
							</tr>								
						{/foreach}
					</tbody>					  
				</table>

              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col -->		
		<div class="col-sm-3">
			<div class="list-group">  
				<a href="/event/list/" class="list-group-item">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/event/list/details.php?code={$eventData.event_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a> 
				<a class="list-group-item" href="/event/list/map.php?code={$eventData.event_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Map
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a> 	
				<a class="list-group-item" href="/event/list/media.php?code={$eventData.event_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Media
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a> 	
				<a class="list-group-item" href="/event/list/market.php?code={$eventData.event_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp; Vendors
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>						
			</div> <!-- /.list-group -->
		</div>		
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
{literal}
<script type="text/javascript">
function updateRecord(code) {
	$.ajax({
			type: "GET",
			url: "media.php?code={/literal}{$eventData.event_code}{literal}",
			data: "update_code="+code+"&media_description="+$("#media_description_"+code).val(),
			dataType: "json",
			success: function(data){
				if(data.result == 1) {
					window.location.href = window.location.href;
				} else {
					$.howl ({
					  type: 'info'
					  , title: 'Notification'
					  , content: data.error
					  , sticky: $(this).data ('sticky')
					  , lifetime: 7500
					  , iconCls: $(this).data ('icon')
					});	
				}
			}
	});								

	return false;
}

function deleteitemModal(event, media) {
	$('#eventcode').val(event);
	$('#mediacode').val(media);
	$('#deleteitemModal').modal('show');
	return false;
}

function statusitemModal(event, media, status) {
	$('#statuseventcode').val(event);
	$('#statuscode').val(media);
	$('#status').val(status);
	$('#statusItemModal').modal('show');
	return false;
}

function deletesubitem() {
		
		var eventcode 	= $('#eventcode').val();
		var mediacode 			= $('#mediacode').val();
		
		$.ajax({
				type: "GET",
				url: "media.php",
				data: "delete_code="+mediacode+"&code="+eventcode,
				dataType: "json",
				success: function(data){
					if(data.result == 1) {
						if(typeof oTable != 'undefined') {
							$('#deleteitemModal').modal('hide');
							oTable.fnDraw();
						} else {
							window.location.href = window.location.href;
						}
					} else {
						
						$('#deleteitemModal').modal('hide');
						
						$.howl ({
						  type: 'danger'
						  , title: 'Error Message'
						  , content: data.error
						  , sticky: $(this).data ('sticky')
						  , lifetime: 7500
						  , iconCls: $(this).data ('icon')
						});					
					}
				}
		});
		
		return false;
	}
	
	function changeItemStatus() {
	
		var event	= $('#statuseventcode').val();
		var media 		= $('#statuscode').val();	
		var status			= $('#status').val();
		
		$.ajax({
				type: "GET",
				url: "/event/list/media.php?code="+event+"&media="+media,
				data: "status_code="+media+"&status="+status,
				dataType: "json",
				success: function(data){
					if(data.result == 1) {
						window.location.href = window.location.href;
					} else {
						$('#statusItemModal').modal('hide');
						$.howl ({
						  type: 'info'
						  , title: 'Notification'
						  , content: data.error
						  , sticky: $(this).data ('sticky')
						  , lifetime: 7500
						  , iconCls: $(this).data ('icon')
						});	
					}
				}
		});								

		return false;		
	}
</script>
{/literal}
<!-- Modal -->
<div class="modal fade" id="statusItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Change Status</h4>
			</div>
			<div class="modal-body">Are you sure you want to change this item's status?</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:changeItemStatus();">Change Item Status</button>
				<input type="hidden" id="statuseventcode" name="statuseventcode" value="" />
				<input type="hidden" id="statusmediacode" name="statusmediacode" value="" />
				<input type="hidden" id="statuscode" name="statuscode" value=""/>
				<input type="hidden" id="status" name="status" value=""/>
			</div>
		</div>
	</div>
</div>
<!-- modal -->

<!-- Modal -->
<div class="modal fade" id="deleteitemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Item</h4>
			</div>
			<div class="modal-body">Are you sure you want to delete this item?</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:deletesubitem();">Delete Item</button>
				<input type="hidden" id="eventcode" name="eventcode" value=""/>
				<input type="hidden" id="mediacode" name="mediacode" value=""/>
				<input type="hidden" id="mediacode" name="mediacode" value=""/>
			</div>
		</div>
	</div>
</div>
<!-- modal -->
</html>