<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
		<h2 class="content-header-title">Companies</h2>
		<ol class="breadcrumb">
			<li><a href="/">Home</a></li>
			<li><a href="/company/list/">Companies</a></li>
			<li><a href="/company/list/details.php?code={$companyData.company_code}">{$companyData.company_name}</a></li>
			<li class="active">Comment(s)</li>
		</ol>
	</div>		
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					{$companyData.company_name}
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="#" method="POST" data-validate="parsley" class="form parsley-form">
              <table 
                class="table table-striped table-bordered" 
              >
					<thead>
						<tr>
							<th>Name</th>
							<th>Message</th>
							<th></th>
						</tr>
					</thead>							
				   <tbody>
				  {foreach from=$commentData item=item}
				  <tr>
					<td align="left" class="{$item.item_config_class}">
						{$item.comment_name}<br />
						{$item.comment_number}<br />
						{$item.comment_email}<br />
						{$item.comment_added}<br />
						{$item.vet_reason}
					</td>
					<td align="left" id="comment_{$item.comment_code}">{$item.comment_text}</td>
					<td>
						<button value="Delete" class="btn btn-danger" onclick="deleteitemModal('{$companyData.company_code}', '{$item.comment_code}'); return false;">Delete</button>
						<br /><br />
						<button value="Rate" class="btn btn-danger" onclick="rateModal('{$item.comment_code}'); return false;">Rate</button>						
					</td>
				  </tr>
				{foreachelse}
				<tr><td colspan="3">No tags have been added yet</td></tr>
				  {/foreach}
				  </tbody>
                </table>          
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col --> 
		<div class="col-sm-3">
			<div class="list-group">  
				<a class="list-group-item" href="/company/list/">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/company/list/details.php?code={$companyData.company_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/company/list/media.php?code={$companyData.company_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Media
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/company/list/tag.php?code={$companyData.company_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Tag(s)
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/company/list/document.php?code={$companyData.company_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Document(s)
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>		
			</div> <!-- /.list-group -->
        </div>			
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
{literal}
<script type="text/javascript" language="javascript">

function rateModal(commentcode) {
	$('#commentcode').val(commentcode);
	$('#rate_message').html($('#comment_'+commentcode).html());
	$('#rateModal').modal('show');
	return false;
}

function rateMessage() {
	
	var commentcode	= $('#commentcode').val();
	var vetcode 			= $('#vetcode').val();
	var ratereason		= $('#ratereason').val();
	
	$.ajax({
		type: "GET",
		url: "comment.php?code={/literal}{$companyData.company_code}{literal}",
		data: "rate_comment="+commentcode+"&vetcode="+vetcode+"&ratereason="+ratereason,
		dataType: "json",
		success: function(data){
			if(data.result == 1) {
				window.location.href = window.location.href;
			} else {
				
				$('#rateModal').modal('hide');
				
				$.howl ({
				  type: 'danger'
				  , title: 'Error Message'
				  , content: data.error
				  , sticky: $(this).data ('sticky')
				  , lifetime: 7500
				  , iconCls: $(this).data ('icon')
				});					
			}
		}
	});
	
	return false;
}

function deleteitemModal(admin, media) {
	$('#admincode').val(admin);
	$('#mediacode').val(media);
	$('#deleteitemModal').modal('show');
	return false;
}

function deletesubitem() {
	
	var admincode 	= $('#admincode').val();
	var mediacode 			= $('#mediacode').val();
	
	$.ajax({
		type: "GET",
		url: "tag.php",
		data: "delete_code="+mediacode+"&code="+admincode,
		dataType: "json",
		success: function(data){
			if(data.result == 1) {
				window.location.href = window.location.href;
			} else {
				
				$('#deleteitemModal').modal('hide');
				
				$.howl ({
				  type: 'danger'
				  , title: 'Error Message'
				  , content: data.error
				  , sticky: $(this).data ('sticky')
				  , lifetime: 7500
				  , iconCls: $(this).data ('icon')
				});					
			}
		}
	});
	
	return false;
}
	
</script>
{/literal}
<!-- Modal -->
<div class="modal fade" id="rateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Rate</h4>
			</div>
			<div class="modal-body">
				<p><b>Message:</b></p>
				<p id="rate_message"></p>
				<label for="vetcode" class="error">Rate</label>
				<select id="vetcode" name="vetcode" class="form-control">
					<option value=""> ---------- </option>
					{html_options options=$itemData}
				</select>
				<label for="ratereason" class="error">Message</label>
				<textarea id="ratereason" name="ratereason" class="form-control"></textarea>
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:rateMessage();">Rate</button>
				<input type="hidden" id="commentcode" name="commentcode" value=""/>
			</div>
		</div>
	</div>
</div>
<!-- modal -->
<!-- Modal -->
<div class="modal fade" id="deleteitemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Item</h4>
			</div>
			<div class="modal-body">Are you sure you want to delete this item?</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:deletesubitem();">Delete Item</button>
				<input type="hidden" id="admincode" name="admincode" value=""/>
				<input type="hidden" id="mediacode" name="mediacode" value=""/>
				<input type="hidden" id="mediacode" name="mediacode" value=""/>
			</div>
		</div>
	</div>
</div>
<!-- modal -->
</body>
</html>
