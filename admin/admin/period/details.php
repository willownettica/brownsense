<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/* Check for login */
require_once 'includes/auth.php';

/* objects. */
require_once 'class/period.php';

$periodObject = new class_period();

/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;
	
	if(isset($_POST['period_name']) && trim($_POST['period_name']) == '') {
		$errorArray['period_name'] = 'Name is required';
		$formValid = false;		
	}
	
	if(isset($_POST['period_type']) && trim($_POST['period_type']) == '') {
		$errorArray['period_type'] = 'Type is required';
		$formValid = false;		
	}
	
	if(isset($_POST['period_date_start']) && trim($_POST['period_date_start']) == '') {
		$errorArray['period_date_start'] = 'Start date is required';
		$formValid = false;		
	}

	if(isset($_POST['period_date_end']) && trim($_POST['period_date_end']) == '') {
		$errorArray['period_date_end'] = 'End date is required';
		$formValid = false;		
	}
	
	if(isset($_POST['period_reference']) && trim($_POST['period_reference']) == '') {
		$errorArray['period_reference'] = 'Reference is required';
		$formValid = false;		
	}
	
	if(count($errorArray) == 0 && $formValid == true) {
	
		$data 								= array();						
		$data['period_name']			= trim($_POST['period_name']);	
		$data['period_type']			= trim($_POST['period_type']);	
		$data['period_date_start']	= trim($_POST['period_date_start']);	
		$data['period_date_end']	= trim($_POST['period_date_end']);	
		$data['period_reference']	= trim($_POST['period_reference']);	

		$success = $periodObject->insert($data);

		header('Location: /admin/period/');	
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

$smarty->display('admin/period/details.tpl');

?>