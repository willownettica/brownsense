<?php 
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

/* objects. */
require_once 'class/event.php';

$eventObject 	= new class_event();

$comingEvent 	= $eventObject->latest();

?>
<!-- HEADER / MENU -->
<header class="header1 header-megamenu">
	<div class="topbar">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-sm-8 hidden-xs">
					<div class="newsfeed">
						<span>UPCOMING EVENTS:</span>
						<div class="news-carousel">
							<?php foreach($comingEvent as $event) { ?>
							<div class="item"><a href="/market/<?php echo $event['event_code']; ?>"><?php echo $event['event_name']; ?></a></div>
							<?php } ?>
						</div>
					</div> 
				</div> 
				<div class="col-md-3 col-sm-4 col-xs-12">
					<div class="pull-right account-options">
					<?php if(!isset($zfsession->memberData)) { ?>
						<a href="/login.php"><img src="/images/facebook_login.png" height="25px" /></a>
						<?php } else { ?>
							<a href="/account/" class="logout">My Account</a> | <a href="/logout.php" class="logout">Log Out</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="navbar-header padding-vertical-10">
		<div class="container">
			<a href="/" class="navbar-brand">
				<img src="/images/brownsense_header.png" class="img-responsive" alt="BrownSense" title="BrownSense" />
			</a><br /><br /><br /><br /><br /> 
		</div>
	</div>
	<div class="clearfix"></div>
	<?php if(isset($zfsession->memberData)) { ?>
	<div class="container">
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						My Account <span class="fa fa-angle-right"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="/account/">My Account</a></li>
						<li><a href="/account/company/">My Companies</a></li>
						<li><a href="/account/classified/">My Classifieds</a></li>
					</ul>
				</li>
				<li><a href="/companies/">Companies</a></li>
				<li><a href="/classified/">Classifieds</a></li>
				<li><a href="/market/">Market</a></li>
				<!-- <li><a href="/members/">Members</a></li> -->
				<li><a href="/contact.php">Contact Us</a></li>				
			</ul>
		</div>
	</div>
	<?php } ?>
</header>
<!-- // HEADER / MENU -->