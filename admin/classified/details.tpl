<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
	<link href="/css/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css"  />
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
	<h2 class="content-header-title">Classified</h2>
	<ol class="breadcrumb">
		<li><a href="/">Home</a></li>
		<li><a href="/classified/">Classified</a></li>
		<li><a href="#">{if isset($classifiedData)}{$classifiedData.classified_subject}{else}Add a classified{/if}</a></li>
		<li class="active">Details</li>
	</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					{if isset($classifiedData)}{$classifiedData.classified_subject}{else}Add a classified{/if}
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
			{if $classifiedData.vet_status eq '1'}
				<div class="alert alert-success">
					<strong>Active</strong> {$classifiedData.vet_reason}
				</div>
			{else}
				{if $classifiedData.vet_status eq ''}
					<div class="alert alert-warning">
						<strong>No Status</strong> This has not been vetted yet.
					</div>
				{else}
					<div class="alert alert-danger">
						<strong>Non-Active</strong> {$classifiedData.vet_reason}
					</div>
				{/if}
			{/if}
              <form id="validate-basic" action="/classified/details.php{if isset($classifiedData)}?code={$classifiedData.classified_code}{/if}" method="POST" data-validate="parsley" class="form parsley-form">
                <div class="form-group">
					<label for="classified_subject" class="error">Subject</label>
					<input type="text" id="classified_subject" name="classified_subject" class="form-control" data-required="true" value="{$classifiedData.classified_subject}" />
					{if isset($errorArray.classified_subject)}<span class="error">{$errorArray.classified_subject}</span>{/if}					  
                </div>			 
				<div class="form-group">
					<label for="category_code" class="error">Category</label>
					<select id="category_code" name="category_code" class="form-control" data-required="true" >
					<option value=""> ---------- </option>
					{html_options options=$categoryPairs selected=$classifiedData.category_code}
					</select>				  
				</div>
				<div class="form-group">
					<label for="section_code" class="error">Section</label>
					<select id="section_code" name="section_code" class="form-control" data-required="true" >
					<option value=""> --- Select category first --- </option>					
					</select>				  
				</div>				
				<div class="form-group">
					<label for="type_code" class="error">Type</label>
					<select id="type_code" name="type_code" class="form-control" data-required="true" >
					<option value=""> ---------- </option>
					{html_options options=$itemPairs selected=$classifiedData.type_code}
					</select>				  
				</div>
                <div class="form-group">
                  <label for="member_name" class="error">Member</label>
                  <input type="text" id="member_name" name="member_name" class="form-control" data-required="true" value="{if isset($classifiedData.member_name)}{$classifiedData.member_name} ( {$classifiedData.member_email} ){/if}" />
				  <input type="hidden" id="member_code" name="member_code" value="{$classifiedData.member_code}" />
				  {if isset($errorArray.member_code)}<span class="error">{$errorArray.member_code}</span>{/if}					  
                </div>				
                <div class="form-group">
                  <label for="areapost_name" class="error">Location of products / services needed or are</label>
                  <input type="text" id="areapost_name" name="areapost_name" class="form-control" data-required="true" value="{$classifiedData.areapost_name}" />
				  <input type="hidden" id="areapost_code" name="areapost_code" value="{$classifiedData.areapost_code}" />
				  {if isset($errorArray.areapost_code)}<span class="error">{$errorArray.areapost_code}</span>{/if}					  
                </div>				
                <div class="form-group">
					<label for="classified_expire" class="error">Expirey Date</label>
					<input type="text" id="classified_expire" name="classified_expire" class="form-control" data-required="true" value="{$classifiedData.classified_expire}" />
					{if isset($errorArray.classified_expire)}<span class="error">{$errorArray.classified_expire}</span>{/if}					  
                </div>					
                <div class="form-group">
                  <label for="classified_message" class="error">Message</label><br /><br />
                  <textarea id="classified_message" name="classified_message" class="form-control wysihtml5" rows="15">{$classifiedData.classified_message}</textarea>
				{if isset($errorArray.classified_message)}<span class="error">{$errorArray.classified_message}</span>{/if}					  
                </div>				
                <div class="form-group"><button type="submit" class="btn btn-primary">Validate and Submit</button></div>
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col --> 
		<div class="col-sm-3">
			<div class="list-group">  
				<a class="list-group-item" href="/classified/">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				{if isset($classifiedData)}					
				<a class="list-group-item" href="/classified/details.php?code={$classifiedData.classified_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a> 
				<a class="list-group-item" href="/classified/media.php?code={$classifiedData.classified_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Media
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>	
				<a class="list-group-item" href="/classified/enquiry.php?code={$classifiedData.classified_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Enquiries
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>				
				{/if}
			</div> <!-- /.list-group -->
        </div>			
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
<script type="text/javascript" src="/library/javascript/plugins/wysihtml5/wysihtml5-0.3.js"></script>
<script type="text/javascript" src="/library/javascript/plugins/wysihtml5/bootstrap-wysihtml5.js"></script>
{literal}
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	
	getSection();
	
	$( "#category_code" ).change(function() {
	  getSection();
	});
	
	$( "#areapost_name" ).autocomplete({
		source: "/feeds/areapost.php",
		minLength: 2,
		select: function( event, ui ) {
			if(ui.item.id == '') {
				$('#areapost_name').html('');
				$('#areapost_code').val('');					
			} else {
				$('#areapost_name').html(ui.item.value);
				$('#areapost_code').val(ui.item.id);	
			}
			$('#areapost_name').val('');										
		}
	});

	$( "#member_name" ).autocomplete({
		source: "/feeds/member.php",
		minLength: 2,
		select: function( event, ui ) {
			if(ui.item.id == '') {
				$('#member_name').html('');
				$('#member_code').val('');					
			} else {
				$('#member_name').html(ui.item.value);
				$('#member_code').val(ui.item.id);	
			}
			$('#member_name').val('');										
		}
	});
	
	$( "#classified_expire" ).datepicker({
		defaultDate: "+1w",
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1
	});
	$('.wysihtml5').wysihtml5();	
});

function getSection() {
	$.ajax({
		type: "GET",
		url: "/classified/details.php{/literal}{if isset($classifiedData)}?code={$classifiedData.classified_code}{/if}{literal}",
		data: "getsection=1&category_code="+$('#category_code :selected').val(),
		dataType: "html",
		success: function(data){				
			$('#section_code').html(data);
		}
	});								
	return false;
}
</script>
{/literal}
</html>
