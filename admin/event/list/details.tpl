<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
	<link href="/css/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css"  />
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
	<h2 class="content-header-title">Events</h2>
	<ol class="breadcrumb">
		<li><a href="/">Home</a></li>
		<li><a href="/event/list/">Events</a></li>
		<li><a href="#">{if isset($eventData)}{$eventData.event_name}{else}Add a event{/if}</a></li>
		<li class="active">Details</li>
	</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					{if isset($eventData)}{$eventData.event_name}{else}Add a event{/if}
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="/event/list/details.php{if isset($eventData)}?code={$eventData.event_code}{/if}" method="POST" data-validate="parsley" class="form parsley-form">			  			  
                <div class="form-group">
                  <label for="areapost_name" class="error">Your area / town / city</label>
                  <input type="text" id="areapost_name" name="areapost_name" class="form-control" data-required="true" value="{$eventData.areapost_name}" />
				  <input type="hidden" id="areapost_code" name="areapost_code" value="{$eventData.areapost_code}" />
				  {if isset($errorArray.areapost_code)}<span class="error">{$errorArray.areapost_code}</span>{/if}					  
                </div>				  
                <div class="form-group">
                  <label for="event_name" class="error">Name</label>
                  <input type="text" id="event_name" name="event_name" class="form-control" data-required="true" value="{$eventData.event_name}" />
				{if isset($errorArray.event_name)}<span class="error">{$errorArray.event_name}</span>{/if}					  
                </div>
				<div class="form-group">
					<label for="branch_code" class="error">Branch</label>
					<select id="branch_code" name="branch_code" class="form-control" data-required="true">
					<option value=""> ---------- </option>
					{html_options options=$branchData selected=$eventData.branch_code}
					</select>
					{if isset($errorArray.item_code)}<span class="error">{$errorArray.item_code}</span>{/if}					  
				</div>
                <div class="form-group">
                  <label for="event_date_start" class="error">Start date and time</label>
                  <input type="text" id="event_date_start" name="event_date_start" class="form-control" data-required="true" value="{$eventData.event_date_start}" />
				{if isset($errorArray.event_date_start)}<span class="error">{$errorArray.event_date_start}</span>{/if}					  
                </div>
                <div class="form-group">
                  <label for="event_date_end" class="error">End date and time</label>
                  <input type="text" id="event_date_end" name="event_date_end" class="form-control" data-required="true" value="{$eventData.event_date_end}" />
				{if isset($errorArray.event_date_end)}<span class="error">{$errorArray.event_date_end}</span>{/if}					  
                </div>	
				<div class="form-group">
					<label for="event_bank_name">Bank Name</label>
					<select id="event_bank_name" name="event_bank_name" class="form-control">
					<option value=""> ---------- </option>
					<option value="First National Bank" {if $eventData.event_bank_name eq 'First National Bank'}selected{/if}> First National Bank </option>
					<option value="Standard Bank" {if $eventData.event_bank_name eq 'Standard Bank'}selected{/if}> Standard Bank </option>
					<option value="Nedbank" {if $eventData.event_bank_name eq 'Nedbank'}selected{/if}> Nedbank </option>
					<option value="Capitec Bank" {if $eventData.event_bank_name eq 'Capitec Bank'}selected{/if}> Capitec Bank </option>
					<option value="ABSA Bank" {if $eventData.event_bank_name eq 'ABSA Bank'}selected{/if}> ABSA Bank </option>
					</select>
					{if isset($errorArray.event_bank_name)}<span class="error">{$errorArray.event_bank_name}</span>{/if}					  
				</div>
				<div class="form-group">
					<label for="event_bank_type">Bank Account Type</label>
					<select id="event_bank_type" name="event_bank_type" class="form-control">
						<option value="Current ( Cheque / Bond ) Account" {if $eventData.event_bank_type eq 'Current ( Cheque / Bond ) Account'}selected{/if}> Current ( Cheque / Bond ) Account </option>
						<option value="Savings Account" {if $eventData.event_bank_type eq 'Savings Account'}selected{/if}> Savings Account </option>
						<option value="Transmission Account" {if $eventData.event_bank_type eq 'Transmission Account'}selected{/if}> Transmission Account </option>
						<option value="Bond Account" {if $eventData.event_bank_type eq 'Bond Account'}selected{/if}> Bond Account </option>
						<option value="Subscription Share Account" {if $eventData.event_bank_type eq 'Subscription Share Account'}selected{/if}> Subscription Share Account </option>
					</select>
					{if isset($errorArray.event_bank_type)}<span class="error">{$errorArray.event_bank_type}</span>{/if}					  
				</div>				
                <div class="form-group">
                  <label for="event_bank_account">Bank Account Number </label>
                  <input type="text" id="event_bank_account" name="event_bank_account" class="form-control" data-required="true" value="{$eventData.event_bank_account}" />
				{if isset($errorArray.event_bank_account)}<span class="error">{$errorArray.event_bank_account}</span>{/if}					  
                </div>				
                <div class="form-group">
                  <label for="event_bank_code">Bank Account Branch Code </label>
                  <input type="text" id="event_bank_code" name="event_bank_code" class="form-control" value="{$eventData.event_bank_code}" />
				{if isset($errorArray.event_bank_code)}<span class="error">{$errorArray.event_bank_code}</span>{/if}					  
                </div>				
                <div class="form-group">
                  <label for="event_address" class="error">Address: Physical</label>
                  <textarea id="event_address" name="event_address" class="form-control">{$eventData.event_address}</textarea>
				{if isset($errorArray.event_address)}<span class="error">{$errorArray.event_address}</span>{/if}					  
                </div>			
                <div class="form-group">
                  <label for="event_page" class="error">Description page</label>
                  <textarea id="event_page" name="event_page" class="form-control wysihtml5" rows="25">{$eventData.event_page}</textarea>
				{if isset($errorArray.event_page)}<span class="error">{$errorArray.event_page}</span>{/if}					  
                </div>					
                <div class="form-group"><button type="submit" class="btn btn-primary">Validate and Submit</button></div>
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col --> 
		<div class="col-sm-3">
			<div class="list-group">  
				<a class="list-group-item" href="/event/list/">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				{if isset($eventData)}					
				<a class="list-group-item" href="/event/list/details.php?code={$eventData.event_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a> 
				<a class="list-group-item" href="/event/list/map.php?code={$eventData.event_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Map
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a> 	
				<a class="list-group-item" href="/event/list/media.php?code={$eventData.event_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Media
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a> 	
				<a class="list-group-item" href="/event/list/market.php?code={$eventData.event_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp; Vendors
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>				
				{/if}
			</div> <!-- /.list-group -->
        </div>			
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
<script type="text/javascript" src="/library/javascript/plugins/wysihtml5/wysihtml5-0.3.js"></script>
<script type="text/javascript" src="/library/javascript/plugins/wysihtml5/bootstrap-wysihtml5.js"></script>
{literal}
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	
	$('.wysihtml5').wysihtml5();	
	
	/* Area auto complete. */
	$( "#areapost_name" ).autocomplete({
		source: "/feeds/areapost.php",
		minLength: 2,
		select: function( event, ui ) {
			if(ui.item.id == '') {
				$('#areapost_name').html('');
				$('#areapost_code').val('');					
			} else {
				$('#areapost_name').html(ui.item.value);
				$('#areapost_code').val(ui.item.id);	
			}
			$('#areapost_name').val('');										
		}
	});
	
	$( "#event_date_start" ).datetimepicker({
		defaultDate: "+1w",
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showSecond: true,
		timeFormat: 'HH:mm:ss'
	});

	$( "#event_date_end" ).datetimepicker({
		defaultDate: "+1w",
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showSecond: true,
		timeFormat: 'HH:mm:ss'
	});
});
</script>
{/literal}
</html>
