<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/*** Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/**
 * Check for login
 */
require_once 'includes/auth.php';
require_once 'class/classified.php';
require_once 'class/item.php';

$classifiedObject	= new class_classified();
$itemObject 			= new class_item();
 
if(isset($_GET['delete_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success					= NULL;
	$code						= trim($_GET['delete_code']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {
		$data	= array();
		$data['classified_deleted'] = 1;
		
		$where 	= array();
		$where[] 	= $classifiedObject->getAdapter()->quoteInto('classified_code = ?', $code);
		$success	= $classifiedObject->update($data, $where);	
		
		if(is_numeric($success) && $success > 0) {
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	
	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(isset($_GET['status_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;
	$data 						= array();
	$formValid				= true;
	$success					= NULL;
	$itemcode					= trim($_GET['status_code']);
	
	if($errorArray['error']  == '') {
		
		$itemData = $classifiedData = $classifiedObject->getCode($itemcode);
		
		if($itemData) {
			$data	= array();
			$data['classified_active'] = trim($_GET['status']);
			
			$where 	= array();
			$where[] 	= $classifiedObject->getAdapter()->quoteInto('classified_code = ?', $itemcode);
			$success	= $classifiedObject->update($data, $where);	
		}
		
		if(is_numeric($success)) {		
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not update, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	
	echo json_encode($errorArray);
	exit;
}

/* Setup Pagination. */
if(isset($_GET['action']) && trim($_GET['action']) == 'search') {

	$filter		= array();

	$csv			= 0;

	$start 		= isset($_REQUEST['iDisplayStart']) ? $_REQUEST['iDisplayStart'] : 0;
	$length 	= isset($_REQUEST['iDisplayLength']) ? $_REQUEST['iDisplayLength'] : 20;
	
	if(isset($_REQUEST['filter_search']) && trim($_REQUEST['filter_search']) != '') $filter[] = array('filter_search' => trim($_REQUEST['filter_search']));
	if(isset($_REQUEST['filter_item']) && trim($_REQUEST['filter_item']) != '') $filter[] = array('filter_item' => trim($_REQUEST['filter_item']));
	if(isset($_REQUEST['filter_csv']) && trim($_REQUEST['filter_csv']) != '') { $filter[] = array('filter_csv' => (int)trim($_REQUEST['filter_csv'])); $csv = (int)trim($_REQUEST['filter_csv']); } 
	
	$classifieds = array();

	if(!$csv) {
		
		$classifiedData = $classifiedObject->getSearch($start, $length, $filter);

		if($classifiedData) {
			for($i = 0; $i < count($classifiedData['records']); $i++) {
				$item = $classifiedData['records'][$i];
				$classifieds[$i] = array(
					$classifiedObject->humanTime($item['classified_added']),
					($item['item_name'] == '' ? 'Pending' : '<span class="'.$item['item_config_class'].'">'.$item['item_name'].'</span>'),
					($item['social_facebook_id'] == '' ? '<img src="/images/no-image.jpg" width="50px" />' : '<a href="http://facebook.com/'.$item['social_facebook_id'].'" target="_blank"><img src="https://graph.facebook.com/'.$item['social_facebook_id'].'/picture?width=300" width="50px" /></a>'),
					'<a href="/classified/details.php?code='.$item['classified_code'].'" alt="'.$item['vet_reason'].'" title="'.$item['vet_reason'].'">'.$item['classified_subject'].'</a>',
					'<button value="" class="btn btn-danger" onclick="vetModal(\''.$item['classified_code'].'\', \'classified\', \''.$item['classified_name'].' '.$item['classified_surname'].'\', \'default\'); return false;">Vet</button>',
					'<button value="Delete" class="btn btn-danger" onclick="deleteModal(\''.$item['classified_code'].'\', \'\', \'default\'); return false;">Delete</button>'
				);
			}
		}

		if($classifiedData) {
			$response['sEcho'] = $_REQUEST['sEcho'];
			$response['iTotalRecords'] = $classifiedData['displayrecords'];		
			$response['iTotalDisplayRecords'] = $classifiedData['count'];
			$response['aaData']	= $classifieds;
		} else {
			$response['result'] 	= false;
			$response['message']	= 'There are no items to show.';			
		}

		echo json_encode($response);
		die();

	} else {
		
		$classifiedData = $classifiedObject->getSearch($start, $length, $filter);
		
		$row = "Category, Seciton, Days passed, Added Date, Expiry date, Status, Reason, Name, Email, Number, Subject\r\n";
		if($classifiedData) {
			for($i = 0; $i < count($classifiedData); $i++) {
				$item = $classifiedData[$i];
				$classifieds[$i] = array(
						str_replace(',', ' ',$item['category_name']),
						str_replace(',', ' ',$item['section_name']),
						$classifiedObject->humanTime($item['classified_added']),
						date('d M Y', strtotime($item['classified_added'])),
						date('d M Y', strtotime($item['classified_expire'])),
						str_replace(',', ' ',$item['vet_name']),
						str_replace(',', ' ',$item['vet_reason']),
						str_replace(',', ' ',$item['member_name']),
						str_replace(',', ' ',$item['member_email']),
						str_replace(',', ' ',$item['member_number']), 
						str_replace(',', ' ',$item['classified_subject']));
			}
			
			foreach ($classifieds as $data) {
				$row .= implode(', ', $data)."\r\n";
			}			
		}
		
		header("Content-Type: text/plain");
		header("Content-Disposition: attachment; filename=classified_report_".date('Y-m-d').".csv");
		Header("Cache-Control: private, must-revalidate" ); // HTTP/1.1
		Header("Pragma: private" ); // HTTP/1.0
	   
		print($row);
		exit;		
	}
}

$itemData = $itemObject->selectType('CLASSIFIED');
if($itemData) $smarty->assign('itemData', $itemData);

$itemvetData = $itemObject->selectParents('VET', 'CLASSIFIED');
if($itemvetData) $smarty->assign('itemvetData', $itemvetData);

/* End Pagination Setup. */
$smarty->display('classified/default.tpl');
?>