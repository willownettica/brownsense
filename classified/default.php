<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/auth.php'; 

/* objects. */
require_once 'class/classified.php';
require_once 'class/areapost.php';
require_once 'class/item.php';

$classifiedObject 	= new class_classified();
$areapostObject 	= new class_areapost();
$itemObject 			= new class_item();

$where			= array();
$parameter	= "";
$filters			= array();

if(isset($_REQUEST['filter_search']) && trim($_REQUEST['filter_search']) != '') $where[] = array('filter_search' => trim($_REQUEST['filter_search']));
if(isset($_REQUEST['filter_province']) && trim($_REQUEST['filter_province']) != '') $where[] = array('filter_province' => trim($_REQUEST['filter_province']));
if(isset($_REQUEST['filter_category']) && trim($_REQUEST['filter_category']) != '') $where[] = array('filter_category' => trim($_REQUEST['filter_category']));
if(isset($_REQUEST['filter_section']) && count($_REQUEST['filter_section']) != 0 && is_array($_REQUEST['filter_section'])) {
	$where[] = array('filter_section' => implode(',',$_REQUEST['filter_section']));
	$filters['filter_section'] = $_REQUEST['filter_section'];
}
if(isset($_REQUEST['filter_section']) && !is_array($_REQUEST['filter_section']) && trim($_REQUEST['filter_section']) != '') {
	$where[] = array('filter_section' => $_REQUEST['filter_section']);
	$filters['filter_section'] = trim($_REQUEST['filter_section']);
}


if(count($where) > 0) {
	for($i = 0; $i < count($where); $i++) { 
		if(isset($where[$i]['filter_search']) && trim($where[$i]['filter_search']) != '') {
			$parameter .= "&filter_search=".$where[$i]['filter_search'];
			$filters['filter_search'] = $where[$i]['filter_search'];
		} else if(isset($where[$i]['filter_province']) && trim($where[$i]['filter_province']) != '') {
			$parameter .= "&filter_province=".$where[$i]['filter_province'];
			$filters['filter_province'] = $where[$i]['filter_province'];
		} else if(isset($where[$i]['filter_category']) && trim($where[$i]['filter_category']) != '') {
			$parameter .= "&filter_category=".$where[$i]['filter_category'];
			$filters['filter_category'] = $where[$i]['filter_category'];
		} else if(isset($where[$i]['filter_section']) && count($where[$i]['filter_section']) != 0) {
			$parameter .= "&filter_section=".$where[$i]['filter_section'];			
		}
	}
}

/* Check posted data. */
if(isset($_GET['getsection'])) {
	
	$categorycode	= trim($_GET['filter_category']);
	$html 				= '';

	if($categorycode  != '') {
		
		$sectionData = $itemObject->getChildren($categorycode, 'TAG');
		 
		if($sectionData) {
			for($i = 0; $i < count($sectionData); $i++) {
				$selected = isset($filters['filter_section']) && in_array($sectionData[$i]['item_code'], explode(',',$filters['filter_section'])) ? 'checked' : '';
				$html .= "<p><input type='checkbox' $selected id='filter_section[]' name='filter_section[]' value='".$sectionData[$i]['item_code']."'>&nbsp;&nbsp;&nbsp;&nbsp;<span id='".$sectionData[$i]['item_code']."'>".$sectionData[$i]['item_name'].'</span></p>';
			}
		} else {
			$html .= "No sections for this category";
		}
	} else {
		$html .= "No sections for this category";
	}
	
	echo $html;
	exit;
}

$page 		= isset($_GET['page']) 		? $_GET['page'] 		: 1;
$perPage	= isset($_GET['perPage'])	? $_GET['perPage']	: 10;

$classifiedData = $classifiedObject->paginate($where, $page, $perPage);

$classifiedItems = $classifiedData->getCurrentItems();

if($classifiedItems) {
	$paginator = $classifiedData->setView()->getPages();
}

$provinceData	= $areapostObject->getProvinces();
$categoryPairs	= $itemObject->selectType('CATEGORYCLASS');
$itemPairs 		= $itemObject->selectParents('CLASSIFIED');
?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<link rel="stylesheet" href="/css/jquery-ui-1.12.1.css">
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/classified/">Classified</a></li>						
						<li><a href="#">Search classified</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-7 dual-posts padding-bottom-30">
				<h2>Classified</h2><br />
				<p>Search classifieds. The number of items found so far is <span class="text_brown"><?php echo $paginator->totalItemCount; ?></span>. <?php echo ((int)$paginator->totalItemCount > 0 ? 'Search has <span class="text_brown">'.$paginator->pageCount.'</span> pages.' : '') ; ?></p>
				<p><?php if(isset($_REQUEST['filter_search']) && $_REQUEST['filter_search'] != '') { ?>Searching for text: '<i><span class="normal-font text_brown"><?php echo $_REQUEST['filter_search']; ?></span></i>'.<?php } ?>
				<?php if(isset($_REQUEST['filter_province']) && $_REQUEST['filter_province'] != '') { ?>Results in <span class="normal-font text_brown"><?php echo $provinceData[$_REQUEST['filter_province']]; ?></span>.<?php } ?>
				<?php if(isset($_REQUEST['filter_type']) && $_REQUEST['filter_type'] != '') { ?>Of the type <span class="normal-font text_brown"><?php echo $itemPairs[$_REQUEST['filter_type']]; ?></span>.<?php } ?><span id="sectionshtml" name="sectionshtml"></span>				
				</p>					
				<br />
				<?php if($paginator->pageCount > 1) { ?>
				<!-- PAGINATION -->
				<ul class="pagination">
					<?php foreach($paginator->pagesInRange as $page) { ?>
					<li <?php echo ((int)$page == (int)$paginator->current ? 'class="active"' : ''); ?>><a href="/classified/?page=<?php echo $page ?>&<?php echo $parameter; ?>"><?php echo $page ?></a></li>
					<?php } ?>
				</ul>
				<?php } ?>					 
				<hr class="l4">
				<?php foreach($classifiedItems as $item) { ?>
				<div class="row">			
					<div class="layout_3--item">						
						<div class="col-md-3 col-sm-4">														
							<div class="thumb">								
								<a href="/classified/<?php echo $item['classified_code']; ?>">									
									<?php if($item['media_code'] != '') { ?>									
									<img src="<?php echo $item['media_path'].'tmb_'.$item['media_code'].$item['media_ext']; ?>" class="img-responsive" alt="<?php echo $item['classified_subject']; ?>" title="<?php echo $item['classified_subject']; ?>" />
									<?php } else { ?>
									<img src="/images/no-image.jpg" class="img-responsive" alt="<?php echo $item['classified_subject']; ?>" title="<?php echo $item['classified_subject']; ?>" />
									<?php } ?>
								</a>
							</div>
						</div>
						<div class="col-md-9 col-sm-8">		

							<span class="cat "><?php echo $item['areapost_name']; ?></span>
							<h4><a href="/classified/<?php echo $item['classified_code']; ?>"><?php echo $item['classified_subject']; ?></a></h4>
							<p><?php echo substr(strip_tags($item['classified_message']), 0, 150).'....'; ?></p>
							<div class="meta" style="font-size: 13px !important; color: #42210b important;">
								<span class="author" alt="Category" title="Category"><?php echo $item['category_name']; ?></span> &nbsp;&nbsp;|&nbsp;&nbsp; 
								<span class="author" alt="Section" title="Section"><?php echo $item['section_name']; ?></span> &nbsp;&nbsp;|&nbsp;&nbsp; 
								<span class="author" alt="Added by member" title="Added by member"><?php echo $item['member_name']; ?></span> &nbsp;&nbsp;|&nbsp;&nbsp;
								<span class="date" alt="Time passed since added" title="Time passed since added"><?php echo $classifiedObject->humanTime($item['classified_added']); ?></span>
							</div>					
						</div>
					</div>
				</div>
				<hr class="l4">
				<?php } ?>
				<?php if($paginator->pageCount > 1) { ?>
				<!-- PAGINATION -->
				<ul class="pagination">
					<?php foreach($paginator->pagesInRange as $page) { ?>
					<li <?php echo ((int)$page == (int)$paginator->current ? 'class="active"' : ''); ?>><a href="/classified/?page=<?php echo $page ?>&<?php echo $parameter; ?>"><?php echo $page ?></a></li>
					<?php } ?>
				</ul>
				<?php } ?>			
			</div>
			<!-- // CATEGORY -->
			<aside class="col-md-3 col-sm-4">			
				<div class="side-widget margin-bottom-30">
					<h3 class="heading-1"><span>Filter</span></h3>		
					<form class="post-comment-form"action="/classified/" method="GET">		
						<div class="row">
							<div class="col-md-12">
								<label>Search</label>
								<input type="text" id="filter_search" name="filter_search" value="<?php echo (isset($_REQUEST['filter_search']) && $_REQUEST['filter_search'] != '' ? $_REQUEST['filter_search'] : ''); ?>" style="margin-bottom: 2px !important;" />
								<span class="tiny_explain">Search by subject of classified</span>
							</div>
						</div>						
						<br />
						<div class="row">
							<div class="col-md-12">
								<label>Type</label>
								<select id="filter_type" name="filter_type" style="margin-bottom: 2px !important;">
									<option value=""> ------------------- </option>
									<?php 
										while ($value = current($itemPairs)) {
											echo '<option '.(isset($_REQUEST['filter_type']) &&  $_REQUEST['filter_type'] == key($itemPairs) ? 'selected' : '').' value="'.key($itemPairs).'">'.$value.'</option>';
											next($itemPairs);
										}
									?>
								</select>
								<span class="tiny_explain">Select the type of items you want to see.</span>
							</div>		
						</div>												
						<br />								
						<div class="row">
							<div class="col-md-12">
								<label>Province</label>
								<select id="filter_province" name="filter_province" style="margin-bottom: 2px !important;">
									<option value=""> ------------------- </option>
									<?php 
										while ($value = current($provinceData)) {
											echo '<option '.(isset($_REQUEST['filter_province']) &&  $_REQUEST['filter_province'] == key($provinceData) ? 'selected' : '').' value="'.key($provinceData).'">'.$value.'</option>';
											next($provinceData);
										}
									?>
								</select>
								<span class="tiny_explain">Select province that you want classified from.</span>
							</div>		
						</div>												
						<br />						
						<div class="row">
							<div class="col-md-12">
								<label>Category</label>
								<select id="filter_category" name="filter_category" style="margin-bottom: 2px !important;">
									<option value=""> ------------------- </option>
									<?php 
										while ($value = current($categoryPairs)) {
											echo '<option '.(isset($_REQUEST['filter_category']) &&  $_REQUEST['filter_category'] == key($categoryPairs) ? 'selected' : '').' value="'.key($categoryPairs).'">'.$value.'</option>';
											next($categoryPairs);
										}
									?>
								</select>
								<span class="tiny_explain">Select the category you want to search in</span>
							</div>		
						</div>
						<br />
						<div class="row">
							<div class="col-md-12">
								<label>Sections</label><br />
								<span id="sectioninput" name="sectioninput">
									No category selected.
								</span><br />
								<span class="tiny_explain">After selecting a category, you must at least select one section</span>
							</div>		
						</div>		
						<br />						
						<button type="submit">Update Filter</button>&nbsp;&nbsp;&nbsp;&nbsp;
						<button type="button" onclick="window.location.href = '/classified/'; return false;">Clear Search</button>
					</form>
				</div>
			</aside>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	getSection();
	$("#filter_category").change(function() {
	  getSection();
	});
});

function getSection() {
	$.ajax({
		type: "GET",
		url: "?getsection=1<?php echo $parameter; ?>",
		data: "filter_category="+$('#filter_category :selected').val(),
		dataType: "html",
		success: function(data){				
			$('#sectioninput').html(data);
			addFilterText();
		}
	});								
	return false;
}


function addFilterText() {
<?php if (isset($filters['filter_category']) && $filters['filter_category'] != '') { ?>
	var category = '<?php echo $filters['filter_category']; ?>';
	var html 		= ' Searching in category <span class="normal-font text_brown">'+$('#filter_category  :selected').text()+'</span>';
<?php 
if(isset($filters['filter_section']) && is_array($filters['filter_section']) && count($filters['filter_section']) > 0) {
?>
		html 		+= ', in sections: ';
		var sections = [<?php echo "'".implode("','",$filters['filter_section'])."'"; ?>];		
		for(var i = 0; i < sections.length; i++) {
			if(i != 0) {
				html += ', ';
			}
			html += '<span class="normal-font text_brown">'+$('#'+sections[i]).html()+'</span>';			
		}		
<?php 	
}
?>
html 		+= '.';
$('#sectionshtml').html(html);
<?php } ?>
return false;
}
</script>
</body>
</html>
