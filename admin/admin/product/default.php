<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/*** Standard includes */
require_once 'config/smarty.php';
/** Check for login */
require_once 'includes/auth.php';

$smarty->display('admin/product/default.tpl');
?>