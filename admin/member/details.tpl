<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
	<h2 class="content-header-title">Members</h2>
	<ol class="breadcrumb">
		<li><a href="/">Home</a></li>
		<li><a href="/member/">Members</a></li>
		<li><a href="#">{if isset($memberData)}{$memberData.member_name} {$memberData.member_surname}{else}Add a member{/if}</a></li>
		<li class="active">Details</li>
	</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					{if isset($memberData)}{$memberData.member_name} {$memberData.member_surname}{else}Add a member{/if}
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="/member/details.php{if isset($memberData)}?code={$memberData.member_code}{/if}" method="POST" data-validate="parsley" class="form parsley-form">
				<div class="form-group">
					<label for="branch_code" class="error">Branch</label>
					<select id="branch_code" name="branch_code" class="form-control" data-required="true" readonly disabled >
					<option value=""> ---------- </option>
					{html_options options=$branchPairs selected=$memberData.branch_code}
					</select>				  
				</div>			  				  
                <div class="form-group">
                  <label for="member_name" class="error">Name</label>
                  <input type="text" id="member_name" name="member_name" class="form-control" data-required="true" value="{$memberData.member_name}"  readonly disabled />
				{if isset($errorArray.member_name)}<span class="error">{$errorArray.member_name}</span>{/if}					  
                </div>				
                <div class="form-group">
                  <label for="member_email" class="error">Email</label>
                  <input type="text" id="member_email" name="member_email" class="form-control" data-required="true" value="{$memberData.member_email}"  readonly disabled />			  
                </div>
                <div class="form-group">
                  <label for="member_cellphone" class="error">Cellphone</label>
                  <input type="text" id="member_cellphone" name="member_cellphone" class="form-control" data-required="true" value="{$memberData.member_cellphone}"  readonly disabled />				  
                </div>	
                <div class="form-group">
                  <label for="member_birthdate">Birth date</label>
                  <input type="text" id="member_birthdate" name="member_birthdate" class="form-control" value="{$memberData.member_birthdate}" />
				{if isset($errorArray.member_birthdate)}<span class="error">{$errorArray.member_birthdate}</span>{/if}					  
                </div>										
                <div class="form-group">
                  <label for="member_social_twitter">Social Media: Twitter Account ( Handler only )</label>
                  <input type="text" id="member_social_twitter" name="member_social_twitter" class="form-control" value="{$memberData.member_social_twitter}" />
				{if isset($errorArray.member_social_twitter)}<span class="error">{$errorArray.member_social_twitter}</span>{/if}					  
                </div>
                <div class="form-group"><button type="submit" class="btn btn-primary">Validate and Submit</button></div>
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col --> 
		<div class="col-sm-3">
			<div class="list-group">  
				<a class="list-group-item" href="/member/">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				{if isset($memberData)}				
				<a class="list-group-item" href="/member/details.php?code={$memberData.member_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a> 
				<a class="list-group-item" href="/member/media.php?code={$memberData.member_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Media
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a> 	
				<a class="list-group-item" href="/member/tag.php?code={$memberData.member_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Tag(s)
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>					
				<a class="list-group-item" href="/member/comm.php?code={$memberData.member_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp;Comms
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>					 
				{/if}
			</div> <!-- /.list-group -->
        </div>			
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
{literal}
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	/* Area auto complete. */
	$( "#areapost_name" ).autocomplete({
		source: "/feeds/areapost.php",
		minLength: 2,
		select: function( event, ui ) {
			if(ui.item.id == '') {
				$('#areapost_name').html('');
				$('#areapost_code').val('');					
			} else {
				$('#areapost_name').html(ui.item.value);
				$('#areapost_code').val(ui.item.id);	
			}
			$('#areapost_name').val('');										
		}
	});
	
	$( "#member_birthdate" ).datepicker({
		defaultDate: "+1w",
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1
	});
});
</script>
{/literal}
</html>
