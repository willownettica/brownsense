<?php

//custom account link class as account table abstraction
class class_link extends Zend_Db_Table_Abstract
{
   //declare table variables
    protected $_name 			= 'link';
	protected $_primary 		= 'link_code';

	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 
	 public function insert(array $data)
    {
        // add a timestamp
        $data['link_added']	= date('Y-m-d H:i:s');        
		$data['link_code']		= $this->createCode();

		return parent::insert($data);
    }
	
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where)
    {
        // add a timestamp
        $data['link_updated'] = date('Y-m-d H:i:s');
        
        return parent::update($data, $where);
    }
	
	/**
	 * get job by job link Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code) {	
		$select = $this->_db->select()	
					->from(array('link' => 'link'))
					->where('link_deleted = 0')
					->where('link_code = ?', $code)
					->limit(1);
       
		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}

	/**
	 * get job by job link Id
 	 * @param string job id
     * @return object
	 */
	public function getByParent($parenttype, $parentcode, $childtype) {	
		
		if($childtype == 'ADMIN') {
			$select = $this->_db->select()	
						->from(array('link' => 'link'))
						->joinLeft(array('admin' => 'admin'), 'admin.admin_code = link.link_child_code')	
						->where('link_deleted = 0 and link_parent_type = ?', $parenttype)
						->where('link_deleted = 0 and link_parent_code = ?', $parentcode)
						->where('link_deleted = 0 and link_child_type = ?', $childtype)
						->where('admin_deleted = 0');
		} else if($childtype == 'TAG') {
			$select = $this->_db->select()	
						->from(array('link' => 'link'))
						->joinLeft(array('item' => 'item'), 'item.item_code = link.link_child_code')	
						->where('link_deleted = 0 and link_parent_type = ?', $parenttype)
						->where('link_deleted = 0 and link_parent_code = ?', $parentcode)
						->where('link_deleted = 0 and link_child_type = ?', $childtype)
						->where('item_deleted = 0');
		} else if($childtype == 'COMPANY') {
			$select = $this->_db->select()	
						->from(array('link' => 'link'))
						->joinLeft(array('company' => 'company'), 'company.company_code = link.link_child_code')	
						->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = company.areapost_code')	
						->where('link_deleted = 0 and link_parent_type = ?', $parenttype)
						->where('link_deleted = 0 and link_parent_code = ?', $parentcode)
						->where('link_deleted = 0 and link_child_type = ?', $childtype)
						->where('company_deleted = 0');
		} else if($childtype == 'EVENT') {
			$select = $this->_db->select()	
						->from(array('link' => 'link'))
						->joinLeft(array('event' => 'event'), 'event.event_code = link.link_child_code')	
						->joinLeft(array('branch' => 'branch'), 'branch.branch_code = event.branch_code')	
						->where('link_deleted = 0 and link_parent_type = ?', $parenttype)
						->where('link_deleted = 0 and link_parent_code = ?', $parentcode)
						->where('link_deleted = 0 and link_child_type = ?', $childtype)
						->where('company_deleted = 0');						
		} else if($childtype == 'MEMBER') {
			$select = $this->_db->select()	
						->from(array('link' => 'link'))
						->joinLeft(array('member' => 'member'), 'member.member_code = link.link_child_code')	
						->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = member.areapost_code')	
						->where('link_deleted = 0 and link_parent_type = ?', $parenttype)
						->where('link_deleted = 0 and link_parent_code = ?', $parentcode)
						->where('link_deleted = 0 and link_child_type = ?', $childtype)
						->where('member_deleted = 0');							
		} else {
			return false;
		}
		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get job by job link Id
 	 * @param string job id
     * @return object
	 */
	public function getByChild($childtype, $childcode, $parenttype) {	
		
		if($childtype == 'EVENT') {
			if($parenttype == 'MARKET') {
			
				$tag = $this->_db->select()	
					->from(array('link' => 'link'), array('link_parent_code', "group_concat(item.item_name separator ',') as tag_name"))
					->joinLeft(array('item' => 'item'), 'item.item_code = link.link_child_code', array())	
					->where("link_parent_type = 'COMPANY' and link_child_type = 'TAG'")
					->group('link_parent_code');	
					
				$select = $this->_db->select()
					->from(array('link' => 'link'))
					->joinLeft(array('event' => 'event'), 'event.event_code = link.link_child_code')
					->joinLeft(array('branch' => 'branch'), 'branch.branch_code = event.branch_code')
					->joinLeft(array('market' => 'market'), 'market.market_code = link.link_parent_code')
					->joinLeft(array('company' => 'company'), 'company.company_code = market.company_code')
					->joinLeft(array('tag' => $tag), 'tag.link_parent_code = company.company_code')
					->where('link_deleted = 0 and link_parent_type = ?', $parenttype)
					->where('link_deleted = 0 and link_child_code = ?', $childcode)
					->where('link_deleted = 0 and link_child_type = ?', $childtype)
					->where('event_deleted = 0 and market_deleted = 0 and branch_deleted = 0 and company_deleted = 0');					
			} else {
				return false;
			}
		} else {
			return false;
		}
		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get job by job link Id
 	 * @param string job id
     * @return object
	 */
	public function getChild($parenttype, $parentcode, $childtype, $childcode) {	
		
		if($childtype == 'ADMIN') {
			$select = $this->_db->select()	
						->from(array('link' => 'link'))
						->joinLeft(array('admin' => 'admin'), 'admin.admin_code = link.link_child_code')	
						->where('link_deleted = 0 and link_parent_type = ?', $parenttype)
						->where('link_deleted = 0 and link_parent_code = ?', $parentcode)
						->where('link_deleted = 0 and link_child_type = ?', $childtype)
						->where('link_deleted = 0 and link_child_code = ?', $childcode)
						->where('admin_deleted = 0');
		} else if($childtype == 'TAG') {
			$select = $this->_db->select()	
						->from(array('link' => 'link'))
						->joinLeft(array('item' => 'item'), 'item.item_code = link.link_child_code')	
						->where('link_deleted = 0 and link_parent_type = ?', $parenttype)
						->where('link_deleted = 0 and link_parent_code = ?', $parentcode)
						->where('link_deleted = 0 and link_child_type = ?', $childtype)
						->where('link_deleted = 0 and link_child_code = ?', $childcode)
						->where('item_deleted = 0');
		} else if($childtype == 'COMPANY') {
			$select = $this->_db->select()	
						->from(array('link' => 'link'))
						->joinLeft(array('company' => 'company'), 'company.company_code = link.link_child_code')	
						->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = company.areapost_code')	
						->where('link_deleted = 0 and link_parent_type = ?', $parenttype)
						->where('link_deleted = 0 and link_parent_code = ?', $parentcode)
						->where('link_deleted = 0 and link_child_type = ?', $childtype)
						->where('link_deleted = 0 and link_child_code = ?', $childcode)
						->where('company_deleted = 0');
		} else if($childtype == 'MEMBER') {
			$select = $this->_db->select()	
						->from(array('link' => 'link'))
						->joinLeft(array('member' => 'member'), 'member.member_code = link.link_child_code')	
						->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = member.areapost_code')	
						->where('link_deleted = 0 and link_parent_type = ?', $parenttype)
						->where('link_deleted = 0 and link_parent_code = ?', $parentcode)
						->where('link_deleted = 0 and link_child_type = ?', $childtype)
						->where('link_deleted = 0 and link_child_code = ?', $childcode)
						->where('member_deleted = 0');							
		} else {
			return false;
		}
		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get job by job link Id
 	 * @param string job id
     * @return object
	 */
	public function getAll($parenttype, $childtype) {	

		$select = $this->_db->select()	
				->from(array('link' => 'link'))
				->where('link_deleted = 0 and link_parent_type = ?', $parenttype)
				->where('link_deleted = 0 and link_child_type = ?', $childtype);

		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}	
	/**
	 * get job by job link Id
 	 * @param string job id
     * @return object
	 */
	public function checkExists($parenttype, $parentcode, $childtype, $childcode) {	
		
		$select = $this->_db->select()	
					->from(array('link' => 'link'))
					->where('link_deleted = 0 and link_parent_type = ?', $parenttype)
					->where('link_parent_code = ?', $parentcode)
					->where('link_child_type = ?', $childtype)
					->where('link_child_code = ?', $childcode);

		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getCode($reference)
	{
		$select = $this->_db->select()	
					->from(array('link' => 'link'))	
					->where('link_code = ?', $reference)
					->limit(1);

	   $result = $this->_db->fetchRow($select);	
        return ($result == false) ? false : $result = $result;					   
	}

	function createCode() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '1234567890';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Number[rand(0,$count)];
		}
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<4;$i++){
			$code .= $Number[rand(0,$count)];
		}		
		/* First check if it exists or not. */
		$linkCheck = $this->getCode($code);
		
		if($linkCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}	
}
?>