<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Management System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
	
	<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQULf4QMovya_unNUL1-kMhI_FxkZib7I&sensor=true&v=3"
    async defer></script> -->

<!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> -->
{literal}
<script type="text/javascript">
var map;
var marker;

function initMap()
{
	var opts = {'center': new google.maps.LatLng({/literal}{$eventData.event_map_latitude|default:"-33.9285481685662"}, {$eventData.event_map_longitude|default:"18.42681884765625"}{literal}), 'zoom':14, 'mapTypeId': google.maps.MapTypeId.SATELLITE }
	map = new google.maps.Map(document.getElementById('mapdiv'),opts);
	
	
	marker = new google.maps.Marker({
		position: new google.maps.LatLng({/literal}{$eventData.event_map_latitude|default:"-33.9285481685662"}, {$eventData.event_map_longitude|default:"18.42681884765625"}{literal}),
		map: map
	});
	
	google.maps.event.addListener(map,'click',function(event) {
		
	//call function to create marker
		if (marker) {
			marker.setMap(null);
			marker = null;
		}
		
		document.getElementById('event_map_latitude').value = event.latLng.lat();
		document.getElementById('event_map_longitude').value = event.latLng.lng();
		marker = new google.maps.Marker({
			position: event.latLng,
			map: map
		});
	});
}

</script>
{/literal}	
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQULf4QMovya_unNUL1-kMhI_FxkZib7I&callback=initMap"
    async defer></script>
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
		<h2 class="content-header-title">Events</h2>
		<ol class="breadcrumb">
		<li><a href="/">Home</a></li>
		<li><a href="/event/list/">Events</a></li>
		<li><a href="#">{$eventData.event_name}</a></li>
		<li class="active">Map Location</li>
		</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					Store Location
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="/event/list/map.php?code={$eventData.event_code}" method="POST" data-validate="parsley" class="form parsley-form" enctype="multipart/form-data">			
				<p>Please select by clicking on top of the shop on the map, you can drag the map to any direction.</p>
				<div class="form-group">
					<div  id="mapdiv"></div>
				</div>
				<p>Below are the selected coordinates from the map.</p>	
                <div class="form-group">
					<label for="event_map_latitude">Latitude</label>
					<input type="text" id="event_map_latitude" name="event_map_latitude" class="form-control" readonly value="{$eventData.event_map_latitude}" />
					{if isset($errorArray.event_map_latitude)}<span class="error">{$errorArray.event_map_latitude}</span>{else}<span class="smalltext">Latitude coordinate of the event</span>{/if}					  
                </div>
                <div class="form-group">
					<label for="event_map_longitude">Longitude</label>
					<input type="text" id="event_map_longitude" name="event_map_longitude" class="form-control" readonly value="{$eventData.event_map_longitude}" />
					{if isset($errorArray.event_map_longitude)}<span class="error">{$errorArray.event_map_longitude}</span>{else}<span class="smalltext">Longitude coordinate of the event</span>{/if}					  
                </div>
                <div class="form-group"><button type="submit" class="btn btn-primary">Upload and Save</button></div>
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col -->		
		<div class="col-sm-3">
			<div class="list-group">  
				<a href="/event/list/" class="list-group-item">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
				<a class="list-group-item" href="/event/list/details.php?code={$eventData.event_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a> 
				<a class="list-group-item" href="/event/list/map.php?code={$eventData.event_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Map
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a> 	
				<a class="list-group-item" href="/event/list/media.php?code={$eventData.event_code}">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Media
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a> 	
				<a class="list-group-item" href="/event/list/market.php?code={$eventData.event_code}">
				  <i class="fa fa-file"></i> &nbsp;&nbsp; Vendors
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>
			</div> <!-- /.list-group -->
		</div>		
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
{literal}
<script type="text/javascript" language="javascript">
$(document).ready(function() {
	$('#mapdiv').css("height",$('.portlet-content').height());
	$('#mapdiv').css("width",$('.portlet-content').width());
	google.maps.event.trigger(map, 'resize');
	map.setZoom( map.getZoom() );
});
</script>
{/literal}
</html>