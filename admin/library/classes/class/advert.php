<?php

//custom account item class as account table abstraction
class class_advert extends Zend_Db_Table_Abstract
{
   //declare table variables
    protected $_name 		= 'advert';
	protected $_primary 	= 'advert_code';
	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 
	public function insert(array $data) {
        // add a timestamp
        $data['advert_added'] = date('Y-m-d H:i:s');        
		$data['advert_code']	= $this->createCode();
		
		return parent::insert($data);
    }
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where) {
        // add a timestamp
        $data['advert_updated'] = date('Y-m-d H:i:s');
        
        return parent::update($data, $where);
    }
	/**
	 * get job by job advert Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code) {	
		$select = $this->_db->select()	
					->from(array('advert' => 'advert'))	
					->where('advert_deleted = 0')
					->where('advert_code = ?', $code)
					->limit(1);
       
		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}

	/**
	 * get job by job advert Id
 	 * @param string job id
     * @return object
	 */
	public function getAll() {
	
		$select = $this->_db->select()	
					->from(array('advert' => 'advert'))	
					->where('advert_deleted = 0');

		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getCode($reference) {
		$select = $this->_db->select()	
				->from(array('advert' => 'advert'))	
				->where('advert_code = ?', $reference)
				->limit(1);

		$result = $this->_db->fetchRow($select);	
        return ($result == false) ? false : $result = $result;					   
	}

	function createCode() {
		/* New code. */
		$code = "";
		$number 	= '234567890';
		
		/* Next six numbers */
		$count = strlen($number) - 1;

		for($i=0;$i<4;$i++){
			$code .= $number[rand(0,$count)];
		}

		/* First check if it exists or not. */
		$itemCheck = $this->getCode($code);
		
		if($itemCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}
}
?>