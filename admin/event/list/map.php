<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/*** Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/*** Check for login */
require_once 'includes/auth.php';

/* objects. */
require_once 'class/event.php';

$eventObject	= new class_event();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$eventData = $eventObject->getByCode($code);
	
	if($eventData) {
		$smarty->assign('eventData', $eventData);
	} else {
		header('Location: /event/list/');
		exit;		
	}
} else {
	header('Location: /event/list/');
	exit;		
}


/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray		= array();
	$data 				= array();
	$formValid		= true;
	$success			= NULL;
	$areaByName	= NULL;
	
	if(isset($_POST['event_map_latitude']) && trim($_POST['event_map_latitude']) == '') {
		$errorArray['event_map_latitude'] = 'Store latitude is required';
		$formValid = false;		
	}
	
	if(isset($_POST['event_map_longitude']) && trim($_POST['event_map_longitude']) == '') {
		$errorArray['event_map_longitude'] = 'Store longitude is required';
		$formValid = false;		
	}
	
	if(count($errorArray) == 0 && $formValid == true) {

		$data 	= array();				
		$data['event_map_latitude']		= trim($_POST['event_map_latitude']);	
		$data['event_map_longitude']	= trim($_POST['event_map_longitude']);		
		
		$where		= $eventObject->getAdapter()->quoteInto('event_code = ?', $eventData['event_code']);
		$eventObject->update($data, $where);			
		
		
		if(count($errorArray) == 0) {
			header('Location: /event/list/media.php?code='.$eventData['event_code']);	
			exit;		
		}	
			
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

$smarty->display('event/list/map.tpl');

?>