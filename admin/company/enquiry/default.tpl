<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header"> 
		<h2 class="content-header-title">Event</h2>
		<ol class="breadcrumb">
		<li><a href="/">Home</a></li>
		<li>Events</li>
		<li class="active">Event Vendors</li>
		</ol>
	</div>	  
	<div class="row">
        <div class="col-md-12"
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                Event Vendors
              </h3>
            </div> <!-- /.portlet-header -->	
			<div class="portlet-content">
				<div class="form-group">
				  <label for="filter_search">Search by company name or products / services</label>
				  <input type="text" id="filter_search" name="filter_search" class="form-control" value="" />
				</div>					
				<div class="form-group">
					<button type="button" onclick="getRecords(); return false;" class="btn btn-primary">Search</button>
					<button type="button" onclick="csv(); return false;" class="btn">Download CSV</button>
				</div>
				<p>In total there are <span id="iTotalDisplayRecords" class="success"></span> records found in total</p>
				<div id="tableContent" align="center">
					<!-- Start Content Table -->
					<div class="content_table">
						<img src="/images/ajax_loader.gif" />
					 </div>
					 <!-- End Content Table -->	
				</div>				
			</div>			
          </div> <!-- /.portlet -->
        </div> <!-- /.col -->	
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
{literal}
<script type="text/javascript">
$(document).ready(function() {
	getRecords();
});

function getRecords() {
	var html		= '';
	
	var filter_search	= $('#filter_search').val() != 'undefined' ? $('#filter_search').val() : '';
	
	/* Clear table contants first. */			
	$('#tableContent').html('');
	
	$('#tableContent').html('<table cellpadding="0" cellspacing="0" width="100%" border="0" class="display" id="dataTable"><thead><tr><th>Date Sent</th><th>Company</th><th></th><th>Name</th><th>Message</th></tr></thead><tbody id="enquirybody"><tr><td colspan="5" align="center"><img src="/images/ajax_loader.gif" /></td></tr></tbody></table>');
					
	oJobsTable = $('#dataTable').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",							
		"bSort": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayStart": 0,
		"iDisplayLength": 20,				
		"bLengthChange": false,									
		"bProcessing": true,
		"bServerSide": true,	
        "aoColumns": [
			{ sWidth: '10%' },
			{ sWidth: '15%' },
            { sWidth: '5%' },
            { sWidth: '20%' },
			{ sWidth: '50%' }],	
		"sAjaxSource": "?action=search&filter_csv=0&filter_search="+filter_search,
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			$.getJSON( sSource, aoData, function (json) {
					if (json.result === false) {
						$('#enquirybody').html('<tr><td colspan="5" align="center">No results</td></tr>');											
					} else {
						$('#iTotalDisplayRecords').html(json.iTotalDisplayRecords);
					}
					fnCallback(json)
			});
		},
		"fnDrawCallback": function(){
		}
	});
	return false;
}

function csv() {
	var filter_search		= $('#filter_search').val() != 'undefined' ? $('#filter_search').val() : '';
	window.location.href 	= "/company/enquiry/?action=search&filter_csv=1&filter_search="+filter_search;
	return false;
}
</script>
{/literal}
</body>
</html>