<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/*** Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/**
 * Check for login
 */
require_once 'includes/auth.php';
require_once 'class/enquiry.php';
require_once 'class/classified.php';

$enquiryObject	= new class_enquiry();
$classifiedObject	= new class_classified();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$classifiedData = $classifiedObject->getByCode($code);

	if($classifiedData) {
		$smarty->assign('classifiedData', $classifiedData);
	} else {
		header('Location: /classified/');
		exit;
	}
} else {
	header('Location: /classified/');
	exit;
}

/* Setup Pagination. */
if(isset($_GET['action']) && trim($_GET['action']) == 'search') {

	$filter	= array();
	$filter[]	= array('filter_classified' => $classifiedData['classified_code']);
	
	$csv				= 0;
	$enquirys		= array();
	
	$start 		= isset($_REQUEST['iDisplayStart']) ? $_REQUEST['iDisplayStart'] : 0;
	$length 	= isset($_REQUEST['iDisplayLength']) ? $_REQUEST['iDisplayLength'] : 20;
	
	if(isset($_REQUEST['filter_search']) && trim($_REQUEST['filter_search']) != '') $filter[] = array('filter_search' => trim($_REQUEST['filter_search']));
	if(isset($_REQUEST['filter_csv']) && trim($_REQUEST['filter_csv']) != '') { $filter[] = array('filter_csv' => (int)trim($_REQUEST['filter_csv'])); $csv = (int)trim($_REQUEST['filter_csv']); }
	
	$enquiryData = $enquiryObject->getSearch('CLASSIFIED', $start, $length, $filter);
	
	if(!$csv) {		

		if($enquiryData) {
			for($i = 0; $i < count($enquiryData['records']); $i++) {
				$item = $enquiryData['records'][$i];

				$enquirys[$i] = array(
					($item['social_facebook_id'] == '' ? '<img src="/images/no-image.jpg" width="50px" />' : '<a href="http://facebook.com/'.$item['social_facebook_id'].'" target="_blank"><img src="https://graph.facebook.com/'.$item['social_facebook_id'].'/picture?width=300" width="50px" /></a>'),
					$item['enquiry_name'],
					$item['enquiry_email'],
					$item['enquiry_number'],
					$item['enquiry_message']
				);
			}
		}

		if($enquiryData) {
			$response['sEcho'] = $_REQUEST['sEcho'];
			$response['iTotalRecords'] = $enquiryData['displayrecords'];		
			$response['iTotalDisplayRecords'] = $enquiryData['count'];
			$response['aaData']	= $enquirys;
		} else {
			$response['result'] 	= false;
			$response['message']	= 'There are no items to show.';			
		}
		echo json_encode($response);
		die();
	} else {
		
		$row = "Sent, Name, Email, Number, Message\r\n";
		if($enquiryData) {
			for($i = 0; $i < count($enquiryData); $i++) {
				$item = $enquiryData[$i];
				$enquirys[$i] = array(
					$item['enquiry_added'],
					$item['enquiry_name'],
					$item['enquiry_email'],
					$item['enquiry_number'],
					str_replace(',', ' ', $item['enquiry_message'])
				);
			}
			
			foreach ($enquirys as $data) {
				$row .= implode(', ', $data)."\r\n";
			}			
		}
		
		header("Content-Type: text/cvs");
		header("Content-Disposition: attachment; filename=enquiry_report_".date('Y-m-d').".csv");
		Header("Cache-Control: private, must-revalidate" ); // HTTP/1.1
		Header("Pragma: private" ); // HTTP/1.0
	   
		print($row);
		exit;		
	}
}

$smarty->display('classified/enquiry.tpl');
?>