<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header"> 
		<h2 class="content-header-title">Rate companies</h2>
		<ol class="breadcrumb">
		<li><a href="/">Home</a></li>
		<li class="active">Rate Company Comments</li>
		</ol>
	</div>	  
	<div class="row">
        <div class="col-md-12">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-hand-o-up"></i>
                Company Rating List
              </h3>
            </div> <!-- /.portlet-header -->	
			<div class="portlet-content">
			<div class="form-group">
			  <label for="filter_search">Search by company name, member who rated, etc</label>
			  <input type="text" id="filter_search" name="filter_search" class="form-control" value="" />
			</div>	
				<div class="form-group">
				  <label for="filter_item">Select a vet outcome</label>
				  <select id="filter_item" name="filter_item" class="form-control">
					<option value=""> --- Show All --- </option>
					<option value="-1"> Unvetted </option>
					{html_options options=$itemData}
				  </select>
				</div>			
			<div class="form-group">
				<button type="button" onclick="getRecords(); return false;" class="btn btn-primary">Search</button>
				<button type="button" onclick="csv(); return false;" class="btn">Download CSV</button>
			</div>
				<p>There are <span id="result_count" name="result_count" class="success">0</span> records showing. We are displaying <span id="result_display" name="result_display" class="success">0</span> records per page.</p>
				<div id="tableContent" align="center">
					<!-- Start Content Table -->
					<div class="content_table">
						<img src="/images/ajax_loader.gif" />
					 </div>
					 <!-- End Content Table -->	
				</div>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col -->	
  </div> <!-- /.content -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
{literal}
<script type="text/javascript">
$(document).ready(function() {
	getRecords();
});

function getRecords() {
	var html		= '';

	var filter_search	= $('#filter_search').val() != 'undefined' ? $('#filter_search').val() : '';
	var filter_item 		= $('#filter_item').val() != 'undefined' ? $('#filter_item').val() : '';
	
	/* Clear table contants first. */			
	$('#tableContent').html('');
	
	$('#tableContent').html('<table cellpadding="0" cellspacing="0" width="100%" border="0" class="display" id="dataTable"><thead><tr><th>Company</th><th>Member</th><th>Message</th><th></th><th></th></tr></thead><tbody id="memberbody"><tr><td colspan="5" align="center"><img src="/images/ajax_loader.gif" /></td></tr></tbody></table>');
					
	oJobsTable = $('#dataTable').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",							
		"bSort": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayStart": 0,
		"iDisplayLength": 20,				
		"bLengthChange": false,									
		"bProcessing": true,
		"bServerSide": true,	
        "aoColumns": [
			{ sWidth: '10%' },
            { sWidth: '5%' },
            { sWidth: '50%' },
            { sWidth: '5%' },	
			{ sWidth: '5%' }],	
		"sAjaxSource": "?action=search&filter_csv=0&filter_search="+filter_search+"&filter_item="+filter_item,
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			$.getJSON( sSource, aoData, function (json) {
				if (json.result === false) {
					$('#memberbody').html('<tr><td colspan="5" align="center">No results</td></tr>');											
				} else {
					$('#result_count').html(json.iTotalDisplayRecords);
					$('#result_display').html(json.iTotalRecords);
				}
				fnCallback(json)
			});
		},
		"fnDrawCallback": function(){
		}
	});
	return false;
}

function csv() {
	var filter_search		= $('#filter_search').val() != 'undefined' ? $('#filter_search').val() : '';
	var filter_item 			= $('#filter_item').val() != 'undefined' ? $('#filter_item').val() : '';
	window.location.href	= "/company/rate/?action=search&filter_csv=1&filter_search="+filter_search+"&filter_item="+filter_item;
	return false;
}

	function vetCompanyModal(commentcode, companycode, companyname) {
		$('#commentcode').val(commentcode);
		$('#companycode').val(companycode);
		$('.companyname').html(companyname);
		$('#vetCompanyModal').modal('show');
		return false;
	}

	function vet() {

		var commentitemcode	= $('#commentitemcode').val();
		var commentcode			= $('#commentcode').val();
		var itemmessage			= $('#itemmessage').val();
		var companycode			= $('#companycode').val();

		$.ajax({
			type: "GET",
			url: "default.php?itemcode="+commentitemcode+"&itemmessage="+itemmessage+"&companycode="+companycode+"&commentcode="+commentcode,
			dataType: "json",
			success: function(data){
				if(data.result == 1) {
					window.location.href = window.location.href;
				} else {
					$('#vetCompanyModal').modal('hide');
					$.howl ({
					  type: 'info'
					  , title: 'Notification'
					  , content: data.error
					  , sticky: $(this).data ('sticky')
					  , lifetime: 7500
					  , iconCls: $(this).data ('icon')
					});	
				}
			}
		});								

		return false;		
	}
</script>
{/literal}
<div class="modal fade" id="vetCompanyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Vet <span class="companyname"></span></h4>
			</div>
			<div class="modal-body">
				<p>Add a reason for the vetting you are giving this post for <span class="companyname success"></span></p>
				<div class="form-group">  
					<label for="commentitemcode">Status</label>
					<select class="form-control" name="commentitemcode" id="commentitemcode">
						{html_options options=$itemData}
					</select>
				</div>
				<div class="form-group">
					<label for="itemmessage">Reasons</label>
					<textarea class="form-control" rows="5" cols="10" id="itemmessage" name="itemmessage" data-required="true"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:vet();">Vet</button>
				<input type="hidden" id="commentcode" name="commentcode" value=""/>
				<input type="hidden" id="companycode" name="companycode" value=""/>
			</div>
		</div>
	</div>
</div>
</html>