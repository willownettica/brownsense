<?php

require_once '_comm.php';
require_once 'market.php';

//custom account item class as account table abstraction
class class_member extends Zend_Db_Table_Abstract
{
   //declare table variables
    protected $_name 		= 'member';
	protected $_primary 	= 'member_code';
	
	public 			$_comm		= null;
	public 			$_market		= null;
	
	function init()	{
		global $zfsession;
		$this->_comm	= new class_comm();
		$this->_market	= new class_market();
	}
	
	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 

	 public function insert(array $data)
    {
        // add a timestamp
        $data['member_added']		= date('Y-m-d H:i:s');
		$data['member_code']		= $this->createCode();
		$data['member_password']	= $this->createPassword();
		$data['member_hashcode']	= md5($data['member_code'].date('Y-m-d h:i:s'));

		$success = parent::insert($data);	

		if($success) {

			$mailData = $this->getByCode($success);

			if($mailData) {
				
				if($mailData['member_email'] != '') {
					$templateData = $this->_comm->_template->getTemplate('REGISTRATION_ACTIVATE', 'EMAIL');

					if($templateData) {

						$recipient	= array();
						$recipient['recipient_code'] 		= $mailData['member_code'];
						$recipient['recipient_name'] 		= $mailData['member_name'];
						$recipient['recipient_surname'] 	= $mailData['member_surname'];
						$recipient['recipient_cellphone'] 	= $mailData['member_cellphone'];
						$recipient['recipient_type'] 			= 'MEMBER';
						$recipient['recipient_email'] 		= $mailData['member_email'];
						$recipient['recipient_password'] 	= $mailData['member_password'];
						$recipient['recipient_hashcode'] 	= $mailData['member_hashcode'];

						$this->_comm->sendEmail($recipient, $templateData);
					}
				}
				
				if($mailData['member_cellphone'] != '') {
					$templateData = null;
					$templateData = $this->_comm->_template->getTemplate('REGISTRATION_ACTIVATE', 'SMS');

					if($templateData) {
						$recipient	= array();
						$recipient['recipient_code'] 		= $mailData['member_code'];
						$recipient['recipient_name'] 		= $mailData['member_name'];
						$recipient['recipient_surname'] 	= $mailData['member_surname'];
						$recipient['recipient_type'] 			= 'MEMBER';
						$recipient['recipient_cellphone'] 	= $mailData['member_cellphone'];
						$recipient['recipient_email'] 		= $mailData['member_email'];
						$recipient['recipient_password'] 	= $mailData['member_password'];
						$recipient['recipient_hashcode'] 	= $mailData['member_hashcode'];
						
						$this->_comm->sendSMS($recipient, $templateData);
					}
				}
			} else {
				return false;
			}			

			return $success;
		} else {
			return false;
		}
    }

	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where)
    {
        // add a timestamp
        $data['member_updated'] = date('Y-m-d H:i:s');
        
        return parent::update($data, $where);
    }
	
	/**
	 * get job by job member Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code) {

		$tag = $this->_db->select()	
					->from(array('link' => 'link'), array('link_parent_code as member_code', "group_concat(item.item_name separator ',') as tag_name"))
					->joinLeft(array('item' => 'item'), 'item.item_code = link.link_child_code', array())	
					->where("link_parent_type = 'MEMBER' and link_child_type = 'TAG'")
					->where('link_parent_code = ?', $code)
					->group('link_parent_code');	

		$vet = $this->_db->select()	
					->from(array('vet' => 'vet'))
					->joinLeft(array('item' => 'item'), 'item.item_code = vet.item_code', array('item_name', 'item_config_color', 'item_config_class', 'item_config_status'))
					->where("vet_deleted = 0 and item_deleted = 0")
					->where('vet_item_type = ?', 'MEMBER')
					->where('vet_item_code = ?', $code)
					->order('vet_added desc')
					->limit(1);
		
		
		$select = $this->_db->select()
					->from(array('member' => 'member'))
					->joinLeft(array('tag' => $tag), 'tag.member_code = member.member_code', array('tag_name'))
					->joinLeft(array('vet' => $vet), 'vet.vet_item_code = member.member_code')
					->joinLeft(array('media' => 'media'), "media.media_item_code = member.member_code and media.media_item_type = 'MEMBER' and media.media_primary = '1' and media_deleted = 0", array('media_path', 'media_code', 'media_ext'))
					->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = member.areapost_code')
					->joinLeft(array('branch' => 'branch'), 'branch.branch_code = member.branch_code', array('branch_name', 'mailbok_code'))
					->where('member.member_code = ?', $code)
					->group('member.member_code')
					->limit(1);		
	   
	   $result = $this->_db->fetchRow($select);
       return ($result == false) ? false : $result = $result;		
	}
	
	public function getByHash($hash, $status) {

		$select = $this->_db->select()	
					->from(array('member' => 'member'))
					->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = member.areapost_code')
					->joinLeft(array('branch' => 'branch'), 'branch.branch_code = member.branch_code', array('branch_name', 'mailbok_code'))
					->where('branch_deleted = 0 and member_hashcode = ?', $hash)
					->where('member_active = ?', $status)
					->limit(1);		
	   
	   $result = $this->_db->fetchRow($select);
       return ($result == false) ? false : $result = $result;
		
	}
	
	public function login($username, $password) {
		
		$select = $this->_db->select()	
					->from(array('member' => 'member'))
					->where('member_email = ?' , $username)
					->where('member_deleted = 0 and member_password = ?', $password)
					->limit(1);		

	   $result = $this->_db->fetchRow($select);
       return ($result == false) ? false : $result = $result;
		
	}
	
	public function checkChange($code, $facebook) {
		$select = $this->_db->select()	
					->from(array('member' => 'member'))
					->where('member_code = ?' , $code)
					->where('member_deleted = 0 and social_facebook_id = ?', $facebook);
					
	   $result = $this->_db->fetchRow($select);
       return ($result == false) ? false : $result = $result;
	}

	public function getFacebook($id) {

		$select = $this->_db->select()
					->from(array('member' => 'member'))			
					->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = member.areapost_code')
					->joinLeft(array('branch' => 'branch'), 'branch.branch_code = member.branch_code', array('branch_name'))
					->where('member_deleted = 0 and member.social_facebook_id = ?', $id);

		$result = $this->_db->fetchRow($select); 
		return ($result == false) ? false : $result = $result;
	}
	
	public function paginate($filter = array(), $page = 1, $perPage = 10, $listedPages = 10, $scrollingStyle = 'Sliding') {
		
		$where = 'member_deleted = 0';

		if(count($filter) > 0) {
			for($i = 0; $i < count($filter); $i++) { 
				if(isset($filter[$i]['filter_province']) && trim($filter[$i]['filter_province']) != '') {
					$province = $filter[$i]['filter_province'];
					$this->sanitize($province);
					$where .= " and areapostregion.demarcation_id = '$province'";
				} else if(isset($filter[$i]['filter_search']) && trim($filter[$i]['filter_search']) != '') {
					$array = explode(" ",trim($filter[$i]['filter_search']));					
					if(count($array) > 0) {
						$where .= " and (";
						for($i = 0; $i < count($array); $i++) {
							$text = $array[$i];
							$this->sanitize($text);
							$where .= "lower(concat(member.member_name, tag.tag_name)) like lower('%$text%')";
							if(($i+1) != count($array)) {
								$where .= ' or ';
							}
						}
					}
					$where .= ")";
				} else if(isset($filter[$i]['filter_category']) && trim($filter[$i]['filter_category']) != '') {
					$category = $filter[$i]['filter_category'];
					$this->sanitize($category);
					$where .= " and find_in_set('$category', tag.category_code) > 0'";
				}
			}
		}

		$tag = $this->_db->select()
			->from(array('link' => 'link'), array('link_parent_code as member_code'))
			->joinInner(array('item' => 'item'), 'item.item_code = link.link_child_code', array("group_concat(item.item_name separator ',') as tag_name"))
			->joinInner(array('category' => 'item'), 'category.item_code = item.item_parent', array("group_concat(category.item_code separator ',') as category_code"))
			->where("link_parent_type = 'MEMBER' and link_child_type = 'TAG' and category.item_type = 'CATEGORY'")
			->where("link_deleted = 0 and item.item_deleted = 0 and category.item_deleted = 0")
			->group('link_parent_code');	

		$vet = $this->_db->select()
			->from(array('vet' => 'vet'), array('vet_reason', 'vet_item_code'))
			->joinInner(array('item' => 'item'), 'item.item_code = vet.item_code', array('item_name as vet_name', 'item_config_color as vet_color', 'item_config_class as vet_class', 'item_config_status as vet_status'))
			->where("vet_deleted = 0 and item_deleted = 0 and vet_active = 1")
			->where('vet_item_type = ?', 'MEMBER');

		$select = $this->_db->select()
			->from(array('member' => 'member'))
			->joinInner(array('tag' => $tag), 'tag.member_code = member.member_code', array('tag_name', 'category_code'))
			->joinInner(array('vet' => $vet), 'vet.vet_item_code = member.member_code')
			->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = member.areapost_code')
			->joinLeft(array('branch' => 'branch'), 'branch.branch_code = member.branch_code', array('branch_name', 'mailbok_code'))
			->where($where);

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage($perPage);
		$paginator->setPageRange($listedPages);
		$paginator->setDefaultScrollingStyle($scrollingStyle);

		return $paginator;
	}

	public function _random($limit = 20) {

		$select = $this->_db->select()
						->from(array('member' => 'member'))
						->joinLeft(array('branch' => 'branch'), 'branch.branch_code = member.branch_code', array('branch_name', 'mailbok_code'))
						->where('member_active = 1 and member_deleted = 0')
					   ->limit($limit)
					   ->order("RAND()");

	   $result = $this->_db->fetchAll($select);	
        return ($result == false) ? false : $result = $result;					
	}
	/**
	 * get job by job participant Id
 	 * @param string job id
     * @return object
	 */
	public function getByCell($cell, $code = null) {
	
		if($code == null) {
			$select = $this->_db->select()	
						->from(array('member' => 'member'))						
						->where('member_cellphone = ?', $cell)						
						->where('member_deleted = 0')
						->limit(1);
       } else {
			$select = $this->_db->select()	
						->from(array('member' => 'member'))
						->where('member_cellphone = ?', $cell)
						->where('member_code != ?', $code)
						->where('member_deleted = 0')
						->limit(1);		
	   }
	   
		$result = $this->_db->fetchRow($select);
		return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get job by job member Id
 	 * @param string job id
     * @return object
	 */
	public function getByEmail($email, $code = null) {
	
		if($code == null) {
			$select = $this->_db->select()	
						->from(array('member' => 'member'))							
						->where('member_email = ?', $email)
						->where('member_deleted = 0')
						->limit(1);
       } else {
			$select = $this->_db->select()	
						->from(array('member' => 'member'))													
						->where('member_email = ?', $email)
						->where('member_code != ?', $code)
						->where('member_deleted = 0')
						->limit(1);		
	   }

	   $result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getCode($code)
	{
		$select = $this->_db->select()	
						->from(array('member' => 'member'))	
						->where('member_code = ?', $code)
						->limit(1);

	   $result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;					   

	}
	
	function createCode() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '1234567890';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Number[rand(0,$count)];
		}
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Number[rand(0,$count)];
		}		
		/* First check if it exists or not. */
		$itemCheck = $this->getCode($code);
		
		if($itemCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}
		
	function createPassword() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "abcdefghijklmnopqrstuvwxyz1234567890";
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<6;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		return $code;
	}
	
	public function validateTwitter($handler) {
		
		$handler = str_replace('@' , '' , $handler);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, 'https://twitter.com/users/username_available?username='.$handler);
		$result = curl_exec($ch);
		curl_close($ch);

		$obj = json_decode($result);

		if($obj->reason == 'taken') {
			return $handler;
		} else {
			return '';
		}
	}
	
	public function _mailbok($member) {

		$url = "http://www.mailbok.co.za/webservice/paticipant";

		$data = array(
			'username' 					=> 'admin@brownsense.co.za',
			'password' 					=> 'sakile',
			'subscription' 				=> $member['mailbok_code'],
			'subscriber_name' 			=> $member['member_name'].' '.$member['member_surname'],
			'subscriber_email' 			=> $member['member_email'],
			'subscriber_cellphone'	=> $member['member_cellphone']
		);
		
		$content = '';
		foreach($data as $key=>$value) { $content .= $key.'='.$value.'&'; }

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

		$json_response = curl_exec($curl);

		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		return json_decode($json_response, true);
	}
	
	public function validateEmail($string) {
		if(!filter_var($string, FILTER_VALIDATE_EMAIL)) {
			return '';
		} else {
			return trim($string);
		}
	}
	
	public function validateNumber($string) {
		if(preg_match('/^0[0-9]{9}$/', $this->onlyNumber(trim($string)))) {
			return $this->onlyNumber(trim($string));
		} else {
			return '';
		}
	}
	
	public function validateDate($string) {
		if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $string)) {
			if(date('Y-m-d', strtotime($string)) != $string) {
				return '';
			} else {
				return $string;
			}
		} else {
			return '';
		}
	}
	
	public function onlyNumber($string) {
		/* Remove some weird charactors that windows dont like. */
		$string = strtolower($string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("�", "", $string);	
		$string = str_replace('___' , '' , $string);
		$string = str_replace('__' , '' , $string);	 
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("�", "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("�", "", $string);	
		$string = str_replace("–", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("�", "", $string);	
		$string = str_replace("#", "", $string);	
		$string = str_replace("$", "", $string);	
		$string = str_replace("@", "", $string);	
		$string = str_replace("!", "", $string);	
		$string = str_replace("&", "", $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(':' , '' , $string);		
		$string = str_replace('[' , '' , $string);		
		$string = str_replace(']' , '' , $string);		
		$string = str_replace('|' , '' , $string);		
		$string = str_replace('\\' , '' , $string);		
		$string = str_replace('%' , '' , $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);	
		$string = str_replace('-' , '' , $string);	
		$string = str_replace('+27' , '0' , $string);	
		$string = str_replace('(0)' , '' , $string);	
		
		$string = preg_replace('/^00/', '0', $string);
		$string = preg_replace('/^27/', '0', $string);
		
		$string = preg_replace('!\s+!',"", strip_tags($string));
		
		return $string;
	}	
}
?>