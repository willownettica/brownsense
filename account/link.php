<?php 
/* Add this on all pages on top. */
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/auth.php';

/* objects. */
require_once 'class/member.php';

$memberObject = new class_member();

if(count($_POST) > 0 && isset($_POST['member_login'])) {

	$errorLoginArray	= array();
	$data 					= array();
	$formValid			= true;
	$success				= NULL;
	
	if(isset($_POST['member_email']) && trim($_POST['member_email']) == '') {
		$errorLoginArray[] = 'Your email address is required.';
		$formValid = false;		
	}

	if(isset($_POST['member_password']) && trim($_POST['member_password']) == '') {
		$errorLoginArray[] = 'Your password is required.';
		$formValid = false;		
	}
	
	if(count($errorLoginArray) == 0 && $formValid == true) {
		
		$memberLoginData	= $memberObject->login(trim($_POST['member_email']), trim($_POST['member_password']));
		
		$message					= '';
		
		if($memberLoginData) {
			/* Successfull log in. */
			$data = array();
			$data['member_name'] 		= $zfsession->memberData['member_name'];
			$data['social_facebook_id']	= $zfsession->memberData['social_facebook_id'];

			$where		= $memberObject->getAdapter()->quoteInto('member_code = ?', $memberLoginData['member_code']);
			$memberObject->update($data, $where);
			/* Delete new account if all went well. */
			$newData = $memberObject->checkChange($memberLoginData['member_code'], $zfsession->memberData['social_facebook_id']);

			if($newData) {
				/* Successfull log in. */
				$data = array();
				$data['member_deleted'] 	= 1;

				$where		= $memberObject->getAdapter()->quoteInto('member_code = ?', $zfsession->memberData['member_code']);
				$memberObject->update($data, $where);
				/* Delete and change session id. */
				$zfsession->memberData	= $newData;
				$zfsession->identity 			= $newData['member_code'];

				header('Location: /');
				exit;

			} else {
				$errorLoginArray[] = 'Could not link your account, if this continues please contact administrator via our facebook group/page or email us on <a href="mailto:support@brownsense.co.za">support@brownsense.co.za</a>';
			}
		} else {
			$errorLoginArray[] = 'The email and/or password you entered could not be found.';
		}
	}
}

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<link rel="stylesheet" href="/css/jquery-ui-1.12.1.css">
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/account/"><?php echo $zfsession->memberData['member_name']; ?></a></li>
						<li><a href="#">Link current facebook account with previous email account</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-8">
				<h3 class="heading-1"><span>Account</span></h3>	
				<ul class="nav nav-tabs">
					<li><a href="/account/">Person Information</a></li>
					<li><a href="/account/tag.php">My skillset</a></li>					
					<li><a href="/account/document.php">My Documents</a></li>
					<li class="active"><a href="/account/link.php">Link with old Email Account</a></li>
				</ul>
				<p style="clear: both;">&nbsp;&nbsp;&nbsp;&nbsp;</p>
				<p>If you have registered with us before using your email address in the old brownsense website, you can link your email profile with your facebook profile by giving us your log in credentials below</p>
				<form class="post-comment-form"action="/account/link.php" method="POST" >		
					<br />
					<?php if(isset($errorLoginArray) && count($errorLoginArray) != 0) { ?>
						<div class="alert alert-danger reg_member_fail" style="">
							<strong>Oh snap! There were some issue with your credentials</strong>
							<br>
							<p>
							<?php
							for($i = 0; $i < count($errorLoginArray); $i++) {
							 echo $errorLoginArray[$i].'<br />'; 
							} 
							 ?>								
							</p>
						</div>
					<?php } ?>						
					<div class="row">
						<div class="col-md-4">
							<label>Your Email</label>
							<input type="text" id="member_email" name="member_email" value="" placeholder="Email address you used to register" />
						</div>
						<div class="col-md-4">
							<label>Password</label>
							<input type="password" id="member_password" name="member_password" value="" placeholder="Password emailed after account activation" />
						</div>											
					</div>		
					<button type="submit">Link facebook account with email account</button>
					<br /><br />
					<input type="hidden" value="1" id="member_login" name="member_login" />
				</form>			
			</div>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
</body>
</html>
