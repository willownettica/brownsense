<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

//custom account item class as account table abstraction
class class_classified extends Zend_Db_Table_Abstract
{
	//declare table variables
    protected $_name 		= 'classified';
	protected $_primary 	= 'classified_code';
	
	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 
	 public function insert(array $data) {
        // add a timestamp
        $data['classified_added'] = date('Y-m-d H:i:s');        
		$data['classified_code']	= $this->createCode();

		$vet = $this->makePending($data['classified_code']);
		
		if($vet) {
			return parent::insert($data);
		} else {
			return false;
		}
    }
	
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean 
	 */
    public function update(array $data, $where)
    {
        // add a timestamp
        $data['classified_updated'] = date('Y-m-d H:i:s');
        
        return parent::update($data, $where);
    }
	
	/**
	 * get job by job classified Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code) {	

		$vet = $this->_db->select()	
				->from(array('vet' => 'vet'), array('vet_reason', 'vet_item_code'))
				->joinLeft(array('item' => 'item'), 'item.item_code = vet.item_code', array('item_name as vet_name', 'item_config_color as vet_color', 'item_config_class as vet_class', 'item_config_status as vet_status'))
				->where("vet_deleted = 0 and item_deleted = 0")
				->where('vet_item_type = ?', 'CLASSIFIED')
				->where('vet_item_code = ?', $code)
				->order('vet_added desc')
				->limit(1);

			$select = $this->_db->select()	
				->from(array('classified' => 'classified'))
				->joinLeft(array('member' => 'member'), 'member.member_code = classified.member_code')
				->joinLeft(array('areapost' => 'areapost'), "areapost.areapost_code = classified.areapost_code", array('areapost_name'))
				->joinLeft(array('areapostregion' => 'areapostregion'), "areapostregion.areapostregion_code = areapost.areapostregion_code", array('demarcation_name'))
				->joinLeft(array('type' => 'item'), "type.item_code = classified.type_code and type.item_type = 'CLASSIFIED'", array('item_name as type_name'))
				->joinLeft(array('section' => 'item'), "section.item_code = classified.section_code", array('item_name as section_name'))
				->joinLeft(array('category' => 'item'), "category.item_code = section.item_parent", array('item_name as category_name', 'item_code as  category_code'))
				->joinLeft(array('vet' => $vet), 'vet.vet_item_code = classified.classified_code')
				->where('member_deleted = 0 and classified_deleted = 0 and type.item_deleted = 0 and section.item_deleted = 0 and category.item_deleted = 0')
				->where('classified_code = ?', $code)
				->limit(1);

		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}
	
	public function makePending($code) {

			require_once 'class/item.php';
			require_once 'class/vet.php';
			
			$itemObject 	= new class_item();
			$vetObject 	= new class_vet();
			
			$itemData = $itemObject->getPrimary('VET', 'CLASSIFIED');

			if($itemData) {
			
				$data 	= array();				
				$data['item_code']			= $itemData['item_code'];		
				$data['vet_reason']		= 'Pending approval.';
				$data['vet_item_type']	= 'CLASSIFIED';	
				$data['vet_item_code']	= $code;	

				return $vetObject->insert($data);
			}
			
			return false;
	}
	
	public function getSearch($start, $length, $filter = array()) {
		$where 		= 'classified_deleted = 0';
		$eventcode	= '';
		$csv 			= 0;

		if(count($filter) > 0) {	
			for($i = 0; $i < count($filter); $i++) { 
				if(isset($filter[$i]['filter_search']) && trim($filter[$i]['filter_search']) != '') {
					$array = explode(" ",trim($filter[$i]['filter_search']));					
					if(count($array) > 0) {
						$where .= " and (";
						for($s = 0; $s < count($array); $s++) {
							$text = $array[$s];
							$this->sanitize($text);
							$where .= "lower(classified_subject) like lower('%$text%')";
							if(($s+1) != count($array)) {
								$where .= ' or ';
							}
						}
					}
					$where .= ")";
				} else if(isset($filter[$i]['filter_csv']) && (int)trim($filter[$i]['filter_csv']) == 1) {
					$csv = 1;
				} else if(isset($filter[$i]['filter_item']) && trim($filter[$i]['filter_item']) != '') {
					$itemcode = $filter[$i]['filter_item'];
					$this->sanitize($itemcode);
					if((int)$itemcode == 1) {
						$where .= " and vet.item_code = '' ";
					} else {
						$where .= " and vet.item_code = '$itemcode' ";
					}
					
				}
			}
		}

		$vet = $this->_db->select()
			->from(array('vet' => 'vet'), array('vet_item_code'))
			->joinLeft(array('item' => 'item'), 'item.item_code = vet.item_code', array('item_code', 'item_name', 'item_config_color', 'item_config_class', 'item_config_status'))
			->where("vet_deleted = 0 and item_deleted = 0 and vet_active = 1")
			->where('vet_item_type = ?', 'CLASSIFIED');

		$select = $this->_db->select()
			->from(array('classified' => 'classified'))
			->joinLeft(array('areapost' => 'areapost'), "areapost.areapost_code = classified.areapost_code", array('areapost_name'))
			->joinLeft(array('areapostregion' => 'areapostregion'), "areapostregion.areapostregion_code = areapost.areapostregion_code", array('demarcation_name'))
			->joinLeft(array('member' => 'member'), 'member.member_code = classified.member_code', array('member_name', 'social_facebook_id'))
			->joinLeft(array('type' => 'item'), "type.item_code = classified.type_code and type.item_type = 'CLASSIFIED'", array('item_name as type_name'))
			->joinLeft(array('section' => 'item'), "section.item_code = classified.section_code", array('item_name as section_name'))
			->joinLeft(array('category' => 'item'), "category.item_code = section.item_parent", array('item_name as category_name', 'item_code as  category_code'))
			->joinLeft(array('vet' => $vet), 'vet.vet_item_code = classified.classified_code')
			->where('member_deleted = 0 and classified_deleted = 0 and type.item_deleted = 0 and section.item_deleted = 0 and category.item_deleted = 0')
			->where($where);

		if($csv) {
			$result = $this->_db->fetchAll($select);
			return ($result == false) ? false : $result = $result;	
		} else {
			$result_count = $this->_db->fetchRow("select count(*) as query_count from ($select) as query");
			$result = $this->_db->fetchAll($select . " limit $start, $length");
			return ($result === false) ? false : $result = array('count'=>$result_count['query_count'],'displayrecords'=>count($result),'records'=>$result);	
		}
	}
	
	public function csv($search = '') {
	
		$vet = $this->_db->select()	
					->from(array('vet' => 'vet'), array('vet_item_code'))
					->joinLeft(array('item' => 'item'), 'item.item_code = vet.item_code', array('item_name as vet_name', 'item_config_color as vet_color', 'item_config_class as vet_class', 'item_config_status as vet_status'))	
					->where("vet_deleted = 0 and item_deleted = 0 and vet_active = 1")
					->where('vet_item_type = ?', 'CLASSIFIED')
					->order('vet_added desc');	

		if($search == '') {					
			$select = $this->_db->select()	
				->from(array('classified' => 'classified'))
				->joinLeft(array('areapost' => 'areapost'), "areapost.areapost_code = classified.areapost_code", array('areapost_name'))
				->joinLeft(array('areapostregion' => 'areapostregion'), "areapostregion.areapostregion_code = areapost.areapostregion_code", array('demarcation_name'))			
				->joinLeft(array('member' => 'member'), 'member.member_code = classified.member_code')
				->joinLeft(array('type' => 'item'), "type.item_code = classified.type_code and type.item_type = 'CLASSIFIED'", array('item_name as type_name'))
				->joinLeft(array('section' => 'item'), "section.item_code = classified.section_code", array('item_name as section_name'))
				->joinLeft(array('category' => 'item'), "category.item_code = section.item_parent", array('item_name as category_name', 'item_code as  category_code'))
				->joinLeft(array('vet' => $vet), 'vet.vet_item_code = classified.classified_code')
				->where('member_deleted = 0 and classified_deleted = 0 and type.item_deleted = 0 and section.item_deleted = 0 and category.item_deleted = 0')
				->order('classified_added desc');
		} else {
			$select = $this->_db->select()	
				->from(array('classified' => 'classified'))
				->joinLeft(array('member' => 'member'), 'member.member_code = classified.member_code')
				->joinLeft(array('areapost' => 'areapost'), "areapost.areapost_code = classified.areapost_code", array('areapost_name'))
				->joinLeft(array('areapostregion' => 'areapostregion'), "areapostregion.areapostregion_code = areapost.areapostregion_code", array('demarcation_name'))			
				->joinLeft(array('type' => 'item'), "type.item_code = classified.type_code and type.item_type = 'CLASSIFIED'", array('item_name as type_name'))
				->joinLeft(array('section' => 'item'), "section.item_code = classified.section_code", array('item_name as section_name'))
				->joinLeft(array('category' => 'item'), "category.item_code = section.item_parent", array('item_name as category_name', 'item_code as  category_code'))
				->joinLeft(array('vet' => $vet), 'vet.vet_item_code = classified.classified_code')
				->where('member_deleted = 0 and classified_deleted = 0 and type.item_deleted = 0 and section.item_deleted = 0 and category.item_deleted = 0')
				->where("lower(concat_ws(member_name, item_name, classified_subject)) like lower(?)", "%$search%")
				->order("LOCATE('$search', concat(member_name, item_name, classified_subject))");
		}

		$result = $this->_db->fetchAll($select);
		return ($result == false) ? false : $result = $result;		
	}
	
	public function validateDate($string) {
		if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $string)) {
			if(date('Y-m-d', strtotime($string)) != $string) {
				return '';
			} else {
				return $string;
			}
		} else {
			return '';
		}
	}

	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getCode($reference) {
		$select = $this->_db->select()	
				->from(array('classified' => 'classified'))	
				->where('classified_code = ?', $reference)
				->limit(1);

	   $result = $this->_db->fetchRow($select);	
        return ($result == false) ? false : $result = $result;					   
	}

	function humanTime ($date) {
		$time = strtotime($date);
		$time = time() - $time; // to get the time since that moment
		$time = ($time<1)? 1 : $time;
		$tokens = array (
			31536000 => 'year',
			2592000 => 'month',
			604800 => 'week',
			86400 => 'day',
			3600 => 'hour',
			60 => 'minute',
			1 => 'second'
		);

		foreach ($tokens as $unit => $text) {
			if ($time < $unit) continue;
			$numberOfUnits = floor($time / $unit);
			return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s ago':' ago');
		}
	}
		
	function createCode() {
		/* New code. */
		$code = "";
		 
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '1234567890';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<1;$i++){
			$code .= $Number[rand(0,$count)];
		}
		 
		/* Last alphabet. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)]; 
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Number[rand(0,$count)];
		}	
		
		/* First check if it exists or not. */
		$itemCheck = $this->getCode($code);
		
		if($itemCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}
	
    function sanitize(&$string) 
    {        
        $string = preg_replace("/[^a-zA-Z0-9_]+/", "", $string);
    }
	
    function sanitizeArray(&$array) 
    {        
		for($i = 0; $i < count($array); $i++) {
			$array[$i] = preg_replace("/[^a-zA-Z0-9_]+/", "", $array[$i]);
		}
    } 			
}
?>