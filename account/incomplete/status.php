<?php 
/* Add this on all pages on top. */
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/iauth.php';

if((int)trim($zfsession->memberData['item_config_status']) == 1) {
	/* Activated, completed but not approved yet. */
	header('Location: /');
	exit;
}

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<link rel="stylesheet" href="/css/jquery-ui-1.12.1.css">
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/account/">Account</a></li>
						<li><a href="#"><?php echo $zfsession->memberData['member_name']; ?></a></li>
						<li><a href="/account/incomplete/">Completed Profile</a></li>
						<li><a href="#">Account vetting results</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8">		
				<form class="post-comment-form"action="/account/incomplete/activate.php" method="POST" >	
					<p>Below are your vetting results as done by our administrators to make sure that you are a proper brownie.</p><br />
					<?php if((int)$zfsession->memberData['item_config_status'] == 0 && trim($zfsession->memberData['item_config_color']) == '') { ?>
						<div class="alert alert-warning reg_member_fail" style="">
							<strong>Oh snap!</strong>
							<br>
							<p>
									Your profile has not yet been vetted by our administrators, you will receive an email address when the vetting is completed. Please be patient as we have a lot of registered people to vet.	
							</p>
						</div>
					<?php } else { ?>
						<div class="<?php echo $zfsession->memberData['item_config_class']; ?>" style="">
							<strong><?php echo $zfsession->memberData['item_name']; ?></strong>
							<br>
							<p>
								<?php echo $zfsession->memberData['vet_reason']; ?>
							</p>
						</div>
					<?php } ?>
				</form>			
			</div>
			<aside class="col-md-4 col-sm-4">
			
			<div class="side-widget margin-bottom-30">
				<h3 class="heading-1"><span>Facebook profile picture</span></h3>
				<img src="https://graph.facebook.com/<?php echo $zfsession->memberData['social_facebook_id']; ?>/picture?width=300">
				</div>
			</aside>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>

<?php require_once 'includes/javascript.php'; ?>
</body>
</html>
