<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* standard config include. */
require_once 'config/database.php';

require_once 'Zend/Session.php';

$zfsession = new Zend_Session_Namespace('LOGIN_MEMBER');		

// added in v4.0.0
require_once 'Facebook/autoload.php';
require_once 'class/member.php';

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

// init app with app id and secret
FacebookSession::setDefaultApplication('1764604877195312','ae5496bc108428336d9041647e3876db');

// login helper with redirect_uri
if($_SERVER['HTTP_HOST'] == 'brownsense.dev') {
	$helper = new FacebookRedirectLoginHelper('http://brownsense.dev/login.php' );
} else if($_SERVER['HTTP_HOST'] == 'dev.brownsense.co.za'){
	$helper = new FacebookRedirectLoginHelper('http://dev.brownsense.co.za/login.php' );
} else {
	$helper = new FacebookRedirectLoginHelper('http://www.brownsense.co.za/login.php' );
}

try {
  $session = $helper->getSessionFromRedirect();
} catch( FacebookRequestException $ex ) {
  // When Facebook returns an error
} catch( Exception $ex ) {
  // When validation fails or other local issues
}

// see if we have a session
if ( isset( $session ) ) {
	// graph api request for user data
	$request 			= new FacebookRequest( $session, 'GET', '/me' );
	$memberObject	= new class_member();
	
	$response = $request->execute(); 
	// get response
	$graphObject = $response->getGraphObject();

	$memberData = $memberObject->getFacebook($graphObject->getProperty('id'));

	$data 	= array();				 
	$data['member_name']		= $graphObject->getProperty('name');
	$data['social_facebook_id']	= $graphObject->getProperty('id');	
	
	if($memberData) {
		$where		= $memberObject->getAdapter()->quoteInto('member_code = ?', $memberData['member_code']);
		$success	= $memberObject->update($data, $where);
		$success	= $memberData['member_code'];
	} else {

		$data['member_policy']		= 0;
		$data['member_email']		= $graphObject->getProperty('email');
		$data['member_active']		= $memberObject->validateEmail($data['member_email']) == '' ? 0 : 1;

		$success = $memberObject->insert($data);
	}
	
	$zfsession->identity	= $success;
	
	header("Location: /");
} else {
  $loginUrl = $helper->getLoginUrl();
 header("Location: ".$loginUrl);
}
?>