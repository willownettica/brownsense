<?php

//custom account enquiry class as account table abstraction
class class_role extends Zend_Db_Table_Abstract
{
   //declare table variables
    protected $_name 			= 'role';
	protected $_primary 		= 'role_code';
	
	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 
	 public function insert(array $data) {
        // add a timestamp
        $data['role_added']	= date('Y-m-d H:i:s');
		$data['role_code']		= $this->createCode();

		return parent::insert($data);	
    }
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where) {
        // add a timestamp
        $data['role_updated']	= date('Y-m-d H:i:s');		
        return parent::update($data, $where);
    }
	/**
	 * get job by job role Id
 	 * @param string job id
     * @return object
	 */
	public function getRoles($code, $type) {
		
		$select = $this->_db->select()	
					->from(array('role' => 'role'))
					->joinLeft(array('admin' => 'admin'), 'admin.admin_code = role.admin_code')
					->where('role_type = ?', $type)
					->where('admin.admin_code = ?', $code)
					->where('role_deleted = 0 and admin_deleted = 0')
					->order('role_value');

		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}
	/**
	 * get job by job role Id
 	 * @param string job id
     * @return object
	 */
	public function listRoles($code, $type) {
		
		$select = $this->_db->select()	
					->from(array('role' => 'role'), array('role_value'))
					->joinLeft(array('admin' => 'admin'), 'admin.admin_code = role.admin_code', array())
					->where('role_type = ?', $type)
					->where('admin.admin_code = ?', $code)
					->where('role_deleted = 0 and admin_deleted = 0');

		$result = $this->_db->fetchCol($select);
        return ($result == false) ? false : $result = $result;
	}	
	/**
	 * get job by job role Id
 	 * @param string job id
     * @return object
	 */
	public function checkExists($code, $type, $value) {
		$select = $this->_db->select()	
					->from(array('role' => 'role'))
					->joinLeft(array('admin' => 'admin'), 'admin.admin_code = role.admin_code')
					->where('role_type = ?', $type)
					->where('role_value = ?', $value)
					->where('admin.admin_code = ?', $code)
					->where('role_deleted = 0 and admin_deleted = 0');

		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}

	/**
	 * get job by job role Id
 	 * @param string job id
     * @return object
	 */
	public function getCode($code) {

		$select = $this->_db->select()	
					->from(array('role' => 'role'))
					->where('role_code = ?', $code);

		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}
	
	function createCode() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '1234567890';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Number[rand(0,$count)];
		}
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<4;$i++){
			$code .= $Number[rand(0,$count)];
		}		
		/* First check if it exists or not. */
		$roleCheck = $this->getCode($code);
		
		if($roleCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}
	
    function sanitize(&$string) { $string = preg_replace("/[^a-zA-Z0-9_]+/", "", $string);}
	
    function sanitizeArray(&$array) 
    {        
		for($i = 0; $i < count($array); $i++) {
			$array[$i] = preg_replace("/[^a-zA-Z0-9_]+/", "", $array[$i]);
		}
    }		
}
?>
