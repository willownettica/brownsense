<?php 
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/auth.php'; 

require_once 'class/classified.php';
require_once 'class/item.php';

$classifiedObject	= new class_classified();
$itemObject 			= new class_item();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$classifiedData = $classifiedObject->getByMember($code);

	if(!$classifiedData) {
		header('Location: /account/classified/');
		exit;
	}
}

/* Check posted data. */
if(isset($_GET['getsection'])) {
	
	$categorycode	= trim($_GET['category_code']);
	$html 				= '';

	if($categorycode  != '') {
		
		$sectionData = $itemObject->getChildren($categorycode, 'TAG');
		
		if($sectionData) {
			for($i = 0; $i < count($sectionData); $i++) {
				$selected = isset($classifiedData) && $classifiedData['section_code'] == $sectionData[$i]['item_code'] ? 'selected' : '';
				$html .= "<option value='".$sectionData[$i]['item_code']."' $selected >".$sectionData[$i]['item_name']."</option>";
			}
		} else {
			$html .= "<option value=''>No sections for this category</option>";
		}
	} else {
		$html .= "<option value=''>No sections for this category</option>";
	}
	
	echo $html;
	exit;
}

if(isset($_POST['save_classified'])) {

	$classifiederror		= array();
	
	if(isset($_POST['classified_subject']) && trim($_POST['classified_subject']) == '') {
		$classifiederror[] = 'Subject is required'; 
	}
	
	if(isset($_POST['type_code']) && trim($_POST['type_code']) == '') {
		$classifiederror[] = 'Type is required'; 
	}
	
	if(isset($_POST['category_code']) && trim($_POST['category_code']) == '') {
		$classifiederror[] = 'Category is required.';
	} else {
		if(isset($_POST['section_code']) && trim($_POST['section_code']) == '') {
			$classifiederror[] = 'Section is required.';
		}
	}

	if(isset($_POST['areapost_code']) && trim($_POST['areapost_code']) == '') {
		$classifiederror[] = 'Area location is required'; 
	}
	
	if(isset($_POST['classified_message']) && trim($_POST['classified_message']) == '') {
		$classifiederror[] = 'Message is required';
	}
	
	if(isset($_POST['classified_expire']) && trim($_POST['classified_expire']) == '') {
		$classifiederror[] = 'Expiry date is required';
	} else {
		if($classifiedObject->validateDate(trim($_POST['classified_expire'])) == '') {
			$classifiederror[] = 'Valid expiry date is required';
		}	
	}

	if(count($classifiederror) == 0) {

		$data 									= array();				
		$data['member_code']			= $zfsession->identity;	
		$data['classified_subject']		= trim($_POST['classified_subject']);	
		$data['type_code']					= trim($_POST['type_code']);	
		$data['section_code']				= trim($_POST['section_code']);
		$data['areapost_code']			= trim($_POST['areapost_code']);	
		$data['classified_message']	= trim($_POST['classified_message']);
		$data['classified_expire']		= trim($_POST['classified_expire']);

		if(isset($classifiedData)) {
			$where		= array();
			$where[]	= $classifiedObject->getAdapter()->quoteInto('member_code = ?', $zfsession->identity);
			$where[]	= $classifiedObject->getAdapter()->quoteInto('classified_code = ?', $classifiedData['classified_code']);
			$success	= $classifiedObject->update($data, $where);
			$success	= $classifiedData['classified_code'];
		} else {
			$success = $classifiedObject->insert($data);
		}
		header('Location: /account/classified/media.php?code='.$success);
		exit;
	}
}

$itemPairs = $itemObject->selectParents('CLASSIFIED');
$categoryPairs = $itemObject->selectType('CATEGORYCLASS');

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<link href="/css/jquery-ui-1.12.1.css" rel="stylesheet" />
	<link href="/css/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css"  />
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/account/"><?php echo $zfsession->memberData['member_name']; ?></a></li>
						<li><a href="/account/classified/">My Classifieds</a></li>
						<?php if(isset($classifiedData)) { ?>
						<li><a href="/account/classified/details.php?code<?php echo $classifiedData['classified_code']; ?>"><?php echo $classifiedData['classified_subject']; ?></a></li>
						<li><a href="#">Details</a></li>						
						<?php } else { ?>
						<li><a href="#">Add classified</a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">			
		<div class="row">
			<div class="col-md-12 col-sm-8">
				<h3 class="heading-1"><span><?php if(isset($classifiedData)) { echo "Edit Classified"; } else { ?>Add classified<?php } ?></span></h3>				
				<?php if(isset($classifiedData)) { ?>				
				<p class="text_brown"><?php echo $classifiedData['classified_subject']; ?></p><br />
				<?php if(isset($classifiedData)) {  ?>
					<div class="<?php echo $classifiedData['vet_class']; ?>">
						<strong><?php echo $classifiedData['vet_name']; ?></strong><br /><p><?php echo $classifiedData['vet_reason']; ?></p>
					</div>
				<?php } ?>
				<ul class="nav nav-tabs">
					<li class="active"><a href="/account/classified/details.php?code=<?php echo $classifiedData['classified_code']; ?>">Details</a></li>
					<li><a href="/account/classified/media.php?code=<?php echo $classifiedData['classified_code']; ?>">Images</a></li>
				</ul>
				<p style="clear: both;">&nbsp;&nbsp;&nbsp;&nbsp;</p>	
				<?php } ?>
				<form class="post-comment-form"action="/account/classified/details.php<?php if(isset($classifiedData)) { echo '?code='.$classifiedData['classified_code']; } ?>" method="POST" >
					<div class="row">
						<div class="col-md-12">
							<label class="text_red">Subject</label>
							<input type="text" id="classified_subject" name="classified_subject" value="<?php if(isset($classifiedData['classified_subject'])) { echo $classifiedData['classified_subject']; } ?>" placeholder="Add a subject" />
						</div>
					</div>						
					<div class="row">
						<div class="col-md-4">
							<label class="text_red">Type</label>
							<select name="type_code" id="type_code">
								<option value=""> --------------- </option>
								<?php 
									foreach($itemPairs as $key => $value) {
										echo "<option value='$key' ".(isset($classifiedData) && $key == $classifiedData['type_code'] ? 'selected' : '').">$value</option>";
									}
								?>
							</select>
						</div>
						<div class="col-md-4">
							<label class="text_red">Category</label>
							<select name="category_code" id="category_code">
								<option value=""> --------------- </option>
								<?php 
									foreach($categoryPairs as $key => $value) {
										echo "<option value='$key' ".(isset($classifiedData) && $key == $classifiedData['category_code'] ? 'selected' : '').">$value</option>";
									}
								?>
							</select>
						</div>
						<div class="col-md-4">
							<label>Section</label>
							<select id="section_code" name="section_code" class="form-control" data-required="true" >
							<option value=""> --- Select category first --- </option>					
							</select>
						</div>							
					</div>
					<div class="row">
						<div class="col-md-4">
							<label>Expirey date</label>
							<input type="text" name="classified_expire" id="classified_expire" value="<?php if(isset($classifiedData['classified_expire'])) { echo $classifiedData['classified_expire']; } ?>" placeholder="Expiry date of the post format: YYYY-MM-DD" />
						</div>					
						<div class="col-md-8">
							<label class="text_red">Company location, city or town.</label>
							<input type="text" id="areapost_name" name="areapost_name" data-required="true" value="<?php if(isset($classifiedData['areapost_name'])) { echo $classifiedData['areapost_name']; } ?>" placeholder="Business location or area, add first 3 letters and select from the drop down." />
							<input type="hidden" id="areapost_code" name="areapost_code" value="<?php if(isset($classifiedData['areapost_code'])) { echo $classifiedData['areapost_code']; } ?>" />
						</div>
					</div>						
					<div class="row">
						<div class="col-md-12">
							<label>Message</label>
							<textarea name="classified_message" id="classified_message" rows="50" class="wysihtml5" style="height: 500px !important;"><?php if(isset($classifiedData['classified_message'])) { echo $classifiedData['classified_message']; } ?></textarea>
						</div>	
					</div>	
					<br />					
					<?php if(isset($classifiederror)) { ?>
					<div class="alert alert-danger">
						<strong>Oh snap!</strong><br /><p><?php echo implode("<br />",$classifiederror); ?></p>
					</div>				
					<?php } ?>
					<button type="submit">Save Company</button>
					<input type="hidden" id="save_classified" name="save_classified" value="1" />
					<br /><br />
				</form>
			</div>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="/library/javascript/wysihtml5-0.3.js"></script>
<script type="text/javascript" src="/library/javascript/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" language="javascript">
	$(document).ready(function(){
		$( "#areapost_name" ).autocomplete({
			source: "/feeds/areapost.php",
			minLength: 2,
			select: function( event, ui ) {
				if(ui.item.id == '') {
					$('#areapost_name').html('');
					$('#areapost_code').val('');					
				} else {
					$('#areapost_name').html(ui.item.value);
					$('#areapost_code').val(ui.item.id);	
				}
				$('#areapost_name').val('');										
			}
		});
		getSection();
		
		$( "#category_code" ).change(function() {
		  getSection();
		});
		$('.wysihtml5').wysihtml5();	
	});

	function getSection() {
		$.ajax({
			type: "GET",
			url: "?getsection=1<?php if(isset($classifiedData['classified_code'])) { echo '&code='.$classifiedData['classified_code']; } ?>",
			data: "category_code="+$('#category_code :selected').val(),
			dataType: "html",
			success: function(data){				
				$('#section_code').html(data);
			}
		});								
		return false;
	}
</script>
</body>
</html>
