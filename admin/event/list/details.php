<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';
/* Check for login */
require_once 'includes/auth.php';
/* objects. */
require_once 'class/event.php';
require_once 'class/branch.php';

$eventObject 	= new class_event();
$branchObject	= new class_branch();

if(isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);

	$eventData = $eventObject->getByCode($code);

	if($eventData) {
		$smarty->assign('eventData', $eventData);
	} else {
		header('Location: /event/list/');
		exit;
	}
}

/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;
	
	if(isset($_POST['event_name']) && trim($_POST['event_name']) == '') {
		$errorArray['event_name'] = 'Name is required';
		$formValid = false;		
	}
	
	if(isset($_POST['branch_code']) && trim($_POST['branch_code']) == '') {
		$errorArray['branch_code'] = 'Branch is required';
		$formValid = false;		
	}

	if(isset($_POST['event_date_start']) && trim($_POST['event_date_start']) == '') {
		$errorArray['event_date_start'] = 'Start date is required';
		$formValid = false;		
	}

	if(isset($_POST['event_date_end']) && trim($_POST['event_date_end']) == '') {
		$errorArray['event_date_end'] = 'End date is required';
		$formValid = false;		
	}
	
	if(isset($_POST['areapost_code']) && trim($_POST['areapost_code']) == '') {
		$errorArray['areapost_code'] = 'Area is required';
		$formValid = false;		
	}

	if(count($errorArray) == 0 && $formValid == true) {

		$data 	= array();				
		$data['areapost_code']			= trim($_POST['areapost_code']);		
		$data['event_name']				= trim($_POST['event_name']);
		$data['event_page']				= trim($_POST['event_page']);
		$data['event_address']			= trim($_POST['event_address']);
		$data['event_date_start']		= trim($_POST['event_date_start']);
		$data['event_date_end']			= trim($_POST['event_date_end']);
		$data['branch_code']				= trim($_POST['branch_code']);
		$data['event_bank_name']		= trim($_POST['event_bank_name']);
		$data['event_bank_account']	= trim($_POST['event_bank_account']);
		$data['event_bank_type']		= trim($_POST['event_bank_type']);
		$data['event_bank_code']		= trim($_POST['event_bank_code']);

		if(isset($eventData)) {
			$where		= $eventObject->getAdapter()->quoteInto('event_code = ?', $eventData['event_code']);
			$success	= $eventObject->update($data, $where);
			$success	= $eventData['event_code'];
		} else {
			$success = $eventObject->insert($data);
		}

		header('Location: /event/list/map.php?code='.$success);	
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

/* Setup Pagination. */
$branchData = $branchObject->pairs(array(4));
if($branchData) $smarty->assign('branchData', $branchData);

$smarty->display('event/list/details.tpl');

?>