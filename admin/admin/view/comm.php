<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/**
 * Standard includes
 */
require_once 'config/database.php';
require_once 'config/smarty.php';

require_once 'includes/auth.php';

/* objects. */
require_once 'class/admin.php';
require_once 'class/_comm.php';

$adminObject	= new class_admin();
$commObject 		= new class_comm();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$tempData = $adminObject->getByCode($code);

	if($tempData) {
		$smarty->assign('tempData', $tempData);

		$commData = $commObject->getByRecipient('ADMIN', $code);

		if($commData) {
			$smarty->assign('commData', $commData);
		}	

	} else {
		header('Location: /admin/view/');
		exit;	
	}
} else {
	header('Location: /admin/view/');
	exit;	
}

/* Display the template */	
$smarty->display('admin/view/comm.tpl');
?>