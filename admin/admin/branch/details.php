<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/* Check for login */
require_once 'includes/auth.php';

/* objects. */
require_once 'class/branch.php';

$branchObject = new class_branch();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$branchData = $branchObject->getByCode($code);

	if($branchData) {
		$smarty->assign('branchData', $branchData);
	} else {
		header('Location: /admin/branch/');
		exit;
	}
}

/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;
	
	if(isset($_POST['branch_name']) && trim($_POST['branch_name']) == '') {
		$errorArray['branch_name'] = 'Name is required';
		$formValid = false;		
	}
	
	if(isset($_POST['branch_level']) && trim($_POST['branch_level']) == '') {
		$errorArray['branch_level'] = 'Level is required';
		$formValid = false;		
	}
	
	if(count($errorArray) == 0 && $formValid == true) {
	
		$data 	= array();						
		$data['branch_name']	= trim($_POST['branch_name']);	
		$data['parent_code']		= trim($_POST['parent_code']);	
		$data['branch_level']		= trim($_POST['branch_level']);	
		$data['mailbok_code']	= trim($_POST['mailbok_code']);	
		
		if(isset($branchData)) {
			$where		= $branchObject->getAdapter()->quoteInto('branch_code = ?', $branchData['branch_code']);
			$success	= $branchObject->update($data, $where);
			$success	= $branchData['branch_code'];
		} else {
			$success = $branchObject->insert($data);
		}

		header('Location: /admin/branch/admin.php?code='.$success);	
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

$code = isset($branchData) ? $branchData['branch_code'] : null;
$branchPairs = $branchObject->pairs(array(1, 2, 3), $code);

if($branchPairs) $smarty->assign('branchPairs', $branchPairs);

$smarty->display('admin/branch/details.tpl');

?>