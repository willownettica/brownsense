<?php 

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/auth.php'; 

/* objects. */
require_once 'class/company.php';
require_once 'class/event.php';
require_once 'class/areapost.php';
require_once 'class/item.php';

$companyObject 	= new class_company();
$eventObject 		= new class_event();
$areapostObject 	= new class_areapost();
$itemObject 			= new class_item();

$where			= array();
$parameter	= "";

if(isset($_REQUEST['filter_search']) && trim($_REQUEST['filter_search']) != '') $where[] = array('filter_search' => trim($_REQUEST['filter_search']));
if(isset($_REQUEST['filter_province']) && trim($_REQUEST['filter_province']) != '') $where[] = array('filter_province' => trim($_REQUEST['filter_province']));
if(isset($_REQUEST['filter_category']) && trim($_REQUEST['filter_category']) != '') $where[] = array('filter_category' => trim($_REQUEST['filter_category']));

if(count($where) > 0) {
	for($i = 0; $i < count($where); $i++) { 
		if(isset($where[$i]['filter_search']) && trim($where[$i]['filter_search']) != '') {
			$parameter .= "&filter_search=".$where[$i]['filter_search'];
		} else if(isset($where[$i]['filter_province']) && trim($where[$i]['filter_province']) != '') {
			$parameter .= "&filter_province=".$where[$i]['filter_province'];
		} else if(isset($where[$i]['filter_category']) && trim($where[$i]['filter_category']) != '') {
			$parameter .= "&filter_category=".$where[$i]['filter_category'];
		}
	}
}

$page 		= isset($_GET['page']) 		? $_GET['page'] 		: 1;
$perPage	= isset($_GET['perPage'])	? $_GET['perPage']	: 10;

$companyData = $companyObject->paginate($where, $page, $perPage);

$companyItems = $companyData->getCurrentItems();

if($companyItems) {
	$paginator = $companyData->setView()->getPages();
}

$eventData		= $eventObject->upcoming(3);
$provinceData	= $areapostObject->getProvinces();
$itemData			= $itemObject->selectType('CATEGORY');

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
	<link rel="stylesheet" href="/css/jquery-ui-1.12.1.css">
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/companies/">Companies</a></li>						
						<li><a href="#">Search companies</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-7 dual-posts padding-bottom-30">
				<h2>Companies</h2><br />
				<p>Search by company name, service or products and/or location of the business. The number of items found so far is <span class="text_brown"><?php echo $paginator->totalItemCount; ?></span>. <?php echo ((int)$paginator->totalItemCount != 0 ? 'Search has <span class="text_brown">'.$paginator->pageCount.'</span> pages'.($paginator->pageCount > 1 ? ', with <span class="text_brown">'.$paginator->itemCountPerPage.'</span> companies per page.' : '.') : '') ; ?></span></p><p>
				<?php if(isset($_REQUEST['filter_search']) && $_REQUEST['filter_search'] != '') { ?>You are searching for companies with: <span class="normal-font text_brown"><?php echo $_REQUEST['filter_search']; ?></span>. <?php } ?>
				<?php if(isset($_REQUEST['filter_province']) && $_REQUEST['filter_province'] != '') { ?> Filtering by province <span class="normal-font text_brown"><?php echo $provinceData[$_REQUEST['filter_province']]; ?></span>. <?php } ?>
				<?php if(isset($_REQUEST['filter_category']) && $_REQUEST['filter_category'] != '') { ?>Filtering by category <span class="normal-font text_brown"><?php echo $itemData[$_REQUEST['filter_category']]; ?></span>.<?php } ?></p>					
				<p></p>
				<br />
				<?php if($paginator->pageCount > 1) { ?>
				<!-- PAGINATION -->
				<ul class="pagination">
					<?php foreach($paginator->pagesInRange as $page) { ?>
					<li <?php echo ((int)$page == (int)$paginator->current ? 'class="active"' : ''); ?>><a href="/companies/?page=<?php echo $page ?>&<?php echo $parameter; ?>"><?php echo $page ?></a></li>
					<?php } ?>
				</ul>
				<?php } ?>					
				<hr class="l4">
				<?php foreach($companyItems as $item) { ?>
				<div class="row">
					<div class="layout_3--item">						
						<div class="col-md-3 col-sm-4">
							<div class="thumb">
								<div class="icon-24 video2"></div>
								<a href="/company/<?php echo $item['company_code']; ?>">
									<?php if($item['media_code'] != '') { ?>									
									<img src="<?php echo $item['media_path'].'tmb_'.$item['media_code'].$item['media_ext']; ?>" class="img-responsive" alt="<?php echo $item['company_name']; ?>" title="<?php echo $item['company_name']; ?>" />
									<?php } else { ?>
									<img src="/images/no-image.jpg" class="img-responsive" alt="<?php echo $item['company_name']; ?>" title="<?php echo $item['company_name']; ?>" />
									<?php } ?>
								</a>
							</div>
						</div>
						<div class="col-md-9 col-sm-8">
							<?php if((int)$item['item_config_paid'] == 1) { ?>
							<div class="ribbon-wrapper-green">
								<div class="ribbon-green">Vetted</div>
							</div>					
							<?php } ?>
							<span class="cat "><?php echo $item['areapost_name']; ?></span>
							<h4><a href="/company/<?php echo $item['company_code']; ?>"><?php echo $item['company_name']; ?></a></h4>
							<p><?php echo $item['tag_name']; ?></p>
							<?php if($item['company_registration_number'] != '') { ?>Registration Number: <b><?php echo $item['company_registration_number'].'</b>'; } ?>
							<div class="meta">
								<span class="author"><?php echo $item['member_name'].' '.$item['member_surname']; ?></span>
								<span class="date"><?php echo date('d F Y', strtotime($item['company_added'])); ?></span>
								<span class="comments"><?php echo $item['comment_count']; ?> Comments</span>
								<span class="views"><?php echo $item['rate_avg']; ?> / 10 rating</span>
							</div>
						</div>
					</div>
				</div>
				<hr class="l4">
				<?php } ?>
				<?php if($paginator->pageCount > 1) { ?>
				<!-- PAGINATION -->
				<ul class="pagination">
					<?php foreach($paginator->pagesInRange as $page) { ?>
					<li <?php echo ((int)$page == (int)$paginator->current ? 'class="active"' : ''); ?>><a href="/companies/?page=<?php echo $page ?>&<?php echo $parameter; ?>"><?php echo $page ?></a></li>
					<?php } ?>
				</ul>
				<?php } ?>			
			</div>
			<!-- // CATEGORY -->
			<aside class="col-md-4 col-sm-4">			
				<div class="side-widget margin-bottom-30">
					<h3 class="heading-1"><span>Filter</span></h3>			
					<form class="post-comment-form"action="/companies/" method="GET">		
						<div class="row">
							<div class="col-md-12">
								<label>Search</label>
								<input type="text" id="filter_search" name="filter_search" value="<?php echo (isset($_REQUEST['filter_search']) && $_REQUEST['filter_search'] != '' ? $_REQUEST['filter_search'] : ''); ?>" style="margin-bottom: 2px !important;" />
								<span class="tiny_explain">Search by name, service, or even by member.</span>
							</div>
						</div>
						<br />
						<div class="row">
							<div class="col-md-12">
								<label>Province</label>
								<select id="filter_province" name="filter_province" style="margin-bottom: 2px !important;">
									<option value=""> ------------------- </option>
									<?php 
										while ($value = current($provinceData)) {
											echo '<option '.(isset($_REQUEST['filter_province']) &&  $_REQUEST['filter_province'] == key($provinceData) ? 'selected' : '').' value="'.key($provinceData).'">'.$value.'</option>';
											next($provinceData);
										}
									?>  
								</select>
								<span class="tiny_explain">Select province that you want companies from.</span>
							</div>		
						</div>
						<br />
						<div class="row">
							<div class="col-md-12">
								<label>Category</label>
								<select id="filter_category" name="filter_category" style="margin-bottom: 2px !important;">
									<option value=""> ------------------- </option>
									<?php 
										while ($value = current($itemData)) {
											echo '<option '.(isset($_REQUEST['filter_category']) &&  $_REQUEST['filter_category'] == key($itemData) ? 'selected' : '').' value="'.key($itemData).'">'.$value.'</option>';
											next($itemData);
										}
									?>  
								</select>
								<span class="tiny_explain">Select categories that you want companies from.</span>
							</div>		
						</div>
						<br />						
						<button type="submit">Update Filter</button>&nbsp;&nbsp;&nbsp;&nbsp;
						<button type="button" onclick="window.location.href = '/companies/'; return false;">Clear Search</button>
					</form>
				</div>
			</aside>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
</body>
</html>
