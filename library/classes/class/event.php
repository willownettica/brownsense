<?php

require_once "Zend/Paginator.php";  

//custom account item class as account table abstraction
class class_event extends Zend_Db_Table_Abstract
{
   //declare table variables
    protected $_name 	= 'event';
	public $_primary		= 'event_code';
	
	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 
	 public function insert(array $data) {
        // add a timestamp
        $data['event_added'] 	= date('Y-m-d H:i:s');
		$data['event_code'] 	= $this->createCode();
		
		return parent::insert($data);
    }
	
	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where) {
        // add a timestamp        
		$data['event_updated'] = date('Y-m-d H:i:s');
		
        return parent::update($data, $where);
    }

	/**
	 * get job by job event Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code)
	{		
		$select = $this->_db->select()	
					->from(array('event' => 'event'))
					->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = event.areapost_code')
					->joinLeft(array('areapostregion' => 'areapostregion'), 'areapostregion.areapostregion_code = areapost.areapostregion_code')
					->joinLeft(array('media' => 'media'), "media.media_item_code = event.event_code and media.media_item_type = 'EVENT' and media.media_primary = '1' and media_deleted = 0", array('media_path', 'media_code', 'media_ext'))
					->joinLeft(array('branch' => 'branch'), 'branch.branch_code = event.branch_code')					
					->where('branch_deleted = 0 and event_deleted = 0')
					->where('event.event_code = ?', $code)
					->limit(1);

		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}

	public function paginate($filter = array(), $page = 1, $perPage = 10, $listedPages = 10, $scrollingStyle = 'Sliding') {
		
		$where = 'event_deleted = 0';

		if(count($filter) > 0) {	
			for($i = 0; $i < count($filter); $i++) { 
				if(isset($filter[$i]['filter_province']) && trim($filter[$i]['filter_province']) != '') {
					$province = $filter[$i]['filter_province'];
					$this->sanitize($province);
					$where .= " and areapostregion.demarcation_id = '$province'";
				} else if(isset($filter[$i]['filter_search']) && trim($filter[$i]['filter_search']) != '') {
					$array = explode(" ",trim($filter[$i]['filter_search']));					
					if(count($array) > 0) {
						$where .= " and (";
						for($i = 0; $i < count($array); $i++) {
							$text = $array[$i];
							$this->sanitize($text);
							$where .= "lower(concat(event.event_name)) like lower('%$text%')";

							if(($i+1) != count($array)) {
								$where .= ' or ';
							}
						}
					}
					$where .= ")";
				}
			}
		}

		$select = $this->_db->select()	
			->from(array('event' => 'event'))
			->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = event.areapost_code', array('areapost_city', 'areapost_name'))
			->joinLeft(array('areapostregion' => 'areapostregion'), 'areapostregion.areapostregion_code = areapost.areapostregion_code')
			->joinLeft(array('media' => 'media'), "media.media_item_code = event.event_code and media.media_item_type = 'EVENT' and media.media_primary = '1' and media_deleted = 0", array('media_path', 'media_code', 'media_ext'))					
			->joinLeft(array('branch' => 'branch'), 'branch.branch_code = event.branch_code', array('branch_name'))					
			->where('branch_deleted = 0 and event_deleted = 0 and media_path != \'\'')			
			->where($where);
		
		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage($perPage);
		$paginator->setPageRange($listedPages);
		$paginator->setDefaultScrollingStyle($scrollingStyle);

		return $paginator;
	}

	/**
	 * get job by job event Id
 	 * @param string job id
     * @return object
	 */
	public function latest($limit = 3) {
		$select = $this->_db->select()	
					->from(array('event' => 'event'))
					->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = event.areapost_code')
					->joinLeft(array('areapostregion' => 'areapostregion'), 'areapostregion.areapostregion_code = areapost.areapostregion_code')
					->joinLeft(array('media' => 'media'), "media.media_item_code = event.event_code and media.media_item_type = 'EVENT' and media.media_primary = '1' and media_deleted = 0", array('media_path', 'media_code', 'media_ext'))						
					->joinLeft(array('branch' => 'branch'), 'branch.branch_code = event.branch_code')					
					->where('branch_deleted = 0 and event_deleted = 0')
					->order('event_added desc')
					->limit($limit);

		$result = $this->_db->fetchAll($select);
		return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get job by job event Id
 	 * @param string job id
     * @return object
	 */
	public function upcoming($limit = 3, $code = null) {
	
		if($code == null) {
			$select = $this->_db->select()	
				->from(array('event' => 'event'))
				->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = event.areapost_code')
				->joinLeft(array('areapostregion' => 'areapostregion'), 'areapostregion.areapostregion_code = areapost.areapostregion_code')
				->joinLeft(array('media' => 'media'), "media.media_item_code = event.event_code and media.media_item_type = 'EVENT' and media.media_primary = '1' and media_deleted = 0", array('media_path', 'media_code', 'media_ext'))								
				->joinLeft(array('branch' => 'branch'), 'branch.branch_code = event.branch_code')					
				->where('branch_deleted = 0 and event_deleted = 0')
				->order('event_date_start desc')
				->limit($limit);
		} else {
			$select = $this->_db->select()	
				->from(array('event' => 'event'))
				->joinLeft(array('item' => 'item'), 'item.item_code = event.item_code')
				->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = event.areapost_code')
				->joinLeft(array('areapostregion' => 'areapostregion'), 'areapostregion.areapostregion_code = areapost.areapostregion_code')
				->joinLeft(array('media' => 'media'), "media.media_item_code = event.event_code and media.media_item_type = 'EVENT' and media.media_primary = '1' and media_deleted = 0", array('media_path', 'media_code', 'media_ext'))								
				->joinLeft(array('branch' => 'branch'), 'branch.branch_code = event.branch_code')					
				->where('branch_deleted = 0 and event_deleted = 0')
				->where('event_code != ?', $code)
				->order('event_date_start desc')
				->limit($limit);		
		}

		$result = $this->_db->fetchAll($select);
		return ($result == false) ? false : $result = $result;
	}

	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getCode($code)
	{
		$select = $this->_db->select()	
					->from(array('event' => 'event'))	
					   ->where('event_code = ?', $code)
					   ->limit(1);

	   $result = $this->_db->fetchRow($select);	
        return ($result == false) ? false : $result = $result;					   
	}

	function createCode() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '1234567890';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<1;$i++){
			$code .= $Number[rand(0,$count)];
		}
		
		/* Last alphabet. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Number[rand(0,$count)];
		}	
		
		/* First check if it exists or not. */
		$itemCheck = $this->getCode($code);
		
		if($itemCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}
	
	public function clean($string) {
		/* Remove some weird charactors that windows dont like. */
		$string = strtolower($string);
		$string = str_replace('__' , ' ' , $string);
		$string = str_replace("é", "e", $string);
		$string = str_replace("è", "e", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", " ", $string);
		$string = str_replace("\\", " ", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", " ", $string);
		$string = str_replace(".", " ", $string);
		$string = str_replace("ë", "e", $string);	
		$string = str_replace('___' , ' ' , $string);
		$string = str_replace('__' , ' ' , $string);	
		$string = str_replace('__' , ' ' , $string);
		$string = str_replace("é", "e", $string);
		$string = str_replace("è", "e", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", " ", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("ë", "e", $string);	
		$string = str_replace("â€“", "", $string);	
		$string = str_replace("â", "a", $string);	
		$string = str_replace("€", "", $string);	
		$string = str_replace("“", "", $string);	
		$string = str_replace("#", "", $string);	
		$string = str_replace("$", "", $string);	
		$string = str_replace("@", "", $string);	
		$string = str_replace("!", "", $string);	
		$string = str_replace("&", "", $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(':' , '' , $string);		
		$string = str_replace('[' , '' , $string);		
		$string = str_replace(']' , '' , $string);		
		$string = str_replace('|' , '' , $string);		
		$string = str_replace('\\' , '' , $string);		
		$string = str_replace('%' , '' , $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace('__' , ' ' , $string);	
		$string = str_replace('-' , ' ' , $string);	

		return $string;
	}
	
    function sanitize(&$string) { $string = preg_replace("/[^a-zA-Z0-9_]+/", "", $string);}
	
    function sanitizeArray(&$array) 
    {        
		for($i = 0; $i < count($array); $i++) {
			$array[$i] = preg_replace("/[^a-zA-Z0-9_]+/", "", $array[$i]);
		}
    } 		
}
?>