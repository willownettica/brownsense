<?php

require_once '_comm.php';

//custom account item class as account table abstraction
class class_member extends Zend_Db_Table_Abstract
{
   //declare table variables
    protected $_name 		= 'member';
	protected $_primary 	= 'member_code';
	
	public 			$_comm		= null;
	
	function init()	{
		global $zfsession;
		$this->_comm	= new class_comm();
	}
	
	/**
	 * Insert the database record
	 * example: $table->insert($data);
	 * @param array $data
     * @return boolean
	 */ 

	 public function insert(array $data)
    {
        // add a timestamp
        $data['member_added']		= date('Y-m-d H:i:s');
		$data['member_code']		= $this->createCode();
		$data['member_password']	= $this->createPassword();
		$data['member_hashcode']	= md5($data['member_code'].date('Y-m-d h:i:s'));
		
		$success = parent::insert($data);	

		if($success) {

			$mailData = $this->getByCode($success);

			if($mailData) {
				
				if($mailData['member_email'] != '') {
					$templateData = $this->_comm->_template->getTemplate('REGISTRATION_ACTIVATE', 'EMAIL');

					if($templateData) {

						$recipient	= array();
						$recipient['recipient_code'] 		= $mailData['member_code'];
						$recipient['recipient_name'] 		= $mailData['member_name'];
						$recipient['recipient_surname'] 	= $mailData['member_surname'];
						$recipient['recipient_cellphone'] 	= $mailData['member_cellphone'];
						$recipient['recipient_type'] 			= 'MEMBER';
						$recipient['recipient_email'] 		= $mailData['member_email'];
						$recipient['recipient_password'] 	= $mailData['member_password'];
						$recipient['recipient_hashcode'] 	= $mailData['member_hashcode'];

						$this->_comm->sendEmail($recipient, $templateData);
					}
				}
				
				if($mailData['member_cellphone'] != '') {
					$templateData = null;
					$templateData = $this->_comm->_template->getTemplate('REGISTRATION_ACTIVATE', 'SMS');

					if($templateData) {
						$recipient	= array();
						$recipient['recipient_code'] 		= $mailData['member_code'];
						$recipient['recipient_name'] 		= $mailData['member_name'];
						$recipient['recipient_surname'] 	= $mailData['member_surname'];
						$recipient['recipient_type'] 			= 'MEMBER';
						$recipient['recipient_cellphone'] 	= $mailData['member_cellphone'];
						$recipient['recipient_email'] 		= $mailData['member_email'];
						$recipient['recipient_password'] 	= $mailData['member_password'];
						$recipient['recipient_hashcode'] 	= $mailData['member_hashcode'];
						
						$this->_comm->sendSMS($recipient, $templateData);
					}
				}
			} else {
				return false;
			}
			
			return $success;
		} else {
			return false;
		}
    }

	/**
	 * Update the database record
	 * example: $table->update($data, $where);
	 * @param query string $where
	 * @param array $data
     * @return boolean
	 */
    public function update(array $data, $where)
    {
        // add a timestamp
        $data['member_updated'] = date('Y-m-d H:i:s');
        
        return parent::update($data, $where);
    }
	
	/**
	 * get job by job member Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code) {

		$vet = $this->_db->select()
					->from(array('vet' => 'vet'))
					->joinLeft(array('item' => 'item'), 'item.item_code = vet.item_code', array('item_name', 'item_config_color', 'item_config_class', 'item_config_status'))
					->where("vet_deleted = 0 and item_deleted = 0")
					->where('vet_item_type = ?', 'MEMBER')
					->where('vet_item_code = ?', $code)
					->order('vet_added desc')
					->group('vet_item_code');	
					
		$select = $this->_db->select()	
					->from(array('member' => 'member'))
					->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = member.areapost_code')
					->joinLeft(array('branch' => 'branch'), 'branch.branch_code = member.branch_code', array('branch_name'))
					->joinLeft(array('vet' => $vet), 'vet.vet_item_code = member.member_code')					
					->where('branch_deleted = 0 and member_code = ?', $code)
					->limit(1);		
	   
	   $result = $this->_db->fetchRow($select);
       return ($result == false) ? false : $result = $result;		
	}	
	
	public function getSearch($start, $length, $filter = array()) {
	
		$where 		= 'member_deleted = 0';
		$csv 			= 0;

		if(count($filter) > 0) {	
			for($i = 0; $i < count($filter); $i++) { 
				if(isset($filter[$i]['filter_search']) && trim($filter[$i]['filter_search']) != '') {
					$array = explode(" ",trim($filter[$i]['filter_search']));					
					if(count($array) > 0) {
						$where .= " and (";
						for($s = 0; $s < count($array); $s++) {
							$text = $array[$s];
							$this->sanitize($text);
							$where .= "lower(concat_ws(member.member_name, ' ', member.member_surname, ' ', member.member_email, member.member_cellphone, branch.branch_name)) like lower('%$text%')";
							if(($s+1) != count($array)) {
								$where .= ' or ';
							}
						}
					}
					$where .= ")";
				} else if(isset($filter[$i]['filter_csv']) && (int)trim($filter[$i]['filter_csv']) == 1) {
					$csv = 1;
				} else if(isset($filter[$i]['filter_item']) && trim($filter[$i]['filter_item']) != '') {
					$itemcode = $filter[$i]['filter_item'];
					$this->sanitize($itemcode);
					if((int)$itemcode == 1) {
						$where .= " and vet.item_code = '' ";
					} else {
						$where .= " and vet.item_code = '$itemcode' ";
					}
					
				}
			}
		}

		$select = $this->_db->select()	
			->from(array('member' => 'member'))
			->joinInner(array('branch' => 'branch'), 'branch.branch_code = member.branch_code', array('branch_name'))
			->joinLeft(array('vet' => 'vet'), "vet_item_type = 'MEMBER' and vet_item_code = member_code and vet_deleted = 0  and vet_active = 1", array('vet_item_code', 'vet_reason', 'item_code'))
			->joinLeft(array('item' => 'item'), 'item.item_code = vet.item_code and item_deleted = 0', array('item_name', 'item_config_color', 'item_config_class', 'item_config_status'))
			->where('branch_deleted = 0 and member_deleted = 0')
			->where($where)
			->order('member.member_added desc');

		if($csv) {
			$result = $this->_db->fetchAll($select);
			return ($result == false) ? false : $result = $result;	
		} else {
			$result_count = $this->_db->fetchRow("select count(*) as query_count from ($select) as query");
			$result = $this->_db->fetchAll($select . " limit $start, $length");
			return ($result === false) ? false : $result = array('count'=>$result_count['query_count'],'displayrecords'=>count($result),'records'=>$result);	
		}
	}
	
	public function search($query, $limit = 20) {

		$vet = $this->_db->select()	
				->from(array('vet' => 'vet'))
				->joinLeft(array('item' => 'item'), 'item.item_code = vet.item_code', array('item_name', 'item_config_color', 'item_config_class', 'item_config_status'))	
				->where("vet_deleted = 0 and item_deleted = 0 and vet_active = 1")
				->where('vet_item_type = ?', 'MEMBER')
				->order('vet_added desc');	
					
		$select = $this->_db->select()
				->from(array('member' => 'member'), array('member.member_code', 'member.member_email', 'member.member_cellphone', 'member.member_name'))	
				->joinLeft(array('vet' => $vet), 'vet.vet_item_code = member.member_code')								
				->where("item_config_status = 1 and member_deleted = 0 and concat(member.member_name, member.member_surname, member.member_email, member.member_cellphone) like lower(?)", "%$query%")
				->limit($limit)
				->order("LOCATE('$query', concat_ws(member.member_name, member.member_email, member.member_cellphone) )");

	   $result = $this->_db->fetchAll($select);	
        return ($result == false) ? false : $result = $result;					

	}
	
	/**
	 * get job by job participant Id
 	 * @param string job id
     * @return object
	 */
	public function getByCell($cell, $code = null) {
	
		if($code == null) {
			$select = $this->_db->select()	
				->from(array('member' => 'member'))						
				->where('member_cellphone = ?', $cell)						
				->where('member_deleted = 0')
				->limit(1);
       } else {
			$select = $this->_db->select()	
				->from(array('member' => 'member'))
				->where('member_cellphone = ?', $cell)
				->where('member_code != ?', $code)
				->where('member_deleted = 0')
				->limit(1);		
	   }
	   
		$result = $this->_db->fetchRow($select);
		return ($result == false) ? false : $result = $result;
	}
	
	/**
	 * get job by job member Id
 	 * @param string job id
     * @return object
	 */
	public function getByEmail($email, $code = null) {
	
		if($code == null) {
			$select = $this->_db->select()	
						->from(array('member' => 'member'))							
						->where('member_email = ?', $email)
						->where('member_deleted = 0')
						->limit(1);
       } else {
			$select = $this->_db->select()	
						->from(array('member' => 'member'))													
						->where('member_email = ?', $email)
						->where('member_code != ?', $code)
						->where('member_deleted = 0')
						->limit(1);		
	   }

	   $result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}
	/**
	 * get domain by domain Account Id
 	 * @param string domain id
     * @return object
	 */
	public function getCode($code)
	{
		$select = $this->_db->select()	
						->from(array('member' => 'member'))	
						->where('member_code = ?', $code)
						->limit(1);

	   $result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;					   

	}
	
	function createCode() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '1234567890';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Number[rand(0,$count)];
		}
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Number[rand(0,$count)];
		}		
		/* First check if it exists or not. */
		$itemCheck = $this->getCode($code);
		
		if($itemCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}
		
	function createPassword() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "abcdefghijklmnopqrstuvwxyz1234567890";
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<6;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		return $code;
	}
	
	public function validateEmail($string) {
		if(!filter_var($string, FILTER_VALIDATE_EMAIL)) {
			return '';
		} else {
			return trim($string);
		}
	}
	
	public function validateNumber($string) {
		if(preg_match('/^0[0-9]{9}$/', $this->onlyNumber(trim($string)))) {
			return $this->onlyNumber(trim($string));
		} else {
			return '';
		}
	}
	
	public function validateDate($string) {
		if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $string)) {
			if(date('Y-m-d', strtotime($string)) != $string) {
				return '';
			} else {
				return $string;
			}
		} else {
			return '';
		}
	}

	function buildCSVRow($l_data) {
		$l_csv = "";
		foreach ($l_data as $l_key => $l_val) {
			if ($l_key != "pointstype") {
				if ($l_csv) {
					$l_csv .= ",";
				}
				$l_csv .= $l_val;
			}
		}
		$l_csv .= "\r\n";
		return $l_csv;
	}
		
	public function onlyNumber($string) {
		/* Remove some weird charactors that windows dont like. */
		$string = strtolower($string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace("é", "", $string);
		$string = str_replace("è", "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("ë", "", $string);	
		$string = str_replace('___' , '' , $string);
		$string = str_replace('__' , '' , $string);	 
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);
		$string = str_replace("é", "", $string);
		$string = str_replace("è", "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("\\", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace("(", "", $string);
		$string = str_replace(")", "", $string);
		$string = str_replace("-", "", $string);
		$string = str_replace(".", "", $string);
		$string = str_replace("ë", "", $string);	
		$string = str_replace("â€“", "", $string);	
		$string = str_replace("â", "", $string);	
		$string = str_replace("€", "", $string);	
		$string = str_replace("“", "", $string);	
		$string = str_replace("#", "", $string);	
		$string = str_replace("$", "", $string);	
		$string = str_replace("@", "", $string);	
		$string = str_replace("!", "", $string);	
		$string = str_replace("&", "", $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(':' , '' , $string);		
		$string = str_replace('[' , '' , $string);		
		$string = str_replace(']' , '' , $string);		
		$string = str_replace('|' , '' , $string);		
		$string = str_replace('\\' , '' , $string);		
		$string = str_replace('%' , '' , $string);	
		$string = str_replace(';' , '' , $string);		
		$string = str_replace(' ' , '' , $string);
		$string = str_replace('__' , '' , $string);
		$string = str_replace(' ' , '' , $string);	
		$string = str_replace('-' , '' , $string);	
		$string = str_replace('+27' , '0' , $string);	
		$string = str_replace('(0)' , '' , $string);	
		
		$string = preg_replace('/^00/', '0', $string);
		$string = preg_replace('/^27/', '0', $string);
		
		$string = preg_replace('!\s+!',"", strip_tags($string));
		
		return $string;
	}	

    function sanitize(&$string) { $string = preg_replace("/[^a-zA-Z0-9_]+/", "", $string);}
	
    function sanitizeArray(&$array) 
    {        
		for($i = 0; $i < count($array); $i++) {
			$array[$i] = preg_replace("/[^a-zA-Z0-9_]+/", "", $array[$i]);
		}
    }		
}
?>