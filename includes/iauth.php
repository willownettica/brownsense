<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* standard config include. */
require_once 'config/database.php';

//include the Zend class for Authentification
require_once 'Zend/Session.php';
 
// Set up the namespace
$zfsession	= new Zend_Session_Namespace('LOGIN_MEMBER');

// Check if logged in, otherwise redirect
if (!isset($zfsession->identity) || is_null($zfsession->identity) || $zfsession->identity == '') {
	/* Redirect else where. */
	header('Location: /');
	exit;
} else {

	require_once 'class/member.php';

	$memberObject 	= new class_member();
	//get user details by username
	$tempData = $memberObject->getByCode($zfsession->identity);

	if($tempData) {
		/* Administrator selected campaign. */
		$zfsession->memberData 	= $tempData;
		$zfsession->identity 			= $tempData['member_code'];
	} else {
		header('Location: /');
		exit;	
	}
}

/* Check if the account has been completed, active and approved by administrators. */
if(
	($zfsession->memberData['branch_code'] != '' && $zfsession->memberData['member_email'] != '' && $zfsession->memberData['member_cellphone'] != '' && $zfsession->memberData['member_birthdate'] != '')
	&& 
	((int)trim($zfsession->memberData['member_active']) == 1)
	&&
	((int)trim($zfsession->memberData['item_config_status']) == 1)
	&&
	(trim($zfsession->memberData['member_linked']) != '')	
) {
	header('Location: /');
	exit;
}
global $zfsession;

?>