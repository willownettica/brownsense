<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/*** Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/**
 * Check for login
 */
require_once 'includes/auth.php'; 
require_once 'class/_social.php';
 
$socialObject = new class_social();
 
 if(isset($_GET['delete_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success					= NULL;
	$code						= trim($_GET['delete_code']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {
		$data	= array();
		$data['social_deleted'] = 1;
		
		$where 	= array();
		$where[] 	= $socialObject->getAdapter()->quoteInto('social_code = ?', $code);
		$success	= $socialObject->update($data, $where);	
		
		if(is_numeric($success) && $success > 0) {
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	 
	echo json_encode($errorArray);
	exit;
}

/* Setup Pagination. */
if(isset($_GET['action']) && trim($_GET['action']) == 'search') {

	$search 	= isset($_REQUEST['search']) && trim($_REQUEST['search']) != '' ? trim($_REQUEST['search']) : null;
	$start 		= isset($_REQUEST['iDisplayStart']) ? $_REQUEST['iDisplayStart'] : 0;
	$length 	= isset($_REQUEST['iDisplayLength']) ? $_REQUEST['iDisplayLength'] : 20;
	$csv 		= isset($_REQUEST['csv']) && (int)trim($_REQUEST['csv']) == 0 ? false : true;
	$socials 	= array();

	$socialData = $socialObject->getSearch($start, $length, $search);

	if($socialData) {
		for($i = 0; $i < count($socialData['records']); $i++) {
			$item = $socialData['records'][$i]; 
			$socials[$i] = array(
				(trim($item['media_code']) != '' ? '<img src="http://www.brownsense.co.za'.$item['media_path'].'tny_'.$item['media_code'].$item['media_ext'].'" width="90px" />' : 'No image'),
				'<span class="'.(trim($item['_social_sent_result']) == '' ? '' : ((int)$item['_social_sent_result'] == 1 ? 'success' : 'error')).'">'.$item['_social_message'].'</span>',
				(trim($item['_social_sent_type']) == '' ? 'Standard Post' : trim($item['_social_sent_type'])), 
				trim($item['_social_sent_date']),
				((int)$item['_social_sent_result'] == 1 ? 'Successful Post' : trim($item['_social_sent_output'])),
				((int)$item['_social_sent_result'] == 1 ? 'Already sent' : '<button value="Delete" class="btn btn-danger" onclick="deleteModal(\''.$item['social_code'].'\', \'\', \'default\'); return false;">Delete</button>'));
		}
	}

	if($socialData) {
		$response['sEcho'] = $_REQUEST['sEcho'];
		$response['iTotalRecords'] = $socialData['displayrecords'];		
		$response['iTotalDisplayRecords'] = $socialData['count'];
		$response['aaData']	= $socials;
	} else {
		$response['result'] 	= false;
		$response['message']	= 'There are no items to show.';			
	}

	echo json_encode($response);
	die();

}

$smarty->display('social/default.tpl');
?>