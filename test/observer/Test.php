<?php
class Test {
	
	public $html = '';
	
	function __construct() {
		$this->log('CLASS: '.get_class($this).'<br />');
		$this->log($this->parameters(func_get_args()));
	}

	public function output() {
		/* CSS. */
		$CSS = '.error{background-color:#b90000;}.success{background-color:green;}.warning{background-color:#FF9933;}';
		/* Create HTML content. */
		$logHTML = "<html><head><title>Test</title><style type='text/css'>$CSS</style></head><body><table cellspacing='0' cellpadding='0'>".$this->html."<</table></body></html>";
		/* Make sure we have a directory. */
		$directory	= dirname(__FILE__)."/logs/".date('Y-m-d').'/';
		$file			= $directory.date('Y-m-d H.i.s').'.html';	
		/* Double check if folder is tehre. */
		if(!is_dir($directory)) mkdir($directory, 0777, true); 
		/* Create the file to write the error on. */
		touch($file);
	}
	
	public function log($line) {
		$this->html .= $line;			
	}
	
	public function parameters(array $args) {
		$this->log('METHOD: '.__METHOD__.'<BR />');
		if(count($args) > 0) {
			for($i = 0; $i < count($args); $i++) {
				$this->log('VALUE '.($i+1).': '.$args[$i].' - TYPE: '.gettype($args[$i]).'<br />');
			}
		} else {
			$this->log('NO ARGS');
		}
	}
}
?>