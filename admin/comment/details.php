<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/* Check for login */
require_once 'includes/auth.php';

/* objects. */
require_once 'class/event.php';
require_once 'class/vet.php';
require_once 'class/item.php';
require_once 'class/_comm.php';

$eventObject	= new class_event();
$vetObject 			= new class_vet();
$itemObject 			= new class_item();
$commObject 			= new class_comm();

if (isset($_GET['code']) && trim($_GET['code']) != '') {

	$code = trim($_GET['code']);
	
	$eventData = $eventObject->getByCode($code);

	if($eventData) {

		$smarty->assign('eventData', $eventData);
		
		$vetData = $vetObject->getAll('EVENT', $eventData['event_code']);
		if($vetData) $smarty->assign('vetData', $vetData);

	} else {
		header('Location: /event/vendors/');
		exit;
	}
}

/* Check posted data. */
if(count($_POST) > 0 && isset($_POST['areapost_code'])) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;
	
	if(isset($_POST['member_code']) && trim($_POST['member_code']) == '') {
		$errorArray['member_code'] = 'Member is required';
		$formValid = false;		
	}
	
	if(isset($_POST['areapost_code']) && trim($_POST['areapost_code']) == '') {
		$errorArray['areapost_code'] = 'Area is required';
		$formValid = false;		
	}

	if(isset($_POST['event_name']) && trim($_POST['event_name']) == '') {
		$errorArray['event_name'] = 'Name is required'; 
		$formValid = false;		
	}

	if(isset($_POST['event_contact_name']) && trim($_POST['event_contact_name']) == '') {
		$errorArray['event_contact_name'] = 'Contact name is required'; 
		$formValid = false;		
	}

	if(isset($_POST['event_contact_email']) && trim($_POST['event_contact_email']) == '') {
		$errorArray['event_contact_email'] = 'Contact email is required'; 
		$formValid = false;		
	} else if($eventObject->validateEmail(trim($_POST['event_contact_email'])) == ''){
		$errorArray['event_contact_email'] = 'Contact email needs to be valid'; 
		$formValid = false;		
	}
	
	if(isset($_POST['event_contact_number']) && trim($_POST['event_contact_number']) == '') {
		$errorArray['event_contact_number'] = 'Contact number is required';
		$formValid = false;		
	}

	if(count($errorArray) == 0 && $formValid == true) {
	
		$data 	= array();				
		$data['member_code']							= trim($_POST['member_code']);		
		$data['areapost_code']							= trim($_POST['areapost_code']);	
		$data['event_name']							= trim($_POST['event_name']);	
		$data['event_registration_name']		= trim($_POST['event_registration_name']);	
		$data['event_registration_number']	= trim($_POST['event_registration_number']);	
		$data['event_registration_vat']			= trim($_POST['event_registration_vat']);
		$data['event_social_facebook']			= trim($_POST['event_social_facebook']);
		$data['event_social_twitter']				= trim($_POST['event_social_twitter']);
		$data['event_social_instagram']			= trim($_POST['event_social_instagram']);
		$data['event_social_linkedin']			= trim($_POST['event_social_linkedin']);
		$data['event_website']						= trim($_POST['event_website']);
		$data['event_address_physical']			= trim($_POST['event_address_physical']);
		$data['event_address_postal']			= trim($_POST['event_address_postal']);
		$data['event_contact_name']				= trim($_POST['event_contact_name']);
		$data['event_contact_email']				= trim($_POST['event_contact_email']);
		$data['event_contact_number']			= trim($_POST['event_contact_number']);
		
		if(isset($eventData)) {
			$where		= $eventObject->getAdapter()->quoteInto('event_code = ?', $eventData['event_code']);
			$success	= $eventObject->update($data, $where);
			$success	= $eventData['event_code'];
		} else {
			$success = $eventObject->insert($data);
		}

		header('Location: /event/vendors/media.php?code='.$success);	
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}


if(count($_POST) > 0 && isset($_POST['item_code'])) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;

	if(isset($_POST['item_code']) && trim($_POST['item_code']) == '') {
		$errorArray['item_code'] = 'Please select a status';
		$formValid = false;		
	}

	if(isset($_POST['vet_reason']) && trim($_POST['vet_reason']) == '') {
		$errorArray['vet_reason'] = 'Please add a reason.';
		$formValid = false;		
	}

	if(count($errorArray) == 0 && $formValid == true) {

		$data 	= array();				
		$data['item_code']			= trim($_POST['item_code']);		
		$data['vet_reason']		= trim($_POST['vet_reason']);	
		$data['vet_item_type']	= 'EVENT';	
		$data['vet_item_code']	= $eventData['event_code'];	

		$success = $vetObject->insert($data);
		
		if($success) {

			$itemData = $itemObject->getByCode(trim($_POST['item_code']));

			if($itemData) {
				$data	= array();
				$data['event_active']	= $itemData['item_config_status'];

				$where		= $eventObject->getAdapter()->quoteInto('event_code = ?', $eventData['event_code']);
				$success	= $eventObject->update($data, $where);
				
				/* Email event for vetting. */
				$templateData = $commObject->_template->getTemplate('VET_NOTIFY', 'EMAIL');

				if($templateData) {

					$recipient	= array();
					$recipient['recipient_code'] 		= $eventData['event_code'];
					$recipient['recipient_name'] 		= $eventData['event_name'];
					$recipient['recipient_type'] 		= 'EVENT';
					$recipient['recipient_email'] 		= $eventData['member_email'];
					$recipient['recipient_vet_type'] 	= 'event';
					$recipient['recipient_message'] 	= 'The event has been vetted and the result is: <span style="color: '.$itemData['item_config_color'].'"><b>'.$itemData['item_name'].'</b></span>, reasons being: <br /><br /><b><i>'.trim($_POST['vet_reason']).'</i></b><br /><br />. '.((int)$itemData['item_config_status'] == 1 ? '<b style="color: green;">The event will be visible on the website and searchable</b>' : '<b style="color: red">Please action accordingly</b>');

					$commObject->sendEmail($recipient, $templateData);
				}			
			}
		}

		header('Location: /event/vendors/details.php?code='.$eventData['event_code']);	
		exit;
	}
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

$itemData = $itemObject->selectParents('VET', 'EVENT');
if($itemData) $smarty->assign('itemData', $itemData);

$smarty->display('event/vendors/details.tpl');

?>