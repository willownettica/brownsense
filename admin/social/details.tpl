<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header">
	<h2 class="content-header-title">Socials</h2>
	<ol class="breadcrumb">
		<li><a href="/">Home</a></li>
		<li><a href="/social/">Socials</a></li>
		<li><a href="#">{if isset($socialData)}{$socialData._social_message}{else}Add a social{/if}</a></li>
		<li class="active">Details</li>
	</ol>
	</div>	
      <div class="row">
        <div class="col-sm-9">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-tasks"></i>
					{if isset($socialData)}{$socialData._social_message}{else}Add a social{/if}
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">
              <form id="validate-basic" action="/social/details.php{if isset($socialData)}?code={$socialData._social_id}{/if}" method="POST" data-validate="parsley" class="form parsley-form">
				<div class="form-group">
					<label for="_social_message">Message</label>
					<textarea id="_social_message" name="_social_message" class="form-control">{$socialData._social_message}</textarea>
					<p id="social_count">0 characters entered.</p>					
					<p>N.B.: If you are uploading an image, the text needs to be at least maximum 100 characters, otherwise twitter wont upload it.</p>
					{if isset($errorArray._social_message)}<span class="error">{$errorArray._social_message}</span>{/if}	
				</div>	
                <div class="form-group">
					<label for="mediafile">Image Upload</label>
					<input type="file" id="mediafile" name="mediafile" />
					{if isset($errorArray.mediafile)}<br /><span class="error">{$errorArray.mediafile}</span>{/if}					  
					<br /><span>Allowed files are png, jpg, jpeg.</span>
                </div>
				<div class="form-group">
					<button class="btn btn-warning" type="submit">Add social</button>		
				</div>				
				<hr />
				<div class="form-group">
					<label for="bit">Create Bit</label>
					<input type="text" id="bit" name="bit" class="form-control" />
					<br />
					<p class="socialbiturl"></p>
					<button class="btn" type="button" onclick="javascript:createBit('bit');">Create bit</button>
				</div>
              </form>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col --> 
		<div class="col-sm-3">
			<div class="list-group">  
				<a class="list-group-item" href="/social/">
				  <i class="fa fa-asterisk"></i> &nbsp;&nbsp;List
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a>				
				<a class="list-group-item" href="#">
				  <i class="fa fa-book"></i> &nbsp;&nbsp;Details
				  <i class="fa fa-chevron-right list-group-chevron"></i>
				</a> 				 
			</div> <!-- /.list-group -->
        </div>			
      </div> <!-- /.row -->
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
{literal}
<script type="text/javascript" language="javascript">
	$(document).ready(function(){
		$("#_social_message").keyup(function () {
			var i = $("#_social_message").val().length;
			$("#social_count").html(i+' characters entered.');
			if (i > 140) {
				$('#social_count').removeClass('success');
				$('#social_count').addClass('error');
			} else if(i == 0) {
				$('#social_count').removeClass('success');
				$('#social_count').addClass('error');
			} else {
				$('#social_count').removeClass('error');
				$('#social_count').addClass('success');
			} 
		});
	});
</script>
{/literal}

</body>
</html>
