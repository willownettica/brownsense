<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';
/* Check for login */
require_once 'includes/auth.php';
/* objects. */
require_once 'class/member.php';
require_once 'class/branch.php';

$memberObject = new class_member();
$branchObject 	= new class_branch();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$memberData = $memberObject->getByCode($code);

	if($memberData) {
		$smarty->assign('memberData', $memberData);
	} else {
		header('Location: /member/');
		exit;
	}
} else {
	header('Location: /member/');
	exit;
}

/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;

	if(isset($_POST['member_birthdate']) && trim($_POST['member_birthdate']) == '') {
		$errorArray['member_birthdate'] = 'Birth date is required';
		$formValid = false;		
	} else {
		if($memberObject->validateDate(trim($_POST['member_birthdate'])) == '') {
			$errorArray['member_birthdate'] = 'Valid birth date is required';
			$formValid = false;				
		}
	}

	if(count($errorArray) == 0 && $formValid == true) {

		$data 										= array();				
		$data['areapost_code']				= trim($_POST['areapost_code']);		
		$data['member_birthdate']			= trim($_POST['member_birthdate']);
		$data['member_social_twitter']	= trim($_POST['member_social_twitter']);
		
		$where		= $memberObject->getAdapter()->quoteInto('member_code = ?', $memberData['member_code']);
		$success	= $memberObject->update($data, $where);
		$success	= $memberData['member_code'];

		header('Location: /member/media.php?code='.$success);	
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

$branchPairs = $branchObject->pairs(array(4));
if($branchPairs) $smarty->assign('branchPairs', $branchPairs);

$smarty->display('member/details.tpl');

?>