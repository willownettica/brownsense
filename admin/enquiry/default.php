<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/*** Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/**
 * Check for login
 */
require_once 'includes/auth.php';
require_once 'class/enquiry.php';
require_once 'class/company.php';
require_once 'class/vet.php';
require_once 'class/item.php';
require_once 'class/_comm.php';

$enquiryObject	= new class_enquiry();
$companyObject	= new class_company();
$vetObject 			= new class_vet();
$itemObject 			= new class_item();
$commObject 		= new class_comm();

if(isset($_GET['action']) && trim($_GET['action']) == 'search') {

	$filter	= array();
	$csv		= 0;
	$start 	= isset($_REQUEST['iDisplayStart']) ? $_REQUEST['iDisplayStart'] : 0;
	$length	= isset($_REQUEST['iDisplayLength']) ? $_REQUEST['iDisplayLength'] : 20;
	
	if(isset($_REQUEST['filter_search']) && trim($_REQUEST['filter_search']) != '') $filter[] = array('filter_search' => trim($_REQUEST['filter_search']));
	if(isset($_REQUEST['filter_csv']) && trim($_REQUEST['filter_csv']) != '') { $filter[] = array('filter_csv' => (int)trim($_REQUEST['filter_csv'])); $csv = (int)trim($_REQUEST['filter_csv']); }
	
	$enquirys 	= array();
	
	if(!$csv) {
		$enquiryData = $enquiryObject->getSearch('ENQUIRY', $start, $length, $filter);

		if($enquiryData) {
			for($i = 0; $i < count($enquiryData['records']); $i++) {
				$item = $enquiryData['records'][$i];
				$enquirys[$i] = array(
					'<a href="https://facebook.com/'.$item['social_facebook_id'].'" target="_blank">'.$item['enquiry_name'].'</a>',
					$item['enquiry_email'],
					$item['enquiry_number'],				
					$item['enquiry_message']);
			}
		}

		if($enquiryData) {
			$response['sEcho'] = $_REQUEST['sEcho'];
			$response['iTotalRecords'] = $enquiryData['displayrecords'];		
			$response['iTotalDisplayRecords'] = $enquiryData['count'];
			$response['aaData']	= $enquirys;
		} else {
			$response['result'] 	= false;
			$response['message']	= 'There are no items to show.';			
		}

		echo json_encode($response);
		die();
	} else {
		
		$enquiryData = $enquiryObject->getSearch('ENQUIRY', $start, $length, $filter);
		
		$row = "Date, Name, Email, Number, Message\r\n";
		if($enquiryData) {
			for($i = 0; $i < count($enquiryData); $i++) {
				$item = $enquiryData[$i];
				$enquirys[$i] = array(
						$item['enquiry_added'],
						preg_replace( "/\r|\n/", "", str_replace(',', '', $item['enquiry_name'])),
						preg_replace( "/\r|\n/", "", str_replace(',', '', $item['enquiry_email'])),
						preg_replace( "/\r|\n/", "", str_replace(',', '', $item['enquiry_number'])), 
						preg_replace( "/\r|\n/", "", str_replace(',', '', $item['enquiry_message']))
				);
			}

			foreach ($enquirys as $data) {
				$row .= implode(', ', $data)."\r\n";
			}			
		}

		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=enquiry_report_".date('Y-m-d').".csv");
		Header("Cache-Control: private, must-revalidate" ); // HTTP/1.1
		Header("Pragma: private" ); // HTTP/1.0
	   
		print($row);
		exit;	
	}
}

$itemData = $itemObject->selectParents('VET', 'COMPANY');
if($itemData) $smarty->assign('itemData', $itemData);

$smarty->display('enquiry/default.tpl');
?>