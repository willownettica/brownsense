<?php

//custom account enquiry class as account table abstraction
class class_enquiry extends Zend_Db_Table_Abstract
{
   //declare table variables
    protected $_name 			= 'enquiry';
	protected $_primary 		= 'enquiry_code';
	
	/**
	 * get job by job enquiry Id
 	 * @param string job id
     * @return object
	 */
	public function getByTypeCode($type, $code) {
		
		$select = $this->_db->select()	
					->from(array('enquiry' => 'enquiry'))
					->joinLeft(array('company' => 'company'), 'company.company_code = enquiry.enquiry_item_code')
					->joinLeft(array('member' => 'member'), 'member.member_code = enquiry.member_code')
					->where('enquiry_item_type = ?', $type)
					->where('enquiry_item_code = ?', $code)
					->where('enquiry_deleted = 0 and company_deleted = 0');

		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}

	public function getSearch($type, $start, $length, $filter = array()) {	

		$where 		= 'enquiry_deleted = 0';
		$csv 			= 0;
		
		$search			= '';
		
		if($type == 'COMPANY') {
			$search = 'company_name, enquiry_email, enquiry_name, enquiry_number, enquiry_message';
		} else if($type == ''){
			$search = 'classified_subject, member_name, areapost_name, item.item_name, category.item_name';
		} else if($type == 'ENQUIRY') {
			$search = 'member_name, enquiry_message, member_email';
		} else if($type == 'CLASSIFIED') {
			$search = 'classified_subject, enquiry_name, enquiry_email, enquiry_number';
		}

		if(count($filter) > 0) {
			for($i = 0; $i < count($filter); $i++) { 
				if(isset($filter[$i]['filter_search']) && trim($filter[$i]['filter_search']) != '') {
					$array = explode(" ",trim($filter[$i]['filter_search']));					
					if(count($array) > 0) {
						$where .= " and (";
						for($i = 0; $i < count($array); $i++) {
							$text = $array[$i];
							$this->sanitize($text);
							$where .= " lower(concat_ws($search)) like lower('%$text%') ";
							if(($i+1) != count($array)) {
								$where .= ' or ';
							}
						}
					}
					$where .= ")";
				} 

				if(isset($filter[$i]['filter_classified']) && trim($filter[$i]['filter_classified']) != '') {
					$this->sanitize($filter[$i]['filter_classified']);
					$where .= " and enquiry_item_code = '".$filter[$i]['filter_classified']."' ";
				}
				
				if(isset($filter[$i]['filter_csv']) && (int)trim($filter[$i]['filter_csv']) == 1) {
					$csv = 1;
				}
			}
		}

		if($type == 'COMPANY') {
			$select = $this->_db->select()	
				->from(array('enquiry' => 'enquiry'))
				->joinLeft(array('classified' => 'classified'), 'classified.classified_code = enquiry.enquiry_item_code', array('classified_code', 'classified_subject', 'classified_expire'))
				->joinLeft(array('item' => 'item'), "item.item_code = classified.item_code and item.item_code and item_type = 'CLASSIFIED'", array('item_name'))
				->joinLeft(array('category' => 'item'), "category.item_code = classified.category_code and  category.item_type = 'CATEGORY'", array('item_name as category_name'))
				->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = classified.areapost_code', array('areapost_name'))						
				->joinLeft(array('areapostregion' => 'areapostregion'), 'areapostregion.areapostregion_code = areapost.areapostregion_code', array('demarcation_name'))
				->joinLeft(array('member' => 'member'), 'member.member_code = enquiry.member_code', array('social_facebook_id', 'member_name'))
				->where('enquiry_item_type = ? and enquiry_deleted = 0', $type)
				->where($where)
				->order('enquiry_added desc');
		} else if($type == 'CLASSIFIED') {
			$select = $this->_db->select()
				->from(array('enquiry' => 'enquiry'))
				->joinLeft(array('classified' => 'classified'), "classified.classified_code = enquiry_item_code", array('classified_code', 'classified_subject', 'classified_expire'))
				->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = classified.areapost_code', array('areapost_name'))
				->joinLeft(array('areapostregion' => 'areapostregion'), 'areapostregion.areapostregion_code = areapost.areapostregion_code', array('demarcation_name'))
				->joinLeft(array('member' => 'member'), 'member.member_code = enquiry.member_code', array('social_facebook_id', 'member_name'))
				->where('enquiry_item_type = ? and enquiry_deleted = 0 and member_deleted = 0', $type)
				->where($where)
				->order('enquiry_added desc');
		} else if($type == 'ENQUIRY') {
			$select = $this->_db->select()	
				->from(array('enquiry' => 'enquiry'))
				->joinLeft(array('member' => 'member'), 'member.member_code = enquiry.member_code', array('social_facebook_id', 'member_name'))
				->joinLeft(array('areapost' => 'areapost'), 'areapost.areapost_code = member.areapost_code', array('areapost_name'))			
				->joinLeft(array('areapostregion' => 'areapostregion'), 'areapostregion.areapostregion_code = areapost.areapostregion_code', array('demarcation_name'))
				->where('enquiry_item_type = ? and enquiry_deleted = 0 and member_deleted = 0', $type)
				->where($where)
				->order('enquiry_added desc');
		}

		if($csv) {
			$result = $this->_db->fetchAll($select);
			return ($result == false) ? false : $result = $result;	
		} else {
			$result_count = $this->_db->fetchRow("select count(*) as query_count from ($select) as query");
			$result = $this->_db->fetchAll($select . " limit $start, $length");
			return ($result === false) ? false : $result = array('count'=>$result_count['query_count'],'displayrecords'=>count($result),'records'=>$result);	
		}
	}
	
	/**
	 * get job by job enquiry Id
 	 * @param string job id
     * @return object
	 */
	public function getByCode($code) {

		$select = $this->_db->select()	
					->from(array('enquiry' => 'enquiry'))
					->joinLeft(array('company' => 'company'), 'company.company_code = enquiry.enquiry_item_code and company_deleted = 0')
					->where('enquiry_code = ?', $code)
					->where('enquiry_deleted = 0');

		$result = $this->_db->fetchRow($select);
        return ($result == false) ? false : $result = $result;
	}

	/**
	 * get job by job enquiry Id
 	 * @param string job id
     * @return object
	 */
	public function getCode($code) {

		$select = $this->_db->select()	
					->from(array('enquiry' => 'enquiry'))
					->where('enquiry_code = ?', $code);

		$result = $this->_db->fetchAll($select);
        return ($result == false) ? false : $result = $result;
	}
	
	function createCode() {
		/* New code. */
		$code = "";
		
		$Alphabet 	= "ABCEGHKMNOPQRSUXZ";
		$Number 	= '1234567890';
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<3;$i++){
			$code .= $Number[rand(0,$count)];
		}
		
		/* First two Alphabets. */
		$count = strlen($Alphabet) - 1;
		
		for($i=0;$i<2;$i++){
			$code .= $Alphabet[rand(0,$count)];
		}
		
		/* Next six numbers */
		$count = strlen($Number) - 1;
		
		for($i=0;$i<4;$i++){
			$code .= $Number[rand(0,$count)];
		}		
		/* First check if it exists or not. */
		$enquiryCheck = $this->getCode($code);
		
		if($enquiryCheck) {
			/* It exists. check again. */
			$this->createCode();
		} else {
			return $code;
		}
	}
	
    function sanitize(&$string) { $string = preg_replace("/[^a-zA-Z0-9_]+/", "", $string);}
	
    function sanitizeArray(&$array) 
    {        
		for($i = 0; $i < count($array); $i++) {
			$array[$i] = preg_replace("/[^a-zA-Z0-9_]+/", "", $array[$i]);
		}
    }		
}
?>