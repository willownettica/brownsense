<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/*** Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/**
 * Check for login
 */
require_once 'includes/auth.php';
require_once 'class/member.php';
require_once 'class/item.php';

$memberObject = new class_member();
$itemObject 		= new class_item();
 
 if(isset($_GET['delete_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success					= NULL;
	$code						= trim($_GET['delete_code']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {
		$data	= array();
		$data['member_deleted'] = 1;
		
		$where 	= array();
		$where[] 	= $memberObject->getAdapter()->quoteInto('member_code = ?', $code);
		$success	= $memberObject->update($data, $where);	
		
		if(is_numeric($success) && $success > 0) {
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	
	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(isset($_GET['status_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;
	$data 						= array();
	$formValid				= true;
	$success					= NULL;
	$itemcode					= trim($_GET['status_code']);
	
	if($errorArray['error']  == '') {
		
		$itemData = $memberData = $memberObject->getCode($itemcode);
		
		if($itemData) {
			$data	= array();
			$data['member_active'] = trim($_GET['status']);
			
			$where 	= array();
			$where[] 	= $memberObject->getAdapter()->quoteInto('member_code = ?', $itemcode);
			$success	= $memberObject->update($data, $where);	
		}
		
		if(is_numeric($success)) {		
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not update, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	
	echo json_encode($errorArray);
	exit;
}

/* Setup Pagination. */
if(isset($_GET['action']) && trim($_GET['action']) == 'search') {

	$filter	= array();
	$csv		= 0;
	$start 	= isset($_REQUEST['iDisplayStart']) ? $_REQUEST['iDisplayStart'] : 0;
	$length	= isset($_REQUEST['iDisplayLength']) ? $_REQUEST['iDisplayLength'] : 20;
	
	if(isset($_REQUEST['filter_search']) && trim($_REQUEST['filter_search']) != '') $filter[] = array('filter_search' => trim($_REQUEST['filter_search']));
	if(isset($_REQUEST['filter_item']) && trim($_REQUEST['filter_item']) != '') $filter[] = array('filter_item' => trim($_REQUEST['filter_item']));
	if(isset($_REQUEST['filter_csv']) && trim($_REQUEST['filter_csv']) != '') { $filter[] = array('filter_csv' => (int)trim($_REQUEST['filter_csv'])); $csv = (int)trim($_REQUEST['filter_csv']); }

	if(!$csv) {
		
		$memberData = $memberObject->getSearch($start, $length, $filter);
		
		$members = array();

		if($memberData) {
			for($i = 0; $i < count($memberData['records']); $i++) {
				$item = $memberData['records'][$i];

				$members[$i] = array(
					($item['social_facebook_id'] == '' ? '<img src="/images/no-image.jpg" width="50px" />' : '<a href="http://facebook.com/'.$item['social_facebook_id'].'" target="_blank"><img src="https://graph.facebook.com/'.$item['social_facebook_id'].'/picture?width=300" width="50px" /></a>'),
					$item['branch_name'],
					'<a href="/member/details.php?code='.$item['member_code'].'" style="color: '.$item['item_config_color'].';"><b>'.$item['member_name'].' '.$item['member_surname'].'</b></a>', 
					$item['member_email'],
					($item['item_name'] == '' ? 'Not vetted' : '<span style="color: '.$item['item_config_color'].';">'.$item['item_name'].'</span>'),
					'<button value="" class="btn btn-danger" onclick="vetModal(\''.$item['member_code'].'\', \'member\', \''.$item['member_name'].' '.$item['member_surname'].'\', \'default\'); return false;">Vet</button>',
					'<button value="'.((int)$item['member_active'] == 0 ? 'Activate' : 'Deactivate').'" class="btn btn-danger" onclick="statusModal(\''.$item['member_code'].'\', \''.((int)$item['member_active'] == 0 ? '1' : '0').'\', \'default\'); return false;">'.((int)$item['member_active'] == 0 ? 'Activate' : 'Deactivate').'</button>',
					'<button value="Delete" class="btn btn-danger" onclick="deleteModal(\''.$item['member_code'].'\', \'\', \'default\'); return false;">Delete</button>'
				);
			}
		}

		if($memberData) {
			$response['sEcho'] = $_REQUEST['sEcho'];
			$response['iTotalRecords'] = $memberData['displayrecords'];		
			$response['iTotalDisplayRecords'] = $memberData['count'];
			$response['aaData']	= $members;
		} else {
			$response['result'] 	= false;
			$response['message']	= 'There are no items to show.';			
		}

		echo json_encode($response);
		die();
	} else {
		
		$memberData = $memberObject->getSearch($start, $length, $filter);
		
		$row = "Vet, Vet Message, Registration Date, Branch Name, Name, Surname, Email Address, Cellphone, Twitter Account\r\n";
		if($memberData) {
			for($i = 0; $i < count($memberData); $i++) {
				$item = $memberData[$i];
				$members[$i] = array(						
						($item['item_name'] == '' ? 'Not Vetted' : $item['item_name']),
						($item['vet_reason'] == '' ? 'N / A' : preg_replace( "/\r|\n/", "", str_replace(',', '', $item['vet_reason']))),
						str_replace(',', ' ',$item['member_added']),
						str_replace(',', ' ',$item['branch_name']),
						str_replace(',', ' ',$item['member_name']),
						str_replace(',', ' ',$item['member_surname']),
						str_replace(',', ' ',$item['member_email']),
						str_replace(',', ' ',$item['member_cellphone']),
						str_replace(',', ' ',$item['member_social_twitter'])
				);
			}
			
			foreach ($members as $data) {
				$row .= implode(', ', $data)."\r\n";
			}			
		}
		
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=member_report_".date('Y-m-d').".csv");
		Header("Cache-Control: private, must-revalidate" ); // HTTP/1.1
		Header("Pragma: private" ); // HTTP/1.0
	   
		print($row);
		exit;		
	}
}

$itemData = $itemObject->selectParents('VET', 'MEMBER');
if($itemData) $smarty->assign('itemData', $itemData);

/* End Pagination Setup. */
$smarty->display('member/default.tpl');
?>