<?php

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/**
 * Standard includes
 */
require_once 'config/database.php';
require_once 'config/smarty.php';

require_once 'includes/auth.php';

/* objects. */
require_once 'class/admin.php';
require_once 'class/role.php';

$adminObject	= new class_admin();
$roleObject 		= new class_role();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$tempData = $adminObject->getByCode($code);

	if($tempData) {
		$smarty->assign('tempData', $tempData);

		$roleData = $roleObject->getRoles($code, 'PAGE');

		if($roleData) {
			$smarty->assign('roleData', $roleData);
		}	

	} else {
		header('Location: /admin/view/');
		exit;	
	}
} else {
	header('Location: /admin/view/');
	exit;	
}

if(isset($_GET['delete_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success					= NULL;
	$code						= trim($_GET['delete_code']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {
		$data	= array();
		$data['role_deleted'] = 1;
		
		$where 	= array();
		$where[] 	= $roleObject->getAdapter()->quoteInto('role_code = ?', $code);
		$success	= $roleObject->update($data, $where);	
		
		if($success) {
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	
	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(count($_POST) > 0 && isset($_POST['addRole'])) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;

	if(isset($_POST['role_value']) && trim($_POST['role_value']) == '') {
		$errorArray['role_value'] = 'Please add a page';
		$formValid = false;		
	} else {
		/* Check or duplicates. */
		$roleData = $roleObject->checkExists($tempData['admin_code'], 'PAGE', trim($_POST['role_value']));
		
		if($roleData) {
			$errorArray['role_value'] = 'Already been added, please try another page.';
			$formValid = false;	
		}
	}
	
	if(count($errorArray) == 0 && $formValid == true) {

		$data 						= array();				
		$data['admin_code']	= $tempData['admin_code'];		
		$data['role_type']		= 'PAGE';
		$data['role_value']		= trim($_POST['role_value']);
		
		$success = $roleObject->insert($data);
		
		header('Location: /admin/view/role.php?code='.$tempData['admin_code']);	
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

/* Display the template */	
$smarty->display('admin/view/role.tpl');
?>