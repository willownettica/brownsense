<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header"> 
		<h2 class="content-header-title">Event</h2>
		<ol class="breadcrumb">
		<li><a href="/">Home</a></li>
		<li><a href="/event/list/">Events</a></li>
		<li><a href="/event/list/details.php?code={$eventData.event_code}">{$eventData.event_name}</a></li>
		<li class="active">Market vendors</li>
		</ol>
	</div>	  
	<div class="row">
        <div class="col-md-12">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-hand-o-up"></i>
                Event vendors list
              </h3>
            </div> <!-- /.portlet-header -->			  
			<div class="portlet-content">
				<div class="form-group">
				  <label for="filter_search">Search by company name or products / services</label>
				  <input type="text" id="filter_search" name="filter_search" class="form-control" value="" />
				</div>
				<div class="form-group">
				  <label for="filter_item">Select application by vetting outcome</label>
				  <select id="filter_item" name="filter_item" class="form-control">
					<option value=""> --- Show All --- </option>
					<option value="-1"> Unvetted </option>
					{html_options options=$itemData}
				  </select>
				</div>				
				<div class="form-group">
					<button type="button" onclick="getRecords(); return false;" class="btn btn-primary">Search</button>
					<button type="button" onclick="csv(); return false;" class="btn">Download CSV</button>
				</div>
				<div class="form-group">
					<p>This is a list of all vendors who applied for the market <b>{$eventData.event_name}</b> which is on the date of <b>{$eventData.event_date_start|date_format}</b>. <br />Address of the market is <b>{$eventData.event_address}</b>.</p>
					<p>There are <span id="result_count" name="result_count" class="success">0</span> records showing.</p>					
				</div>				
				<div id="tableContent" align="center">
					<!-- Start Content Table -->
					<div class="content_table">
						<img src="/images/ajax_loader.gif" />
					 </div>
					 <!-- End Content Table -->	
				</div>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col -->	
  </div> <!-- /.content -->		
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
{literal}
<script type="text/javascript">
$(document).ready(function() {
	var oTable;
	getRecords();
});

function getRecords() {
	var html		= '';

	var filter_search	= $('#filter_search').val() != 'undefined' ? $('#filter_search').val() : '';
	var filter_item		= $('#filter_item').val() != 'undefined' ? $('#filter_item').val() : '';
	
	/* Clear table contants first. */			
	$('#tableContent').html('');
	
	$('#tableContent').html('<table cellpadding="0" cellspacing="0" width="100%" border="0" class="display" id="dataTable"><thead><tr><th></th><th>Name</th><th>Services / Products</th><th>Outcome</th><th></th><th></th></tr></thead><tbody id="memberbody"><tr><td colspan="5" align="center"><img src="/images/ajax_loader.gif" /></td></tr></tbody></table>');
					
	oTable = $('#dataTable').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",							
		"bSort": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayStart": 0,
		"iDisplayLength": 20,				
		"bLengthChange": false,									
		"bProcessing": true,
		"bServerSide": true,	
        "aoColumns": [
			{ sWidth: '5%' },
            { sWidth: '15%' },
            { sWidth: '50%' },
            { sWidth: '10%' },
			{ sWidth: '10%' },
			{ sWidth: '10%' }],	
		"sAjaxSource": "?code={/literal}{$eventData.event_code}{literal}&action=search&filter_csv=0&filter_search="+filter_search+"&filter_item="+filter_item,
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			$.getJSON( sSource, aoData, function (json) {
					if (json.result === false) {
						$('#memberbody').html('<tr><td colspan="5" align="center">No results</td></tr>');											
					} else {
						$('#result_count').html(json.iTotalDisplayRecords);
					}
					fnCallback(json)
			});
		},
		"fnDrawCallback": function(){
		}
	});
	return false;
}

function csv() {
	var filter_search		= $('#filter_search').val() != 'undefined' ? $('#filter_search').val() : '';
	var filter_item			= $('#filter_item').val() != 'undefined' ? $('#filter_item').val() : '';
	window.location.href	= "/event/list/market.php?code={/literal}{$eventData.event_code}{literal}&action=search&filter_csv=1&filter_search="+filter_search+"&filter_item="+filter_item;
	return false;
}

function vendorHistoryModal(companycode) {
	$.ajax({
		type: "GET",
		url: "?code={/literal}{$eventData.event_code}{literal}&historycompany="+companycode,
		dataType: "json",
		success: function(data){
			if(data.result == 1) {
				$('#historytable').html(data.html);
				$('#historycompany').html($('#company_name_'+companycode).html());
				$('#vendorHistoryModal').modal('show');
				return false;
			} else {
				$('#vendorHistoryModal').modal('hide');
				$.howl ({
				  type: 'info'
				  , title: 'Notification'
				  , content: data.error
				  , sticky: $(this).data ('sticky')
				  , lifetime: 7500
				  , iconCls: $(this).data ('icon')
				});	
			}
		}
	});								

	return false;	
}
	
function vetVendorModal(companycode) {
	$.ajax({
		type: "GET",
		url: "?code={/literal}{$eventData.event_code}{literal}&vetvendormarket="+companycode,
		dataType: "json",
		success: function(data){
			if(data.result == 1) {
				$('#vetvendorcompany').html($('#company_name_'+companycode).html());
				$('#market_code').val(data.records.market_code);
				$('#vetVendorModal').modal('show');
				return false;
			} else {
				$('#vetVendorModal').modal('hide');
				$.howl ({
				  type: 'info'
				  , title: 'Notification'
				  , content: data.error
				  , sticky: $(this).data ('sticky')
				  , lifetime: 7500
				  , iconCls: $(this).data ('icon')
				});	
			}
		}
	});	

	return false;
}

function vetVendor() {
	
	var reason			= $('#vet_reason').val();
	var status				= $('#item_code :selected').val();
	var marketcode	= $('#market_code').val();

	$.ajax({
		type: "GET",
		url: "?code={/literal}{$eventData.event_code}{literal}&vetvendor=1",
		data: "marketcode="+marketcode+"&reason="+reason+"&status="+status,
		dataType: "json",
		success: function(data){				
			if(data.result == 1) {
				$('#vetVendorModal').modal('hide');
				oTable.fnDraw(false);
			} else {
				$('#vetvendorerror').html(data.error);
			}
		}
	});								

	return false;
}
</script>
{/literal}
<!-- Modal -->
<div class="modal fade" id="vetVendorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Vet <span id="vetvendorcompany" class="success"></span> for the event <span class="success">{$eventData.event_name}</span></h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="item_code">Vet</label>
					<select id="item_code" name="item_code" class="form-control">
						<option value=""> --------- </option>
						{html_options options=$itemData}
					</select>
				</div>				
				<div class="form-group">
					<label for="vet_reason">Reason</label>
					<textarea id="vet_reason" name="vet_reason" class="form-control"></textarea>
				</div>
				<p id="vetvendorerror" class="error"></p>
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:vetVendor();">Vet</button>
					<input type="hidden" id="market_code" name="market_code" value="" />				
			</div>
		</div>
	</div>
</div>
<!-- modal -->
<!-- Modal -->
<div class="modal fade" id="vendorHistoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><span id="historycompany" class="success"></span>'s history in the <span class="success">{$eventData.branch_name}</span> branch.</h4>
			</div>
			<div class="modal-body">
				<p>This is a list of the last 10 events in this event's branch as to which ones it attended and which ones were not.</p>
				<div class="form-group">
					<table class="table table-zebra">
						<thead>
							<th>Event</th>
							<th></th>
						</thead>
						<tbody id="historytable"></tbody>
					</table>
				</div>				
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- modal -->
</body>
</html>