<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/* Check for login */
require_once 'includes/auth.php';

/* objects. */
require_once 'class/template.php';
require_once 'class/File.php';

$templateObject	= new class_template();
$htmlObject			= new File(array('html', 'htm'));
$imageObject		= new File();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$templateData = $templateObject->getByCode($code);

	if($templateData) {
		$smarty->assign('templateData', $templateData);
	} else {
		header('Location: /admin/template/');
		exit;
	}
}

/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;
	
	if(isset($_POST['template_name']) && trim($_POST['template_name']) == '') {
		$errorArray['template_name'] = 'Name is required';
		$formValid = false;		
	}
	
	if(isset($_POST['template_category']) && trim($_POST['template_category']) == '') {
		$errorArray['template_category'] = 'Category is required';
		$formValid = false;		
	}
	
	if(isset($_POST['template_type']) && trim($_POST['template_type']) == '') {
		$errorArray['template_type'] = 'Type is required';
		$formValid = false;		
	} else {
		
		$template = isset($templateData['template_code']) ? $templateData['template_code'] : null;
		
		$temp = $templateObject->getByType(trim($_POST['template_type']), $template);
		
		if($temp) {
			$errorArray['template_type'] = 'Type is already being used, please try another one';
			$formValid = false;	
		}
	}
	
	if(isset($_FILES['htmlfile'])) {
		/* Check validity of the CV. */
		if((int)$_FILES['htmlfile']['size'] != 0 && trim($_FILES['htmlfile']['name']) != '') {
			/* Check if its the right file. */
			$ext = $htmlObject->file_extention($_FILES['htmlfile']['name']); 

			if($ext != '') {				
				$checkExt = $htmlObject->getValidateExtention('htmlfile', $ext);				
				if(!$checkExt) {
					$errorArray['htmlfile'] = 'Invalid file type something funny with the file format';
					$formValid = false;						
				} else {
					/* Check width and height */
					$htmlfile = $htmlObject->getValidateSize($_FILES['htmlfile']['size']);
					
					if(!$htmlfile) {
						$errorArray['htmlfile'] = 'File needs to be less than 2MB.';
						$formValid = false;							
					}
				}
			} else {
				$errorArray['htmlfile'] = 'Invalid file type';
				$formValid = false;									
			}
		} else {			
			switch((int)$_FILES['htmlfile']['error']) {
				case 1 : $errorArray['htmlfile'] = 'The uploaded file exceeds the maximum upload file size, should be less than 1M'; $formValid = false; break;
				case 2 : $errorArray['htmlfile'] = 'File size exceeds the maximum file size'; $formValid = false; break;
				case 3 : $errorArray['htmlfile'] = 'File was only partically uploaded, please try again'; $formValid = false; break;
				//case 4 : $errorArray['htmlfile'] = 'No file was uploaded'; $formValid = false; break;
				case 6 : $errorArray['htmlfile'] = 'Missing a temporary folder'; $formValid = false; break;
				case 7 : $errorArray['htmlfile'] = 'Faild to write file to disk'; $formValid = false; break;
			}
		}
	}

	if(isset($_FILES['imagefiles']['name']) && count($_FILES['imagefiles']['name']) > 0) {
		for($i = 0; $i < count($_FILES['imagefiles']['name'][$i]); $i++) {
			/* Check validity of the CV. */
			if((int)$_FILES['imagefiles']['size'][$i] != 0 && trim($_FILES['imagefiles']['name'][$i]) != '') {
				
			} else {			
				switch((int)$_FILES['imagefiles']['error'][$i]) {
					case 1 : $errorArray['imagefiles'] = 'The uploaded file exceeds the maximum upload file size, should be less than 1M'; $formValid = false; break;
					case 2 : $errorArray['imagefiles'] = 'File size exceeds the maximum file size'; $formValid = false; break;
					case 3 : $errorArray['imagefiles'] = 'File was only partically uploaded, please try again'; $formValid = false; break;
					//case 4 : $errorArray['imagefiles'] = 'No file was uploaded'; $formValid = false; break;
					case 6 : $errorArray['imagefiles'] = 'Missing a temporary folder'; $formValid = false; break;
					case 7 : $errorArray['imagefiles'] = 'Faild to write file to disk'; $formValid = false; break;
				}
			}
		}
	}

	if(count($errorArray) == 0 && $formValid == true) {
	
		$data 	= array();				
		$data['template_name']			= trim($_POST['template_name']);
		$data['template_category']		= trim($_POST['template_category']);		
		$data['template_type']			= trim($_POST['template_type']);
		$data['template_message']		= trim($_POST['template_message']);
		$data['template_subject']		= trim($_POST['template_subject']);
		$data['template_send_sms']	= isset($_POST['template_send_sms']) && (int)trim($_POST['template_send_sms']) == 1 ? 1 : 0;
		$data['template_send_email']	= isset($_POST['template_send_email']) && (int)trim($_POST['template_send_email']) == 1 ? 1 : 0;
		
		if(isset($templateData)) {
			$where		= $templateObject->getAdapter()->quoteInto('template_code = ?', $templateData['template_code']);
			$success	= $templateObject->update($data, $where);			
			$success 	= $templateData['template_code'];
		} else {
			$success = $templateObject->insert($data);
			
			$directory	= realpath(__DIR__.'/../../../../public_html/').'/media/templates/'.$success.'/';	
			if(!is_dir($directory)) mkdir($directory, 0777, true);

			$imagedirectory	= realpath(__DIR__.'/../../../../public_html/').'/media/templates/'.$success.'/media/';		
			if(!is_dir($imagedirectory)) mkdir($imagedirectory, 0777, true);					
		}
			
		if(count($errorArray) == 0) {
			/* Upload actual .html and .htm file. */
			if(isset($templateData) && isset($_FILES['htmlfile'])) {
				if((int)$_FILES['htmlfile']['size'] != 0 && trim($_FILES['htmlfile']['name']) != '') {
				
					$ext 			= strtolower($htmlObject->file_extention($_FILES['htmlfile']['name']));					
					$filename	= $success.'.'.$ext;
					$directory	= realpath(__DIR__.'/../../../../public_html/').'/media/templates/'.$success.'/';	
					$file			= $directory.$filename;
					
					if(!is_dir($directory)) mkdir($directory, 0777, true);
					$imagedirectory	= realpath(__DIR__.'/../../../../public_html/').'/media/templates/'.$success.'/media/';
					if(!is_dir($imagedirectory)) mkdir($imagedirectory, 0777, true);	
					
					if(file_put_contents($file, file_get_contents($_FILES['htmlfile']['tmp_name']))) {

						$template = array();
						$template['template_file']	= '/home/brownsen/public_html/media/templates/'.$success.'/'.$filename;
						
						$where		= $templateObject->getAdapter()->quoteInto('template_code = ?', $templateData['template_code']);
						$templateObject->update($template, $where);	

					} else {
						$errorArray['htmlfile'] = 'could not upload file, please try again';
						$formValid = false;			
					}
				}
			}
		}
		
		if(count($errorArray) == 0) {
			/* Upload image files. */
			if(isset($templateData) && isset($_FILES['imagefiles']) && count($_FILES['imagefiles']['name']) > 0) {
				for($i = 0; $i < count($_FILES['imagefiles']['name']); $i++) {
					if(isset($_FILES['imagefiles']['size'][$i])) {
						if((int)$_FILES['imagefiles']['size'][$i] != 0 && trim($_FILES['imagefiles']['name'][$i]) != '') {
							$filename	= $_FILES['imagefiles']['name'][$i];
							$directory	= realpath(__DIR__.'/../../../../public_html/').'/media/templates/'.$success.'/media/';
							$file			= $directory.$filename;
						
							if(!file_put_contents($file, file_get_contents($_FILES['imagefiles']['tmp_name'][$i]))) {					
								$errorArray['imagefiles'] = 'could not upload file, please try again';
								$formValid = false;			
							}
						}
					}
				}
			}
		}

		header('Location: /admin/template/details.php?code='.$success);	
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

$smarty->display('admin/template/details.tpl');

?>