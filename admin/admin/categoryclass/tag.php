<?php
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

/* Standard includes */
require_once 'config/database.php';
require_once 'config/smarty.php';

/* Check for login */
require_once 'includes/auth.php';

/* objects. */
require_once 'class/item.php';

$itemObject	= new class_item();

if (isset($_GET['code']) && trim($_GET['code']) != '') {
	
	$code = trim($_GET['code']);
	
	$itemData = $itemObject->getByCode($code);

	if($itemData) {
		$smarty->assign('itemData', $itemData);
		
		$tagData = $itemObject->getChildren($code, 'TAG');
		if($tagData) $smarty->assign('tagData', $tagData);
		
	} else {
		header('Location: /admin/categoryclass/');
		exit;
	}
} else {
	header('Location: /admin/categoryclass/');
	exit;
}

/* Check posted data. */
if(isset($_GET['delete_code'])) {
	
	$errorArray				= array();
	$errorArray['error']	= '';
	$errorArray['result']	= 0;	
	$formValid				= true;
	$success					= NULL;
	$itemcode					= trim($_GET['delete_code']);
		
	if($errorArray['error']  == '' && $errorArray['result']  == 0 ) {	
		$data	= array();
		$data['item_deleted'] = 1;
		
		$where		= array();
		$where[]	= $itemObject->getAdapter()->quoteInto('item_code = ?', $itemcode);
		$where[]	= $itemObject->getAdapter()->quoteInto('item_parent = ?', $itemData['item_code']);
		$where[]	= $itemObject->getAdapter()->quoteInto("item_type = ?", 'TAG');
		
		$success	= $itemObject->update($data, $where);	
		
		if(is_numeric($success) && $success > 0) {		
			$errorArray['error']	= '';
			$errorArray['result']	= 1;			
		} else {
			$errorArray['error']	= 'Could not delete, please try again.';
			$errorArray['result']	= 0;				
		}
	}
	
	echo json_encode($errorArray);
	exit;
}

/* Check posted data. */
if(count($_POST) > 0) {

	$errorArray	= array();
	$data 			= array();
	$formValid	= true;
	$success		= NULL;

	if(isset($_POST['tag_name']) && trim($_POST['tag_name']) == '') {
		$errorArray['tag_name'] = 'Tag name is required';
		$formValid = false;		
	}

	if(count($errorArray) == 0 && $formValid == true) {
	
		$data 	= array();				
		$data['item_parent']		= $itemData['item_code'];		
		$data['item_type']			= 'TAG';		
		$data['item_name']		= trim($_POST['tag_name']);	
		
		$success 	= $itemObject->insert($data);

		header('Location: /admin/categoryclass/tag.php?code='.$itemData['item_code']);	
		exit;
	}
	
	/* if we are here there are errors. */
	$smarty->assign('errorArray', $errorArray);	
}

$smarty->display('admin/categoryclass/tag.tpl');

?>