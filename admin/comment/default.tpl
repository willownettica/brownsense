<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Black Sense System</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	{include_php file='includes/css.php'}
</head>
<body>
{include_php file='includes/header.php'}
<div class="container">
  <div class="content">
    <div class="content-container">
	<div class="content-header"> 
		<h2 class="content-header-title">Comments</h2>
		<ol class="breadcrumb">
		<li><a href="/">Home</a></li>
		<li class="active">Comments</li>
		</ol>
	</div>	  
	<div class="row">
        <div class="col-md-12">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-hand-o-up"></i>
                Vet comments
              </h3>
            </div> <!-- /.portlet-header -->			  
			<div class="portlet-content">
			<div class="form-group">
			  <label for="filter_search">Search for comments</label>
			  <input type="text" id="filter_search" name="filter_search" class="form-control" value="" />
			</div>	
				<div class="form-group">
				  <label for="filter_item">Select a vet outcome</label>
				  <select id="filter_item" name="filter_item" class="form-control">
					<option value=""> --- Show All --- </option>
					<option value="-1"> Unvetted </option>
					{html_options options=$itemData}
				  </select>
				</div>			
			<div class="form-group">
				<button type="button" onclick="getRecords(); return false;" class="btn btn-primary">Search</button>
				<button type="button" onclick="csv(); return false;" class="btn">Download CSV</button>
			</div>
			<p>There are <span id="result_count" name="result_count" class="success">0</span> records showing. We are displaying <span id="result_display" name="result_display" class="success">0</span> records per page.</p>			
			<div id="tableContent" align="center">
				<!-- Start Content Table -->
				<div class="content_table">
					<img src="/images/ajax_loader.gif" />
				 </div>
				 <!-- End Content Table -->	
			</div>
            </div> <!-- /.portlet-content -->
          </div> <!-- /.portlet -->
        </div> <!-- /.col -->	
  </div> <!-- /.content -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
{include_php file='includes/footer.php'}
{include_php file='includes/javascript.php'}
{literal}
<script type="text/javascript">
$(document).ready(function() {
	getRecords();
});

function getRecords() {

	var html				= '';
	var filter_search	= $('#filter_search').val() != 'undefined' ? $('#filter_search').val() : '';
	var filter_item		= $('#filter_item').val() != 'undefined' ? $('#filter_item').val() : '';
	
	/* Clear table contants first. */			
	$('#tableContent').html('');
	
	$('#tableContent').html('<table cellpadding="0" cellspacing="0" width="100%" border="0" class="display" id="dataTable"><thead><tr><th></th><th>Message</th><th></th><th></th></tr></thead><tbody id="memberbody"><tr><td colspan="4" align="center"><img src="/images/ajax_loader.gif" /></td></tr></tbody></table>');
					
	oJobsTable = $('#dataTable').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",							
		"bSort": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayStart": 0,
		"iDisplayLength": 20,				
		"bLengthChange": false,									
		"bProcessing": true,
		"bServerSide": true,	
        "aoColumns": [
			{ sWidth: '15%' },
            { sWidth: '75%' },
            { sWidth: '25%' },
			{ sWidth: '10%' }],	
		"sAjaxSource": "?action=search&filter_search="+filter_search+"&filter_item="+filter_item,
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			$.getJSON( sSource, aoData, function (json) {
					if (json.result === false) {
						$('#memberbody').html('<tr><td colspan="4" align="center">No results</td></tr>');											
					} else {
						$('#result_count').html(json.iTotalDisplayRecords);
						$('#result_display').html(json.iTotalRecords);
					}
					fnCallback(json)
			});
		},
		"fnDrawCallback": function(){
		}
	});
	return false;
}

function csv() {
	var filter_search	= $('#filter_search').val() != 'undefined' ? $('#filter_search').val() : '';
	var filter_item		= $('#filter_item').val() != 'undefined' ? $('#filter_item').val() : '';
	window.location.href 	= "/comment/?action=search&filter_csv=1&filter_search="+filter_search+"&filter_item="+filter_item;
	return false;
}

function vetCommentModal(code, type) {
	$.ajax({
		type: "GET",
		url: "?itemcode="+type,
		dataType: "html",
		success: function(html){
			$('#comment_code').val(code);
			$('#itemselect').html(html);
			$('#commenttext').html($('#comment_'+code).html());
			$('#vetCommentModal').modal('show');
		}
	});
	return false;	
}

function vet() {

	var commentcode	= $('#comment_code').val();
	var itemcode		= $('#item_code').val();

	$.ajax({
		type: "GET",
		url: "default.php?vetcomment=1&commentcode="+commentcode+"&itemcode="+itemcode,
		dataType: "json",
		success: function(data){
			if(data.result == 1) {
				$('#vetCommentModal').modal('hide');
				getRecords();
			} else {
				$('#vetCommentModal').modal('hide');
				$.howl ({
				  type: 'info'
				  , title: 'Notification'
				  , content: data.error
				  , sticky: $(this).data ('sticky')
				  , lifetime: 7500
				  , iconCls: $(this).data ('icon')
				});	
			}
		}
	});								

	return false;		
}
</script>
{/literal}
<div class="modal fade" id="vetCommentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Vet comment</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="commenttext">Comment</label>
					<p id="commenttext" name="commenttext"></p>
				</div>
				<div class="form-group" id="itemselect" name="itemselect"></div>				
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
				<button class="btn btn-warning" type="button" onclick="javascript:vet();">Vet</button>
				<input type="hidden" id="comment_code" name="comment_code" value=""/>
			</div>
		</div>
	</div>
</div>
</html>