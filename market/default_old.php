<?php 

/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';
 
require_once 'includes/auth.php'; 

/* objects. */
require_once 'class/event.php';
require_once 'class/areapost.php';

$eventObject 	= new class_event();
$areapostObject 	= new class_areapost();

$where			= array();
$parameter	= "";

if(isset($_REQUEST['filter_province']) && trim($_REQUEST['filter_province']) != '') $where[] = array('filter_province' => trim($_REQUEST['filter_province']));

if(count($where) > 0) {	
	for($i = 0; $i < count($where); $i++) { 
		if(isset($where[$i]['filter_province']) && trim($where[$i]['filter_province']) != '') {
			$parameter .= "&filter_province=".$where[$i]['filter_province'];
		}
	}
}

$page 		= isset($_GET['page']) 		? $_GET['page'] 		: 1;
$perPage	= isset($_GET['perPage'])	? $_GET['perPage']	: 10;

$eventData = $eventObject->paginate($where, $page, $perPage);

$eventItems = $eventData->getCurrentItems();

if($eventItems) {
	$paginator = $eventData->setView()->getPages();
}

$provinceData	= $areapostObject->getProvinces();

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bcrumbs">
						<li><a href="/">Home</a></li>
						<li><a href="/events/">Events</a></li>						
						<li><a href="javascript:void(0); return false;">Search events</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- // PAGE HEADER -->	
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-9 dual-posts padding-bottom-30">
				<h2>Events</h2><br />
				<p>Search by province where the BrownSense event will be taking place. The number of events found so far is <span class="text_brown"><?php echo $paginator->totalItemCount; ?></span>. <?php echo ((int)$paginator->totalItemCount != 0 ? 'Search has <span class="text_brown">'.$paginator->pageCount.'</span> pages'.($paginator->pageCount > 1 ? ', with <span class="text_brown">'.$paginator->itemCountPerPage.'</span> events per page.' : '.') : '') ; ?></span></p>				
				<p></p>
				<br />
				<?php if($paginator->pageCount > 1) { ?>
				<!-- PAGINATION -->
				<ul class="pagination">
					<?php foreach($paginator->pagesInRange as $page) { ?>
					<li <?php echo ((int)$page == (int)$paginator->current ? 'class="active"' : ''); ?>><a href="/events/?page=<?php echo $page ?>&<?php echo $parameter; ?>"><?php echo $page ?></a></li>
					<?php } ?>
				</ul>
				<?php } ?>					
				<hr class="l4">
				<?php foreach($eventItems as $item) { ?>
				<div class="row">
					<div class="layout_3--item">
						<div class="col-md-3 col-sm-4">
							<div class="thumb">
								<div class="icon-24 video2"></div>
								<a href="/event/<?php echo $item['event_code']; ?>">								
									<img src="<?php echo $item['media_path'].'tmb_'.$item['media_code'].$item['media_ext']; ?>" class="img-responsive" alt="<?php echo $item['event_name']; ?>" title="<?php echo $item['event_name']; ?>" />
								</a>
							</div>
						</div>
						<div class="col-md-9 col-sm-8">								
							<span class="cat "><?php echo $item['event_address']; ?></span>
							<h4><a href="/event/<?php echo $item['event_code']; ?>"><?php echo $item['event_name']; ?></a></h4>
							<span class="cat ">( <?php echo $item['branch_name']; ?> )</span>
							<p><?php echo $item['event_page']; ?></p>
							<div class="meta">
								<span class="date"><?php echo date('d F Y H:i A', strtotime($item['event_date_start'])); ?></span>
							</div>
						</div>
					</div>
				</div>
				<hr class="l4">
				<?php } ?>
				<?php if($paginator->pageCount > 1) { ?>
				<!-- PAGINATION -->
				<ul class="pagination">
					<?php foreach($paginator->pagesInRange as $page) { ?>
					<li <?php echo ((int)$page == (int)$paginator->current ? 'class="active"' : ''); ?>><a href="/events/?page=<?php echo $page ?>&<?php echo $parameter; ?>"><?php echo $page ?></a></li>
					<?php } ?>
				</ul>
				<?php } ?>			
			</div>
			<!-- // CATEGORY -->
			<aside class="col-md-3 col-sm-4">			
				<div class="side-widget margin-bottom-30">
					<h3 class="heading-1"><span>Filter</span></h3>
					<?php if(isset($_REQUEST['filter_province']) && $_REQUEST['filter_province'] != '') { ?><p>You are searching for <span class="normal-font text_brown"><?php echo $provinceData[$_REQUEST['filter_province']]; ?></span>.</p><?php } ?>			
					<form class="post-comment-form"action="/events/" method="GET">		
						<div class="row">
							<div class="col-md-12">
								<label>Province</label>
								<select id="filter_province" name="filter_province" style="margin-bottom: 2px !important;">
									<option value=""> ------------------- </option>
									<?php 
										while ($value = current($provinceData)) {
											echo '<option '.(isset($_REQUEST['filter_province']) &&  $_REQUEST['filter_province'] == key($provinceData) ? 'selected' : '').' value="'.key($provinceData).'">'.$value.'</option>';
											next($provinceData);
										}
									?>  
								</select>
								<span class="tiny_explain">Select province that you want events from.</span>
							</div>		
						</div>
						<br />
						<button type="submit">Update Filter</button>&nbsp;&nbsp;&nbsp;&nbsp;
						<button type="button" onclick="window.location.href = '/events/'; return false;">Clear Search</button>
					</form>
				</div>
			</aside>
		</div>
	</div>
	<?php require_once 'includes/footer.php'; ?>		
</div>
<?php require_once 'includes/javascript.php'; ?>
</body>
</html>
<?php 
$eventObject = $areapostObject = $where = $parameter = $page = $perPage = $eventData = $eventObject = $eventItems = $paginator = $provinceData;
unset($eventObject, $areapostObject, $where, $parameter, $page, $perPage, $eventData, $eventObject, $eventItems, $paginator, $provinceData);
?>
