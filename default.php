<?php 
/* Add this on all pages on top. */
/* Add this on all pages on top. */
set_include_path($_SERVER['DOCUMENT_ROOT'].'/'.PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/library/classes/');

require_once 'config/database.php';

require_once 'includes/auth.php';

/* objects. */
require_once 'class/event.php';

$eventObject 	= new class_event();

$eventData		= $eventObject->upcoming(1);

?>
<!doctype html>
<!--[if IE 7 ]>
<html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BrownSense - Black business-to-business organization.</title>
	<?php require_once 'includes/css.php'; ?>
</head>
<body>
<div class="wrapper">
	<?php require_once 'includes/header.php'; ?>
	<br />
	<!-- // CATEGORIES -->
	<div class="container padding-bottom-50">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-8">
				<div id="mygallery" class="popup-gallery">	
					<a href="/images/home/image_21.jpg" alt="BrownSense Market Cape Town - Africa necklace - Black owned business" title="BrownSense Market Cape Town - Africa necklace - Black owned business" >
						<img alt="BrownSense Market Cape Town - Africa necklace - Black owned business" title="BrownSense Market Cape Town - Africa necklace - Black owned business" src="/images/home/image_21.jpg"/>
					</a>
					<a href="/images/home/image_31.jpg" alt="BrownSense Market Midrand - Black owned business support" title="BrownSense Market Midrand - Africa necklace - Black owned business">
						<img alt="BrownSense Market Midrand - Black owned business support" title="BrownSense Market Midrand - Africa necklace - Black owned business" src="/images/home/image_31.jpg"/>
					</a>					
					<a href="/images/home/image_33.jpg" alt="BrownSense Market Cape Town - Family friendly environment" title="BrownSense Market Cape Town - Family friendly environment" >
						<img alt="BrownSense Market Cape Town - Family friendly environment" title="BrownSense Market Cape Town - Family friendly environment"  src="/images/home/image_33.jpg"/>
					</a>
					<a href="/images/home/image_28.jpg" alt="BrownSense Market Cape Town - Friends chill and view products sold" title="BrownSense Market Cape Town - Friends chill and view products sold" >
						<img alt="BrownSense Market Cape Town - Friends chill and view products sold" title="BrownSense Market Cape Town - Friends chill and view products sold" src="/images/home/image_28.jpg" />
					</a>
					<a href="/images/home/image_23.jpg" alt="BrownSense Market Cape Town - Floor at the market in Woodstock" title="BrownSense Market Cape Town - Floor at the market in Woodstock" >
						<img alt="BrownSense Market Cape Town - Floor at the market in Woodstock" title="BrownSense Market Cape Town - Floor at the market in Woodstock" src="/images/home/image_23.jpg" />
					</a>
					<a href="/images/home/image_22.jpg" alt="BrownSense Market Soweto market on the green grass with the vendors" title="BrownSense Market Soweto market on the green grass with the vendors" >
						<img alt="BrownSense Market Soweto market on the green grass with the vendors" title="BrownSense Market Soweto market on the green grass with the vendors" src="/images/home/image_22.jpg"/>
					</a>
					<a href="/images/home/image_29.jpg" alt="BrownSense Market Soweto market for a sunday chillaz with brownies" title="BrownSense Market Soweto market for a sunday chillaz with brownies">
						<img  alt="BrownSense Market Soweto market for a sunday chillaz with brownies" title="BrownSense Market Soweto market for a sunday chillaz with brownies" src="/images/home/image_29.jpg"/>
					</a>
					<a href="/images/home/image_30.jpg"  alt="BrownSense Market Cape Town - Black owned clothing" title="BrownSense Market Cape Town - Black owned clothing" >
						<img alt="BrownSense Market Cape Town - Black owned clothing" title="BrownSense Market Cape Town - Black owned clothing" src="/images/home/image_30.jpg"/>
					</a>		
					<a href="/images/home/image_26.jpg" alt="BrownSense first location market in Johannesburg" title="BrownSense first location market in Johannesburg" >
						<img alt="BrownSense first location market in Johannesburg" title="BrownSense first location market in Johannesburg" src="/images/home/image_26.jpg"/>
					</a>
					<a href="/images/home/image_32.jpg" alt="BrownSense market in Soweto - come with family and chill on sunday" title="BrownSense market in Soweto - come with family and chill on sunday">
						<img alt="Title 2" src="/images/home/image_32.jpg"/>
					</a>
					<a href="/images/home/image_36.jpg" alt="BrownSense market in Soweto - come with family and chill on sunday" title="BrownSense market in Soweto - come with family and chill on sunday">
						<img  alt="BrownSense market in Soweto - come with family and chill on sunday" title="BrownSense market in Soweto - come with family and chill on sunday" src="/images/home/image_36.jpg"/>
					</a>
					<a href="/images/home/image_37.jpg" alt="BrownSense Market in Soweto to products sold by vendors" title="BrownSense Market in Soweto to products sold by vendors">
						<img alt="BrownSense Market in Soweto to products sold by vendors" title="BrownSense Market in Soweto to products sold by vendors" src="/images/home/image_37.jpg"/>
					</a> 	
					<a href="/images/home/image_38.jpg" alt="BrownSense Market in Soweto is a family friendly place with pets for the kids" title="BrownSense Market in Soweto is a family friendly place with pets for the kids">
						<img  alt="BrownSense Market in Soweto is a family friendly place with pets for the kids" title="BrownSense Market in Soweto is a family friendly place with pets for the kids" src="/images/home/image_38.jpg"/>
					</a>
					<a href="/images/home/image_39.jpg" alt="BrownSense live music at the Soweto market" title="BrownSense live music at the Soweto market" title="BrownSense live music at the Soweto market">
						<img alt="BrownSense live music at the Soweto market" title="BrownSense live music at the Soweto market" title="BrownSense live music at the Soweto market" src="/images/home/image_39.jpg"/>
					</a> 					
				</div>
				</div>
			</div>
		</div>
	</div>
	<!-- // CATEGORIES -->
	<?php require_once 'includes/footer.php'; ?>
</div>
<?php require_once 'includes/javascript.php'; ?>
<script type="text/javascript">
	jQuery(document).ready(function() {
	
		jQuery("#mygallery").justifiedGallery({
			rowHeight : 150,
			captions : false, 
			margins : 6
		});
		
		$('.popup-gallery').magnificPopup({
			delegate: 'a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] 
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
					return item.el.attr('title');
				}
			}
		});		
	});
</script>
</body>
</html>
